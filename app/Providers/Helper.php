<?php

namespace App\Providers;

use App\Attribute;
use App\Charts\AdChart;
use App\Charts\UserChart;
use App\City;
use App\Currency;
use App\Direction;
use App\Follower;
use App\MainCategory;
use App\Page;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Str;

class Helper extends ServiceProvider
{

    public const ACTIVE = 1;
    public const DEACTIVE = 0;
    /**
     * Register services.
     *
     * @return void
     */
    /** 
     * @const 
     */
    private static  $colorArray = array(
        "#fbb735", "#e98931", "#eb403b", "#b32E37", "#6c2a6a",
        "#5c4399", "#274389", "#1f5ea8", "#227FB0", "#2ab0c5",
        "#39c0b3",
        '#FF6630', '#FFB399', '#FF33FF', '#FFFF99', '#00B3E6',
        '#E6B333', '#3366E6', '#999966', '#99FF99', '#B34D4D',
        '#80B300', '#809900', '#E6B3B3', '#6680B3', '#66991A',
        '#FF99E6', '#CCFF1A', '#FF1A66', '#E6331A', '#33FFCC',
        '#66994D', '#B366CC', '#4D8000', '#B33300', '#CC80CC',
        '#66664D', '#991AFF', '#E666FF', '#4DB3FF', '#1AB399',
        '#E666B3', '#33991A', '#CC9999', '#B3B31A', '#00E680',
        '#4D8066', '#809980', '#E6FF80', '#1AFF33', '#999933',
        '#FF3380', '#CCCC00', '#66E64D', '#4D80CC', '#9900B3',
        '#E64D66', '#4DB380', '#FF4D4D', '#99E6E6', '#6666FF'
    );

    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    public static function getMainCategories()
    {
        return MainCategory::pluck("name", "id")->all();
    }

    public static function getCities()
    {
        return City::pluck("name", "id")->all();
    }

    public static function getCurrencies()
    {
        return Currency::pluck("name", "id")->all();
    }

    public static function getAttributes($ad_attribute_id)
    {
        return Attribute::where('ad_attribute_id', $ad_attribute_id)->get()->pluck('value', 'id');
    }

    public static function getPage($slug)
    {
        return Page::where('slug', '=', $slug)->first();
    }

    public static function Slugify($text)
    {
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);

        // transliterate
        // $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
        $text = Str::slug($text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, '-');

        // remove duplicate -
        $text = preg_replace('~-+~', '_', $text);

        // lowercase
        $text = strtolower($text);

        return $text;
    }

    public static function CountFollowing($user_id)
    {
        return Follower::where('user_id', '=', $user_id)->count();
    }
    public static function CountFollowers($user_id)
    {
        return Follower::where('following_id', '=', $user_id)->count();
    }

    public static function getTypeMessage()
    {
        return [
            'مشكلة تقنية' => 'مشكلة تقنية',
            'شكوى' => 'شكوى',
            'اقتراح' => 'اقتراح',
        ];
    }


    public static function ChartAd()
    {
        $label = collect([]); // Could also be an array
        $values = collect([]);
        foreach (City::all() as $city) {
            $label->push($city->name);
            $values->push($city->ads->count());
        }
        $chart = new AdChart();
        $chart->labels($label);
        $chart->dataset('Number of Users', 'doughnut', $values)->options([
            'color' => ['#ff0000'],
            'backgroundColor' => array_slice(self::$colorArray, 0, $values->count()),
            'scales' => [
                'xAxes' => [
                    [
                        'display' => false,
                    ],
                ],
                'yAxes' => [
                    [
                        'display' => false,
                    ],
                ],
            ],
            'scaleLabel' => [
                'fontSize' => 30
            ],
            'legend'=>['labels'=>[['fontSize'=>30]]]
        ]);
        $chart->displayAxes(false);
        $chart->title('The number of ads by city');
        return $chart;
    }
    public static function ChartUserReg()
    {
        $data = collect([]); // Could also be an array
        $dates = collect([]);
        for ($day = 7; $day >= -1; $day--) {
            $data->push(User::whereDate('created_at', today()->subDays($day))->count());
            $dates->push(today()->subDays($day)->format('M d Y'));
        }

        $chart = new UserChart();
        $chart->labels($dates);
        $chart->dataset('The number of new users within 7 days', 'line', $data)->options([
            'color' => ['#ff0000'],
            'backgroundColor' => '#fda1a1',
            'borderColor' => '#ff0000',
            'title' => [
                'fontSize' =>  50,
                'display' => false,
            ]
        ]);

        return $chart;
    }
}
