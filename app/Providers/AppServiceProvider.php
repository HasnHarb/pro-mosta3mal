<?php

namespace App\Providers;

use App\Actions\BlockAd;
use App\Actions\BlockService;
use App\Actions\BlockUser;
use App\Actions\ShowAd;
use App\Actions\ShowAuction;
use App\Actions\ShowOffer;
use App\Actions\ShowService;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use TCG\Voyager\Facades\Voyager;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        Voyager::addAction(ShowAd::class);
        Voyager::addAction(ShowOffer::class);
        Voyager::addAction(ShowService::class);
        Voyager::addAction(ShowAuction::class);
        Voyager::addAction(BlockAd::class);
        Voyager::addAction(BlockService::class);
        Voyager::addAction(BlockUser::class);
    }
}
