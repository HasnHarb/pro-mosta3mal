<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class AdReport extends Model
{
    protected $fillable = [
        'ad_id',
        'body',
        'email',
    ];
}
