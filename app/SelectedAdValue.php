<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SelectedAdValue extends Model
{
    protected $fillable = [
        'ad_id',
        'ad_attribute_id',
        'attribute_id',
    ];

    public function ad()
    {
        return $this->belongsTo(Ad::class, 'ad_id');
    }

    public function ad_attribute()
    {
        return $this->belongsTo(AdAttribute::class, 'ad_attribute_id');
    }

    public function attribute()
    {
        return $this->belongsTo(Attribute::class, 'attribute_id');
    }
}
