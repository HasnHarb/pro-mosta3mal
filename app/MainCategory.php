<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class MainCategory extends Model
{
    public function sub_categories()
    {
        return $this->hasMany(SubCategory::class);
    }

    public function ads()
    {
        return $this->hasManyThrough(
            Ad::class,
            SubCategory::class,
            'main_category_id',
            'sub_category_id',
            'id',
            'id'
        );
    }
    public function services()
    {
        return $this->hasManyThrough(
            Service::class,
            SubCategory::class,
            'main_category_id',
            'sub_category_id',
            'id',
            'id'
        );
    }
}
