<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Comment extends Model
{
    protected $fillable = [
        'ad_id',
        'body',
        'user_id',
    ];
    protected $appends = ['cauth'];

    public function ad()
    {
        return $this->belongsTo(Ad::class, 'ad_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function scopeCommentAds($query)
    {
        if (Auth::user()->role->name == 'user') {
            $ads = Auth::user()->ads->pluck('id')->toArray();
            return $query->whereIn('ad_id', $ads);
        }
    }
    public function getCauthAttribute()
    {
        if(Auth::check()){
            return $this->user_id == Auth::id();
        }
        return false;
    }
}
