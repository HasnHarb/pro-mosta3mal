<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class Ad extends Model
{
    protected $fillable = [
        'sub_category_id',
        'title',
        'price',
        'description',
        'city_id',
        'location',
        'user_name',
        'phone',
        'images',
        'currency_id',
    ];
    protected $appends = ['link_images', 'saved'];
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($question) {
            $temp = preg_replace('~[^\pL\d]+~u', '-', $question->title);;
            if (!Ad::all()->where('slug', $temp)->isEmpty()) {
                $i = 1;
                $new_slug = $temp . '-' . $i;
                while (!Ad::all()->where('slug', $new_slug)->isEmpty()) {
                    $i++;
                    $new_slug = $temp . '-' . $i;
                }
                $temp = $new_slug;
            }
            $question->slug = $temp;
            $question->status = 0;
            $question->user_id = Auth::id();
        });
    }

    public function selected_ad_values()
    {
        return $this->hasMany(SelectedAdValue::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function input_ad_values()
    {
        return $this->hasMany(InputAdValue::class);
    }

    public function getLinkImagesAttribute()
    {
        if ($this->images) {
            $images = json_decode($this->images);
            foreach ($images as $image) {
                $image->download_link = str_replace("\\", '/', url(Storage::url($image->download_link)));
            }
            return $images;
        }
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function city()
    {
        return $this->belongsTo(City::class, 'city_id');
    }

    public function sub_category()
    {
        return $this->belongsTo(SubCategory::class, 'sub_category_id');
    }

    public function currency()
    {
        return $this->belongsTo(Currency::class, 'currency_id');
    }

    public function scopeWhereStatus($query, $status)
    {
        return $query->where('status', '=', $status);
    }

    public function scopeFindBySlug($query, $slug)
    {
        return $query->where('slug', '=', $slug);
    }

    public function getSavedAttribute()
    {
        if (!Auth::check()) {
            return 0;
        } else {
            $saved_ad = SavedAd::where('user_id', Auth::id())
                ->where('ad_id', $this->id)->first();
            return ($saved_ad) ? 1 : 0;
        }
    }

    public function auction()
    {
        return $this->hasOne(Auction::class);
    }

    public function scopeAdUser($query)
    {
        if (Auth::user()->role->name == 'user') {
            return $query->where('user_id', '=', Auth::id());
        }
    }
}
