<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Rating extends Model
{
    protected $fillable = [
        'user_id',
        'body',
        'user_rate_id',
        'value',
    ];

    public function scopeRateUser($query)
    {
        if (Auth::user()->role->name == 'user') {
            return $query->where('user_id', '=', Auth::id());
        }
    }

    public function user()
    {
        return $this->belongsTo(User::class , 'user_id');
    }
    
    public function user_rater()
    {
        return $this->belongsTo(User::class , 'user_rate_id');
    }

}
