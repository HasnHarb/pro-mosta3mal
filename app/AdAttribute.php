<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class AdAttribute extends Model
{
    public $additional_attributes = ['ad_att_name'];

    public function getAdAttNameAttribute()
    {
        $sub_name = SubCategory::find($this->sub_category_id)->name ?? 'xxx';
        $att_group_name = AttributeGroup::find($this->attribute_group_id)->name ?? "xxx";

        return "{$sub_name} ⬅ {$att_group_name}";
    }


    public function attributes()
    {
        return $this->hasMany(Attribute::class);
    }

    public function selected_ad_values()
    {
        return $this->hasMany(SelectedAdValue::class)->with('ad_attribute');
    }

    public function attribute_group()
    {
        return $this->belongsTo(AttributeGroup::class, 'attribute_group_id');
    }

    protected static function boot()
    {
        parent::boot();
        static::creating(function ($query) {
            $query->required = 0;
        });
    }
}
