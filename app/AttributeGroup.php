<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class AttributeGroup extends Model
{
    public function sub_categories()
    {
        return $this->belongsToMany(SubCategory::class, 'ad_attributes');
    }

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($question) {
            $temp = preg_replace('~[^\pL\d]+~u', '-', $question->name);;
            if (!AttributeGroup::all()->where('slug', $temp)->isEmpty()) {
                $i = 1;
                $new_slug = $temp . '-' . $i;
                while (!AttributeGroup::all()->where('slug', $new_slug)->isEmpty()) {
                    $i++;
                    $new_slug = $temp . '-' . $i;
                }
                $temp = $new_slug;
            }
            $question->slug = $temp;
        });
    }

    // public function ad_attribute()
    // {
    //     return $this->belongsTo(AttributeGroup::class , 'attribute_group_id');
    // }

    public function units()
    {
        return $this->hasMany(Unit::class);
    }
}
