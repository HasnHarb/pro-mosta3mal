<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Unit extends Model
{
    public function input_ad_values()
    {
        return $this->hasMany(InputAdValue::class);
    }
}
