<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class AuctionOffer extends Model
{
    protected $fillable = [
        'price',
        'user_id',
        'auction_id',
    ];

    public function auction()
    {
        return $this->belongsTo(Auction::class, 'auction_id');
    }

    public function scopeOfferUser($query)
    {
        if (Auth::user()->role->name == 'user') {
            $s_id = Auth::user()->ads->pluck('id')->toArray();
            $auctions = Auction::whereIn('ad_id', $s_id)->pluck('id')->toArray();
            return $query->whereIn('auction_id', $auctions);
        }
    }
}
