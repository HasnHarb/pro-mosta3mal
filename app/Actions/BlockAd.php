<?php

namespace App\Actions;

use App\Auction;
use App\Providers\Helper;
use TCG\Voyager\Actions\AbstractAction;

class BlockAd extends AbstractAction
{
    public function getTitle()
    {
        return ($this->data->status == Helper::ACTIVE) ?  'Block' : 'Unblock';;
    }
    public function getIcon()
    {
        return 'voyager-key';
    }
    public function getPolicy()
    {
        return 'read';
    }
    public function getAttributes()
    {
        return [
            'class' => 'btn btn-sm btn-danger pull-right block',
            'target' => '_self',
            'data-id' => $this->data->id,
            'id' => "block-".$this->data->id
        ];
    }
    public function getDefaultRoute()
    {
        return "javascript:;";
    }

    public function shouldActionDisplayOnDataType()
    {
        return $this->dataType->slug == 'ads';
    }
}
