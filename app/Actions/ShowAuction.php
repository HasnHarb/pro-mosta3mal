<?php

namespace App\Actions;

use App\Auction;
use TCG\Voyager\Actions\AbstractAction;

class ShowAuction extends AbstractAction
{
    public function getTitle()
    {
        return 'Auction';
    }
    public function getIcon()
    {
        return 'voyager-eye';
    }
    public function getPolicy()
    {
        return 'read';
    }
    public function getAttributes()
    {
        return [
            'class' => 'btn btn-sm btn-success pull-right',
            'target' => '_blank'
        ];
    }
    public function getDefaultRoute()
    {
        $a = Auction::where('ad_id' , $this->data->id)->first();
        if(isset($a->id) and $a->id){
            return route('voyager.auctions.show', [$a->id]);
        }
        else{
            return route('voyager.auctions.show', ['null']);

        }
    }

    public function shouldActionDisplayOnDataType()
    {
        return $this->dataType->slug == 'ads';
    }
}
