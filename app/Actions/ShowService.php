<?php

namespace App\Actions;

use TCG\Voyager\Actions\AbstractAction;

class ShowService extends AbstractAction
{
    public function getTitle()
    {
        return 'Preview';
    }
    public function getIcon()
    {
        return 'voyager-eye';
    }
    public function getPolicy()
    {
        return 'read';
    }
    public function getAttributes()
    {
        return [
            'class' => 'btn btn-sm btn-success pull-right',
            'target' => '_blank'
        ];
    }
    public function getDefaultRoute()
    {
        return route('services.show', [$this->data->slug]);
    }

    public function shouldActionDisplayOnDataType()
    {
        return $this->dataType->slug == 'services';
    }
}
