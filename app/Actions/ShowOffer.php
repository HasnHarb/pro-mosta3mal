<?php

namespace App\Actions;

use TCG\Voyager\Actions\AbstractAction;

class ShowOffer extends AbstractAction
{
    public function getTitle()
    {
        return 'Offer';
    }
    public function getIcon()
    {
        return 'voyager-activity';
    }
    public function getPolicy()
    {
        return 'read';
    }
    public function getAttributes()
    {
        return [
            'class' => 'btn btn-sm btn-dark pull-right',
            'target' => '_blank'
        ];
    }
    public function getDefaultRoute()
    {
        return route('voyager.offers.index', ['service_id' => $this->data->id]);
    }

    public function shouldActionDisplayOnDataType()
    {
        return $this->dataType->slug == 'services';
    }
}
