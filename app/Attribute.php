<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Attribute extends Model
{
    public function ad_attribute()
    {
        return $this->belongsTo(AdAttribute::class, 'ad_attribute_id');
    }

    public function selected_ad_values()
    {
        return $this->hasMany(SelectedAdValue::class);
    }
}
