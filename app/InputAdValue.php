<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class InputAdValue extends Model
{
    protected $fillable = [
        'ad_id',
        'ad_attribute_id',
        'value',
        'unit_id',
    ];

    public function ad()
    {
        return $this->belongsTo(Ad::class, 'ad_id');
    }

    public function ad_attribute()
    {
        return $this->belongsTo(AdAttribute::class, 'ad_attribute_id');
    }
    
    public function unit()
    {
        return $this->belongsTo(Unit::class, 'unit_id');
    }



}
