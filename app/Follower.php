<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Follower extends Model
{
    protected $fillable = [
        'following_id',
        'user_id',
    ];
}
