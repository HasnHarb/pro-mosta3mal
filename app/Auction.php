<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Auction extends Model
{
    public $additional_attributes = ['ad_title'];

    protected $fillable = [
        'ad_id',
        'base_price',
        'start_date',
        'end_date',
    ];

    public function ad()
    {
        return $this->belongsTo(Ad::class, 'ad_id');
    }

    public function auction_offers()
    {
        return $this->hasMany(AuctionOffer::class);
    }

    public function getAdTitleAttribute()
    {

        return '(' . $this->ad->title . ')' . ' مزاد ';
    }
}
