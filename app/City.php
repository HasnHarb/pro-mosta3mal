<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class City extends Model
{
    public function services()
    {
        return $this->hasMany(Service::class);
    }
    
    public function ads()
    {
        return $this->hasMany(Ad::class);
    }
}
