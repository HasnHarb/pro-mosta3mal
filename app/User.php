<?php

namespace App;

use App\Providers\Helper;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class User extends \TCG\Voyager\Models\User implements MustVerifyEmail
{
    use Notifiable;
    protected $appends = ['link_image', 'following', 'rate'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'username', 'avatar', 'bio', 'city_id', 'phone'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function services()
    {
        return $this->hasMany(Service::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function offers()
    {
        return $this->hasMany(Offer::class);
    }

    public function ads()
    {
        return $this->hasMany(Ad::class);
    }

    public function ratings()
    {
        return $this->hasMany(Rating::class);
    }

    public function scopeFindByUsername($query, $username)
    {
        return $query->where('username', '=', $username);
    }

    public function getLinkImageAttribute()
    {
        if ($this->avatar) {
            return str_replace('\\', '/', url(Storage::url($this->avatar)));
        }
    }


    public function saved_services()
    {
        return $this->belongsToMany(
            Service::class,
            'saved_services',
            'user_id',
            'service_id'
        );
    }

    public function saved_ads()
    {
        return $this->belongsToMany(
            Ad::class,
            'saved_ads',
            'user_id',
            'ad_id'
        );
    }
    public function getFollowingAttribute()
    {
        if (!Auth::check()) {
            return 0;
        } else {
            $follow_user = Follower::where('user_id', Auth::id())
                ->where('following_id', $this->id)->first();
            return ($follow_user) ? 1 : 0;
        }
    }

    public function getRateAttribute()
    {
        $rate = Rating::where('user_id', '=', $this->id);
        $count = $rate->count();
        if ($count == 0) {
            return 0;
        }
        $sum = $rate->sum('value');
        return round(($sum / (5 * $count)) * 5, 1);
    }

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($user) {
            $user->username = Helper::Slugify($user->name . ' ' . Str::random(4));
        });
    }

    public function city()
    {
        return $this->belongsTo(City::class, 'city_id');
    }
}
