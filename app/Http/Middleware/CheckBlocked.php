<?php

namespace App\Http\Middleware;

use App\Providers\Helper;
use Closure;

class CheckBlocked
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth()->check()) {
            if (auth()->user()->status == Helper::DEACTIVE) {
                
                auth()->logout();
                toastr()->error("تم حظرك");
                return redirect()->to('/');
            }
        }
        return $next($request);
    }
}
