<?php

namespace App\Http\Controllers\FrontendController;

use App\AdReport;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Message;
use App\Rating;
use Illuminate\Support\Facades\Auth;

class GeneralController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->only(['storeRating']);
    }

    public function storeMessage(Request $request)
    {
        $validatedData = $this->validate($request, $this->rule('message'), [], $this->niceNames('message'));
        Message::create($request->all());
        return redirect()->back()->with('success', 'تم الرسال رسالتك بنجاح');
    }

    public function storeReportAd(Request $request)
    {
        $validatedData = $this->validate($request, $this->rule('report-ad'), [], $this->niceNames('report-ad'));
        AdReport::create($request->all());
        return redirect()->back()->with('success', 'ارسال ابلاغك بنجاح');
    }

    public function storeRating(Request $request)
    {
        $validatedData = $this->validate($request, $this->rule('rating'), [], $this->niceNames('rating'));
        Rating::create([
            'user_id' => $request->user_id,
            'body' => $request->body,
            'user_rate_id' => Auth::id(),
            'value' => $request->rate_value,
        ]);
        return redirect()->back()->with('success', 'ارسال التقييم بنجاح');
    }

    public function rule($table)
    {
        $rule = [
            'message' => [
                'type' => 'required',
                'email' => 'required|max:255',
                'body' => 'required|max:2000',
            ],
            'report-ad' => [
                'ad_id' => 'required',
                'body' => 'required|max:2000',
                'email' => 'required|max:2000',
            ],
            'rating' => [
                'body' => 'required|max:2000',
                'rate_value' => 'required|numeric|min:1|max:5'
            ]

        ];
        return $rule[$table];
    }

    public function niceNames($table)
    {
        $nice_name = [
            'message' => [
                'type' => __('fillable.type'),
                'email' => __('fillable.email'),
                'body' => 'الرسالة',
            ],
            'report-ad' => [
                'ad_id' => __('fillable.ad_id'),
                'body' => 'الرسالة',
            ],
            'rating' => []
        ];
        return $nice_name[$table];
    }

}
