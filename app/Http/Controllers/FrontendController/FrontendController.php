<?php

namespace App\Http\Controllers\FrontendController;

use App\Http\Controllers\Controller;
use App\MainCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FrontendController extends Controller
{
    public function index()
    {
        $categories = MainCategory::with('sub_categories')->get();
        return view('frontend.pages.index', compact('categories'));
    }

    public function signup()
    {
        if (!Auth::check()) {
            return view('frontend.pages.signup');
        }
        else{
            return redirect()->to('/');
        }
    }

    public function AboutAs()
    {
        return view('frontend.pages.about-as');
    }

    public function Privacy()
    {
        return view('frontend.pages.privacy');
    }
}
