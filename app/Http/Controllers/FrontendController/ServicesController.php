<?php

namespace App\Http\Controllers\FrontendController;

use App\Http\Controllers\Controller;
use App\Mail\ServiceAcceptNotification;
use App\MainCategory;
use App\Offer;
use App\Providers\Helper;
use App\SavedService;
use App\Service;
use App\SubCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class ServicesController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->only(['create', 'store']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $services = Service::select('*')->where('status', Helper::ACTIVE);
        if ($request->main_category_id != null) {
            $services = MainCategory::find($request->main_category_id)->services()->where('status', Helper::ACTIVE);
        }
        if ($request->sub_category_id != null) {
            $services->where('sub_category_id', '=', $request->sub_category_id);
        }
        if ($request->city_id != null) {
            $services->where('city_id', '=', $request->city_id);
        }
        if (!isset($request->order_by) or $request->order_by == null) {
            $services->orderBy('created_at', 'DESC');
        }

        if ($request->order_by != null) {
            $services->orderBy('created_at', $request->order_by);
        }

        $data  =  $services->paginate(7)->setPath('')->appends($request->all());
        return view('frontend.pages.services.index', ['services' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('frontend.pages.services.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $this->validate($request, $this->rule(), [], $this->niceNames());
        $service = Service::create($request->all());
        return redirect()->route('services.review', [$service->slug]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $service =  Service::findBySlug($slug)->with('offers')->first();
        if ($service) {
            if ($service->status == Helper::ACTIVE) {
                return view('frontend.pages.services.show', compact('service'));
            } else {
                return redirect()->route('services.review', [$slug]);
            }
        }
        return view('frontend.pages.404');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $service = Auth::user()->services()->findOrFail($id);
        $service->delete();
        return redirect()->back()->with('success', 'تم الحذف بنجاح');
    }

    public function setOffer(Request $request)
    {
        $validatedData = $this->validate($request, $this->ruleOffer(), [], $this->niceNamesOffer());
        $offer = Offer::create($request->all());
        return redirect()->back()->with('success', 'تم التعليق بنجاح');
    }

    public function review($slug)
    {
        $service =  Service::findBySlug($slug)->first();
        if ($service) {
            if ($service->status == Helper::DEACTIVE) {
                return view('frontend.pages.review');
            } else {
                return view('frontend.pages.services.show', compact('service'));
            }
        }
        return view('frontend.pages.404');
    }

    public function rule()
    {
        return [
            'sub_category_id' => 'required|integer|exists:sub_categories,id',
            'city_id' => 'required|integer|exists:cities,id',
            'title' => 'required|max:255',
            'description' => 'required|max:3000',
            'budget_min' => 'required|integer|min:0',
            'budget_max' => 'required_with|integer|gte:budget_min|min:0',
        ];
    }

    public function niceNames()
    {
        return [
            'sub_category_id' => __('fillable.sub_category_id'),
            'city_id' => __('fillable.city_id'),
            'title' => __('fillable.title'),
            'description' => __('fillable.description'),
            'budget_min' => __('fillable.budget_min'),
            'budget_max' => __('fillable.budget_max'),
        ];
    }


    public function addToSaved(Request $request)
    {
        if (isset($request->service_id) and Auth::check()) {
            $saved_service = SavedService::where('user_id', Auth::id())
                ->where('service_id', $request->service_id)->first();
            if ($saved_service) {
                $saved_service->delete();
                return response()->json([
                    'success' => true,
                    'message' => 'تم الازالة من قائمة المحفوظات',
                    'action' => 'unsaved'
                ]);
            } else {
                SavedService::insert([
                    'service_id' => $request->service_id,
                    'user_id' => Auth::id()
                ]);
                return response()->json([
                    'success' => true,
                    'message' => "تم الاضافة الى قائمة المحفوظات",
                    'action' => 'saved'
                ]);
            }
        }
        return response()->json([
            'success' => false,
            'message' => "يجب تسجيل الدخول اولا"
        ]);
    }

    public function ruleOffer()
    {
        return [
            'service_id' => 'required|integer|min:0',
            'amount' => 'required|integer',
            'body' => 'required|max:1000',
            'currency_id' => 'required|integer|min:0',
        ];
    }

    public function niceNamesOffer()
    {
        return [
            'service_id' => __('fillable.service_id'),
            'amount' => __('fillable.amount'),
            'body' => __('fillable.body'),
            'currency_id' => __('fillable.currency_id'),
        ];
    }

    public function blockService($id)
    {
        $service = Service::findOrFail($id);
        $service->status = ($service->status == Helper::ACTIVE) ?  Helper::DEACTIVE : Helper::ACTIVE;
        $service->save() ;
        if($service->status == Helper::ACTIVE){
            Mail::to($service->user)->send(new ServiceAcceptNotification($service));
            return redirect()->back()->with([
                'message'    => 'The request was successfully accepted',
                'alert-type' => 'success',
            ]);
        }
        return redirect()->back()->with([
            'message'    => 'The block was successful',
            'alert-type' => 'success',
        ]);
    }
}
