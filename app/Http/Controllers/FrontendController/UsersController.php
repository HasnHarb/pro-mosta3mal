<?php

namespace App\Http\Controllers\FrontendController;

use App\Follower;
use App\Http\Controllers\ContentTypes\Image as ContentImage;
use App\Http\Controllers\Controller;
use App\Mail\UserBlockNotification;
use App\Providers\Helper;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class UsersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->except(['profile', 'followUser']);
    }

    public function profile($username)
    {
        $user = User::findByUsername($username)->first();
        if ($user) {
            $countAds = 0;
            $countServices = 0;
            if (Auth::id() == $user->id) {
                $services = $user->services()->orderBy('created_at', 'DESC')->paginate(7);;
                $ads = $user->ads()->orderBy('created_at', 'DESC')->paginate(7);
                $countAds = $user->ads->count();
                $countServices = $user->services->count();
            } else {
                $services = $user->services()->whereStatus(Helper::ACTIVE)->orderBy('created_at', 'DESC')->paginate(7);;
                $ads = $user->ads()->whereStatus(Helper::ACTIVE)->orderBy('created_at', 'DESC')->paginate(7);
                $countAds = $user->ads->count();
                $countServices = $user->services->count();
            }
            return view('frontend.pages.users.profile', compact(
                'user',
                'services',
                'ads',
                'countServices',
                'countAds'
            ));
        }
        return view('frontend.pages.404');
    }

    public function saved()
    {
        $services = Auth::user()->saved_services()->whereStatus(Helper::ACTIVE)->orderBy('created_at', 'DESC')->paginate(7);
        $ads = Auth::user()->saved_ads()->whereStatus(Helper::ACTIVE)->orderBy('created_at', 'DESC')->paginate(7);
        return view('frontend.pages.users.saved', compact('services', 'ads'));
    }

    public function update(Request $request)
    {
        // return $request;
        $validatedData = $this->validate($request, $this->rule(Auth::id()), [], $this->niceNames());
        if ($request->hasFile('img_avatar')) {
            $request['avatar'] = (new ContentImage($request, 'users', 'img_avatar'))->handle();
        }
        // return $request;
        Auth::user()->update($request->all());
        // return $path;
        toastr()->success('تم تحديث بياناتك بنجاح');
        return redirect()->back();
    }

    public function changePassword(Request $request)
    {

        $this->validate($request, [
            'current-password' => 'required',
            'password' => 'required|same:password|min:6',
            'password_confirmation' => 'required|same:password',
        ]);

        $current_password = Auth::User()->password;
        if (Hash::check($request->input('current-password'), $current_password)) {
            $user_id = Auth::User()->id;
            $obj_user = User::find($user_id);
            $obj_user->password = Hash::make($request->input('password'));;
            $obj_user->save();
            toastr()->success('تم تغيير كلمة المرور بنجاح');
            return redirect()->back();
        } else {
            // toastr()->error('كلمة المرور الحالية خاطئة') ;   
            return redirect()->back()->withErrors('كلمة المرور الحالية خاطئة');
        }
    }

    protected function rule($id)
    {
        return  [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', "unique:users,email,$id,id"],
            'bio' => ['string', 'max:1000'],
            'avatar' => ['mimes:jpeg,jpg,png,gif', 'max:10000']
        ];
    }

    public function niceNames()
    {
        return [
            'name' => __('fillable.name'),
            'email' => __('fillable.email'),
            'bio' => __('fillable.bio'),
            'avatar' => __('fillable.avatar'),
        ];
    }


    public function followUser(Request $request)
    {
        if (isset($request->following_id) and Auth::check()) {
            $follow_user = Follower::where('user_id', Auth::id())
                ->where('following_id', $request->following_id)->first();

            if ($follow_user) {
                $follow_user->delete();
                return response()->json([
                    'success' => true,
                    'message' => 'تم الغاء المتابعة',
                    'action' => 'unsaved'
                ]);
            } else {
                Follower::insert([
                    'following_id' => $request->following_id,
                    'user_id' => Auth::id()
                ]);
                return response()->json([
                    'success' => true,
                    'message' => "تم المتابعة بنجاح",
                    'action' => 'saved'
                ]);
            }
        }
        return response()->json([
            'success' => false,
            'message' => "يجب تسجيل الدخول اولا"
        ]);
    }

    public function userBlock($id)
    {

        $user = user::findOrFail($id);
        $user->status = ($user->status == Helper::ACTIVE) ?  Helper::DEACTIVE : Helper::ACTIVE;
        $user->save();
        if ($user->status == Helper::DEACTIVE) {
            Mail::to($user)->send(new UserBlockNotification($user));
            return redirect()->back()->with([
                'message'    => 'The block was successful',
                'alert-type' => 'success',
                ]);
            }
            return redirect()->back()->with([
            'message'    => 'The request was successfully accepted',
            'alert-type' => 'success',
        ]);
    }

    public function allUsers(Request $request)
    {
        // return $request;
        $users = User::select('*');
        
        if ($request->s != null) {
            $users->where('name', 'LIKE', "%$request->s%")
                ->orWhere('bio', 'LIKE', "%$request->s%")
                ->orWhere('email', 'LIKE', "%$request->s%")
                ->orWhere('username', 'LIKE', "%$request->s%");
        }
        if ($request->city_id != null) {
            $users->where('city_id', '=', $request->city_id);
        }
        if (!isset($request->order_by) or $request->order_by == null) {
            $users->orderBy('created_at', 'DESC');
        }

        if ($request->order_by != null) {
            $users->orderBy('created_at', $request->order_by);
        }

        $data  =  $users->paginate(7)->setPath('')->appends($request->all());
        return view('frontend.pages.users.all', ['users' => $data]);
    }

}
