<?php

namespace App\Http\Controllers\FrontendController;

use App\Ad;
use App\Attribute;
use App\Auction;
use App\AuctionOffer;
use App\Comment;
use App\Follower;
use App\Http\Controllers\Controller;
use App\InputAdValue;
use App\Mail\AdAcceptNotification;
use App\MainCategory;
use App\Providers\Helper;
use App\SavedAd;
use App\SelectedAdValue;
use App\SubCategory;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use TCG\Voyager\Events\BreadDataAdded;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Http\Controllers\VoyagerBaseController;

class AdsController extends  VoyagerBaseController
{
    private $prefix = 'frontend.pages.ads.';

    public function __construct()
    {
        $this->middleware('auth')->except(['getAdsWhereCategory', 'showAds'])->only(['createAd' , 'setComment']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createAd()
    {
        return view($this->prefix . 'create');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function create()
    // {
    //     //
    // }

    /**
     * POST BRE(A)D - Store data.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $slug = 'ads';
        // return $request;
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Validate fields with ajax
        $val = $this->validateBread($request->all(), $dataType->addRows)->validate();
        $data = $this->insertUpdateData($request, $slug, $dataType->addRows, new $dataType->model_name());

        if (isset($request['selected'])) {
            $selected = $this->getSelectdAdValues($request->selected, $data->id);
            SelectedAdValue::insert($selected);
        }
        if (isset($request['input'])) {
            $inputs =  $this->getInputAdValues($request->input, $data->id);
            InputAdValue::insert($inputs);
        }


        event(new BreadDataAdded($dataType, $data));
        return view('frontend.pages.review');
        // return redirect()->back()->with(['warning' => 'تم تقدم اعلانك، وهو قيد المراجعة']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function show(Request $request , $id)
    // {
    //     //
    // }


    public function showAds(Request $request)
    {
        // return $request;
        $ads = Ad::select('*')->where('status', Helper::ACTIVE);
        if ($request->main_category_id != null) {
            $ads = MainCategory::find($request->main_category_id)->ads()->where('status', Helper::ACTIVE);
        }
        if ($request->sub_category_id != null) {
            $ads->where('sub_category_id', '=', $request->sub_category_id);
        }
        if ($request->s != null) {
            $ads->where('title', 'LIKE', "%$request->s%")
                ->orWhere('description', 'LIKE', "%$request->s%");
        }
        if ($request->city_id != null) {
            $ads->where('city_id', '=', $request->city_id);
        }
        if (!isset($request->order_by) or $request->order_by == null) {
            $ads->orderBy('created_at', 'DESC');
        }

        if ($request->order_by != null) {
            $ads->orderBy('created_at', $request->order_by);
        }

        $data  =  $ads->paginate(7)->setPath('')->appends($request->all());
        return view('frontend.pages.ads.index', ['ads' => $data]);
    }

    public function showAd($slug)
    {
        $ad =  Ad::findBySlug($slug)->first();
        // return $ad;
        if ($ad) {
            if ($ad->status == Helper::ACTIVE) {
                $auction = $ad->auction;
                $ads = Ad::select('*')->where('status', Helper::ACTIVE)
                    ->where('sub_category_id', '=', $ad->sub_category_id)
                    ->orderBy('created_at', 'DESC')
                    ->take(4)->get();

                $ad_auction_price = 0;
                if ($auction) {
                    $auction_offer = $auction->auction_offers;
                    if (isset($auction_offer) and $auction_offer->count() > 0) {
                        $ad_auction_price = $auction_offer->max('price') ?? 0;
                    } else {
                        $ad_auction_price = $auction->base_price;
                    }
                }

                return view('frontend.pages.ads.show', compact('ad', 'auction', 'ads', 'ad_auction_price'));
            } else {
                return view('frontend.pages.review');
            }
        }
        return view('frontend.pages.404');

        // $ads = Ad::whereStatus(Helper::ACTIVE)->orderBy('created_at', 'DESC')->paginate(7);
        // return view('frontend.pages.ads.index', compact('ads'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getAdsWhereCategory($id)
    {
        $ads = SubCategory::find($id)->ads()->whereStatus(Helper::ACTIVE)->orderBy('created_at', 'DESC')->paginate(7);
        return view('frontend.pages.ads.index', compact('ads'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function edit(Request $request ,$id)
    // {
    //     //
    // }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function update(Request $request, $id)
    // {
    //     //
    // }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function destroy($id)
    // {
    //     //
    // }

    public function generateTemplate(Request $request)
    {
        if ($request->ajax()) {
            if ($request->sub_category_id) {

                $ad_attributes = SubCategory::find($request->sub_category_id)->ad_attributes;

                $data = view('frontend.forms.ad_attributes',  compact('ad_attributes'))->render();

                return response()->json(['data' => $data]);
            }
        }
    }

    public function getInputAdValues($inputs, $ad_id)
    {
        $input_objects = [];
        foreach ($inputs as $key => $value) {
            $input = [];
            if (isset($value['value']) and isset($value['unit_id'])) {
                $input = [
                    'ad_id' => $ad_id,
                    'ad_attribute_id' => $key,
                    'value' => $value['value'],
                    'unit_id' => $value['unit_id']
                ];
            } else {
                $input = [
                    'ad_id' => $ad_id,
                    'ad_attribute_id' => $key,
                    'value' => $value,
                    'unit_id' => null
                ];
            }
            array_push($input_objects, $input);
        }
        return $input_objects;
    }
    public function getSelectdAdValues($selectd, $ad_id)
    {
        $selected_objects = [];
        foreach ($selectd as $key => $value) {
            $selected = [];
            $selected = [
                'ad_id' => $ad_id,
                'ad_attribute_id' => $key,
                'attribute_id' => $value,
            ];
            array_push($selected_objects, $selected);
        }
        return $selected_objects;
    }

    public function addToSaved(Request $request)
    {
        if (isset($request->ad_id) and Auth::check()) {
            $saved_ad = SavedAd::where('user_id', Auth::id())
                ->where('ad_id', $request->ad_id)->first();
            if ($saved_ad) {
                $saved_ad->delete();
                return response()->json([
                    'success' => true,
                    'message' => 'تم الازالة من قائمة المحفوظات',
                    'action' => 'unsaved'
                ]);
            } else {
                SavedAd::insert([
                    'ad_id' => $request->ad_id,
                    'user_id' => Auth::id()
                ]);
                return response()->json([
                    'success' => true,
                    'message' => "تم الاضافة الى قائمة المحفوظات",
                    'action' => 'saved'
                ]);
            }
        }
        return response()->json([
            'success' => false,
            'message' => "يجب تسجيل الدخول اولا"
        ]);
    }

    public function setAuctionOffer(Request $request)
    {
        $auction = Auction::findOrFail($request->auction_id); 
        $auction_offer = $auction->auction_offers;
        $ad_auction_price = 0 ;
        if (isset($auction_offer) and $auction_offer->count() > 0) {
            $ad_auction_price = $auction_offer->max('price');
        } else {
            $ad_auction_price = $auction->base_price;
        }
        $this->validate($request, [
            'price' => 'required|numeric|min:'.($ad_auction_price+1),
            'auction_id' => 'required',
        ]);
        AuctionOffer::create([
            'price' => $request->price,
            'user_id' => Auth::id(),
            'auction_id' => $request->auction_id,
        ]);
        toastr()->success('تم تقديم عرضك بنجاح');
        return redirect()->back();
    }

    public function setComment(Request $request)
    {
        $this->validate($request, [
            'ad_id' => 'required|numeric',
            'body' => 'required',
        ]);
        
        Comment::create([
            'ad_id'=> $request->ad_id,
            'body'=> $request->body,
            'user_id'=> Auth::id()
        ]);
        toastr()->success('تم التعليق بنجاح');
        return redirect()->back();
    }

    public function blockAd($id)
    {
        $ad = Ad::findOrFail($id);
        $ad->status = ($ad->status == Helper::ACTIVE) ?  Helper::DEACTIVE : Helper::ACTIVE;
        $ad->save() ;
        if($ad->status == Helper::ACTIVE){
            Mail::to($ad->user)->send(new AdAcceptNotification($ad));
        }
        return redirect()->back()->with([
            'message'    => 'Bolck Successfly',
            'alert-type' => 'success',
        ]);
    }

    public function deleteComment($id)
    {
        $comment = Auth::user()->comments()->findOrFail($id) ;
        $comment->delete() ;
        toastr()->success('تم حذف تعليقك بنجاح');
        return redirect()->back();
    }
}
