<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\MainCategory;
use App\SubCategory;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function selectSubCategorie(Request $request)
    {
        if ($request->ajax()) {
            if ($request->main_category_id) {
                $sub_categories = MainCategory::find($request->main_category_id)->sub_categories->pluck("name", "id")->all();;
                $data = view('frontend.components.options_selected', [
                    'options' => $sub_categories
                ])->render();
                return response()->json(['options' => $data]);
            }
            else{
                return response()->json(['options' => '<option value="">-- اختر --</option>']);
            }
        }
    }
}
