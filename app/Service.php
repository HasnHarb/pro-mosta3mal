<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Service extends Model
{

    protected $fillable = [
        'title',
        'description',
        'budget_min',
        'budget_max',
        'city_id',
        'sub_category_id',
        'currency_id'
    ];
    protected $appends = ['saved'];
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($question) {
            $temp = preg_replace('~[^\pL\d]+~u', '-', $question->title);;
            if (!Service::all()->where('slug', $temp)->isEmpty()) {
                $i = 1;
                $new_slug = $temp . '-' . $i;
                while (!Service::all()->where('slug', $new_slug)->isEmpty()) {
                    $i++;
                    $new_slug = $temp . '-' . $i;
                }
                $temp = $new_slug;
            }
            $question->slug = $temp;
            $question->user_id = Auth::id();
        });
    }

    public function sub_category()
    {
        return $this->belongsTo(SubCategory::class,  'sub_category_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function city()
    {
        return $this->belongsTo(City::class, 'city_id');
    }

    public function currency()
    {
        return $this->belongsTo(Currency::class, 'currency_id');
    }

    public function offers()
    {
        return $this->hasMany(Offer::class)->with('user');
    }


    public function scopeFindBySlug($query, $slug)
    {
        return $query->where('slug', '=', $slug);
    }

    public function scopeWhereStatus($query, $status)
    {
        return $query->where('status', '=', $status);
    }

    public function getSavedAttribute()
    {
        if (!Auth::check()) {
            return 0;
        } else {
            $saved_service = SavedService::where('user_id', Auth::id())
                ->where('service_id', $this->id)->first();
            return ($saved_service) ? 1 : 0;    
        }
    }

    public function scopeServiceUser($query)
    {
        if (Auth::user()->role->name == 'user') {
            return $query->where('user_id', '=', Auth::id());
        }
    }
}
