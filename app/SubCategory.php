<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class SubCategory extends Model
{
    public function category()
    {
        return $this->belongsTo(MainCategory::class,  'main_category_id');
    }
    
    public function services()
    {
        return $this->hasMany(Service::class);
    }
    
    public function ads()
    {
        return $this->hasMany(Ad::class);
    }

    public function attribute_groups(){
        return $this->belongsToMany(AttributeGroup::class , 'ad_attributes');
    }

    public function ad_attributes()
    {
        return $this->hasMany(AdAttribute::class)->with('attribute_group' , 'attributes');
    }
}
