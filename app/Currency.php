<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Currency extends Model
{

    public function ads()
    {
        return $this->hasMany(Ad::class);
    }
}
