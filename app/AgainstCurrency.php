<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class AgainstCurrency extends Model
{
    public function from_currency()
    {
        return $this->belongsTo(Currency::class, 'from_currency_id');
    }
    public function to_currency()
    {
        return $this->belongsTo(Currency::class, 'to_currency_id');
    }
}
