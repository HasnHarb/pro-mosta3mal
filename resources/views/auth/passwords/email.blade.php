@extends('frontend.layouts.master')

@section('content')

<div class="container-fluid color-gray">
    <div class="container">
        <div class="row">
            <div id="container-center">
                <div class="container-center">
                    <div class="col-md-12">
                        <div class="e-panel panel-default">
                            <div  class="e-panel-heading text-center">{{ __('استعادة كلمة المرور') }}</div>
            
                            <div class="panel-body">
                                @if (session('status'))
                                    <div class="alert alert-success" role="alert">
                                        {{ session('status') }}
                                    </div>
                                @endif
                                <div class="row">
                                    <div class="col-md-8 col-md-offset-2">
                                        <form method="POST" action="{{ route('password.email') }}">
                                            @csrf

                                            <div class="form-group">
                                                <label for="email">{{ __('البريد الالكتروني') }}</label>
                                                    <input id="email" type="email" class="e-form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                                    @error('email')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                            </div>

                                            <div class="form-group row">
                                                <div class="col-md-12 text-center">
                                                    <button type="submit" class="btn btn-red">
                                                        {{ __('إرسال رابط إعادة تعيين كلمة السر') }}
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
