@extends('frontend.layouts.master')

@section('content')

<div class="container-fluid color-gray">
    <div class="container">

        <div id="container-center">
            <div class="container-center">
                <div class="row" >
                    <div class="col-md-12">
                        <div class="e-panel panel-default">
                            <div  class="e-panel-heading text-center i-color-red">{{ __('تأكيد البريد الالكتروني') }}</div>

                            <div class="panel-body">
                            @if (session('resent'))
                                <div class="alert alert-success" role="alert">
                                    {{ __('A fresh verification link has been sent to your email address.') }}
                                </div>
                            @endif

                           <h5>
                               {{ __('قبل المتابعة ، يرجى التحقق من بريدك الإلكتروني للحصول على رابط التحقق. إذا لم تستلم البريد الإلكتروني ، انقر هنا لطلب آخر') }}
                            </h5>
                            <br>
                            <strong> {{ __('إذا لم تستلم البريد الإلكتروني ، انقر هنا لطلب آخر') }}: </strong>
                            <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
                                @csrf
                                <br>
                                <button type="submit" class="btn btn-out-red">{{ __('انقر هنا للارسال مرة اخرى') }}</button>.
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
