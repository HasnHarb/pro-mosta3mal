@extends('voyager::master')



@section('content')
@php
    $chart = Helper::ChartUserReg() ;
    $chart_ad = Helper::ChartAd() ;
@endphp
    <div class="page-content browse container-fluid">
        @include('voyager::alerts')
        @include('voyager::dimmers')

        <div class="row">
            <div class="col-md-12">
                <div class="col-md-12 text-center">
                    <h2 >Welcome To Karakib.me 😉</h2>
                 </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered" >
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12" style="height: 205px;">
                                {!! $chart->container() !!}
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                       
                        <div class="row">
                            <div class="col-md-12" style="height: 260px;">
                                {!! $chart_ad->container() !!}
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('javascript')

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js" charset="utf-8"></script>
{!! $chart->script() !!}
window.{{ $chart->id }}

{!! $chart_ad->script() !!}
window.{{ $chart_ad->id }}

@stop

@section('css')

@stop
