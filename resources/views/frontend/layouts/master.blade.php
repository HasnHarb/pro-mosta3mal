<!DOCTYPE html>
<html land="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <title>
        موقع كراركيب | @stack('title')
    </title>
    <link rel="icon" href="{{ url(Storage::url(setting('site.logo')))}}">

    <link rel='stylesheet' href="{{ asset('assets/css/bootstrap.min.css') }}" />
    <link rel='stylesheet' href="{{ asset('assets/css/bootstrap-rtl.css') }}" />
    <link rel='stylesheet' href="{{ asset('assets/css/font-awesome.min.css') }}" />
    <link rel='stylesheet' href="{{ asset('assets/css/style.css') }}" />
    <link rel='stylesheet' href="{{ asset('assets/css/bootstrap-select.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/toastr.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/sweetalert2.css') }}">
    <link href="https://fonts.googleapis.com/css?family=Droid+Sans" rel="stylesheet">
    {{-- 
        <link rel='stylesheet' href="{{ asset('assets/css/rtl.css') }}" />
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    --}}
    @stack('css')

</head>

<body>
    @include('frontend.includes.navbar')
    
    {{-- <div class="container-fluid"> --}}
        @yield('content')
    {{-- </div> --}}

    @include('frontend.includes.footer')
    @include('frontend.includes.model')

    
    <script src="{{ asset('assets/js/jquery-2.1.4.js') }}"></script>
    <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/menu.js') }}"></script>
    <script src="{{ asset('assets/js/plagn.js') }}"></script>
    <script src="{{ asset('assets/js/bootstrap-select.js') }}"></script>
    <script src="{{ asset('js/toastr.min.js') }}"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/toastify-js"></script>
    <script src="{{ asset('js/sweetalert2.min.js') }}"></script>

    @toastr_render
    <script>
        $.validate({
          lang: 'en'
        });
      </script>
    @stack('scripts')
    <script>
        $('.dropdown-toggle').dropdown();
    </script>
</body>

</html>