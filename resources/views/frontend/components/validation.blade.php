@if ($errors->any())
    @foreach ($errors->all() as $error)
        @php
            toastr()->error($error);
        @endphp
    @endforeach
@endif
@if(Session::has('success'))
    @php
        toastr()->success(Session::get('success'));
    @endphp
@endif