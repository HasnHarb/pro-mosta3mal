

@push('scripts')

    <script src="{{ asset('js/sweet-confirm.js') }}"></script>

@endpush

@if ($errors->any())
    @foreach ($errors->all() as $error)
        @php
            toastr()->error($error);
        @endphp
    @endforeach
@endif

@if(Session::has('success'))
    @php
        toastr()->success(Session::get('success'));
    @endphp
@endif

<div class="table-responsive">
    <table class="table table-hover">
        <thead>
            <tr>
                <th>صورة</th>
                <th>العنوان</th>
                <th class="text-center">الوقت</th>
                <th class="text-center">العروض</th>
                @if ($action == true)
                    <th class="text-center">خيارات</th>
                @else
                    <th class="text-center">الناشر</th>
                @endif
                <th class="text-center">حفظ</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($services as $service)
                @php
                    $url = route('services.show' , [$service->slug]) ;
                @endphp
                <tr>
                    <td scope="row">
                        <img class="avatar img-thumbnail"   src="{{ $service->user->link_image ?? '' }}" width="60px" height="60px">
                    </td>
                    <td onclick="window.location='{{$url}}'" class="cursor-pointer">
                        <a style="font-size: 16px" href="{{ $url }}">{{ $service->title }}</a>
                        <p style="padding: 0px; margin: 4px 0px;">
                            @if (mb_strlen($service->description) > 50)
                                {{  mb_substr($service->description ,0,50,'utf-8') }} ...
                                <a href="{{ $url }}"> قراءة المزيد </a>
                            @else
                                {{ $service->description }}
                            @endif
                        </p>

                        <div class="s-icon">
                            <div class="v-center"> 
                                <i class="fa fa-map-marker i-color-pinck"></i> 
                                <span>{{ $service->city->name }}</span>
                            </div>
                        </div>

                        <div class="s-icon">
                            <div class="v-center"> 
                                <i class="fa fa-list-alt i-color-blue"></i>
                                <span> {{ $service->sub_category->name }}</span>
                            </div>
                        </div>

                        <div class="s-icon">
                            <div class="v-center"> 
                                <i class="fa fa-clock-o i-color-green"></i>    
                                <span> {{ date('yy-m-d',strtotime($service->created_at)) }}</span>
                            </div>
                        </div>
                            
                    </td>

                    <td class="text-center"> {{ $service->created_at->locale('ar')->diffForHumans() }} </td>
                    <td class="text-center">{{ $service->offers->count() }}</td>
                    @if (Auth::id() == $service->user_id and $action == true)
                    <td>
                        {{-- <div class="btn-group" role="group" > --}}
                            <button type="button" onclick="confirmForm('removeService_{{$service->id}}')" class="btn btn-danger btn-xs" style="margin: 2px;">
                                <i class="fa fa-times" aria-hidden="true"></i>
                                حذف
                            </button>
                            <a href="{{ route('voyager.services.edit' , $service) }}" target="_blank" class="btn btn-info btn-xs" style="margin: 2px;">
                                <i class="fa fa-pencil-square-o" aria-hidden="true"></i>                            
                                تعديل
                            </a>
                        {{-- </div> --}}
                        <form action="{{ route('services.destroy' , [$service->id]) }}" method="POST" id="removeService_{{$service->id}}">
                            @csrf
                        </form>
                    </td>
                    @else
                    <td class="text-center">{{ $service->user->name ?? '' }}</td>
                    @endif
                    <td class="text-center">
                        <button type="button" class="btn btn-none" onclick="ajaxSavedService({{$service->id}})">
                            <i class="fa-18 fa fa-bookmark @if ($service->saved)  i-color-red @else  i-color-gray @endif" id="saved_icon_{{$service->id}}" aria-hidden="true"></i>
                        </button>
                    </td>
                </tr>
                @endforeach
            </tbody>
    </table>
</div>
@push('scripts')
    
<script>
    function ajaxSavedService (id){
        jQuery.ajax({
            url: "{{ route('services.addToSaved') }}",
            method: 'post',
            data: {
                _token: "{{csrf_token()}}",
                service_id: id,
            },
            success: function(result){
                console.log(result);
                if(result.success == true){
                    toastr.success(result.message);
                    if(result.action == 'saved'){
                        
                        $("#saved_icon_" + id).removeClass('i-color-gray');
                        $("#saved_icon_" + id).addClass('i-color-red');
                    }
                    if(result.action == 'unsaved'){
                        $("#saved_icon_" + id).removeClass('i-color-red');
                        $("#saved_icon_" + id).addClass('i-color-gray');
                    }
                }
                else{
                    toastr.error(result.message)
                }

            }
        });
    }
</script>
@endpush
