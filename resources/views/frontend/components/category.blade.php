
<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
    <div class="e-panel panel-green">
        <div class="card-panel-heading">
            <img src="{{url('storage/' . $category->icon)}}" height="27px" alt="">
            {{$category->name}}
            <span class="see-all">
            <a href="#"> عرض الجميع </a>
            </span>
        </div>
        <div class="panel-body static-body">
            @foreach ($category->sub_categories as $item)
            <a  class="sub-cat-link inline" href="{{ route('ad.show-ads' , ['sub_category_id' => $item->id , 'main_category_id' => $category->id]) }}">
                <span>
                    {{$item->name}}
                </span>                
            </a>
            @endforeach
        </div>
    </div>
</div>