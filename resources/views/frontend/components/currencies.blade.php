{{-- <table class="table table-borderless" style="margin-bottom: 0px;"> --}}
    <table class="table exchange table-hover  table-striped">
        <thead>
            <tr>
                <th></th>
                <th>شراء</th>
                <th>بيع</th>
            </tr>
        </thead>
        <tbody>
            @foreach (App\AgainstCurrency::all() as $item)
                <tr>
                    <td><span>{{$item->from_currency->name}}/{{$item->to_currency->name}}</span></td>
                    <td>{{ number_format($item->buy,4) }}</td>
                    <td>{{ number_format($item->sell,4) }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
{{-- </table> --}}