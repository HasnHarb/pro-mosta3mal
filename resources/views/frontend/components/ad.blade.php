@foreach ($ads as $ad)
<div class="col-md-12">
    <div class="contant-ad">
        <div class="ad-image">
            <div style="background-image: url({{ $ad->link_images[0]->download_link }});"></div>
        </div>
        <div class="article-text">
            <h2> <a href="{{ route('ad.show-ad' , [$ad->slug]) }}">{{ $ad->title }} <strong>({{ $ad->price . $ad->currency->icon}})</strong> </a> </h2>
            <div class="s-icon">
                <div class="v-center"> 
                    <i class="fa fa-clock-o i-color-green"></i>    
                    <span>{{ $ad->created_at->locale('ar')->diffForHumans() }}</span>
                </div>
            </div>
            <p> 
                {{-- @if (mb_strlen($ad->description) > 130)
                    {{  mb_substr($ad->description ,0,130,'utf-8') }} ...
                    <a href="#"> قراءة المزيد </a>
                @else
                    {{ $ad->description }}
                @endif --}}
                @foreach ($ad->selected_ad_values as $item)
                    <div class="s-icon">
                        <div class="v-center"> 
                            <span> 
                                <strong>
                                    {{ $item->ad_attribute->attribute_group->name }}:
                                </strong>
                                {{ $item->attribute->value }}
                            </span>
                        </div>
                    </div>
                @endforeach
                @foreach ($ad->input_ad_values as $item)
                    @if (isset($item->unit->name) and $item->unit->name != 'مزايا إضافية')            
                        <div class="s-icon">
                            <div class="v-center"> 
                                <span> 
                                    <strong>
                                        {{ $item->ad_attribute->attribute_group->name }}:
                                    </strong>
                                    {{ $item->value }}/{{ $item->unit->name??'' }}
                                </span>
                            </div>
                        </div>
                    @endif
                @endforeach
            </p>
            <div class="">

                <div class="s-icon">
                    <div class="v-center"> 
                        <i class="fa fa-map-marker i-color-red"></i> 
                        <span>{{ $ad->city->name }}</span>
                    </div>
                </div>

                <div class="s-icon">
                    <div class="v-center"> 
                        <i class="fa fa-list i-color-blue"></i>
                        <span> {{ $ad->sub_category->name }}</span>
                    </div>
                </div>

                <div class="s-icon">
                    <div class="v-center"> 
                        <i class="fa fa-clock-o i-color-green"></i>    
                        <span> {{ date('yy-m-d',strtotime($ad->created_at)) }}</span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="btn-group-sm">

                            <button type="button" class="btn btn-none" onclick="ajaxSavedAd({{$ad->id}})">
                                <i class="fa-18 fa fa-bookmark @if ($ad->saved)  i-color-red @else  i-color-gray @endif" id="saved_icon_{{$ad->id}}" aria-hidden="true"></i>
                            </button>
                            <button type="button" class="btn btn-out-red btn-sm" id="open-report-ad" data-toggle="modal" data-target="#reportModel" data-title-ad="{{ $ad->title }}" data-id-ad="{{ $ad->id }}">
                                <i class="fa fa-envelope-o" aria-hidden="true"></i>
                                أبلغنا عن اساءة
                            </button>
                        </div>

                    </div>
                </div>
                  
            </div>
        </div>
        <div class="user-image">
            <img class="img-circle avatar img-thumbnail" height="70px"  width="70px" src="{{ url('storage/' . $ad->user->avatar) }}">
            <p> <a href="{{ route('users.profile' , $ad->user->username ?? 'null') }}"> {{$ad->user->name}} </a></p>
        </div>
        <div class="ad-active @if ($ad->status == Helper::ACTIVE) on @elseif ($ad->status == Helper::DEACTIVE) off @endif">
            <i class="fa @if ($ad->status == Helper::ACTIVE) fa-eye @elseif ($ad->status == Helper::DEACTIVE) fa-eye-slash @endif" aria-hidden="true"></i>
            @if ($ad->status == Helper::ACTIVE)
                <span>مفتوح</span>    
            @elseif ($ad->status == Helper::DEACTIVE) 
                <span>مفلق</span>
            @endif
        </div>
    </div>
</div>

@include('frontend.components.model-report')
@endforeach

@push('scripts')
    
<script>
    function ajaxSavedAd (id){
        jQuery.ajax({
            url: "{{ route('ad.addToSaved') }}",
            method: 'post',
            data: {
                _token: "{{csrf_token()}}",
                ad_id: id,
            },
            success: function(result){
                console.log(result);
                if(result.success == true){
                    toastr.success(result.message);
                    if(result.action == 'saved'){
                        
                        $("#saved_icon_" + id).removeClass('i-color-gray');
                        $("#saved_icon_" + id).addClass('i-color-red');
                    }
                    if(result.action == 'unsaved'){
                        $("#saved_icon_" + id).removeClass('i-color-red');
                        $("#saved_icon_" + id).addClass('i-color-gray');
                    }
                }
                else{
                    toastr.error(result.message)
                }

            }
        });
    }
</script>
@endpush