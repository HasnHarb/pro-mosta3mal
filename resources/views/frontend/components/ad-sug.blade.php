    @foreach ($ads as $ad)
        <div class="col-md-3 col-sm-6 col-xs-12">
            <a href="{{ route('ad.show-ad' , [$ad->slug]) }}">
            <li class="cards__item">
                <div class="card">
                    <div class="card__image" style="background-image: url({{ $ad->link_images[0]->download_link }})"></div>
                    <div class="card__content">
                        <div class="card__title">
                            <a href="{{ route('ad.show-ad' , [$ad->slug]) }}">
                                {{ $ad->title }}
                            </a>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="s-icon" style="margin-left: 0px;">
                                    <div class="v-center"> 
                                        <i class="fa fa-map-marker i-color-red"></i> 
                                        <span>{{ $ad->city->name }}</span>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-8">
                                <div class="s-icon" style="margin-left: 0px;">
                                    <div class="v-center"> 
                                        <i class="fa fa-list i-color-blue"></i>
                                        <span> {{ $ad->sub_category->name }}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="s-icon" style="margin-left: 0px;">
                                    <div class="v-center"> 
                                        <i class="fa fa-clock-o i-color-green"></i>    
                                        <span>{{ $ad->created_at->locale('ar')->diffForHumans() }}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
            </a>
        </div>
    @endforeach
