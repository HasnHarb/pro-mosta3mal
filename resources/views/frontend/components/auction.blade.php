<div class="row">
    @foreach ($auctions as $auction)
        <div class="col-md-4 col-sm-6 col-xs-12">
            <a href="{{ route('ad.show-ad' , [$auction->ad->slug]) }}">
            <li class="cards__item">
                <div class="card">
                    <div class="card__image" style="background-image: url({{ $auction->ad->link_images[0]->download_link }})"></div>
                    <div class="card__content">
                        <div class="timer text-center" style="position: absolute;">
                            <ul>
                                <li><span id="days{{ $auction->id }}"></span>يوم</li>
                                <li><span id="hours{{ $auction->id }}"></span>ساعة</li>
                                <li><span id="minutes{{ $auction->id }}"></span>دقيقة</li>
                                <li><span id="seconds{{ $auction->id }}"></span>ثواني</li>
                            </ul>
                        </div>
                        <div class="card__title">
                            <a href="{{ route('ad.show-ad' , [$auction->ad->slug]) }}">
                                {{ $auction->ad->title }}
                            </a>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="s-icon" style="margin-left: 0px;">
                                    <div class="v-center"> 
                                        <i class="fa fa-map-marker i-color-red"></i> 
                                        <span>{{ $auction->ad->city->name }}</span>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-8">
                                <div class="s-icon" style="margin-left: 0px;">
                                    <div class="v-center"> 
                                        <i class="fa fa-list i-color-blue"></i>
                                        <span> {{ $auction->ad->sub_category->name }}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="s-icon" style="margin-left: 0px;">
                                    <div class="v-center"> 
                                        <i class="fa fa-clock-o i-color-green"></i>    
                                        <span>{{ $auction->created_at->locale('ar')->diffForHumans() }}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
            </a>
        </div>
    @endforeach
</div>
@push('scripts')
    <script src="{{ asset('js/app-all.js') }}"></script>
    <script>
        $( document ).ready(function() {
            let json = ("{{ json_encode( $timer) }}").replace(/&quot;/g,'"');
            var data = JSON.parse(json);
            data.forEach((value, index) => {
                timer(value.id , value.start_date , value.end_date)
            })
        });
    </script>
@endpush