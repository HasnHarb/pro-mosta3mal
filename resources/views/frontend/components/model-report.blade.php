<style>
    #fixedbutton {
        position: fixed;
        bottom: 0px;
        left: 50px;
        border-radius: 10px 10px 0px 0px !important;
        padding: 6px 16px !important;
        font-weight: normal;
    }
    
    .modal-title {
        font-weight: bold;
    }
</style>
<!-- Modal -->
<div class="modal font-kufi fade" id="reportModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title i-color-red font-kufi" id="exampleModalLabel">
                    الابلاغ عن اساءة
                </h4>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <form action="{{ route('general.store-report-ad') }}" method="POST" id="form_message">
                            @csrf 
                            <input type="hidden" name="ad_id" id="ad_id" value="">
                            @include('frontend.forms.input' , [ 
                                'name' => 'title_ad', 
                                'label' => 'عنوان الاعلان', 
                                'placeholder' => '', 
                                'type' => 'text', 
                                'required' => false, 
                                'col' => 12,
                                'redonly' => true
                            ]) 
                            @include('frontend.forms.input' , [
                                'name' => 'email',
                                'label' => 'البريد الالكتروني',
                                'placeholder' => 'أدخل البريد الالكتروني',
                                'type' => 'text',
                                'required' => true,
                                'col' => 12,
                                'value' => Auth::check() ? Auth::user()->email : ''
                            ])
                            @include('frontend.forms.textarea', [ 
                                'name' => 'body', 
                                'label' => 'الرسالة', 
                                'placeholder' => 'ادخل نص الابلاغ', 
                                'required' => true, 
                                'col' => 12 
                            ])
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">إغلاق</button>
                <button type="submit" form="form_message" class="btn btn-red">إرسال</button>
            </div>
        </div>
    </div>
</div>

@push('scripts')
    <script>
        $(document).on("click", "#open-report-ad", function () {
            var titleAd = $(this).data('title-ad');
            var idAd = $(this).data('id-ad');
            $("#reportModel #title_ad").val( titleAd );
            $("#reportModel #ad_id").val( idAd );
           
        });
    </script>
@endpush