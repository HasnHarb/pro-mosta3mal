<div class="comment-body">
    <div class="dropdown">
        <button class="dropdown-toggle" type="button" data-toggle="dropdown">خيارات
            <i class="fa fa-cog" aria-hidden="true"></i>
        </button>
        <ul class="dropdown-menu">
          <li><a href="#">HTML</a></li>
          <li><a href="#">CSS</a></li>
          <li><a href="#">JavaScript</a></li>
        </ul>
      </div>
    <div class="user-img-comment">
        <img class="img-circle avatar img-thumbnail" height="70px"  width="70px" src="{{ url('storage/' . $offer->user->avatar) }}">
    </div>
    <div class="comment-text">
        <strong style="font-size: 16px;">{{ $offer->user->name }}</strong>
        <p> {{ $offer->body }} </p>
        <div class="comment-info">
            <div class="s-icon">
                <div class="v-center"> 
                    <i class="fa fa-clock-o i-color-green"></i>    
                    <span>
                        {{ $offer->created_at->locale('ar')->diffForHumans() }} 
                        <strong> ( {{ date('yy-m-d',strtotime($offer->created_at)) }} ) </strong>
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>