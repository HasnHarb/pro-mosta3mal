

@push('scripts')

    <script src="{{ asset('js/sweet-confirm.js') }}"></script>

@endpush

@if ($errors->any())
    @foreach ($errors->all() as $error)
        @php
            toastr()->error($error);
        @endphp
    @endforeach
@endif

@if(Session::has('success'))
    @php
        toastr()->success(Session::get('success'));
    @endphp
@endif

<div class="table-responsive">
    <table class="table table-hover">
        <thead>
            <tr>
                <th>صورة</th>
                <th>الاسم</th>
                <th class="text-center">عدد الاعلانات</th>
                <th class="text-center">عدد الخدمات</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($users as $user)
                @php
                    $url = route('users.profile' , [$user->username ?? 'null']) ;
                @endphp
                <tr>
                    <td scope="row">
                        <img class="avatar img-circle img-thumbnail"   src="{{ $user->link_image ?? '' }}" width="60px" height="60px">
                    </td>
                    <td onclick="window.location='{{$url}}'" class="cursor-pointer">
                        <a style="font-size: 16px" href="{{ $url }}">{{ $user->name }}</a>
                        <p style="padding: 0px; margin: 4px 0px;">
                            @if (mb_strlen($user->bio) > 50)
                                {{  mb_substr($user->bio ,0,50,'utf-8') }} ...
                                <a href="{{ $url }}"> قراءة المزيد </a>
                            @else
                                {{ $user->bio }}
                            @endif
                        </p>

                        <div class="s-icon">
                            <div class="v-center"> 
                                <i class="fa fa-map-marker i-color-pinck"></i> 
                                <span>{{ $user->city->name ?? "" }}</span>
                            </div>
                        </div>

                        <div class="s-icon">
                            <div class="v-center"> 
                                <i class="fa fa-clock-o i-color-green"></i>    
                                <span>تم الانضمام منذ {{ $user->created_at->locale('ar')->diffForHumans() }}</span>
                                
                            </div>
                        </div>
                            
                    </td>

                    <td class="text-center">
                        {{ $user->ads->count() }}
                    </td>
                    <td class="text-center">
                        {{ $user->services->count() }}
                    </td>
                </tr>
                @endforeach
            </tbody>
    </table>
</div>
@push('scripts')
    
<script>
    function ajaxSavedService (id){
        jQuery.ajax({
            url: "{{ route('services.addToSaved') }}",
            method: 'post',
            data: {
                _token: "{{csrf_token()}}",
                service_id: id,
            },
            success: function(result){
                console.log(result);
                if(result.success == true){
                    toastr.success(result.message);
                    if(result.action == 'saved'){
                        
                        $("#saved_icon_" + id).removeClass('i-color-gray');
                        $("#saved_icon_" + id).addClass('i-color-red');
                    }
                    if(result.action == 'unsaved'){
                        $("#saved_icon_" + id).removeClass('i-color-red');
                        $("#saved_icon_" + id).addClass('i-color-gray');
                    }
                }
                else{
                    toastr.error(result.message)
                }

            }
        });
    }
</script>
@endpush
