<style>
    #fixedbutton {
        position: fixed;
        bottom: 0px;
        left: 50px;
        border-radius: 10px 10px 0px 0px !important;
        padding: 6px 16px !important;
        font-weight: normal;
    }
    
    .modal-title {
        font-weight: bold;
    }
</style>
<!-- Modal -->
<div class="modal font-kufi fade" id="ratingModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title i-color-red font-kufi" id="exampleModalLabel">
                    اضف تقييم
                </h4>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <form action="{{ route('general.store-rating') }}" method="POST" id="form_rating">
                            @csrf 
                            <input type="hidden" name="user_id" id="user_id" value="{{ $user->id }}">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for=""> التقييم: </label>
                                    @include('frontend.forms.rate' , [
                                        'check' => false ,
                                        'name' => 'rate_value',
                                        'required' => true
                                    ])
                                </div>
                            </div>
                            @include('frontend.forms.textarea', [ 
                                'name' => 'body', 
                                'label' => 'تعلييق', 
                                'placeholder' => 'اكتب تعليقك', 
                                'required' => true, 
                                'col' => 12,
                                'row' => 5
                            ])
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">إغلاق</button>
                <button type="submit" form="form_rating" class="btn btn-red">إرسال</button>
            </div>
        </div>
    </div>
</div>
