    <!-- Authentication Links -->
@guest
    <li>
        <a href="#" data-toggle="modal" data-target="#loginModel">
            <i class="fa fa-sign-in" aria-hidden="true"></i>
            تسجيل دخول
        </a>
    </li>

    @if (Route::has('register'))
        <li>
            <a href="{{ route('frontend.signup') }}">
                <i class="fa fa-user-plus" aria-hidden="true"></i>
                حساب جديد
            </a>
        </li>
    @endif
@else
    <li class="dropdown">
        <a id="navbarDropdown" class="dropbtn" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
           <i class="fa fa-user" aria-hidden="true"></i>
            {{ Auth::user()->name }} <span class="caret"></span>
        </a>

        <ul class="dropdown-content text-right" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="{{ route('logout') }}" 
                onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
                        <i class="fa fa-sign-out" aria-hidden="true"></i>
                {{ __('تسجيل الخروج') }}
            </a>
            <a class="dropdown-item" href="{{ route('users.profile' , [Auth::user()->username ?? 'null']) }}" >
                <i class="fa fa-user" aria-hidden="true"></i>
                الصفحة الشخصية
            </a>
            <a class="dropdown-item" href="/admin" >
                <i class="fa fa-user" aria-hidden="true"></i>
                لوحة التحكم
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        </ul>
    </li>
@endguest
<div id="loginModel" class="modal fade text-center" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-body">
                <div id="content">
                    <div class="header-login">
                        <h2>
                            <i class="fa fa-sign-in" aria-hidden="true"></i>
                            تسجيل الدخول 
                        </h2>
                    </div>
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="form_section">
                            <div class="form-group">
                                <input type="text" class="e-form-control" name="email" id="email" placeholder="ادخل البريد الالكتروني" required=""> 
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span> 
                                @enderror
                            </div>
                            <div class="form-group">
                                <input id="password" type="password" class="e-form-control @error('password') is-invalid @enderror" placeholder="كلمة المرور" name="password" required autocomplete="current-password"> 
                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span> 
                                @enderror
                            </div>
                            <button type="submit" class="btn btn-red">تسجيل دخول</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
