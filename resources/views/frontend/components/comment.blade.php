<div class="comment-body">
    @if (Auth::check() and $comment->cauth)    
        <div class="dropdown">
            <button class="dropdown-toggle btn btn-out-red btn-xs" type="button" data-toggle="dropdown">خيارات
                <i class="fa fa-cog" aria-hidden="true"></i>
            </button>
            <ul class="dropdown-menu">
                <li><a href="{{ route('ad.delete-comment' , $comment->id) }}">حذف</a></li>
            </ul>
        </div>
    @endif

      <div class="user-img-comment">
        <img class="img-circle avatar img-thumbnail" height="70px"  width="70px" src="{{ url('storage/' . $comment->user->avatar) }}">
    </div>
    <div class="comment-text">
        <strong style="font-size: 16px;">{{ $comment->user->name }}</strong>
        <p> {{ $comment->body }} </p>
        <div class="comment-info">
            <div class="s-icon">
                <div class="v-center"> 
                    <i class="fa fa-clock-o i-color-green"></i>    
                    <span>
                        {{ $comment->created_at->locale('ar')->diffForHumans() }} 
                        <strong> ( {{ date('yy-m-d',strtotime($comment->created_at)) }} ) </strong>
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>