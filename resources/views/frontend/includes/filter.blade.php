@push('scripts')
    @include('frontend.scripts.ajax_select' , [ 
        'perant' => 'main_category_id', 
        'child' => 'sub_category_id',
        'route' => 'api.sub_categories' 
    ])
@endpush
<div class="row">
    <div class="col-md-12">
        @include('frontend.forms.select' , [
            'name' => 'main_category_id',
            'label' => 'القسم الرئيسي',
            'required' => false,
            'options' => Helper::getMainCategories(),
            'col' => 12
        ])
    </div>
    <div class="col-md-12">
        @include('frontend.forms.select' , [
            'name' => 'sub_category_id',
            'label' => 'القسم الفرعي',
            'required' => false,
            'options' => [],
            'col' => 12
        ])
    </div>
    <div class="col-md-12">
        @include('frontend.forms.select' , [
            'name' => 'city_id',
            'label' => 'المدينة',
            'required' => false,
            'options' => Helper::getCities(),
            'col' => 12
        ])
    </div>
    <div class="col-md-12">
        <div class="form-group col-sm-12 col-md-12">
            <button class="btn btn-red btn-block">بحث</button>
        </div>
    </div>
</div>