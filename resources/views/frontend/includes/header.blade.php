<header class="header-inside">
    <div class="container">
        <div class="row">
        <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
            <div class="body-header-inside">
                <div class="display-center">
                    <img src="{{ asset($image ?? '') }}" height="160px">
                    <div class="header-text">
                        <h2>{{ $title }}</h2>
                        <p>{{ $desc }}</p>
                        @if (isset($link))
                            <a href="{{$link}}" class="btn btn-default">
                                <i class="fa fa-plus" aria-hidden="true"></i>
                                {{$textLink ?? ''}}
                            </a>
                        @endif
                    </div>
                </div>
                {{-- <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                </div> --}}
            </div>
        </div>

        {{-- <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 text-center display-center">
        </div> --}}
    </div>
    </div>
</header>