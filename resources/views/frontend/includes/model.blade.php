<style>
    #fixedbutton {
        position: fixed;
        bottom: 0px;
        left: 50px; 
        border-radius: 10px 10px 0px 0px !important;
        padding: 6px 16px !important;
        font-weight: normal;
    }
    .modal-title{
        font-weight: bold;
    }

</style>
@include('frontend.components.validation')
<!-- Button trigger modal -->
<button type="button" id="fixedbutton" class="btn btn-red" data-toggle="modal" data-target="#exampleModal">
    <i class="fa fa-envelope-o" aria-hidden="true"></i>
    أرسل ملاحظاتك
  </button>
  
  <!-- Modal -->
  <div class="modal font-kufi fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header text-center">
          <h4 class="modal-title i-color-red font-kufi" id="exampleModalLabel">
              أرسل ملاحظاتك
          </h4>
          {{-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button> --}}
        </div>
        <div class="modal-body">
            <div class="container-fluid">
                <div class="row">
                    <form action="{{ route('general.store-message') }}" method="POST" id="form_message">
                        @csrf
                        @include('frontend.forms.select' , [
                            'name' => 'type',
                            'label' => 'نوع الملاحظة',
                            'required' => false,
                            'options' => Helper::getTypeMessage(),
                            'col' => 12
                        ])
                        @include('frontend.forms.input' , [
                            'name' => 'email',
                            'label' => 'البريد الالكتروني',
                            'placeholder' => 'أدخل البريد الالكتروني',
                            'type' => 'text',
                            'required' => false,
                            'col' => 12,
                            'value' => Auth::check() ? Auth::user()->email : ''
                        ])
                        @include('frontend.forms.textarea' , [
                            'name' => 'body',
                            'label' => 'الرسالة',
                            'placeholder' => 'ادخل نص الرسالة',
                            'required' => false,
                            'col' => 12
                        ])
                    </form>
                </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">إغلاق</button>
          <button type="submit" form="form_message" class="btn btn-red">إرسال</button>
        </div>
      </div>
    </div>
  </div>