<div class="articles-color-white">
    <h2>التصنيفات</h2>
    @foreach ($categories as $main_cat)        
        <div class="panel-group">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" href="#category{{$main_cat->id}}">
                            {{ $main_cat->name }}
                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                        </a>
                    </h4>
                </div>
                <div id="category{{$main_cat->id}}" class="panel-collapse collapse">
                    @foreach ($main_cat->sub_categories as $sub_cat)
                        <div class="panel-body">
                            <a href="{{ $link .'?category='.$sub_cat->id }}">{{ $sub_cat->name }}</a> 
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    @endforeach
</div>
