<header class="header-inside" style="background-image: url('{{ asset('assets/images/cover3.jpg') }}')">
    <div class="container">
        <div class="body-header-inside">
            <form  action="{{ route('ad.show-ads') }}" method="GET">
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-8 bg-filter">
                    <div class="form-group col-sm-12 col-md-6">
                        <label class="text-right" for="main_category_id"> 
                            القسم الرئيسي:
                        </label>
                        <select class="e-form-control" id="main_category_id" name="main_category_id">
                            <option value="">-- اختر --</option>
                            @foreach(Helper::getMainCategories() as $key => $value)
                                <option value="{{ $key }}">{{ $value }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group col-sm-12 col-md-6">
                        <label class="text-right" for="sub_category_id"> 
                            القسم الفرعي:
                        </label>
                        <select class="e-form-control" id="sub_category_id" name="sub_category_id">
                            <option value="">-- اختر --</option>
                        </select>
                    </div>

                    <div class="form-group col-sm-12 col-md-12">
                        <label class="text-right" for=""> 
                            ابحث: 
                        </label>
                        <div class="input-group">

                            <input type="text" class="e-form-control" name="s" placeholder="ابحث عن اي شيء ...">
                            <div class="input-group-btn">
                                <button class="btn btn-red" type="submit">
                                    <i class="fa fa-search" aria-hidden="true"></i>
                                    بحث
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                    <div class="col-md-2"></div>

                </div>
            </form>
        </div>
    </div>
</header>