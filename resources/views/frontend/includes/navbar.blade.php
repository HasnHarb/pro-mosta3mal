<div class="nav_bar">
    <nav class="navbar  navbar-icon-top navbar-default">
        <div class="container">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                    
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    {{-- <h2>كراركيب</h2> --}}
                    {{-- <img src="{{ asset('images/logo.png') }}" width="110"> --}}
                    <a class="navbar-brand" rel="home" href="/" title="Buy Sell Rent Everyting">
                        <img style="max-width:110px; margin-top: -11px;"
                             src="{{ asset('images/logo.png') }}">
                    </a>
                </div>
           
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        <li>

                            <a href="{{ route('frontend.index') }}">
                                <i class="fa fa-home"></i>
                                الرئيسية 
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('frontend.about-as') }}">
                                <i class="fa fa-info"></i>
                                من نحن
                            </a>
                        </li>
                        
                        <li>
                            <a href="{{ route('users.all') }}">
                                <i class="fa fa-users"></i>
                                الاعضاء
                            </a>
                        </li>
                        
                        <li>
                            <a href="{{ route('services.index') }}">
                                <i class="fa fa-archive" aria-hidden="true"></i>
                                طلب خدمة                            
                            </a>
                        </li>
                        
                        <li>
                            <a href="{{ route('ad.show-ads') }}">
                                <i class="fa fa-paper-plane" aria-hidden="true"></i>
                                اخر الاعلانات                            
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('auctions.index') }}">
                                <i class="fa fa-line-chart " aria-hidden="true"></i>
                                المزاد                            
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('users.saved') }}">
                                <i class="fa fa-bookmark" aria-hidden="true"></i>
                                المحفوظات
                            </a>
                        </li>
                        {{-- <li class="dropdown">
                            <a href="sections.html" class="dropbtn">الأقسام<span class="caret"></span></a>
                            <ul class="dropdown-content text-right">
                                <li><a href="inside-section.html">قسم التصميم</a></li>
                                <li><a href="inside-section.html">قسم البرمجة</a></li>
                                <li><a href="inside-section.html">قسم التسويق</a></li>
                                <li><a href="inside-section.html">قسم الرياضيات</a></li>
                            </ul>
                        </li>
                        <li><a href="articles.html">المقالات</a></li> --}}
                        @include('frontend.components.login-model')
                        <li>
                            <a href="{{ route('ad.create') }}" class="active">
                                <i class="fa fa-plus" aria-hidden="true"></i>
                                أضف إعلان مجاناً
                            </a>
                        </li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </div>
    </nav>
</div>