    <div class="col-md-3 text-center">
        <a href="{{ route('ad.show-ads' , ['main_category_id' => $category->id]) }}">
            <div class="cat-card">
                <img src="{{url('storage/' . $category->icon)}}" height="50px" alt="">
                <p>{{$category->name}}</p>
                <span style="font-size: 12px">اعلان: {{$category->ads->count()}}</span>
            </div>
        </a>
    </div>
