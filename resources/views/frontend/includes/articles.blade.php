<section class="articles">
    <div class="container">
        <h2>مقالات كورسات</h2>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="box-article">
                <div class="col-lg-2 col-md-4 col-sm-4 col-xs-4 box-article img-none">
                    <a href="articles.html">
                        <div class="section-title color-pink">
                            <img src="{{ asset('assets/images/articels-img.jpg') }}">
                            <span>أسئلة برمجية</span>
                        </div>
                    </a>
                </div>
                <div class="col-lg-8 col-lg-offset-2 col-md-8 col-sm-8 col-xs-12 box-article">
                    <a href="articles.html">
                        <h2> ماهو الفرق بين nodejs و angualrjs</h2>
                        <p>كيفية اتمام تنصيب ووردبريس وتركيبه وتنسيقه واعداده من البداية مع مجموعة مهارات اضافيةمن
                            البداية مع مجموعة مهارات اضافية</p>
                    </a>
                    <div class="article-comments">
                        <span>29<img src="{{ asset('assets/images/comment.jpg') }}"></span>
                        <span>2<img src="{{ asset('assets/images/like.jpg') }}"></span>
                        <span>FEB 12,2015<img src="{{ asset('assets/images/share.jpg') }}"></span>
                    </div>
                </div>
            </div>
            <div class="box-article">
                <div class="col-lg-2 col-md-4 col-sm-4 col-xs-4 box-article img-none">
                    <a href="articles.html">
                        <div class="section-title">
                            <img src="{{ asset('assets/images/articels-img.jpg') }}">
                            <span>مقالات عامة</span>
                        </div>
                    </a>
                </div>
                <div class="col-lg-8 col-lg-offset-2 col-md-8 col-sm-8 col-xs-12 box-article">
                    <a href="articles.html">
                        <h2>ايهما أفضل التخزين الوقت في القواعد</h2>
                        <p>هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد أن تولد
                            مثل هذا النص أو العديد من النصوص</p>
                    </a>
                    <div class="article-comments">
                        <span>29<img src="{{ asset('assets/images/comment.jpg') }}"></span>
                        <span>2<img src="{{ asset('assets/images/like.jpg') }}"></span>
                        <span>FEB 12,2015<img src="{{ asset('assets/images/share.jpg') }}"></span>
                    </div>
                </div>
            </div>
            <div class="box-article">
                <div class="col-lg-2 col-md-4 col-sm-4 col-xs-4 box-article img-none">
                    <a href="articles.html">
                        <div class="section-title color-green">
                            <img src="{{ asset('assets/images/articels-img.jpg') }}">
                            <span>لقائات برمجية</span>
                        </div>
                    </a>
                </div>
                <div class="col-lg-8 col-lg-offset-2 col-md-8 col-sm-8 col-xs-12 box-article">
                    <a href="articles.html">
                        <h2>ايهما أفضل التخزين الوقت في القواعد</h2>
                        <p>هذا النص هو دل في نفس المساحة، لقد تم توليد هذا النص من أو العديد من النصوص</p>
                    </a>
                    <div class="article-comments">
                        <span>29<img src="{{ asset('assets/images/comment.jpg') }}"></span>
                        <span>2<img src="{{ asset('assets/images/like.jpg') }}"></span>
                        <span>FEB 12,2015<img src="{{ asset('assets/images/share.jpg') }}"></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">

            <div class="box-article">
                <div class="col-lg-2 col-md-4 col-sm-4 col-xs-4 box-article img-none">
                    <a href="articles.html">
                        <div class="section-title">
                            <img src="{{ asset('assets/images/articels-img.jpg') }}">
                            <span>أسئلة برمجية</span>
                        </div>
                    </a>
                </div>
                <div class="col-lg-8 col-lg-offset-2 col-md-8 col-sm-8 col-xs-12 box-article">
                    <a href="articles.html">
                        <h2>مجموعة تصاميم ومخطوطات</h2>
                        <p>هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد أن تولد
                            مثل هذا النص أو العديد من النصوص</p>
                    </a>
                    <div class="article-comments">
                        <span>29<img src="{{ asset('assets/images/comment.jpg') }}"></span>
                        <span>2<img src="{{ asset('assets/images/like.jpg') }}"></span>
                        <span>FEB 12,2015<img src="{{ asset('assets/images/share.jpg') }}"></span>
                    </div>
                </div>
            </div>
            <div class="box-article">
                <div class="col-lg-2 col-md-4 col-sm-4 col-xs-4 box-article img-none">
                    <a href="articles.html">
                        <div class="section-title color-green">
                            <img src="{{ asset('assets/images/articels-img.jpg') }}">
                            <span>أسئلة برمجية</span>
                        </div>
                    </a>
                </div>
                <div class="col-lg-8 col-lg-offset-2 col-md-8 col-sm-8 col-xs-12 box-article">
                    <a href="articles.html">
                        <h2>لكل من يعشق عالم الهواتف</h2>
                        <p>هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد أن تولد
                            مثل هذا النص أو العديد من النصوص</p>
                    </a>
                    <div class="article-comments">
                        <span>29<img src="{{ asset('assets/images/comment.jpg') }}"></span>
                        <span>2<img src="{{ asset('assets/images/like.jpg') }}"></span>
                        <span>FEB 12,2015<img src="{{ asset('assets/images/share.jpg') }}"></span>
                    </div>
                </div>
            </div>
            <div class="box-article">
                <div class="col-lg-2 col-md-4 col-sm-4 col-xs-4 box-article img-none">
                    <a href="articles.html">
                        <div class="section-title">
                            <img src="{{ asset('assets/images/articels-img.jpg') }}">
                            <span>أسئلة برمجية</span>
                        </div>
                    </a>
                </div>
                <div class="col-lg-8 col-lg-offset-2 col-md-8 col-sm-8 col-xs-12 box-article">
                    <a href="articles.html">
                        <h2>ايهما أفضل التخزين الوقت في القواعد</h2>
                        <p>هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد أن تولد
                            مثل هذا النص أو العديد من النصوص</p>
                    </a>
                    <div class="article-comments">
                        <span>29<img src="{{ asset('assets/images/comment.jpg') }}"></span>
                        <span>2<img src="{{ asset('assets/images/like.jpg') }}"></span>
                        <span>FEB 12,2015<img src="{{ asset('assets/images/share.jpg') }}"></span>
                    </div>
                </div>
            </div>
        </div>
        <a href="articles.html"><button type="submit">عرض المزيد من المقالات</button></a>
    </div>
</section>