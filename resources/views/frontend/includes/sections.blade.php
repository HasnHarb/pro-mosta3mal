<div class="sections">
    <div class="container">
        <ul class="nav nav-pills">
            <li class="active">
                <a data-toggle="pill" href="#menu1">
                    <h3>اخر دورات الموقع</h3>
                </a>
            </li>
            <li>
                <a data-toggle="pill" href="#menu2">
                    <h3>افضل الدورات</h3>
                </a>
            </li>
            <li>
                <a data-toggle="pill" href="#menu3">
                    <h3>الدورات المجانية</h3>
                </a>
            </li>
        </ul>
    </div>
</div>
<section class="website-sessions">
    <div class="container">
        <div class="tab-content">
            <div id="menu1" class="tab-pane fade in active">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div>
                        <img src="{{ asset('assets/images/wordpress.jpg') }}" class="img-responsive">
                        <div class="level-image">
                            <span class="color-wordpress">ووردبريس</span>
                            <span class="level-easy">سهل</span>
                        </div>
                    </div>
                    <div class="boxs">
                        <div class="cours-price">
                            <h4> اساسيات الوردبريس</h4>
                            <span>$20</span>
                        </div>
                        <p>هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد لى
                            زيادة عدد الحروف مولد النص العربى زيادة</p>
                        <hr>
                        <div class="explained-cours">
                            <button>ابدأ الدورة</button>
                            <div class="time-writer">
                                <a href="#" data-toggle="tooltip" data-placement="top" title="ساعة ونصف"><i
                                        class="fa fa-clock-o" aria-hidden="true"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="top" title="مشاهدة عرض سريع">
                                    <i class="fa fa-play" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div>
                        <img src="{{ asset('assets/images/html.jpg') }}" class="img-responsive">
                        <div class="level-image">
                            <span class="color-html">HTML</span>
                            <span class="level-easy">سهل</span>
                        </div>
                    </div>
                    <div class="boxs">
                        <div class="cours-price">
                            <h4> بناء صفحة ملف شخصي</h4>
                            <span>$20</span>
                        </div>
                        <p>هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد لى
                            زيادة عدد الحروف مولد النص العربى زيادة</p>
                        <hr>
                        <div class="explained-cours">
                            <button>ابدأ الدورة</button>
                            <div class="time-writer">
                                <a href="#" data-toggle="tooltip" data-placement="top" title="ساعة ونصف"><i
                                        class="fa fa-clock-o" aria-hidden="true"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="top" title="مشاهدة عرض سريع">
                                    <i class="fa fa-play" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div>
                        <img src="{{ asset('assets/images/sass.jpg') }}" class="img-responsive">
                        <div class="level-image">
                            <span class="color-Sass">Sass</span>
                            <span class="level-med">وسط</span>
                        </div>
                    </div>
                    <div class="boxs">
                        <div class="cours-price">
                            <h4> بناء صفحة ملف شخصي</h4>
                            <span>$20</span>
                        </div>
                        <p>هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد لى
                            زيادة عدد الحروف مولد النص العربى زيادة</p>
                        <hr>
                        <div class="explained-cours">
                            <button>ابدأ الدورة</button>
                            <div class="time-writer">
                                <a href="#" data-toggle="tooltip" data-placement="top" title="ساعة ونصف"><i
                                        class="fa fa-clock-o" aria-hidden="true"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="top" title="مشاهدة عرض سريع">
                                    <i class="fa fa-play" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="menu2" class="tab-pane fade">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div>
                        <img src="{{ asset('assets/images/blogger.jpg') }}" class="img-responsive">
                        <div class="level-image">
                            <span class="color-Blogger">Blogger</span>
                            <span class="level-med">وسط</span>
                        </div>
                    </div>
                    <div class="boxs">
                        <div class="cours-price">
                            <h4> تصميم تطبيق محادثة</h4>
                            <span>$100</span>
                        </div>
                        <p>هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد لى
                            زيادة عدد الحروف مولد النص العربى زيادة</p>
                        <hr>
                        <div class="explained-cours">
                            <div class="explained-cours">
                                <button>ابدأ الدورة</button>
                                <div class="time-writer">
                                    <a href="#" data-toggle="tooltip" data-placement="top" title="ساعة ونصف"><i
                                            class="fa fa-clock-o" aria-hidden="true"></i></a>
                                    <a href="#" data-toggle="tooltip" data-placement="top" title="مشاهدة عرض سريع">
                                        <i class="fa fa-play" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div>
                        <img src="{{ asset('assets/images/css.jpg') }}" class="img-responsive">
                        <div class="level-image">
                            <span class="color-Css">CSS</span>
                            <span class="level-hard">صعب</span>
                        </div>
                    </div>
                    <div class="boxs">
                        <div class="cours-price">
                            <h4> تصميم تطبيق محادثة</h4>
                            <span>$55</span>
                        </div>
                        <p>هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد لى
                            زيادة عدد الحروف مولد النص العربى زيادة</p>
                        <hr>
                        <div class="explained-cours">
                            <div class="explained-cours">
                                <button>ابدأ الدورة</button>
                                <div class="time-writer">
                                    <a href="#" data-toggle="tooltip" data-placement="top" title="ساعة ونصف"><i
                                            class="fa fa-clock-o" aria-hidden="true"></i></a>
                                    <a href="#" data-toggle="tooltip" data-placement="top" title="مشاهدة عرض سريع">
                                        <i class="fa fa-play" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div>
                        <img src="{{ asset('assets/images/wordpress.jpg') }}" class="img-responsive">
                        <div class="level-image">
                            <span class="color-wordpress">وردبريس</span>
                            <span class="level-hard">صعب</span>
                        </div>
                    </div>
                    <div class="boxs">
                        <div class="cours-price">
                            <h4>لوحة تحكم الوردبريس</h4>
                            <span>$60</span>
                        </div>
                        <p>هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد لى
                            زيادة عدد الحروف مولد النص العربى زيادة</p>
                        <hr>
                        <div class="explained-cours">
                            <div class="explained-cours">
                                <button>ابدأ الدورة</button>
                                <div class="time-writer">
                                    <a href="#" data-toggle="tooltip" data-placement="top" title="ساعة ونصف"><i
                                            class="fa fa-clock-o" aria-hidden="true"></i></a>
                                    <a href="#" data-toggle="tooltip" data-placement="top" title="مشاهدة عرض سريع">
                                        <i class="fa fa-play" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="menu3" class="tab-pane fade">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div>
                        <img src="{{ asset('assets/images/css.jpg') }}" class="img-responsive">
                        <div class="level-image">
                            <span class="color-wordpress">ووردبريس</span>
                            <span class="level-easy">سهل</span>
                        </div>
                    </div>
                    <div class="boxs">
                        <div class="cours-price">
                            <h4> عناصر الوردبريس</h4>
                            <span>$0</span>
                        </div>
                        <p>هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد لى
                            زيادة عدد الحروف مولد النص العربى زيادة</p>
                        <hr>
                        <div class="explained-cours">
                            <div class="explained-cours">
                                <button>ابدأ الدورة</button>
                                <div class="time-writer">
                                    <a href="#" data-toggle="tooltip" data-placement="top" title="ساعة ونصف"><i
                                            class="fa fa-clock-o" aria-hidden="true"></i></a>
                                    <a href="#" data-toggle="tooltip" data-placement="top" title="مشاهدة عرض سريع">
                                        <i class="fa fa-play" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div>
                        <img src="{{ asset('assets/images/blogger.jpg') }}" class="img-responsive">
                        <div class="level-image">
                            <span class="color-Blogger">Blogger</span>
                            <span class="level-med">وسط</span>
                        </div>
                    </div>
                    <div class="boxs">
                        <div class="cours-price">
                            <h4> تصميم تطبيق محادثة</h4>
                            <span>$100</span>
                        </div>
                        <p>هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد لى
                            زيادة عدد الحروف مولد النص العربى زيادة</p>
                        <hr>
                        <div class="explained-cours">
                            <div class="explained-cours">
                                <button>ابدأ الدورة</button>
                                <div class="time-writer">
                                    <a href="#" data-toggle="tooltip" data-placement="top" title="ساعة ونصف"><i
                                            class="fa fa-clock-o" aria-hidden="true"></i></a>
                                    <a href="#" data-toggle="tooltip" data-placement="top" title="مشاهدة عرض سريع">
                                        <i class="fa fa-play" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div>
                        <img src="{{ asset('assets/images/sass.jpg') }}" class="img-responsive">
                        <div class="level-image">
                            <span class="color-Sass">Sass</span>
                            <span class="level-med">وسط</span>
                        </div>
                    </div>
                    <div class="boxs">
                        <div class="cours-price">
                            <h4> بناء صفحة ملف شخصي</h4>
                            <span>$20</span>
                        </div>
                        <p>هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد لى
                            زيادة عدد الحروف مولد النص العربى زيادة</p>
                        <hr>
                        <div class="explained-cours">
                            <button>ابدأ الدورة</button>
                            <div class="time-writer">
                                <a href="#" data-toggle="tooltip" data-placement="top" title="ساعة ونصف"><i
                                        class="fa fa-clock-o" aria-hidden="true"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="top" title="مشاهدة عرض سريع">
                                    <i class="fa fa-play" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="button-show-more text-center">
            <a href="sections.html"><button type="submit">عرض كل الدورات</button></a>
        </div>
    </div>
</section>