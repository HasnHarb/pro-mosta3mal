<div class="articles-color-white">
    <div class="follow-us">
        <h2>تابعنا</h2>
        <div class="social-media">
            <div class="color-facebook"><a href="#"><i class="fa fa-facebook fa-2x" aria-hidden="true"></i></a>
            </div>
            <div class="color-twitter"> <a href="#"><i class="fa fa-twitter fa-2x" aria-hidden="true"></i></a>
            </div>
            <div class="color-instagram"><a href="#"><i class="fa fa-instagram fa-2x"
                        aria-hidden="true"></i></a></div>
            <div class="color-behance"><a href="#"><i class="fa fa-behance fa-2x" aria-hidden="true"></i></a>
            </div>
        </div>
    </div>
</div>