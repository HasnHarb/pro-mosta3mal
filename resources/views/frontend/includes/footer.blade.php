<section class="footer-section text-center">
    <ul>
        <li><a href="/">الرئيسية </a></li>
        <li><a href="{{ route('frontend.about-as') }}">من نحن</a></li>
        <li><a href="{{ route('frontend.privacy') }}">شروط الاستخدام</a></li> 
        <img src="{{ asset('images/logo2.png') }}" width="90px">
        <li><a href="{{ route('services.index') }}">الخدمات</a></li>
        <li><a href="{{ route('ad.show-ads') }}">اخر الاعلانات</a></li>
        <li><a href="{{ route('frontend.signup') }}">حساب جديد</a></li>
    </ul>
    <p>جميع الحقوق محفوظة لموقع كراكيب فلسطين 2020</p>
    <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
    <a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a>
    <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
</section>
