
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <style>
        @import url('https://fonts.googleapis.com/css?family=El+Messiri&display=swap');
        body {
            font-family: 'El Messiri', sans-serif;
        }
        
        * {
            font-family: 'El Messiri', sans-serif;
        }
        
        @media only screen and (max-width: 600px) {
            .main {
                width: 320px !important;
            }
            .top-image {
                width: 100% !important;
            }
            .inside-footer {
                width: 320px !important;
            }
            table[class="contenttable"] {
                width: 320px !important;
                text-align: left !important;
            }
            td[class="force-col"] {
                display: block !important;
            }
            td[class="rm-col"] {
                display: none !important;
            }
            .mt {
                margin-top: 15px !important;
            }
            *[class].width300 {
                width: 255px !important;
            }
            *[class].block {
                display: block !important;
            }
            *[class].blockcol {
                display: none !important;
            }
            .emailButton {
                width: 100% !important;
            }
            .emailButton a {
                display: block !important;
                font-size: 18px !important;
            }
        }
    </style>
  </head>

    @php
        $page = Helper::getPage('ad-accept');
    @endphp

    <body link="#e00000" vlink="#e00000" alink="#e00000" dir="rtl">
        <table class=" main contenttable" align="center" style="font-weight: normal;border-collapse: collapse;border: 0;margin-left: auto;margin-right: auto;padding: 0;color: #555559;background-color: white;font-size: 16px;line-height: 26px;width: 600px;">
            <tr>
                <td class="border" style="border-collapse: collapse;border: 1px solid #eeeff0;margin: 0;padding: 0;-webkit-text-size-adjust: none;color: #555559;font-size: 16px;line-height: 26px;">
                    <table style="font-weight: normal;border-collapse: collapse;border: 0;margin: 0;padding: 0;">
                        <tr>
                            <td colspan="4" valign="top" class="image-section" style="border-collapse: collapse;border: 0;margin: 0;padding: 0;-webkit-text-size-adjust: none;color: #555559;font-size: 16px;line-height: 26px;background-color: #fff;border-bottom: 4px solid #e00000">
                                <a href="https://v1.karakib.me/"><img class="top-image" height="100px" src="{{ asset('images/logo.png') }}" style="line-height: 1;" alt="Tenable Network Security"></a>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" class="side title" style="border-collapse: collapse;border: 0;margin: 0;padding: 20px;-webkit-text-size-adjust: none;color: #555559;font-size: 16px;line-height: 26px;vertical-align: top;background-color: white;border-top: none;">
                                <table style="font-weight: normal;border-collapse: collapse;border: 0;margin: 0;padding: 0;">
                                    <tr>
                                        <td class="head-title" style="border-collapse: collapse;border: 0;margin: 0;padding: 0;-webkit-text-size-adjust: none;color: #555559;font-size: 28px;line-height: 34px;font-weight: bold; text-align: center;">
                                            <div class="mktEditable" id="main_title">
                                                {{ $page->title }}
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="top-padding" style="border-collapse: collapse;border: 0;margin: 0;padding: 5px;-webkit-text-size-adjust: none;color: #555559;font-size: 16px;line-height: 26px;">
                                        </td>
                                    </tr>
                                    
                                    <tr>
                                        <td class="top-padding" style="border-collapse: collapse;border: 0;margin: 0;padding: 15px 0;-webkit-text-size-adjust: none;color: #555559;font-size: 16px;line-height: 21px;">
                                            <hr size="1" color="#eeeff0">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text" style="border-collapse: collapse;border: 0;margin: 0;padding: 0;-webkit-text-size-adjust: none;color: #555559;font-size: 16px;line-height: 26px;">
                                            <div class="mktEditable" id="main_text">
                                            
                                                <strong> مرحبا {{ $ad->user->name }} , </strong>
                                                <br>
                                                <p> {!! $page->body !!} </p>
                                                <br>
                                                <p>
                                                    <strong>عنوان الاعلان: </strong>
                                                    {{ $ad->title }}
                                                </p>
                                                <p>
                                                    <strong>وصف الاعلان: </strong>
                                                    {{ $ad->description }}
                                                </p>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="grey-block" style="border-collapse: collapse;border: 0;margin: 0;-webkit-text-size-adjust: none;color: #555559;font-size: 16px;line-height: 26px;background-color: #fff; text-align:center;">
                                            <div class="mktEditable" id="cta">
                                                <a style="color:#ffffff; background-color: #e00000;  padding: 10px 31px;; border-radius: 50px; text-decoration:none;" href="{{ route('ad.show-ad' , $ad->slug) }}">عرض اعلانك اضغط هنا</a>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="border-collapse: collapse;border: 0;margin: 0;padding: 0;-webkit-text-size-adjust: none;color: #555559;font-size: 16px;line-height: 24px;">
                                            &nbsp;<br>
                                        </td>
                                    </tr>

                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding:20px;  -webkit-text-size-adjust: none;" align="center">
                                <table>

                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="border-collapse: collapse;border: 0;margin: 0;padding: 0;-webkit-text-size-adjust: none;color: #555559;font-size: 16px;line-height: 24px; padding: 20px;">
                                <div class="mktEditable" id="cta_try">
                                    <table border="0" cellpadding="0" cellspacing="0" class="mobile" style="font-weight: normal;border-collapse: collapse;border: 0;margin: 0;padding: 0;">
                                        <tr>
                                            <td class="force-col" valign="top" style="border-collapse: collapse;border: 0;margin: 0;padding: 0;-webkit-text-size-adjust: none;color: #555559;font-size: 16px;line-height: 24px;">

                                                <table class="mb mt" style="font-weight: normal;border-collapse: collapse;border: 0;margin: 0;padding: 0;margin-bottom: 15px;margin-top: 0;">
                                                    <tr>
                                                        <td class="grey-block" style="border-collapse: collapse;border: 0;margin: 0;padding: 18px;-webkit-text-size-adjust: none;color: #555559;font-size: 16px;line-height: 24px;background-color: #fff; border-top: 3px solid #e00000; border-left: 1px solid #E6E6E6; border-right: 1px solid #E6E6E6; border-bottom: 1px solid #E6E6E6; width: 250px; text-align: center;">

                                                            <span style=" font-size: 24px; line-height: 39px; border-collapse: collapse;border: 0;margin: 0;padding: 0;-webkit-text-size-adjust: none;color: #555559; text-align: center;font-weight: bold;">
                                                                مشاهدة اخر الخدمات
                                                            </span>

                                                            <br><br>
                                                            <a style="color:#ffffff; background-color: #e00000;  border-top: 10px solid #e00000; border-bottom: 10px solid #e00000; border-left: 20px solid #e00000; border-right: 20px solid #e00000; border-radius: 50px; text-decoration:none;" href="{{ route('services.index') }}">
                                                                اضغط هنا
                                                            </a>

                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td class="rm-col" style="border-collapse: collapse;border: 0;margin: 0;padding: 0;-webkit-text-size-adjust: none;color: #555559;font-size: 16px;line-height: 24px;padding-right: 15px;">
                                            </td>
                                            <td class="force-col" valign="top" style="border-collapse: collapse;border: 0;margin: 0;padding: 0;-webkit-text-size-adjust: none;color: #555559;font-size: 16px;line-height: 24px;">
                                                <table class="mb mt" style="font-weight: normal;border-collapse: collapse;border: 0;margin: 0;padding: 0;margin-bottom: 15px;margin-top: 0;">
                                                    <tr>
                                                        <td class="grey-block" style="border-collapse: collapse;border: 0;margin: 0;padding: 18px;-webkit-text-size-adjust: none;color: #555559;font-size: 16px;line-height: 24px;background-color: #fff; border-top: 3px solid #e00000; border-left: 1px solid #E6E6E6; border-right: 1px solid #E6E6E6; border-bottom: 1px solid #E6E6E6; width: 250px; text-align: center;">

                                                            <span style=" font-size: 24px; line-height: 39px; border-collapse: collapse;border: 0;margin: 0;padding: 0;-webkit-text-size-adjust: none;color: #555559; text-align: center;font-weight: bold;">
                                                                شاهد اخر الاعلانات</span>
                                                            <br>
                                                            <br>
                                                            <a style="color:#ffffff; background-color: #e00000;  border-top: 10px solid #e00000; border-bottom: 10px solid #e00000; border-left: 20px solid #e00000; border-right: 20px solid #e00000; border-radius: 50px; text-decoration:none;" href="{{ route('ad.show-ads') }}">
                                                                اضغط هنا
                                                            </a>

                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" align="center" style="border-collapse: collapse;border: 0;margin: 0;padding: 0;-webkit-text-size-adjust: none;color: #555559;font-size: 16px;line-height: 26px;">
                                <table style="font-weight: normal;border-collapse: collapse;border: 0;margin: 0;padding: 0;">
                                    <tr>
                                        <td align="center" valign="middle" class="social" style="border-collapse: collapse;border: 0;margin: 0;padding: 10px;-webkit-text-size-adjust: none;color: #555559;font-size: 16px;line-height: 26px;text-align: center;">
                                            <table style="font-weight: normal;border-collapse: collapse;border: 0;margin: 0;padding: 0;">
                                                <tr>

                                                    <td style="border-collapse: collapse;border: 0;margin: 0;padding: 5px;-webkit-text-size-adjust: none;color: #555559;font-size: 16px;line-height: 26px;">
                                                        <a href="https://twitter.com/karakib.me"><img src="https://info.tenable.com/rs/tenable/images/twitter-teal.png"></a>
                                                    </td>
                                                    <td style="border-collapse: collapse;border: 0;margin: 0;padding: 5px;-webkit-text-size-adjust: none;color: #555559;font-size: 16px;line-height: 26px;">
                                                        <a href="https://www.facebook.com/karakib.me"><img src="https://info.tenable.com/rs/tenable/images/facebook-teal.png"></a>
                                                    </td>

                                                    <td style="border-collapse: collapse;border: 0;margin: 0;padding: 5px;-webkit-text-size-adjust: none;color: #555559;font-size: 16px;line-height: 26px;">
                                                        <a href="https://www.linkedin.com/company/tenable-network-security"><img src="https://info.tenable.com/rs/tenable/images/linkedin-teal.png"></a>
                                                    </td>


                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr bgcolor="#fff" style="border-top: 4px solid #e00000;">
                            <td valign="top" class="footer" style="border-collapse: collapse;border: 0;margin: 0;padding: 0;-webkit-text-size-adjust: none;color: #555559;font-size: 16px;line-height: 26px;background: #fff;text-align: center;">
                                <table style="font-weight: normal;border-collapse: collapse;border: 0;margin: 0;padding: 0;">
                                    <tr>
                                        <td class="inside-footer" align="center" valign="middle" style="border-collapse: collapse;border: 0;margin: 0;padding: 20px;-webkit-text-size-adjust: none;color: #555559;font-size: 12px;line-height: 16px;vertical-align: middle;text-align: center;width: 580px;">
                                            <div id="address" class="mktEditable">
                                                <b>موقع كراركيب</b><br>
                                                <a style="color: #e00000;" href="https://v1.karakib.me">تواصل معنا</a>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </body>
</html>