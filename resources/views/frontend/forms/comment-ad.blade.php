<div class="e-panel panel-default">
    <div class="e-panel-heading">
        <a class="btn-collapse pull-left" data-toggle="collapse" href="#offer" aria-expanded="true">
            <i class="fa fa-angle-right"></i>
            <i class="fa fa-angle-down"></i>
        </a>
        أضف عرضك
    </div>
    <div id="offer" class="panel-collapse collapse collapse in">
        <div class="panel-body">
            <div class="row">
                @if (Auth::check())
                    <form action="{{ route('ad.set-comment') }}" method="POST">
                        @csrf
                        <input type="hidden" name="ad_id" value="{{ $ad_id }}">
                        @include('frontend.forms.textarea' , [
                            'name' => 'body',
                            'label' => 'اضف تعليق',
                            'placeholder' => 'اضف تعليق',
                            'type' => 'number',
                            'required' => 1,
                            'col' => 12,
                        ])
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-red">
                                <i class="fa fa-plus" aria-hidden="true"></i>
                                أضف تعليق
                            </button>
                        </div>
                    </form>
                @else
                    <div class="col-md-12  text-center">
                        <p>قم بتسجيل الدخول أولا لإضافة عرضك</p>
                        <a href="#" class="btn btn-red" data-toggle="modal" data-target="#loginModel">
                            <i class="fa fa-sign-in" aria-hidden="true"></i>
                            تسجيل دخول
                        </a>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>