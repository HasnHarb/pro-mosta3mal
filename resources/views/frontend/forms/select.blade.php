@if (count($options) > 4 or $name == 'sub_category_id')
    
    <div class="form-group col-sm-12 {{ isset($col) ? 'col-md-' . $col : '' }}">
        <label class="text-right" for="{{ $name }}"> 
            @if ($required)
            <span style="color: red">*</span>
            @endif
            {{ $label ?? '' }}:
        </label>
        <select 
            class="e-form-control" 
            id="{{ $name }}" 
            name="{{ $name }}" 
            @if ($required) 
                data-validation="required"
                data-validation-error-msg="حقل {{$label}} مطلوب"
            @endif 
            >
            <option value="">-- اختر --</option>
            @foreach($options as $key => $value)
                <option value="{{ $key }}" 
                @if (old("$name") == $key or Request::get("$name") == $key or (isset($selected) and $selected == $key) ) 
                    selected="selected" 
                @endif>
                        {{ $value }}
                </option>
            @endforeach
        </select>
    </div>
        
@else

    <div class="form-group col-sm-12 {{ isset($col) ? 'col-md-' . $col : '' }}">
        <label class="text-right" for="{{ $name }}"> 
                @if ($required)
                <span style="color: red">*</span>
                @endif
                {{ $label ?? '' }}:
            </label>
        <div class="switch-field">
            @foreach($options as $key => $value)
                <input 
                    type="radio" 
                    id="radio-{{$key}}" 
                    name="{{$name}}" 
                    value="{{ $key }}"
                    @if (old("$name") == $key) checked @endif
                    @if ($required) 
                        data-validation="required"
                        data-validation-error-msg="حقل {{$label}} مطلوب"
                    @endif 
                />
                <label for="radio-{{$key}}">{{$value}}</label>
            @endforeach
        </div>
    </div>

@endif