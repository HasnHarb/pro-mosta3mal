<div class="row">
    @include('frontend.forms.select' , [
        'name' => 'main_category_id',
        'label' => 'القسم الرئيسي',
        'required' => true,
        'options' => Helper::getMainCategories(),
        'col' => 6
    ])

    @include('frontend.forms.select' , [
        'name' => 'sub_category_id',
        'label' => 'القسم الفرعي',
        'required' => true,
        'options' => [],
        'col' => 6
    ])
</div>

<div class="row">
    @include('frontend.forms.input' , [
        'name' => 'title',
        'label' => 'العنوان',
        'placeholder' => 'أدخل عنوان الطلب',
        'type' => 'text',
        'required' => true,
        'col' => 12
    ])
</div>

<div class="row">    
    @include('frontend.forms.textarea' , [
        'name' => 'description',
        'label' => 'الوصف',
        'placeholder' => 'ادخل وصف الخدمة او الطلب  مثل "مطلوب كمبيوتر مستعمل"',
        'required' => true,
        'col' => 12
    ])
</div>

<div class="row">    
    @include('frontend.forms.input' , [
        'name' => 'budget_min',
        'label' => 'القيمة الدنيا',
        'placeholder' => '',
        'type' => 'number',
        'required' => true,
        'col' => 6
    ])

    @include('frontend.forms.input' , [
        'name' => 'budget_max',
        'label' => 'القيمة العليا',
        'placeholder' => '',
        'type' => 'number',
        'required' => true,
        'col' => 6
    ])
    @include('frontend.forms.select' , [
        'name' => 'currency_id',
        'label' => 'العملة',
        'required' => true,
        'options' => Helper::getCurrencies(),
        'col' => 6
    ])
    @include('frontend.forms.select' , [
        'name' => 'city_id',
        'label' => 'المدينة',
        'required' => true,
        'options' => Helper::getCities(),
        'col' => 6
    ])
</div>
<div class="row">
    <div class="form-group col-md-6">
        <button type="submit" class="btn btn-red width-100">نشر</button>
        <a href="{{ url('/') }}" class="btn btn-default width-100">الغاء</a>
    </div>
</div>