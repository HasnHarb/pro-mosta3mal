<div class="col-md-6 col-md-offset-3">
    <h3>تغيير كلمة المرور</h3>
    <br>
</div>
<div class="col-md-6 col-md-offset-3">
    @include('frontend.forms.input' , [
        'name' => 'current-password',
        'label' => 'كلمة المرور الحالية',
        'placeholder' => 'كلمة المرور الحالية',
        'type' => 'password',
        'required' => 1,
        'col' => 12,
    ])
</div>
<div class="col-md-6 col-md-offset-3">
    @include('frontend.forms.input' , [
        'name' => 'password',
        'label' => 'كلمة المرور الجديدة',
        'placeholder' => 'كلمة المرور الجديدة',
        'type' => 'password',
        'required' => 1,
        'col' => 12,
    ])
</div>
<div class="col-md-6 col-md-offset-3">
    @include('frontend.forms.input' , [
        'name' => 'password_confirmation',
        'label' => 'تأكيد كلمة المرور',
        'placeholder' => 'تأكيد كلمة المرور',
        'type' => 'password',
        'required' => 1,
        'col' => 12,
        ])
</div>
<div class="col-md-6 col-md-offset-3">
    <div class="form-group" style="padding-right: 15px;">
        <button type="submit" class="btn btn-red">حفظ</button>
    </div>
</div>
<br>