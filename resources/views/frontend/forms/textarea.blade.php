<div class="form-group col-sm-12 {{ isset($col) ? 'col-md-' . $col : '' }}">
    <label class="text-right" for="{{ $name }}"> 
        @if ($required)
            <span style="color: red">*</span>
        @endif
        {{ $label ?? '' }}: 
    </label>
    <textarea 
        class="e-form-control" 
        id="{{ $name }}" 
        placeholder="{{ $placeholder }}" 
        name="{{ $name }}"
        rows="{{ $row ?? '10' }}"
        @if ($required) 
            data-validation="required"
            data-validation-error-msg="حقل {{$label}} مطلوب"
        @endif 
        >{{ old("$name") ?? $value ?? '' }}</textarea>
</div>
