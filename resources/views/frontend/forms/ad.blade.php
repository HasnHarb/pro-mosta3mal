@push('scripts')
    @include('frontend.scripts.ajax_select' , [ 
        'perant' => 'main_category_id', 
        'child' => 'sub_category_id',
        'route' => 'api.sub_categories' 
    ])
    @include('frontend.scripts.get_template' , [ 
        'perant' => 'sub_category_id', 
        'child' => 'template',
        'route' => 'ad.template' 
    ])
    <script src="{{ asset('js/image-uploader.min.js') }}"></script>
@endpush

@push('css')
    <link rel="stylesheet" href="{{ asset('css/image-uploader.min.css') }}">
@endpush

<div class="row">    
    <p class="p-title" style="margin-top: 0px">الاقسام</p>
    @include('frontend.forms.select' , [
        'name' => 'main_category_id',
        'label' => 'القسم الرئيسي',
        'required' => true,
        'options' => Helper::getMainCategories(),
        'col' => 6
    ])

    @include('frontend.forms.select' , [
        'name' => 'sub_category_id',
        'label' => 'القسم الفرعي',
        'required' => true,
        'options' => [],
        'col' => 6
    ])
</div>
<div id="template" class="row"></div>
<div class="row">
    <p class="p-title">معلومات الطلب</p>

    @include('frontend.forms.input' , [
        'name' => 'title',
        'label' => 'العنوان',
        'placeholder' => 'أدخل عنوان الطلب',
        'type' => 'text',
        'required' => true,
        'col' => 6
    ])
    @include('frontend.forms.input' , [
        'name' => 'price',
        'label' => 'السعر',
        'placeholder' => 'السعر',
        'type' => 'number',
        'required' => true,
        'col' => 2
    ])
    @include('frontend.forms.select' , [
        'name' => 'currency_id',
        'label' => 'العملة',
        'required' => true,
        'options' => Helper::getCurrencies(),
        'col' => 4
    ])
</div>
<div class="row">
    @include('frontend.forms.textarea' , [
        'name' => 'description',
        'label' => 'الوصف',
        'placeholder' => 'ادخل وصف الخدمة او الطلب  مثل "مطلوب كمبيوتر مستعمل"',
        'required' => true,
        'col' => 6
    ])
    @include('frontend.forms.image' , [
        'name' => 'images',
        'label' => 'أضف صور',
        'required' => true,
        'col' => 6
    ])
</div>

<div class="row">
    <p class="p-title">معلومات الاتصال</p>
    @include('frontend.forms.select' , [
        'name' => 'city_id',
        'label' => 'المدينة',
        'required' => true,
        'options' => Helper::getCities(),
        'col' => 6
    ])
    @include('frontend.forms.input' , [
        'name' => 'location',
        'label' => 'العنوان',
        'placeholder' => 'العنوان',
        'type' => 'text',
        'required' => 0,
        'col' => 6
    ])
    @include('frontend.forms.input' , [
        'name' => 'user_name',
        'label' => 'الاسم',
        'placeholder' => 'الاسم',
        'type' => 'text',
        'required' => 1,
        'col' => 6,
        'value' => Auth::user()->name
    ])
    @include('frontend.forms.input' , [
        'name' => 'phone',
        'label' => 'الهاتف',
        'placeholder' => 'رقم الهاتف',
        'type' => 'text',
        'required' => 0,
        'col' => 6,
        'value' => Auth::user()->phone
    ])
</div>