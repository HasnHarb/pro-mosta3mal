@if ($check == true)
    <div class="no-rate">
        @for ($i = 1; $i <= 5; $i++)
            <input type="radio" id="star{{ $i }}" />
            <label  style="font-size: {{$size ?? '30'}}px" @if ($i <= intval($value)) class="i-color-red" @endif for="star{{ $i }}" title="text">5 stars</label>
        @endfor
    </div>

@else 
    <div class="rate">
        <input type="radio" id="{{ $name }}5" name="{{ $name }}" value="5" @if($required) data-validation="required" data-validation-error-msg="حقل التقييم مطلوب"
        @endif />
        <label for="{{ $name }}5" title="text">5</label>
        
        <input type="radio" id="{{ $name }}4" name="{{ $name }}" value="4" />
        <label for="{{ $name }}4" title="text">4</label>
        
        <input type="radio" id="{{ $name }}3" name="{{ $name }}" value="3" />
        <label for="{{ $name }}3" title="text">3</label>
        
        <input type="radio" id="{{ $name }}2" name="{{ $name }}" value="2" />
        <label for="{{ $name }}2" title="text">2</label>
        
        <input type="radio" id="{{ $name }}1" name="{{ $name }}" value="1" />
        <label for="{{ $name }}1" title="text">1</label>
    </div>
@endif
