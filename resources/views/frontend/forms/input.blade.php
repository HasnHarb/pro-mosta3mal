@php
    $isSetUnit = (isset($units) and count($units) != 0);
    $validation = [
        'number' => " data-validation=number data-validation-error-msg-number='يجب ان يكون الحقل رقم' "
    ];
@endphp
<div class="form-group col-sm-12 {{ isset($col) ? 'col-md-' . $col : '' }}">
    <label class="text-right" for="{{ $name }}"> 
        @if ($required)
            <span style="color: red">*</span>
        @endif
        {{ $label ?? '' }}: 
    </label>
    <div class="@if ($isSetUnit) input-select @endif">
        <input
            
            type="{{ $type }}"
            class="e-form-control" 
            id="{{ $name }}"
            
            placeholder="{{ $placeholder }}" 
            name="{{ $name }}@if ($isSetUnit)[value]@endif"
            value="{{ old("$name") ??  ($value ?? '') }}"
            @if ($required) 
                data-validation="required"
                data-validation-error-msg-required="حقل {{$label}} مطلوب"
            @endif
            <?php print( $validation[$type] ?? ''); ?>
            
            @isset($redonly)
                disabled
            @endisset
            
            />
        @if ($isSetUnit)
            <select 
                class="e-form-control" 
                id="unit_{{ $name }}" 
                name="{{ $name }}[unit_id]" 
                @if ($required) required @endif
            >
                <option value="">-- الوحدة --</option>
                @foreach($units as $key => $value)
                    <option value="{{ $key }}">
                        <span>
                            {{ $value }}
                        </span>
                    </option>
                @endforeach
            </select>
        @endif
        <small>{!! $note ?? '' !!}</small>
    </div>  
</div>
