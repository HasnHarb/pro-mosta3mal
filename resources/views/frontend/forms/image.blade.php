@push('scripts')
    <script>
        $('.input-images').imageUploader({
            imagesInputName: '{{ $name }}',
            preloadedInputName: '{{ $name }}',
            maxSize: 2 * 1024 * 1024,
            maxFiles: 10,
            label: 'قم بالضغط هنا لتحديد الصور'
        });
    </script>
@endpush

<div class="form-group col-sm-12 {{ isset($col) ? 'col-md-' . $col : '' }}">
    <label class="text-right" for="{{ $name }}"> 
        @if ($required)
            <span style="color: red">*</span>
        @endif
        {{ $label ?? '' }}: 
    </label>
    <div class="input-images"></div>
</div>