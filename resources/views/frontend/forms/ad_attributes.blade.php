
<p class="p-title">الخصائص وميزات</p>
@foreach ($ad_attributes as $attribute)
    @if ($attribute->form_type == 'selected')
        @include('frontend.forms.select' , [
            'name' =>  "selected[$attribute->id]",
            'label' => $attribute->attribute_group->name,
            'required' => $attribute->required,
            'options' => Helper::getAttributes($attribute->id),
            'col' => 6
        ])
    @else
        @include('frontend.forms.input' , [
            'name' =>  "input[$attribute->id]",
            'label' => $attribute->attribute_group->name,
            'placeholder' => $attribute->attribute_group->label,
            'type' => $attribute->form_type,
            'required' => $attribute->required,
            'units' => $attribute->attribute_group->units->pluck('name', 'id') ?? [],
            'col' => 6
        ])
    @endif
@endforeach
