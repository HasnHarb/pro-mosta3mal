@push('scripts')
    <script>
        function readURL(input) {
        if (input.files && input.files[0]) {
                var reader = new FileReader();
                
                reader.onload = function(e) {
                $('#img-avatar').attr('src', e.target.result);
                }
                
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#img_avatar").change(function() {
            readURL(this);
        });
    </script>
@endpush

{{-- @include('frontend.components.validation') --}}

<div class="col-md-6 col-xs-12 col-sm-12">

    @include('frontend.forms.input' , [
        'name' => 'name',
        'label' => 'الاسم',
        'placeholder' => 'الاسم',
        'type' => 'text',
        'required' => 1,
        'col' => 12,
        'value' => Auth::user()->name
    ])

    @include('frontend.forms.input' , [
        'name' => 'email',
        'label' => 'البريد الالكتروني',
        'placeholder' => 'البريد الالكتروني',
        'type' => 'email',
        'required' => 1,
        'col' => 12,
        'value' => Auth::user()->email
    ])

    @include('frontend.forms.textarea' , [
        'name' => 'bio',
        'label' => 'نبذة عني',
        'placeholder' => 'نبذة عني',
        'type' => 'email',
        'required' => 1,
        'col' => 12,
        'value' => Auth::user()->bio
    ])
    
    <div class="col-md-12">
        <button type="submit" class="btn btn-red">حفظ</button>
    </div>
    
</div>

<div class="col-md-6 col-xs-12 col-sm-12">
    <div class="row text-center">
        <div class="col-md-12">
            <img class="avatar img-thumbnail" id="img-avatar" src="{{ Auth::user()->link_image }}" height="150px" width="150px" alt="">
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-12">
            @include('frontend.forms.input' , [
                'name' => 'img_avatar',
                'label' => 'الصورة الشخصية',
                'placeholder' => 'الصورة الشخصية',
                'type' => 'file',
                'required' => 0,
                'col' => 12,
            ])
        </div>
        <div class="col-md-12">
            
            @include('frontend.forms.select' , [
                'name' => 'city_id',
                'label' => 'المدينة',
                'required' => true,
                'options' => Helper::getCities(),
                'col' => 12,
                'selected' =>  Auth::user()->city_id
                ])
        </div>
        <div class="col-md-12">
            @include('frontend.forms.input' , [
                'name' => 'phone',
                'label' => 'رقم الهاتف',
                'placeholder' => 'رقم الهاتف',
                'type' => 'text',
                'required' => 1,
                'col' => 12,
                'value' => Auth::user()->phone
            ])
        </div>
    </div>
</div>