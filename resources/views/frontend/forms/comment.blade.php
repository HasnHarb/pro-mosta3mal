<div class="row">
    <br>
    <form action="{{ route('ad.set-comment') }}" method="POST">
        @csrf
        <input type="hidden" name="ad_id" value="{{ $id }}">
        @include('frontend.forms.textarea' , [
            'name' => 'body',
            'label' => 'اضف تعليق',
            'placeholder' => 'اضف تعليق',
            'type' => 'number',
            'required' => 1,
            'col' => 12,
            'row' => 4
        ])

        <div class="col-md-12">
            <button type="submit" class="btn btn-sm btn-red">
                <i class="fa fa-plus" aria-hidden="true"></i>
                أضف تعليق
            </button>
        </div>
    </form>                                        
</div>