@extends('frontend.layouts.master')

@php
    $page = Helper::getPage('about-as');
@endphp

@push('title')
    {{ $page->title ?? '' }}
@endpush

@section('content')
<div class="container-fluid color-gray">
    <div class="container">
        <div class="font-kufi" style="height: 80vh;">
            <div class="row">
                <br> <br> <br><br> <br> <br><br> <br>
                <div class="row">
                    <div class="col-md-12">
                        <h1 style="font-weight: bold"> {{ $page->title ?? ''  }} </h1>     
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8 col-sm-6 col-xs-12 font-noto" style="line-height: 30px">
                        {!! $page->body ?? '' !!}
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <img src="{{ url(Storage::url($page->image  ?? '')) }}" class="img-fluid" alt="" >
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection