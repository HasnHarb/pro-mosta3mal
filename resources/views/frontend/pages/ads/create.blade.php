@extends('frontend.layouts.master')

@push('title')
اطلب خدمة
@endpush

@section('content')

@include('frontend.components.validation')

<div class="container-fluid color-gray">
    <div class="container">
        <div class="row">
            <br><br>
            <div class="col-sm-12 col-md-12">
                <div class="e-panel panel-black">
                    <div class="e-panel-heading">اطلب خدمة او منتج مجاناً</div>
                    <div class="panel-body">
                        <form class="" action="{{route('ad.store')}}" method="POST"  enctype="multipart/form-data">
                            @csrf
                            @include('frontend.forms.ad')
                            <div class="form-group">
                                <button type="submit" class="btn btn-red">حفظ</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection