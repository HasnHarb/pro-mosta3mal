@extends('frontend.layouts.master')

@push('title')
اطلب خدمة
@endpush

@section('content')

<section class="articles-section color-gray">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <h3 class="font-kufi font-bold text-center"> <i class="fa fa-angle-left i-color-red" aria-hidden="true"></i> {{ Helper::getPage('ads.index')->title }}</h3> 
                <p class="font-naskh font-size-17 text-justify">
                    {!! Helper::getPage('ads.index')->body !!}
                </p>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                <div class="e-panel panel-default">
                    <div class="e-panel-heading"> حدد التصنيف الخاص بك </div>
                    <div id="service-info" class="panel-collapse collapse collapse in">
                        <div class="panel-body">
                            <form id="filterForm" action="{{ route('ad.show-ads') }}" method="GET">
                                @include('frontend.includes.filter')
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                <div class="row">
                    <div class="col-md-12">
                        <div class="head-filter color-fff">
                        <div class="row">
                                <div class="col-md-6">
                                    <div class="v-center">
                                        <i class="fa fa-search i-color-red" aria-hidden="true" style="margin: 0px 4px"></i>
                                        <input type="text" class="e-form-control" name="s" form="filterForm" value="{{ Request::get('s') }}" placeholder="ابحث عن اي شيء ...">
                                    </div>
                                </div>                            
                                <div class="col-md-6">                            
                                    <div class="order-by">
                                        <span>فرز حسب:</span>
                                        <select form="filterForm" name="order_by" id="order_by" class="e-form-control" onchange="this.form.submit()">
                                            <option value="DESC" @if (Request::get('order_by') == 'DESC') selected @endif>الأحث</option>
                                            <option value="ASC" @if (Request::get('order_by') == 'ASC') selected @endif>الأقدم</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="e-panel panel-default">
                            {{-- <div class="e-panel-heading">
                                <a class="btn-collapse pull-left" data-toggle="collapse" href="#all-ads" aria-expanded="true">
                                    <i class="fa fa-angle-right"></i>
                                    <i class="fa fa-angle-down"></i>
                                </a>
                                حديثا 
                            </div> --}}
                            <div id="all-ads" class="panel-collapse collapse collapse in">
                                <div class="panel-body">
                                    <div class="row">
                                        @if ($ads->count() == 0)
                                            <div class="col-md-12 text-center">
                                                <p> لا توجد نتائج 😉 </p>
                                                <a href="{{ route('ad.create') }}" class="btn btn-red"> 
                                                    <i class="fa fa-plus-circle" aria-hidden="true"></i> 
                                                    قم باضافة اعلان مجانا
                                                </a>
                                            </div>
                                        @endif
                                        
                                            <div class="col-md-12">
                                                @include('frontend.components.ad' , compact('ads'))
                                            </div>
                                        
                                    </div>
                                    <div class="row text-center">
                                        {{ $ads->links() }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection