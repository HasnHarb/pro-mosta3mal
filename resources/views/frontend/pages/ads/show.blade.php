@extends('frontend.layouts.master') 

@push('title') اطلب خدمة @endpush 
@section('content')
<div class="container-fluid  color-gray">
    <br>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-sm-12 col-xs-12">
                <div class="info-ad">
                    <div class="e-panel panel-dark">
                        <div class="e-panel-heading">
                            <a class="btn-collapse pull-left" data-toggle="collapse" href="#ad-info" aria-expanded="true">
                                <i class="fa fa-angle-right"></i>
                                <i class="fa fa-angle-down"></i>
                            </a>
                            <span class="badge dark-green"> مفتوح </span>{{ $ad->title }}
                        </div>
                        <div id="ad-info" class="panel-collapse collapse collapse in">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div id="carousel" class="carousel slide" data-ride="carousel">
                                            <div class="carousel-inner">
                                                <div class="item active">
                                                    <img class="img-cover" src="{{ $ad->link_images[0]->download_link }}" alt="">
                                                    {{-- <div class="img-item" style="background: url('{{ $ad->link_images[0]->download_link }}') no-repeat center ;"></div> --}}
                                                </div>
                                                @for ($i=1 ; $i< count( $ad->link_images) ;$i++)
                                                <div class="item">
                                                    <img class="img-cover" src="{{ $ad->link_images[$i]->download_link }}" alt="">
                                                </div>
                                                @endfor
                                            </div>
                                        </div>
                                        <div class="clearfix">
                                            <div id="thumbcarousel" class="carousel slide" data-interval="false">
                                                <div class="carousel-inner">
                                                    <div class="item active">
                                                        @php
                                                            $count = 0;
                                                        @endphp
                                                        @foreach ($ad->link_images as $img)
                                                            <div data-target="#carousel" data-slide-to="{{$count++}}" class="thumb">
                                                                {{-- <img class="img-cover" src="{{ $img->download_link }}" alt=""> --}}
                                                                <img src="{{ $img->download_link }}" >
                                                            </div>
                                                        @endforeach
                                                    </div>
                                                    <!-- /item -->
                                                </div>
                                                <!-- /carousel-inner -->
                                                <a class="left carousel-control" href="#thumbcarousel" role="button" data-slide="prev">
                                                    <span class="glyphicon glyphicon-chevron-left"></span>
                                                </a>
                                                <a class="right carousel-control" href="#thumbcarousel" role="button" data-slide="next">
                                                    <span class="glyphicon glyphicon-chevron-right"></span>
                                                </a>
                                            </div>
                                            <!-- /thumbcarousel -->
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        @if (isset($auction) and $auction)
                                            <h2 class="i-color-red">
                                                {{ $ad->title }} <strong>({{ $ad_auction_price . $ad->currency->icon}} <small>اعلى سعر تم دفعه بالمزاد</small> )</strong>
                                            </h2>
                                        @else
                                            <h2 class="i-color-red">
                                                {{ $ad->title }} <strong>({{ $ad->price . $ad->currency->icon}})</strong>
                                            </h2>
                                        @endif
                                        <div class="s-icon pull-left">
                                            <div class="v-center">
                                                <i class="fa fa-clock-o i-color-green"></i>
                                                <span>{{ $ad->created_at->locale('ar')->diffForHumans() }}</span>
                                            </div>
                                        </div>
                                        <div class="s-icon">
                                            <div class="v-center">
                                                <i class="fa fa-user i-color-gold"></i>
                                                <span>{{ $ad->user->name }}</span>
                                            </div>
                                        </div>

                                        <div class="s-icon">
                                            <div class="v-center">
                                                <i class="fa fa-map-marker i-color-pinck"></i>
                                                <span>{{ $ad->city->name }}</span>
                                            </div>
                                        </div>

                                        <div class="s-icon">
                                            <div class="v-center">
                                                <i class="fa fa-list-alt i-color-blue"></i>
                                                <span> {{ $ad->sub_category->name }}</span>
                                            </div>
                                        </div>

                                        <div class="s-icon">
                                            <div class="v-center">
                                                <i class="fa fa-clock-o i-color-green"></i>
                                                <span> {{ date('yy-m-d',strtotime($ad->created_at)) }}</span>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <button type="button" class="btn btn-none" onclick="ajaxSavedAd({{$ad->id}})">
                                                    <i class="fa-18 fa fa-bookmark @if ($ad->saved)  i-color-red @else  i-color-gray @endif" id="saved_icon_{{$ad->id}}" aria-hidden="true"></i>
                                                    @if ($ad->saved) تم الحفظ @else  حفظ @endif
                                                </button>
                                                <button type="button" class="btn btn-out-red btn-sm" id="open-report-ad" data-toggle="modal" data-target="#reportModel" data-title-ad="{{ $ad->title }}" data-id-ad="{{ $ad->id }}">
                                                    <i class="fa fa-envelope-o" aria-hidden="true"></i>
                                                    أبلغنا عن اساءة
                                                  </button>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h3> <strong> وصف الاعلان:</strong></h3>
                                            </div>                                            
                                            <div class="col-md-12">
                                                <div style="line-height: 30px">
                                                    {{ $ad->description }}
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">                                                
                                            <div class="col-md-12">
                                                <div class="row" style="background-color: #6d6d6d;color: #fff;">
                                                    <div class="col-md-12">
                                                        <h3> <strong> تفاصيل الاعلان:</strong></h3>
                                                    </div>
                                                    @foreach ($ad->selected_ad_values as $item)
                                                        <div class="col-md-6">
                                                            <p> 
                                                                <strong>
                                                                    {{ $item->ad_attribute->attribute_group->name }}:
                                                                </strong>
                                                                {{ $item->attribute->value }}
                                                            </p>
                                                        </div>
                                                    @endforeach
                                                    @foreach ($ad->input_ad_values as $item)
                                                        <div class="col-md-6">
                                                            <p> 
                                                                <strong>
                                                                    {{ $item->ad_attribute->attribute_group->name }}:
                                                                </strong>
                                                                {{ $item->value }}/{{ $item->unit->name??'' }}
                                                            </p>                                                        
                                                        </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="h-comment">
                                                <h4>التعليقات</h4>
                                            </div>
                                            @foreach ($ad->comments as $item)
                                                <div class="col-md-12">
                                                    @include('frontend.components.comment' , ['comment' => $item])
                                                </div>
                                            @endforeach
                                        </div>
                                        @include('frontend.forms.comment', ['id' => $ad->id])
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <div class="e-panel panel-default">
                    <div class="e-panel-heading">
                        <a class="btn-collapse pull-left" data-toggle="collapse" href="#ad-user" aria-expanded="true">
                            <i class="fa fa-angle-right"></i>
                            <i class="fa fa-angle-down"></i>
                        </a>
                        معلومات المُعلن:
                    </div>
                    <div id="ad-user" class="panel-collapse collapse collapse in">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="user-info">
                                        <img class="img-circle avatar img-thumbnail" height="70px"  width="70px" src="{{ url('storage/' . $ad->user->avatar) }}">
                                        <div class="info">
                                            <h4> <a href="{{ route('users.profile' , $ad->user->username ?? 'null') }}"> {{ $ad->user->name }} </a></h4>
                                            <p> <i class="fa fa-shield i-color-green" aria-hidden="true"></i> حساب موثق</p>
                                            {{-- <p> <strong> تم الانضمام: </strong> {{ $ad->user->created_at->locale('ar')->diffForHumans() }}</p> --}}
                                            @include('frontend.forms.rate', ['check' => true , 'value' => $ad->user->rate , 'size' => 20])
                                        </div>
                                    </div>
                                    <p class="text-center phone"> <i class="fa fa-phone" aria-hidden="true"></i> {{ $ad->user->phone ?? '' }}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @if (isset($auction) and $auction )
                    @php
                        $now = new \DateTime() ;
                        $date = new \DateTime($auction->end_date);
                    @endphp
            
                    @if ($now <=  $date) 
                        <div class="e-panel panel-default">
                            <div class="e-panel-heading">
                                <a class="btn-collapse pull-left" data-toggle="collapse" href="#ad-auction" aria-expanded="true">
                                    <i class="fa fa-angle-right"></i>
                                    <i class="fa fa-angle-down"></i>
                                </a>
                            المشاركة في مزاد
                            </div>
                            <div id="ad-auction" class="panel-collapse collapse collapse in">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="timer text-center">
                                                <strong>الوقت المتبقي: <br> </strong>
                                                <br>
                                                <ul>
                                                    <li><span id="days{{ $auction->id }}"></span>يوم</li>
                                                    <li><span id="hours{{ $auction->id }}"></span>ساعة</li>
                                                    <li><span id="minutes{{ $auction->id }}"></span>دقيقة</li>
                                                    <li><span id="seconds{{ $auction->id }}"></span>ثواني</li>
                                                </ul>
                                            </div>
                                            <form action="{{ route('ad.set-auction-offer') }}" method="POST">
                                                @csrf
                                                <input type="hidden" name="auction_id" value="{{ $auction->id }}" >
                                                @include('frontend.forms.input' , [
                                                    'name' => 'price',
                                                    'label' => 'قيمة العرض',
                                                    'placeholder' => 'قيمة العرض',
                                                    'type' => 'number',
                                                    'required' => 1,
                                                    'col' => 12,
                                                    'note' => '> يجب ان تكون القيمة بعملة ' . '<strong> (' . $ad->currency->icon . ' ' . $ad->currency->name . ')</strong>'
                                                ])
                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                        <button type="submit" name="" id="" class="btn btn-red btn-block">اضف عرضك</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                @endif
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3 class="font-kufi">اعلانات مشابهة</h3>
            </div>
            @include('frontend.components.ad-sug')
        </div>
    </div>
</div>
@include('frontend.components.model-report')

@endsection

@push('scripts')
    <script src="{{ asset('js/app-all.js') }}"></script>
    <script>
        $( document ).ready(function() {
            @if (isset($auction) and $auction)
                timer("{{ $auction->id }}" , "{{ $auction->start_date  }}", "{{ $auction->end_date }}")
            @endif
        });
    </script>
@endpush
@push('scripts')
    
<script>
    function ajaxSavedAd (id){
        jQuery.ajax({
            url: "{{ route('ad.addToSaved') }}",
            method: 'post',
            data: {
                _token: "{{csrf_token()}}",
                ad_id: id,
            },
            success: function(result){
                console.log(result);
                if(result.success == true){
                    toastr.success(result.message);
                    if(result.action == 'saved'){
                        
                        $("#saved_icon_" + id).removeClass('i-color-gray');
                        $("#saved_icon_" + id).addClass('i-color-red');
                    }
                    if(result.action == 'unsaved'){
                        $("#saved_icon_" + id).removeClass('i-color-red');
                        $("#saved_icon_" + id).addClass('i-color-gray');
                    }
                }
                else{
                    toastr.error(result.message)
                }

            }
        });
    }
</script>
@endpush