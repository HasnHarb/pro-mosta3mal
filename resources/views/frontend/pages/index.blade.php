@extends('frontend.layouts.master')

@push('title')
    الصفحة الرئيسية
@endpush

@push('scripts')
    @include('frontend.scripts.ajax_select' , [ 
        'perant' => 'main_category_id', 
        'child' => 'sub_category_id',
        'route' => 'api.sub_categories' 
    ])
    @include('frontend.scripts.get_template' , [ 
        'perant' => 'sub_category_id', 
        'child' => 'template',
        'route' => 'ad.template' 
    ])
@endpush

@section('content')

    @include('frontend.includes.filter_header' , [
        'title' => 'مرحبا بك في موقع كراكيب فلسطين' , 
        'desc' => 'يمكنك من خلال موقع كراركيب الاعلان عن اي شيء بشكل مجاني',
        'image' => 'assets/images/cover1.png',
        'link' => route('services.create'),
        'textLink' => 'أضف إعلان مجاناً'
    ])
    <div class="container-fluid color-gray">
        <div class="container">
            <br>
            <div class="row">
                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                    <div class="e-panel panel-default">
                        <div class="e-panel-heading">
                            <a class="btn-collapse pull-left" data-toggle="collapse" href="#service-info" aria-expanded="true">
                                <i class="fa fa-angle-right"></i>
                                <i class="fa fa-angle-down"></i>
                            </a>
                            الاقسام
                        </div>
                        <div id="service-info" class="panel-collapse collapse collapse in">
                            <div class="panel-body">
                                {{-- <h3 class="title">الاقسام</h3> --}}
                                <div class="row">
                                    <div class="col-md-12">
                                        @foreach ($categories as $item)
                                            @include('frontend.components.category' , ['category' => $item])
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="e-panel panel-default">
                        <div class="e-panel-heading">
                            <a class="btn-collapse pull-left" data-toggle="collapse" href="#service-info" aria-expanded="true">
                                <i class="fa fa-angle-right"></i>
                                <i class="fa fa-angle-down"></i>
                            </a>
                            العملات
                        </div>
                        <div id="service-info" class="panel-collapse collapse collapse in">
                            <div class="panel-body">
                                @include('frontend.components.currencies')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="container">
            <br>
            <div class="row">
                <section class="top-categories">
                    <div class="container">
                        <div class="row">  
                            <div class="col-md-12 text-center">
                                <h3 class="title">شائع</h3>
                            </div>  
                            @for ($i = 0; $i < 4; $i++)
                                @include('frontend.includes.top-categories' , ['category' => $categories[$i]])
                            @endfor
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
    <hr>

@endsection