@extends('frontend.layouts.master')

@php
    $page = Helper::getPage('privacy');
@endphp

@push('title')
    {{ $page->title ?? '' }}
@endpush

@section('content')
<div class="container-fluid color-gray">
    <div class="container">
        <div class="font-kufi" style="height: 80vh;">
            <div class="row">
                <br> <br>
                <div class="row">
                    <div class="col-md-12 text-center">
                        <h1 style="font-weight: bold"> {{ $page->title ?? ''  }} </h1>     
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10 col-sm-12 col-xs-12 col-md-offset-1 font-noto " style="line-height: 30px">
                        {!! $page->body ?? '' !!}
                        <br> <br>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection