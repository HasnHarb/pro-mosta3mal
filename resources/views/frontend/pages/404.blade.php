@extends('frontend.layouts.master')

@push('title')
    لم يتم العثور على الصفحة المطلوبة
@endpush

@section('content')
<div class="container">
    <div class="row">
        <div id="container-center">
            <div class="container-center">
                <div class="notfound-404">
                    <h1>4<span style="background-image: url({{ asset('images/emoji.png') }})"></span>4</h1>
                </div>
                <h2>المعذرة! لم يتم العثور على الصفحة</h2>
                <p>عذرًا ، الصفحة التي تبحث عنها غير موجودة او تمت إزالتها أو غير متاح مؤقتًا</p>
                <a href="/">الرجوع الى الصفحة الرئيسية</a>
            </div>
        </div>
    </div>
</div>
@endsection