@extends('frontend.layouts.master')

@push('title')
اطلب خدمة
@endpush

@push('scripts')
    @include('frontend.scripts.ajax_select' , [ 
        'perant' => 'main_category_id', 
        'child' => 'sub_category_id',
        'route' => 'api.sub_categories' 
    ])
@endpush

@section('content')

<div class="container-fluid color-gray">
    <div class="container">
        <div class="row">
            <br><br>
            <div class="col-sm-12 col-md-8">
                <div class="e-panel panel-black">
                    <div class="e-panel-heading">اطلب خدمة او منتج مجاناً</div>
                    <div class="panel-body">
                        @include('frontend.components.validation')
                        <form  action="{{ route('services.store') }}" method="POST">
                            @csrf
                            @include('frontend.forms.service')
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-4">
                <h3 class="font-kufi font-bold">ابدأ ببناء مشروعك</h3> 
                <p class="font-naskh font-size-17 text-justify">
                    من خلال مستقل تستطيع بناء مشروعك بالشكل الذي تريد، أدخل تفاصيل المشروع والميزانية المتوقعة، ليتم مراجعة مشروعك ونشره مجاناً على مستقل.
بعد ذلك، سيتقدّم أفضل المستقلين المسجلين بعروض مختلفة لتختار العرض المناسب لك وتبدأ بتنفيذ مشروعك.
                </p>
                <h3 class="font-kufi font-bold">ابدأ ببناء مشروعك</h3> 
                <p class="font-naskh font-size-17 text-justify">
                    من خلال مستقل تستطيع بناء مشروعك بالشكل الذي تريد، أدخل تفاصيل المشروع والميزانية المتوقعة، ليتم مراجعة مشروعك ونشره مجاناً على مستقل.
بعد ذلك، سيتقدّم أفضل المستقلين المسجلين بعروض مختلفة لتختار العرض المناسب لك وتبدأ بتنفيذ مشروعك.
                </p>
            </div>
        </div>
    </div>
</div>
@endsection