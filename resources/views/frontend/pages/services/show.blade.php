@extends('frontend.layouts.master')

@push('title')
    اطلب خدمة
@endpush

@section('content')
    <section class="articles-section color-gray">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                    <div class="info-service">
                        <div class="e-panel panel-default">
                            <div class="e-panel-heading">
                                <a class="btn-collapse pull-left" data-toggle="collapse" href="#service-info" aria-expanded="true">
                                    <i class="fa fa-angle-right"></i>
                                    <i class="fa fa-angle-down"></i>
                                </a>
                                <h2> <span class="badge dark-green">مفتوح</span>{{ $service->title }}</h2>
                                <div class="s-icon pull-left">
                                    <div class="v-center"> 
                                        <i class="fa fa-clock-o i-color-green"></i>    
                                        <span>{{ $service->created_at->locale('ar')->diffForHumans() }}</span>
                                    </div>
                                </div>
                                <div class="s-icon">
                                    <div class="v-center"> 
                                        <i class="fa fa-user i-color-gold"></i> 
                                        <span>{{ $service->user->name }}</span>
                                    </div>
                                </div>
                    
                                <div class="s-icon">
                                    <div class="v-center"> 
                                        <i class="fa fa-map-marker i-color-pinck"></i> 
                                        <span>{{ $service->city->name }}</span>
                                    </div>
                                </div>
                    
                                <div class="s-icon">
                                    <div class="v-center"> 
                                        <i class="fa fa-list-alt i-color-blue"></i>
                                        <span> {{ $service->sub_category->name }}</span>
                                    </div>
                                </div>
                    
                                <div class="s-icon">
                                    <div class="v-center"> 
                                        <i class="fa fa-clock-o i-color-green"></i>    
                                        <span> {{ date('yy-m-d',strtotime($service->created_at)) }}</span>
                                    </div>
                                </div>
                            </div>
                            <div id="service-info" class="panel-collapse collapse collapse in">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="articles-body color-fff">
                                            <p>{!! nl2br($service->description) !!}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="offer">
                        @if ($errors->any())
                            @foreach ($errors->all() as $error)
                                @php
                                    toastr()->error($error);
                                @endphp
                            @endforeach
                        @endif
                        @if(Session::has('success'))
                            @php
                                toastr()->success(Session::get('success'));
                            @endphp
                        @endif
                        @include('frontend.forms.offer' , ['service_id' => $service->id])
                    </div>
                    
                    @guest
                    @else
                        @if (Auth::id() == $service->user_id)
                            <div class="offers">
                                @include('frontend.pages.services.offers' , ['offers' => $service->offers])
                            </div>
                        @endif
                    @endguest
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="articles-body color-fff">
                        <table class="table table-borderless" style="margin-bottom: 0px;">
                            <tbody>
                                <tr>
                                    <th style="width: 115px;">الميزانية:</th>
                                    <td> {{ $service->budget_min . ($service->currency->icon ?? '') }} - {{ $service->budget_max . ($service->currency->icon?? '') }} </td>
                                </tr>
                                <tr>
                                    <th style="width: 115px;">عدد العروض:</th>
                                    <td>{{ $service->offers->count() }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="articles-body color-fff">
                        <div class="user-info">
                            <img class="img-circle avatar img-thumbnail" height="70px"  width="70px" src="{{ url('storage/' . $service->user->avatar) }}">
                            <div class="info">
                                <h4>{{ $service->user->name }}</h4>
                                <p> <i class="fa fa-shield i-color-green" aria-hidden="true"></i> حساب موثق</p>
                                <p> <strong> تم الانضمام: </strong> {{ $service->user->created_at->locale('ar')->diffForHumans() }}</p>
                            </div>
                        </div>
                        <p class="text-center phone">{{ $service->user->phone ?? '' }}</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection