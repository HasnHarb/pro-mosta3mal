@extends('frontend.layouts.master')

@push('title')
    اطلب خدمة
@endpush

@section('content')
<section class="color-gray">
    <div class="container">
        <div class="row">
            <h3 class="font-kufi font-bold">ابدأ ببناء مشروعك</h3> 
            <p class="font-naskh font-size-17 text-justify">
                من خلال مستقل تستطيع بناء مشروعك بالشكل الذي تريد، أدخل تفاصيل المشروع والميزانية المتوقعة، ليتم مراجعة مشروعك ونشره مجاناً على مستقل.شروعك. 
            </p>
        </div>
        <br>
        <div class="row">
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                <div class="row">
                    <div class="col-md-12">
                        <div class="head-filter color-fff">
                        <div class="row">
                                <div class="col-md-6">                            
                                    <div class="order-by">
                                        <span>فرز حسب:</span>
                                        <select form="filterForm" name="order_by" id="order_by" class="e-form-control" style="width: 40%;"  onchange="this.form.submit()">
                                            <option value="DESC" @if (Request::get('order_by') == 'DESC') selected @endif>الأحث</option>
                                            <option value="ASC" @if (Request::get('order_by') == 'ASC') selected @endif>الأقدم</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <a href="{{ route('services.create') }}" class="btn btn-red pull-left">
                                        <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                        أضف طلبك
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="e-panel panel-default">
                    <div class="panel-body">
                        <div class="row">
                            @if ($services->count() == 0)
                                <div class="col-md-12 text-center">
                                    <p> لا توجد نتائج 😉 </p>
                                    <a href="{{ route('services.create') }}" class="btn btn-red"> 
                                        <i class="fa fa-plus-circle" aria-hidden="true"></i> 
                                        قم باضافة طلبك
                                    </a>
                                </div>
                            @else
                                <div class="col-md-12">
                                    @include('frontend.components.service' , ['services' => $services , 'action' => false])
                                </div>
                            @endif
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-center">
                                {{ $services->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                <div class="e-panel panel-default">
                    <div class="e-panel-heading"> حدد التصنيف الخاص بك </div>
                    <div id="service-info" class="panel-collapse collapse collapse in">
                        <div class="panel-body">
                            <form id="filterForm" action="{{ route('services.index') }}" method="GET">
                                @include('frontend.includes.filter')
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            {{-- <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                <div class="row">
                    <div class="col-md-12">
                        <a href="{{ route('services.create') }}" class="btn btn-red btn-block">
                            <i class="fa fa-plus-circle" aria-hidden="true"></i>
                            أضف طلبك
                        </a>
                    </div>
                </div>
                <br>
                <div class="left-articles"> 
                    @include('frontend.includes.categories-list' , ['categories' => $categories , 'link' => route('services.index')])

                    @include('frontend.includes.social-link')
                </div>
            </div> --}}

        </div>
    </div>
</section>
@endsection