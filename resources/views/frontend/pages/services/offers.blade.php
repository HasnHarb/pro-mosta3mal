<div class="e-panel panel-default">
    <div class="e-panel-heading">
        <a class="btn-collapse pull-left" data-toggle="collapse" href="#offers" aria-expanded="true">
            <i class="fa fa-angle-right"></i>
            <i class="fa fa-angle-down"></i>
        </a>
        عروض تم طرحها
    </div>
    <div id="offers" class="panel-collapse collapse collapse in">
        <div class="panel-body">
            <div class="row">
                @foreach ($offers as $offer)
                    
                <div class="col-md-12">
                    @include('frontend.components.offer' , compact('offer'))
                </div>
                <hr>
                @endforeach
            </div>
        </div>
    </div>
</div>