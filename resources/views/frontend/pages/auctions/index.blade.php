@extends('frontend.layouts.master')

@push('title')
    المزاد
@endpush

@section('content')
<div class="container-fluid color-gray">
    <div class="container-fluid">
        <div class="row text-center font-kufi">
            <br>
            <h1>
                <i class="fa fa-line-chart i-color-red" aria-hidden="true"></i>
                المزاد  
            </h1>
            <br>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                <div class="e-panel panel-default">
                    <div class="e-panel-heading"> حدد التصنيف الخاص بك </div>
                    <div id="service-info" class="panel-collapse collapse collapse in">
                        <div class="panel-body">
                            <form id="filterForm" action="" method="GET">
                                @include('frontend.includes.filter')
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                <div class="row">
                    <div class="col-md-12">
                        <div class="head-filter color-fff">
                            <div class="row">
                                <div class="col-md-6"></div>
                                    <div class="col-md-6">                            
                                    <div class="order-by">
                                        <span>فرز حسب:</span>
                                        <select form="filterForm" name="order_by" id="order_by" class="e-form-control" style="width: 40%;"  onchange="this.form.submit()">
                                            <option value="DESC" @if (Request::get('order_by') == 'DESC') selected @endif>الأحث</option>
                                            <option value="ASC" @if (Request::get('order_by') == 'ASC') selected @endif>الأقدم</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-12">
                        <div class="e-panel panel-default">
                            {{-- <div class="e-panel-heading">
                                <a class="btn-collapse pull-left" data-toggle="collapse" href="#all-ads" aria-expanded="true">
                                    <i class="fa fa-angle-right"></i>
                                    <i class="fa fa-angle-down"></i>
                                </a>
                                حديثا 
                            </div> --}}
                            @if ($auctions->count() == 0)
                                <div class="col-md-12 text-center">
                                    <h4> لا توجد نتائج 😉 </h4>
                                    <a href="{{ route('ad.create') }}" class="btn btn-red"> 
                                        <i class="fa fa-plus-circle" aria-hidden="true"></i> 
                                        قم باضافة اعلان مجانا
                                    </a>
                                </div>
                            @endif
                            
                            <div id="all-ads" class="panel-collapse collapse collapse in">
                                <div class="panel-body">
                                    @include('frontend.components.auction' , compact('auctions' , 'timer'))
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection
