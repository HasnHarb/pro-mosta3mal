@extends('frontend.layouts.master')

@push('title')
    اطلب خدمة
@endpush

@section('content')
<div class="container">
    <div class="row">
        <div id="container-center">
            <div class="container-center">
                <h1> شكراً لك </h1>
                <span style="background-image: url({{ asset('images/mark.png') }})"></span>
                <p> تم تقديم طلبك بنجاح ، وسيتم ارسال بريد الكتروني يفيد بنجاح او فشل العملية </p>
                <a href="/">الرجوع الى الصفحة الرئيسية</a>
            </div>
        </div>
    </div>
</div>
@endsection