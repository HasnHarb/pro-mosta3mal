@extends('frontend.layouts.master')

@push('title')
    تسجيل الدخول
@endpush

@section('content')
<section class="sign-up">

    <div class="text-center">
        <h1>إنشاء حساب جديد</h1>
        <p>هل لديك حساب بالفعل ؟ <a href="#">تسجيل دخول</a></p>
        <div class="width-div-40">
            <h2>إنشاء حساب جديد</h2>
            <div class="form_section">
                <form method="POST" action="{{ route('register') }}">
                    @csrf
                    <div class="form-group">
                        <input id="name" type="text" placeholder="الاسم" class="e-form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                        @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror 
                    </div>
                    <div class="form-group">
                        <input id="email" type="email" placeholder="البريد الإلكتروني" class="e-form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror                </div>
                    <div class="form-group">
                        <input id="password" type="password" placeholder="كلمة المرور" class="e-form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    
                    
                    <div class="form-group">
                        <input id="password-confirm" type="password" class="e-form-control" placeholder="تأكيد كلمة المرور" name="password_confirmation" required autocomplete="new-password">
                    </div>
                    <div class="form-group">
                        <select id="password"  class="e-form-control @error('city_id') is-invalid @enderror" name="city_id" required>
                            <option value="">اختر المدينة</option>
                            @foreach (Helper::getCities() as $key => $value)
                                <option value="{{ $key }}" @if (old('city_id') == $key) selected @endif> {{ $value }} </option>   
                            @endforeach
                        </select>
                        @error('city_id')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <input id="phone" type="text" placeholder="رقم الهاتف" class="e-form-control @error('phone') is-invalid @enderror" name="phone" value="{{ old('phone') }}" required autocomplete="phone">
                        @error('phone')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror 
                    </div>
                    <div class="form-group">
                        <div class="form-check">
                          <label class="form-check-label">
                            <input type="checkbox" class="form-check-input" name="privacy" id="" required>
                            أوافق على 
                            <a href="{{ route('frontend.privacy') }}"> شروط الاستخدام </a>
                          </label>
                        </div>
                    </div>

                    <button type="submit" class="btn btn-block btn-red">تسجيل دخول</button>
                </form>
            </div>

        </div>
        <div class="width-div-20">
            <div><img src="{{ asset('assets/images/img-signup.jpg') }}"></div>
            <div class="or-div">أو</div>
            <div><img src="{{ asset('assets/images/img-signup.jpg') }}"></div>
        </div>
        <div class="width-div-40 form-left">
            <h2>تسجيل دخول</h2>
            <form method="POST" action="{{ route('login') }}">
                @csrf
                <div class="form-group">
                    <input type="text" name="email" id="email" class="e-form-control" placeholder="ادخل البريد الالكتروني" required=""> 
                </div>
                <div class="form-group">
                    <input id="password" type="password" class="e-form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password"> 
                </div>
                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span> 
                    @enderror
                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span> 
                    @enderror
                <button type="submit" class="btn btn-block btn-out-red">تسجيل دخول</button>
            </form>
            <br>
            <a class="font-kufi" href="{{ route('password.request') }}">
                {{ __('نسيت كلمة المرور ؟') }}
            </a>
            <div class="social-media-login">
                <h2>بأمكانك التسجل عبر وسائل التواصل</h2>
                <br>
                <a href="{{url('/redirect')}}">
                    <div class="facebook-div">
                        <i class="fa fa-facebook" aria-hidden="true"></i>
                        <span>سجل عبر الفيس بوك</span>
                    </div>
                </a>

            </div>
        </div>
    </div>

</section>
@endsection