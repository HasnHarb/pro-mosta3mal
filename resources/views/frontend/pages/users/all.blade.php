@extends('frontend.layouts.master')

@push('title')
    المحفوظات
@endpush

@section('content')
<div class="container-fluid color-gray">
    <div class="container">
        <div class="row text-center font-kufi">
            <br>
            <h2>
                <i class="fa fa-users i-color-red" aria-hidden="true"></i>
                 قائمة مستخدمي النظام 
            </h2>
            <br>
        </div>
    </div>
    <div class="row">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="row">
                        <form action="{{ route('users.all') }}" method="GET">
                            @include('frontend.forms.select' , [
                                'name' => 'city_id',
                                'label' => 'المدينة',
                                'required' => false,
                                'options' => Helper::getCities(),
                                'col' => 4,
                            ])
                            <div class="form-group col-sm-12 col-md-8">

                                <label class="text-right" for=""> 
                                    ابحث: 
                                </label>
                                <div class="input-group">
                                    <input type="text" class="e-form-control" name="s" value="{{ Request::get('s') }}" placeholder="اسم المستخدم او البريد الالكتروني ...">
                                    <div class="input-group-btn">
                                        <button class="btn btn-red" type="submit">
                                            <i class="fa fa-search" aria-hidden="true"></i>
                                            بحث
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br/>
    <div class="container">
        <div class="row" id="">
            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12 col-md-offset-1">
                <div class="e-panel panel-default">
                    <div class="panel-body">                        
                        <div class="row">
                            <div class="col-md-12">
                                @include('frontend.components.users' , ['users' => $users])
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-12 text-center">
                                {{ $users->links() }}
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection