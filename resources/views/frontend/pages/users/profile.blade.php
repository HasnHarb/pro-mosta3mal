@extends('frontend.layouts.master')

@push('title')
    {{$user->name}}
@endpush

@section('content')

<div class="container-fluid color-gray">
    <div class="row "> 
        <br>
        <div class="user-cover text-center v-center">
            <div class="user-information">
                <img class="img-circle avatar img-thumbnail"   src="{{ url('storage/' . $user->avatar) }}" width="115px" height="115px">
                <div class="shop-details">
                    <h3>{{ $user->name }}</h3>
                    <div style="margin-bottom: 8px">
                        <button type="button" class="btn btn-sm @if ($user->following) btn-red @else btn-success @endif" id="btn_follow" onclick="ajaxFollowingUser({{$user->id}})">
                            <i class="fa @if ($user->following) fa-check @else fa-plus @endif" aria-hidden="true" id="icon_following"></i>
                            <span id="text_following">
                                @if($user->following) متابَع @else تابعني @endif
                            </span>
                        </button>
                        @if (Auth::check())
                            @if (Auth::user()->id != $user->id)
                            <button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#ratingModel" >
                                <i class="fa fa-comment-o" aria-hidden="true"></i>
                                <span>
                                    اضف تقييم
                                </span>
                            </button>
                            @endif
                        @endif
                    </div>
                    <span> 
                        <i class="fa fa-shield" aria-hidden="true"></i>
                         حساب موثق 
                    </span>
                    <span>.</span>
                    <span> 
                        <i class="fa fa-clock-o" aria-hidden="true"></i>
                        تم الانضمام:  {{ $user->created_at->locale('ar')->diffForHumans() }}
                    </span>
                    <span>.</span>
                    <span> 
                        <i class="fa fa-location-arrow" aria-hidden="true"></i>
                        {{ $user->city->name ?? '' }}
                    </span>
                    <div class="col-md-12">
                        <div class="content-rate v-center">
                            <div style="margin-bottom: 6px;font-size: 16px;padding-left: 10px;">
                                <strong> ({{ $user->rate }}) </strong>
                            </div>
                            @include('frontend.forms.rate' , ['name' => 'rate' , 'check' => true ,'value' => $user->rate])
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="nav-tab color-fff">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <ul class="nav nav-pills">
                            <li class="active"><a data-toggle="pill" href="#menu2">
                                <i class="fa fa-bullhorn" aria-hidden="true"></i>
                                الاعلانات
                            </a></li>
                            <li class=""><a data-toggle="pill" href="#menu3">
                                <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                الطلبات
                            </a></li>
                            <li class=""><a data-toggle="pill" href="#user-info">
                                <i class="fa fa-user" aria-hidden="true"></i>
                                المعلومات الشخصية
                            </a></li>
                            @if (Auth::check() and Auth::user()->username == $user->username)
                                <li class=""><a data-toggle="pill" href="#menu4">
                                    <i class="fa fa-cog" aria-hidden="true"></i>
                                    الاعدادات
                                </a></li>
                                <li class=""><a data-toggle="pill" href="#change-password">
                                    <i class="fa fa-key" aria-hidden="true"></i>    
                                    تغيير كلمة المرور
                                </a></li>
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br/>
    <div class="container">
        <div class="row" id="">
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                <div class="e-panel panel-default">
                    <div class="panel-body">
                        <div class="tab-content">
                            <div id="user-info"  class="tab-pane fade">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h3>نبذة عني</h3>
                                        <p> {!! nl2br($user->bio) !!} </p>
                                    </div>
                                </div>
                            </div>
                            <div id="menu2"  class="tab-pane fade  in active">
                                <div class="row">
                                    @if ($ads->count() == 0)
                                        <div class="col-md-12 text-center">
                                            <p> لا توجد نتائج 😉 </p>
                                            <a href="{{ route('ad.create') }}" class="btn btn-red"> 
                                                <i class="fa fa-plus-circle" aria-hidden="true"></i> 
                                                قم باضافة اعلان مجانا
                                            </a>
                                        </div>
                                    @endif                                
                                    <div class="col-md-12">
                                        @include('frontend.components.ad' , compact('ads'))
                                    </div>
                                </div>
                                <div class="row text-center">
                                    {{ $ads->links() }}
                                </div>
                            </div>

                            <div id="menu3"  class="tab-pane fade">
                                <div class="row">
                                    @if ($services->count() == 0)
                                        <div class="col-md-12 text-center">
                                            <p> لا توجد نتائج 😉 </p>
                                            <a href="{{ route('services.create') }}" class="btn btn-red"> 
                                                <i class="fa fa-plus-circle" aria-hidden="true"></i> 
                                                قم باضافة طلبك مجانا
                                            </a>
                                        </div>
                                    @else
                                        <div class="col-md-12">
                                            @include('frontend.components.service' , ['services' => $services ,  'action' => true])
                                        </div>
                                    @endif
                                    <div class="row">
                                        <div class="col-md-12 text-center">
                                            {{ $services->links() }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @if (Auth::check() and Auth::user()->username == $user->username)
                                <div id="menu4"  class="tab-pane fade">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h4>تعديل المعلومات الشخصية</h4>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <form action="{{ route('users.update') }}" method="POST" enctype="multipart/form-data">
                                            @csrf
                                            @include('frontend.forms.user')
                                        </form>
                                    </div>     
                                </div>
                                <div id="change-password"  class="tab-pane fade">
                                    <div class="row">
                                        <form action="{{ route('users.change-password') }}" method="POST" enctype="multipart/form-data">
                                            @csrf
                                            @include('frontend.forms.password')
                                        </form>
                                    </div>
                                </div>
                            @endif
                                
                        </div>
                    </div>
                </div>
            </div>
        
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                <div class="e-panel panel-default">
                    <div class="e-panel-heading">
                        <a class="btn-collapse pull-left" data-toggle="collapse" href="#user-info-num" aria-expanded="true">
                            <i class="fa fa-angle-right"></i>
                            <i class="fa fa-angle-down"></i>
                        </a>
                        احصائيات
                    </div>
                    <div id="user-info-num" class="panel-collapse collapse collapse in">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <table class="table table-borderless" style="margin-bottom: 0px;">
                                        <tbody>
                                            <tr>
                                                <th style="width: 115px;">عدد الاعلانات:</th>
                                                <td> {{ $countServices }}</td>
                                            </tr>
                                            <tr>
                                                <th style="width: 115px;">عدد الطلبات:</th>
                                                <td>{{ $countAds }}</td>
                                            </tr>
                                            <tr>
                                                <th style="width: 115px;">المتابعين:</th>
                                                <td>{{ Helper::CountFollowers($user->id) }}</td>
                                            </tr>
                                            <tr>
                                                <th style="width: 115px;">المتابعون:</th>
                                                <td>{{ Helper::CountFollowing($user->id) }}</td>
                                            </tr>
                                            <tr>
                                                <th style="width: 115px;">تاريخ الانضمام:</th>
                                                <td>{{ $user->created_at->locale('ar')->toFormattedDateString() }}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('frontend.components.model-rating')
@endsection

@push('scripts')
    
<script>
    function ajaxFollowingUser (id){
        jQuery.ajax({
            url: "{{ route('users.follow-user') }}",
            method: 'post',
            data: {
                _token: "{{csrf_token()}}",
                following_id: id,
            },
            success: function(result){
                console.log(result);
                if(result.success == true){
                    toastr.success(result.message);
                    if(result.action == 'saved'){
                        $("#btn_follow").removeClass('btn-success');
                        $("#btn_follow").addClass('btn-red');
                        
                        $("#icon_following").removeClass('fa-plus');
                        $("#icon_following").addClass('fa-check');
                        $("#text_following").text('متابَع');
                    }
                    if(result.action == 'unsaved'){
                        $("#btn_follow").removeClass('btn-red');
                        $("#btn_follow").addClass('btn-success');

                        $("#icon_following").removeClass('fa-check');
                        $("#icon_following").addClass('fa-plus');
                        $("#text_following").text('تابعني');
                        
                    }
                }
                else{
                    toastr.error(result.message)
                }

            }
        });
    }
</script>
@endpush