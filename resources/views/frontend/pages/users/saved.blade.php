@extends('frontend.layouts.master')

@push('title')
    المحفوظات
@endpush

@section('content')
<div class="container-fluid color-gray">
    <div class="container">
        <div class="row text-center font-kufi">
            <br>
            <h2>
                <i class="fa fa-bookmark i-color-red" aria-hidden="true"></i>
                 قائمة المحفوظات 
            </h2>
            <br>
        </div>
    </div>
    <div class="row">
        <div class="nav-tab color-fff">
            <div class="container">
                <div class="row">
                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12 col-md-offset-1">
                        <ul class="nav nav-pills">
                            <li class="active"><a data-toggle="pill" href="#ads">
                                <i class="fa fa-bullhorn" aria-hidden="true"></i>
                                الاعلانات
                            </a></li>
                            <li class=""><a data-toggle="pill" href="#services">
                                <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                الطلبات
                            </a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br/>
    <div class="container">
        <div class="row" id="">
            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12 col-md-offset-1">
                <div class="e-panel panel-default">
                    <div class="panel-body">
                        
                        <div class="tab-content">
                        
                            <div id="ads"  class="tab-pane fade in active">
                                <div class="row">
                                    @if ($ads->count() == 0)
                                        <div class="col-md-12 text-center">
                                            <p> لا توجد نتائج 😉 </p>
                                            <a href="{{ route('ad.create') }}" class="btn btn-red"> 
                                                <i class="fa fa-plus-circle" aria-hidden="true"></i> 
                                                قم باضافة اعلان مجانا
                                            </a>
                                        </div>
                                    @else
                                        <div class="col-md-12">
                                            <h3>الاعلانات التي تم حفظها</h3>
                                        </div>
                                        <div class="col-md-12">
                                            @include('frontend.components.ad' , compact('ads'))
                                        </div>
                                    @endif
                                    
                                </div>
                                <div class="row text-center">
                                    {{ $ads->links() }}
                                </div>
                            </div>

                            <div id="services"  class="tab-pane fade">
                                <div class="row">
                                    @if ($services->count() == 0)
                                        <div class="col-md-12 text-center">
                                            <p> لا توجد نتائج 😉 </p>
                                            <a href="{{ route('services.create') }}" class="btn btn-red"> 
                                                <i class="fa fa-plus-circle" aria-hidden="true"></i> 
                                                قم باضافة طلبك مجانا
                                            </a>
                                        </div>
                                    @else
                                        <div class="col-md-12">
                                            <h3>الطلبات التي تم حفظها</h3>
                                        </div>
                                        <div class="col-md-12">
                                            @include('frontend.components.service' , ['services' => $services ,  'action' => false])
                                        </div>
                                    @endif
                                    <div class="row">
                                        <div class="col-md-12 text-center">
                                            {{ $services->links() }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection