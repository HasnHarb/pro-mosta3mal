<script type="text/javascript">
    $("select[name='{{ $perant }}']").change(function() {
        var perant = $(this).val();
        var token = $("input[name='_token']").val();
        $.ajax({
            url: "<?php echo route($route) ?>",
            method: 'POST',
            data: {
                '{{$perant}}': perant,
                '_token': token
            },
            success: function(data) {
                $("select[name='{{ $child }}'").html('');
                $("select[name='{{ $child }}'").html(data.options);
            }
        });
    });

    $( document ).ready(function() {
        var perant = $("select[name='{{ $perant }}']").val();
        var token = $("input[name='_token']").val();
        $.ajax({
            url: "<?php echo route($route) ?>",
            method: 'POST',
            data: {
                '{{$perant}}': perant,
                '_token': token
            },
            success: function(data) {
                $("select[name='{{ $child }}'").html('');
                $("select[name='{{ $child }}'").html(data.options);
                $("select[name='{{ $child }}'").val('{{ Request::get("$child") }}');
            }
        });
    });
</script>