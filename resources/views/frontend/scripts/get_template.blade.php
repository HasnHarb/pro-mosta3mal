<script type="text/javascript">
    $("select[name='{{ $perant }}']").change(function() {
        var perant = $(this).val();
        var token = $("input[name='_token']").val();
        $.ajax({
            url: "<?php echo route($route) ?>",
            method: 'POST',
            data: {
                '{{$perant}}': perant,
                '_token': token
            },
            success: function(data) {
                $("div[id='{{ $child }}'").html('');
                $("div[id='{{ $child }}'").html(data.data);
            }
        });
    });
</script>