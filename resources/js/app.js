require('./bootstrap');
window.Vue = require('vue');

Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('attribute', require('./components/attribute.vue').default);
Vue.component('attributes', require('./components/attributes.vue').default);
Vue.component('unit', require('./components/unit.vue').default);
Vue.component('units', require('./components/units.vue').default);

const app = new Vue({
    el: '#vue_app',
});