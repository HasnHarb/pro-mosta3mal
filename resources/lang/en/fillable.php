<?php

return  [
    'title' => 'العنوان',
    'description' => 'الوصف',
    'budget_min' => 'القيمة الدنيا',
    'budget_max' => 'القيمة العظمى',
    'city_id' => 'المدينة',
    'sub_category_id'=> 'القسم الفرعي',
    'service_id' => 'الخدمة',
    'amount' => 'المبلغ',
    'body' => 'التعليق',
    'user_id' => 'المستخدم',
    'currency_id' => 'العملة',
    'email' => 'البريد الالكتروني',
    'name' => 'الاسم',
    'bio' => 'نبذة عني',
    'avatar' => 'الصورة الشخصية',
    'type' => 'النوع',
    'ad_id' => 'الاعلان',
];