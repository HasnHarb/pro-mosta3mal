-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 05, 2020 at 06:54 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mosta3maldb`
--

-- --------------------------------------------------------

--
-- Table structure for table `ad_attributes`
--

CREATE TABLE `ad_attributes` (
  `id` int(10) UNSIGNED NOT NULL,
  `attribute_group_id` int(11) NOT NULL,
  `sub_category_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ad_attributes`
--

INSERT INTO `ad_attributes` (`id`, `attribute_group_id`, `sub_category_id`, `created_at`, `updated_at`) VALUES
(2, 13, 15, NULL, NULL),
(3, 14, 15, NULL, NULL),
(4, 1, 6, NULL, NULL),
(5, 5, 6, NULL, NULL),
(6, 6, 6, NULL, NULL),
(7, 7, 6, NULL, NULL),
(8, 1, 16, NULL, NULL),
(9, 3, 16, NULL, NULL),
(10, 9, 16, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `attributes`
--

CREATE TABLE `attributes` (
  `id` int(10) UNSIGNED NOT NULL,
  `value` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `ad_attribute_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `attribute_groups`
--

CREATE TABLE `attribute_groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `attribute_groups`
--

INSERT INTO `attribute_groups` (`id`, `name`, `slug`, `label`, `created_at`, `updated_at`) VALUES
(1, 'النوع', 'النوع', 'نوع المنتج او الماركة', '2020-02-04 16:02:19', '2020-02-04 16:02:19'),
(2, 'مساحة الارض', 'مساحة الارض', 'مساحة قطعة الارض', '2020-02-04 16:03:00', '2020-02-04 16:03:00'),
(3, 'الفئة', 'الفئة', 'الفئة', '2020-02-04 17:39:23', '2020-02-04 17:39:23'),
(4, 'سنة الصنع', 'سنة-الصنع', 'سنة الصنع', '2020-02-04 17:39:58', '2020-02-04 17:39:58'),
(5, 'الكيلومترات', 'الكيلومترات', 'الكيلومترات', '2020-02-04 17:40:17', '2020-02-04 17:40:17'),
(6, 'الحالة', 'الحالة', 'الحالة', '2020-02-04 17:40:56', '2020-02-04 17:40:56'),
(7, 'نوع الوقود', 'نوع الوقود', 'نوع الوقود', '2020-02-04 17:41:23', '2020-02-04 17:41:23'),
(8, 'الماركة', 'الماركة', 'الماركة', '2020-02-04 17:42:08', '2020-02-04 17:42:08'),
(9, 'سعة التخزين', 'سعة التخزين', 'سعة التخزين', '2020-02-04 17:42:29', '2020-02-04 17:42:29'),
(13, 'عدد الحمامات', 'عدد-الحمامات', 'عدد الحمامات', '2020-02-04 17:54:07', '2020-02-04 17:54:07'),
(14, 'مساحة البناء', 'مساحة-البناء', 'مساحة البناء', '2020-02-04 17:54:00', '2020-02-04 17:55:09'),
(15, 'طريقة الدفع', 'طريقة-الدفع', 'طريقة الدفع', '2020-02-04 17:55:55', '2020-02-04 17:55:55'),
(16, 'مزايا إضافية', 'مزايا-إضافية', 'مزايا إضافية', '2020-02-04 17:56:40', '2020-02-04 17:56:40');

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'الخليل', '2020-01-31 16:54:48', '2020-01-31 16:54:48'),
(2, 'بيت لحم', '2020-01-31 16:54:58', '2020-01-31 16:54:58'),
(3, 'القدس', '2020-01-31 16:55:06', '2020-01-31 16:55:06'),
(4, 'رام الله', '2020-01-31 16:55:13', '2020-01-31 16:55:13'),
(5, 'قلقيلة', '2020-01-31 16:55:24', '2020-01-31 16:55:24'),
(6, 'جنين', '2020-01-31 16:55:33', '2020-01-31 16:55:33');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `budget` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `comment`, `budget`, `user_id`, `service_id`, `created_at`, `updated_at`) VALUES
(1, 'نص نص نص', 23, 1, 1, '2020-02-02 16:38:02', '2020-02-02 16:38:02');

-- --------------------------------------------------------

--
-- Table structure for table `data_rows`
--

CREATE TABLE `data_rows` (
  `id` int(10) UNSIGNED NOT NULL,
  `data_type_id` int(10) UNSIGNED NOT NULL,
  `field` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `browse` tinyint(1) NOT NULL DEFAULT '1',
  `read` tinyint(1) NOT NULL DEFAULT '1',
  `edit` tinyint(1) NOT NULL DEFAULT '1',
  `add` tinyint(1) NOT NULL DEFAULT '1',
  `delete` tinyint(1) NOT NULL DEFAULT '1',
  `details` text COLLATE utf8mb4_unicode_ci,
  `order` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_rows`
--

INSERT INTO `data_rows` (`id`, `data_type_id`, `field`, `type`, `display_name`, `required`, `browse`, `read`, `edit`, `add`, `delete`, `details`, `order`) VALUES
(1, 1, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(2, 1, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(3, 1, 'email', 'text', 'Email', 1, 1, 1, 1, 1, 1, NULL, 3),
(4, 1, 'password', 'password', 'Password', 1, 0, 0, 1, 1, 0, NULL, 4),
(5, 1, 'remember_token', 'text', 'Remember Token', 0, 0, 0, 0, 0, 0, NULL, 5),
(6, 1, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, NULL, 6),
(7, 1, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 7),
(8, 1, 'avatar', 'image', 'Avatar', 0, 1, 1, 1, 1, 1, NULL, 8),
(9, 1, 'user_belongsto_role_relationship', 'relationship', 'Role', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":0}', 10),
(10, 1, 'user_belongstomany_role_relationship', 'relationship', 'Roles', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}', 11),
(11, 1, 'settings', 'hidden', 'Settings', 0, 0, 0, 0, 0, 0, NULL, 12),
(12, 2, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(13, 2, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(14, 2, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(15, 2, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(16, 3, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(17, 3, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(18, 3, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(19, 3, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(20, 3, 'display_name', 'text', 'Display Name', 1, 1, 1, 1, 1, 1, NULL, 5),
(21, 1, 'role_id', 'text', 'Role', 1, 1, 1, 1, 1, 1, NULL, 9),
(22, 4, 'id', 'hidden', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(23, 4, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '{}', 2),
(24, 4, 'description', 'text_area', 'Description', 0, 1, 1, 1, 1, 1, '{}', 3),
(25, 4, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 5),
(26, 4, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(27, 5, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(28, 5, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '{}', 4),
(29, 5, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 5),
(30, 5, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(31, 5, 'main_category_id', 'hidden', 'Main Category Id', 1, 1, 1, 1, 1, 1, '{}', 3),
(32, 5, 'sub_category_belongsto_main_category_relationship', 'relationship', 'main_categories', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\MainCategory\",\"table\":\"main_categories\",\"type\":\"belongsTo\",\"column\":\"main_category_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"data_rows\",\"pivot\":\"0\",\"taggable\":\"0\"}', 2),
(34, 4, 'main_category_hasmany_sub_category_relationship', 'relationship', 'sub_categories', 0, 1, 1, 0, 0, 1, '{\"model\":\"App\\\\SubCategory\",\"table\":\"sub_categories\",\"type\":\"hasMany\",\"column\":\"main_category_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"data_rows\",\"pivot\":\"0\",\"taggable\":\"0\"}', 7),
(35, 6, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(36, 6, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '{}', 2),
(37, 6, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 3),
(38, 6, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 4),
(39, 7, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(40, 7, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, '{}', 2),
(41, 7, 'slug', 'text', 'Slug', 0, 0, 1, 1, 1, 1, '{}', 3),
(42, 7, 'description', 'rich_text_box', 'Description', 1, 0, 1, 1, 1, 1, '{}', 4),
(43, 7, 'budget_min', 'number', 'Budget Min', 1, 1, 1, 1, 1, 1, '{}', 5),
(44, 7, 'budget_max', 'number', 'Budget Max', 1, 1, 1, 1, 1, 1, '{}', 6),
(45, 7, 'status', 'text', 'Status', 0, 1, 1, 1, 1, 1, '{}', 7),
(46, 7, 'location', 'text', 'Location', 0, 0, 1, 1, 1, 1, '{}', 8),
(47, 7, 'city_id', 'hidden', 'City Id', 1, 1, 1, 1, 1, 1, '{}', 10),
(48, 7, 'sub_category_id', 'hidden', 'Sub Category Id', 1, 1, 1, 1, 1, 1, '{}', 12),
(49, 7, 'user_id', 'hidden', 'User Id', 0, 1, 1, 1, 1, 1, '{}', 14),
(50, 7, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 15),
(51, 7, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 16),
(52, 7, 'service_belongsto_city_relationship', 'relationship', 'cities', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\City\",\"table\":\"cities\",\"type\":\"belongsTo\",\"column\":\"city_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"cities\",\"pivot\":\"0\",\"taggable\":\"0\"}', 9),
(53, 7, 'service_belongsto_sub_category_relationship', 'relationship', 'sub_categories', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\SubCategory\",\"table\":\"sub_categories\",\"type\":\"belongsTo\",\"column\":\"sub_category_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"cities\",\"pivot\":\"0\",\"taggable\":\"0\"}', 11),
(54, 7, 'service_belongsto_user_relationship', 'relationship', 'users', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\User\",\"table\":\"users\",\"type\":\"belongsTo\",\"column\":\"user_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"cities\",\"pivot\":\"0\",\"taggable\":\"0\"}', 13),
(55, 8, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(56, 8, 'comment', 'text', 'Comment', 1, 1, 1, 1, 1, 1, '{}', 6),
(57, 8, 'budget', 'number', 'Budget', 1, 1, 1, 1, 1, 1, '{}', 7),
(58, 8, 'user_id', 'hidden', 'User Id', 1, 1, 1, 1, 1, 1, '{}', 5),
(59, 8, 'service_id', 'hidden', 'Service Id', 1, 1, 1, 1, 1, 1, '{}', 3),
(60, 8, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 8),
(61, 8, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 9),
(62, 8, 'comment_belongsto_user_relationship', 'relationship', 'users', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\User\",\"table\":\"users\",\"type\":\"belongsTo\",\"column\":\"user_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"cities\",\"pivot\":\"0\",\"taggable\":\"0\"}', 4),
(63, 8, 'comment_belongsto_service_relationship', 'relationship', 'services', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Service\",\"table\":\"services\",\"type\":\"belongsTo\",\"column\":\"service_id\",\"key\":\"id\",\"label\":\"title\",\"pivot_table\":\"cities\",\"pivot\":\"0\",\"taggable\":\"0\"}', 2),
(64, 9, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(65, 9, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '{}', 2),
(66, 9, 'slug', 'text', 'Slug', 1, 1, 1, 0, 0, 1, '{}', 3),
(67, 9, 'label', 'text', 'Label', 0, 1, 1, 1, 1, 1, '{}', 4),
(68, 9, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 5),
(69, 9, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(75, 5, 'sub_category_belongstomany_attribute_group_relationship', 'relationship', 'attribute_groups', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\AttributeGroup\",\"table\":\"attribute_groups\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"ad_attributes\",\"pivot\":\"1\",\"taggable\":\"on\"}', 7),
(76, 13, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(77, 13, 'attribute_group_id', 'hidden', 'Attribute Group Id', 1, 1, 1, 1, 1, 1, '{}', 5),
(78, 13, 'sub_category_id', 'hidden', 'Sub Category Id', 1, 1, 1, 1, 1, 1, '{}', 3),
(79, 13, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 6),
(80, 13, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 7),
(81, 13, 'ad_attribute_belongsto_attribute_group_relationship', 'relationship', 'attribute_groups', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\AttributeGroup\",\"table\":\"attribute_groups\",\"type\":\"belongsTo\",\"column\":\"attribute_group_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"ad_attributes\",\"pivot\":\"0\",\"taggable\":\"0\"}', 4),
(82, 13, 'ad_attribute_belongsto_sub_category_relationship', 'relationship', 'sub_categories', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\SubCategory\",\"table\":\"sub_categories\",\"type\":\"belongsTo\",\"column\":\"sub_category_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"ad_attributes\",\"pivot\":\"0\",\"taggable\":\"0\"}', 2),
(83, 14, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(84, 14, 'value', 'text', 'Value', 1, 1, 1, 1, 1, 1, '{}', 4),
(85, 14, 'icon', 'image', 'Icon', 0, 1, 1, 1, 1, 1, '{}', 5),
(86, 14, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 6),
(87, 14, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 7),
(88, 14, 'ad_attribute_id', 'text', 'Ad Attribute Id', 1, 1, 1, 1, 1, 1, '{}', 3),
(89, 14, 'attribute_belongsto_ad_attribute_relationship', 'relationship', 'ad_attributes', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\AdAttribute\",\"table\":\"ad_attributes\",\"type\":\"belongsTo\",\"column\":\"ad_attribute_id\",\"key\":\"id\",\"label\":\"ad_att_name\",\"pivot_table\":\"ad_attributes\",\"pivot\":\"0\",\"taggable\":\"0\"}', 2);

-- --------------------------------------------------------

--
-- Table structure for table `data_types`
--

CREATE TABLE `data_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT '0',
  `server_side` tinyint(4) NOT NULL DEFAULT '0',
  `details` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_types`
--

INSERT INTO `data_types` (`id`, `name`, `slug`, `display_name_singular`, `display_name_plural`, `icon`, `model_name`, `policy_name`, `controller`, `description`, `generate_permissions`, `server_side`, `details`, `created_at`, `updated_at`) VALUES
(1, 'users', 'users', 'User', 'Users', 'voyager-person', 'TCG\\Voyager\\Models\\User', 'TCG\\Voyager\\Policies\\UserPolicy', 'TCG\\Voyager\\Http\\Controllers\\VoyagerUserController', '', 1, 0, NULL, '2020-01-31 13:04:15', '2020-01-31 13:04:15'),
(2, 'menus', 'menus', 'Menu', 'Menus', 'voyager-list', 'TCG\\Voyager\\Models\\Menu', NULL, '', '', 1, 0, NULL, '2020-01-31 13:04:15', '2020-01-31 13:04:15'),
(3, 'roles', 'roles', 'Role', 'Roles', 'voyager-lock', 'TCG\\Voyager\\Models\\Role', NULL, '', '', 1, 0, NULL, '2020-01-31 13:04:15', '2020-01-31 13:04:15'),
(4, 'main_categories', 'main-categories', 'Main Category', 'Main Categories', NULL, 'App\\MainCategory', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-01-31 15:51:50', '2020-01-31 16:22:39'),
(5, 'sub_categories', 'sub-categories', 'Sub Category', 'Sub Categories', NULL, 'App\\SubCategory', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-01-31 15:55:19', '2020-02-04 16:00:18'),
(6, 'cities', 'cities', 'City', 'Cities', NULL, 'App\\City', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-01-31 16:54:20', '2020-01-31 16:54:20'),
(7, 'services', 'services', 'Service', 'Services', NULL, 'App\\Service', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-01-31 17:01:30', '2020-01-31 17:24:03'),
(8, 'comments', 'comments', 'Comment', 'Comments', NULL, 'App\\Comment', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-02-02 16:28:43', '2020-02-02 16:37:33'),
(9, 'attribute_groups', 'attribute-groups', 'Attribute Group', 'Attribute Groups', NULL, 'App\\AttributeGroup', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-02-04 15:57:44', '2020-02-04 17:53:13'),
(13, 'ad_attributes', 'ad-attributes', 'Ad Attribute', 'Ad Attributes', NULL, 'App\\AdAttribute', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-02-04 17:25:43', '2020-02-04 17:35:02'),
(14, 'attributes', 'attributes', 'Attribute', 'Attributes', NULL, 'App\\Attribute', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-02-05 11:09:24', '2020-02-05 11:18:58');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `main_categories`
--

CREATE TABLE `main_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `main_categories`
--

INSERT INTO `main_categories` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'سيارات ومركبات', 'سيارات ومركبات', '2020-01-31 16:00:43', '2020-01-31 16:00:43'),
(2, 'عقارات للبيع', 'عقارات للبيع', '2020-01-31 16:07:54', '2020-01-31 16:07:54'),
(6, 'اثاث وديكور', 'اثاث وديكور', '2020-01-31 16:42:37', '2020-01-31 16:42:37'),
(7, 'اجهزة الكترونية', 'اجهزة الكترونية', '2020-01-31 16:43:16', '2020-01-31 16:43:16'),
(8, 'عقارات للايجار', 'عقارات للايجار  عقارات للايجار', '2020-02-04 18:46:55', '2020-02-04 18:46:55'),
(9, 'كتب - رياضة - أخرى', 'كتب - رياضة - أخرى', '2020-02-04 18:47:28', '2020-02-04 18:47:28'),
(10, 'ازياء - موضة نسائية', 'ازياء - موضة نسائية', '2020-02-04 18:48:08', '2020-02-04 18:48:08'),
(11, 'ازياء - موضة رجالي', 'ازياء - موضة رجالي', '2020-02-04 18:48:28', '2020-02-04 18:48:28'),
(12, 'العاب فيديو وملحقاتها', 'العاب فيديو وملحقاتها', '2020-02-04 18:49:02', '2020-02-04 18:49:02');

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', '2020-01-31 13:04:16', '2020-01-31 13:04:16');

-- --------------------------------------------------------

--
-- Table structure for table `menu_items`
--

CREATE TABLE `menu_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menu_items`
--

INSERT INTO `menu_items` (`id`, `menu_id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `order`, `created_at`, `updated_at`, `route`, `parameters`) VALUES
(1, 1, 'Dashboard', '', '_self', 'voyager-boat', NULL, NULL, 1, '2020-01-31 13:04:16', '2020-01-31 13:04:16', 'voyager.dashboard', NULL),
(2, 1, 'Media', '', '_self', 'voyager-images', NULL, NULL, 5, '2020-01-31 13:04:16', '2020-01-31 13:04:16', 'voyager.media.index', NULL),
(3, 1, 'Users', '', '_self', 'voyager-person', NULL, NULL, 3, '2020-01-31 13:04:16', '2020-01-31 13:04:16', 'voyager.users.index', NULL),
(4, 1, 'Roles', '', '_self', 'voyager-lock', NULL, NULL, 2, '2020-01-31 13:04:17', '2020-01-31 13:04:17', 'voyager.roles.index', NULL),
(5, 1, 'Tools', '', '_self', 'voyager-tools', NULL, NULL, 9, '2020-01-31 13:04:17', '2020-01-31 13:04:17', NULL, NULL),
(6, 1, 'Menu Builder', '', '_self', 'voyager-list', NULL, 5, 10, '2020-01-31 13:04:17', '2020-01-31 13:04:17', 'voyager.menus.index', NULL),
(7, 1, 'Database', '', '_self', 'voyager-data', NULL, 5, 11, '2020-01-31 13:04:17', '2020-01-31 13:04:17', 'voyager.database.index', NULL),
(8, 1, 'Compass', '', '_self', 'voyager-compass', NULL, 5, 12, '2020-01-31 13:04:17', '2020-01-31 13:04:17', 'voyager.compass.index', NULL),
(9, 1, 'BREAD', '', '_self', 'voyager-bread', NULL, 5, 13, '2020-01-31 13:04:17', '2020-01-31 13:04:17', 'voyager.bread.index', NULL),
(10, 1, 'Settings', '', '_self', 'voyager-settings', NULL, NULL, 14, '2020-01-31 13:04:17', '2020-01-31 13:04:17', 'voyager.settings.index', NULL),
(11, 1, 'Hooks', '', '_self', 'voyager-hook', NULL, 5, 13, '2020-01-31 13:04:21', '2020-01-31 13:04:21', 'voyager.hooks', NULL),
(12, 1, 'Main Categories', '', '_self', NULL, NULL, NULL, 15, '2020-01-31 15:51:51', '2020-01-31 15:51:51', 'voyager.main-categories.index', NULL),
(13, 1, 'Sub Categories', '', '_self', NULL, NULL, NULL, 16, '2020-01-31 15:55:19', '2020-01-31 15:55:19', 'voyager.sub-categories.index', NULL),
(14, 1, 'Cities', '', '_self', NULL, NULL, NULL, 17, '2020-01-31 16:54:21', '2020-01-31 16:54:21', 'voyager.cities.index', NULL),
(15, 1, 'Services', '', '_self', NULL, NULL, NULL, 18, '2020-01-31 17:01:30', '2020-01-31 17:01:30', 'voyager.services.index', NULL),
(16, 1, 'Comments', '', '_self', NULL, NULL, NULL, 19, '2020-02-02 16:28:44', '2020-02-02 16:28:44', 'voyager.comments.index', NULL),
(17, 1, 'Attribute Groups', '', '_self', NULL, NULL, NULL, 20, '2020-02-04 15:57:46', '2020-02-04 15:57:46', 'voyager.attribute-groups.index', NULL),
(18, 1, 'Add Attributes', '', '_self', NULL, NULL, NULL, 21, '2020-02-04 15:58:04', '2020-02-04 15:58:04', 'voyager.add-attributes.index', NULL),
(19, 1, 'Ad Attributes', '', '_self', NULL, NULL, NULL, 22, '2020-02-04 17:25:43', '2020-02-04 17:25:43', 'voyager.ad-attributes.index', NULL),
(20, 1, 'Attributes', '', '_self', NULL, NULL, NULL, 23, '2020-02-05 11:09:24', '2020-02-05 11:09:24', 'voyager.attributes.index', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_01_01_000000_add_voyager_user_fields', 1),
(4, '2016_01_01_000000_create_data_types_table', 1),
(5, '2016_05_19_173453_create_menu_table', 1),
(6, '2016_10_21_190000_create_roles_table', 1),
(7, '2016_10_21_190000_create_settings_table', 1),
(8, '2016_11_30_135954_create_permission_table', 1),
(9, '2016_11_30_141208_create_permission_role_table', 1),
(10, '2016_12_26_201236_data_types__add__server_side', 1),
(11, '2017_01_13_000000_add_route_to_menu_items_table', 1),
(12, '2017_01_14_005015_create_translations_table', 1),
(13, '2017_01_15_000000_make_table_name_nullable_in_permissions_table', 1),
(14, '2017_03_06_000000_add_controller_to_data_types_table', 1),
(15, '2017_04_21_000000_add_order_to_data_rows_table', 1),
(16, '2017_07_05_210000_add_policyname_to_data_types_table', 1),
(17, '2017_08_05_000000_add_group_to_settings_table', 1),
(18, '2017_11_26_013050_add_user_role_relationship', 1),
(19, '2017_11_26_015000_create_user_roles_table', 1),
(20, '2018_03_11_000000_add_user_settings', 1),
(21, '2018_03_14_000000_add_details_to_data_types_table', 1),
(22, '2018_03_16_000000_make_settings_value_nullable', 1),
(23, '2019_08_19_000000_create_failed_jobs_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`) VALUES
(1, 'browse_admin', NULL, '2020-01-31 13:04:17', '2020-01-31 13:04:17'),
(2, 'browse_bread', NULL, '2020-01-31 13:04:17', '2020-01-31 13:04:17'),
(3, 'browse_database', NULL, '2020-01-31 13:04:17', '2020-01-31 13:04:17'),
(4, 'browse_media', NULL, '2020-01-31 13:04:17', '2020-01-31 13:04:17'),
(5, 'browse_compass', NULL, '2020-01-31 13:04:17', '2020-01-31 13:04:17'),
(6, 'browse_menus', 'menus', '2020-01-31 13:04:18', '2020-01-31 13:04:18'),
(7, 'read_menus', 'menus', '2020-01-31 13:04:18', '2020-01-31 13:04:18'),
(8, 'edit_menus', 'menus', '2020-01-31 13:04:18', '2020-01-31 13:04:18'),
(9, 'add_menus', 'menus', '2020-01-31 13:04:18', '2020-01-31 13:04:18'),
(10, 'delete_menus', 'menus', '2020-01-31 13:04:18', '2020-01-31 13:04:18'),
(11, 'browse_roles', 'roles', '2020-01-31 13:04:18', '2020-01-31 13:04:18'),
(12, 'read_roles', 'roles', '2020-01-31 13:04:18', '2020-01-31 13:04:18'),
(13, 'edit_roles', 'roles', '2020-01-31 13:04:18', '2020-01-31 13:04:18'),
(14, 'add_roles', 'roles', '2020-01-31 13:04:18', '2020-01-31 13:04:18'),
(15, 'delete_roles', 'roles', '2020-01-31 13:04:18', '2020-01-31 13:04:18'),
(16, 'browse_users', 'users', '2020-01-31 13:04:18', '2020-01-31 13:04:18'),
(17, 'read_users', 'users', '2020-01-31 13:04:18', '2020-01-31 13:04:18'),
(18, 'edit_users', 'users', '2020-01-31 13:04:18', '2020-01-31 13:04:18'),
(19, 'add_users', 'users', '2020-01-31 13:04:18', '2020-01-31 13:04:18'),
(20, 'delete_users', 'users', '2020-01-31 13:04:18', '2020-01-31 13:04:18'),
(21, 'browse_settings', 'settings', '2020-01-31 13:04:18', '2020-01-31 13:04:18'),
(22, 'read_settings', 'settings', '2020-01-31 13:04:19', '2020-01-31 13:04:19'),
(23, 'edit_settings', 'settings', '2020-01-31 13:04:19', '2020-01-31 13:04:19'),
(24, 'add_settings', 'settings', '2020-01-31 13:04:19', '2020-01-31 13:04:19'),
(25, 'delete_settings', 'settings', '2020-01-31 13:04:19', '2020-01-31 13:04:19'),
(26, 'browse_hooks', NULL, '2020-01-31 13:04:21', '2020-01-31 13:04:21'),
(27, 'browse_main_categories', 'main_categories', '2020-01-31 15:51:50', '2020-01-31 15:51:50'),
(28, 'read_main_categories', 'main_categories', '2020-01-31 15:51:50', '2020-01-31 15:51:50'),
(29, 'edit_main_categories', 'main_categories', '2020-01-31 15:51:50', '2020-01-31 15:51:50'),
(30, 'add_main_categories', 'main_categories', '2020-01-31 15:51:50', '2020-01-31 15:51:50'),
(31, 'delete_main_categories', 'main_categories', '2020-01-31 15:51:51', '2020-01-31 15:51:51'),
(32, 'browse_sub_categories', 'sub_categories', '2020-01-31 15:55:19', '2020-01-31 15:55:19'),
(33, 'read_sub_categories', 'sub_categories', '2020-01-31 15:55:19', '2020-01-31 15:55:19'),
(34, 'edit_sub_categories', 'sub_categories', '2020-01-31 15:55:19', '2020-01-31 15:55:19'),
(35, 'add_sub_categories', 'sub_categories', '2020-01-31 15:55:19', '2020-01-31 15:55:19'),
(36, 'delete_sub_categories', 'sub_categories', '2020-01-31 15:55:19', '2020-01-31 15:55:19'),
(37, 'browse_cities', 'cities', '2020-01-31 16:54:21', '2020-01-31 16:54:21'),
(38, 'read_cities', 'cities', '2020-01-31 16:54:21', '2020-01-31 16:54:21'),
(39, 'edit_cities', 'cities', '2020-01-31 16:54:21', '2020-01-31 16:54:21'),
(40, 'add_cities', 'cities', '2020-01-31 16:54:21', '2020-01-31 16:54:21'),
(41, 'delete_cities', 'cities', '2020-01-31 16:54:21', '2020-01-31 16:54:21'),
(42, 'browse_services', 'services', '2020-01-31 17:01:30', '2020-01-31 17:01:30'),
(43, 'read_services', 'services', '2020-01-31 17:01:30', '2020-01-31 17:01:30'),
(44, 'edit_services', 'services', '2020-01-31 17:01:30', '2020-01-31 17:01:30'),
(45, 'add_services', 'services', '2020-01-31 17:01:30', '2020-01-31 17:01:30'),
(46, 'delete_services', 'services', '2020-01-31 17:01:30', '2020-01-31 17:01:30'),
(47, 'browse_comments', 'comments', '2020-02-02 16:28:44', '2020-02-02 16:28:44'),
(48, 'read_comments', 'comments', '2020-02-02 16:28:44', '2020-02-02 16:28:44'),
(49, 'edit_comments', 'comments', '2020-02-02 16:28:44', '2020-02-02 16:28:44'),
(50, 'add_comments', 'comments', '2020-02-02 16:28:44', '2020-02-02 16:28:44'),
(51, 'delete_comments', 'comments', '2020-02-02 16:28:44', '2020-02-02 16:28:44'),
(52, 'browse_attribute_groups', 'attribute_groups', '2020-02-04 15:57:45', '2020-02-04 15:57:45'),
(53, 'read_attribute_groups', 'attribute_groups', '2020-02-04 15:57:45', '2020-02-04 15:57:45'),
(54, 'edit_attribute_groups', 'attribute_groups', '2020-02-04 15:57:45', '2020-02-04 15:57:45'),
(55, 'add_attribute_groups', 'attribute_groups', '2020-02-04 15:57:45', '2020-02-04 15:57:45'),
(56, 'delete_attribute_groups', 'attribute_groups', '2020-02-04 15:57:45', '2020-02-04 15:57:45'),
(57, 'browse_add_attributes', 'add_attributes', '2020-02-04 15:58:03', '2020-02-04 15:58:03'),
(58, 'read_add_attributes', 'add_attributes', '2020-02-04 15:58:03', '2020-02-04 15:58:03'),
(59, 'edit_add_attributes', 'add_attributes', '2020-02-04 15:58:03', '2020-02-04 15:58:03'),
(60, 'add_add_attributes', 'add_attributes', '2020-02-04 15:58:03', '2020-02-04 15:58:03'),
(61, 'delete_add_attributes', 'add_attributes', '2020-02-04 15:58:03', '2020-02-04 15:58:03'),
(62, 'browse_ad_attributes', 'ad_attributes', '2020-02-04 17:25:43', '2020-02-04 17:25:43'),
(63, 'read_ad_attributes', 'ad_attributes', '2020-02-04 17:25:43', '2020-02-04 17:25:43'),
(64, 'edit_ad_attributes', 'ad_attributes', '2020-02-04 17:25:43', '2020-02-04 17:25:43'),
(65, 'add_ad_attributes', 'ad_attributes', '2020-02-04 17:25:43', '2020-02-04 17:25:43'),
(66, 'delete_ad_attributes', 'ad_attributes', '2020-02-04 17:25:43', '2020-02-04 17:25:43'),
(67, 'browse_attributes', 'attributes', '2020-02-05 11:09:24', '2020-02-05 11:09:24'),
(68, 'read_attributes', 'attributes', '2020-02-05 11:09:24', '2020-02-05 11:09:24'),
(69, 'edit_attributes', 'attributes', '2020-02-05 11:09:24', '2020-02-05 11:09:24'),
(70, 'add_attributes', 'attributes', '2020-02-05 11:09:24', '2020-02-05 11:09:24'),
(71, 'delete_attributes', 'attributes', '2020-02-05 11:09:24', '2020-02-05 11:09:24');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(27, 1),
(28, 1),
(29, 1),
(30, 1),
(31, 1),
(32, 1),
(33, 1),
(34, 1),
(35, 1),
(36, 1),
(37, 1),
(38, 1),
(39, 1),
(40, 1),
(41, 1),
(42, 1),
(43, 1),
(44, 1),
(45, 1),
(46, 1),
(47, 1),
(48, 1),
(49, 1),
(50, 1),
(51, 1),
(52, 1),
(53, 1),
(54, 1),
(55, 1),
(56, 1),
(57, 1),
(58, 1),
(59, 1),
(60, 1),
(61, 1),
(62, 1),
(63, 1),
(64, 1),
(65, 1),
(66, 1),
(67, 1),
(68, 1),
(69, 1),
(70, 1),
(71, 1);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Administrator', '2020-01-31 13:04:17', '2020-01-31 13:04:17'),
(2, 'user', 'Normal User', '2020-01-31 13:04:17', '2020-01-31 13:04:17');

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `budget_min` int(10) UNSIGNED NOT NULL,
  `budget_max` int(10) UNSIGNED NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city_id` int(11) NOT NULL,
  `sub_category_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `title`, `slug`, `description`, `budget_min`, `budget_max`, `status`, `location`, `city_id`, `sub_category_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'مطلوب سيارة للبيع', NULL, '<p dir=\"rtl\" style=\"margin: 0px 0px 10px; color: #424242; font-family: tahoma, sans-serif;\">لوريم ايبسوم هو نموذج افتراضي يوضع في التصاميم لتعرض على العميل ليتصور طريقه وضع النصوص بالتصاميم سواء كانت تصاميم مطبوعه ... بروشور او فلاير على سبيل المثال ... او نماذج مواقع انترنت ...</p>\r\n<p>&nbsp;</p>', 23, 55, 'نص نص', 'هناك عناك هناك', 1, 11, 1, '2020-01-31 17:07:03', '2020-01-31 17:07:03'),
(2, 'ما هو لوريم ايبسوم', 'ما هو لوريم ايبسوم', '<p><span style=\"color: #424242; font-family: tahoma, sans-serif;\">\"خسائر اللازمة ومطالبة حدة بل. الآخر الحلفاء أن غزو, إجلاء وتنامت عدد مع. لقهر معركة لبلجيكا، بـ انه, ربع الأثنان المقيتة في, اقتصّت المحور حدة و. هذه ما طرفاً عالمية استسلام, الصين وتنامت حين ٣٠, ونتج والحزب المذابح كل جوي. أسر كارثة المشتّتون بل, وبعض وبداية الصفحة غزو قد, أي بحث تعداد الجنوب.</span></p>', 12, 34, 'بحث تعداد الجنوب.', 'بحث تعداد', 1, 1, 1, '2020-01-31 17:09:19', '2020-01-31 17:09:19');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `details` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `group` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `key`, `display_name`, `value`, `details`, `type`, `order`, `group`) VALUES
(1, 'site.title', 'Site Title', 'Site Title', '', 'text', 1, 'Site'),
(2, 'site.description', 'Site Description', 'Site Description', '', 'text', 2, 'Site'),
(3, 'site.logo', 'Site Logo', '', '', 'image', 3, 'Site'),
(4, 'site.google_analytics_tracking_id', 'Google Analytics Tracking ID', '', '', 'text', 4, 'Site'),
(5, 'admin.bg_image', 'Admin Background Image', '', '', 'image', 5, 'Admin'),
(6, 'admin.title', 'Admin Title', 'Voyager', '', 'text', 1, 'Admin'),
(7, 'admin.description', 'Admin Description', 'Welcome to Voyager. The Missing Admin for Laravel', '', 'text', 2, 'Admin'),
(8, 'admin.loader', 'Admin Loader', '', '', 'image', 3, 'Admin'),
(9, 'admin.icon_image', 'Admin Icon Image', '', '', 'image', 4, 'Admin'),
(10, 'admin.google_analytics_client_id', 'Google Analytics Client ID (used for admin dashboard)', '', '', 'text', 1, 'Admin');

-- --------------------------------------------------------

--
-- Table structure for table `sub_categories`
--

CREATE TABLE `sub_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `main_category_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sub_categories`
--

INSERT INTO `sub_categories` (`id`, `name`, `created_at`, `updated_at`, `main_category_id`) VALUES
(1, 'سيارات للبيع', '2020-01-31 16:02:56', '2020-01-31 16:02:56', 1),
(2, 'شقق للييع', '2020-01-31 16:08:21', '2020-01-31 16:08:21', 2),
(3, 'منازل للايجار', '2020-01-31 16:21:52', '2020-01-31 16:21:52', 2),
(4, 'سيارات للبيع', '2020-01-31 16:45:00', '2020-01-31 16:48:56', 1),
(5, 'كمبيوتر', '2020-01-31 16:47:00', '2020-01-31 16:48:16', 7),
(6, 'سيارات للإيجار', '2020-01-31 16:49:00', '2020-02-04 17:58:33', 1),
(7, 'دراجات نارية', '2020-01-31 16:49:32', '2020-01-31 16:49:32', 1),
(8, 'فلل - قصور للبيع', '2020-01-31 16:49:58', '2020-01-31 16:49:58', 2),
(9, 'اراضي - مزارع للبيع', '2020-01-31 16:50:13', '2020-01-31 16:50:13', 2),
(10, 'مسجلات', '2020-01-31 16:50:57', '2020-01-31 16:50:57', 7),
(11, 'ستيريو - مسجلات - راديو', '2020-01-31 16:51:34', '2020-01-31 16:51:34', 7),
(12, 'مودم - راوتر', '2020-01-31 16:51:59', '2020-01-31 16:51:59', 7),
(13, 'ادوات المطبخ - ضيافة', '2020-01-31 16:52:33', '2020-01-31 16:52:33', 6),
(14, 'أدوات منزلية', '2020-01-31 16:52:49', '2020-01-31 16:52:49', 6),
(15, 'شقق للايجار', '2020-02-04 16:01:00', '2020-02-04 16:05:42', 6),
(16, 'موبايل - هواتف', '2020-02-04 17:59:37', '2020-02-04 17:59:37', 7),
(17, 'شقق للايجار', '2020-02-04 18:54:53', '2020-02-04 18:54:53', 8),
(18, 'عمارات للايجار', '2020-02-04 18:55:58', '2020-02-04 18:55:58', 8),
(19, 'اراضي - مزارع للايجار', '2020-02-04 18:56:23', '2020-02-04 18:56:23', 8),
(20, 'غرف سكن - شقق فندقية', '2020-02-04 18:56:54', '2020-02-04 18:56:54', 8),
(21, 'عقارات أجنبية للإيجار', '2020-02-04 18:57:51', '2020-02-04 18:57:51', 8),
(22, 'فلل - قصور للايجار', '2020-02-04 18:58:34', '2020-02-04 18:58:34', 8),
(23, 'خدمات توصيل', '2020-02-04 18:59:01', '2020-02-04 18:59:01', 1),
(24, 'لوحة مميزة', '2020-02-04 18:59:20', '2020-02-04 18:59:20', 1),
(25, 'شاحنات - اليات ثقيلة', '2020-02-04 18:59:47', '2020-02-04 18:59:47', 1);

-- --------------------------------------------------------

--
-- Table structure for table `translations`
--

CREATE TABLE `translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `avatar`, `email_verified_at`, `password`, `remember_token`, `settings`, `created_at`, `updated_at`) VALUES
(1, 1, 'Hasan', 'admin@admin.com', 'users/default.png', NULL, '$2y$10$FKhgv4kud65PaM7irijRxepA6/bVqAZ9WPuU4.02p/PExiJieCUQm', NULL, NULL, '2020-01-31 13:04:37', '2020-01-31 13:04:38');

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE `user_roles` (
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ad_attributes`
--
ALTER TABLE `ad_attributes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `attributes`
--
ALTER TABLE `attributes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `attribute_groups`
--
ALTER TABLE `attribute_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD PRIMARY KEY (`id`),
  ADD KEY `data_rows_data_type_id_foreign` (`data_type_id`);

--
-- Indexes for table `data_types`
--
ALTER TABLE `data_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `data_types_name_unique` (`name`),
  ADD UNIQUE KEY `data_types_slug_unique` (`slug`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `main_categories`
--
ALTER TABLE `main_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menus_name_unique` (`name`);

--
-- Indexes for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_items_menu_id_foreign` (`menu_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_key_index` (`key`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_key_unique` (`key`);

--
-- Indexes for table `sub_categories`
--
ALTER TABLE `sub_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `translations`
--
ALTER TABLE `translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `user_roles_user_id_index` (`user_id`),
  ADD KEY `user_roles_role_id_index` (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ad_attributes`
--
ALTER TABLE `ad_attributes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `attributes`
--
ALTER TABLE `attributes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `attribute_groups`
--
ALTER TABLE `attribute_groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `data_rows`
--
ALTER TABLE `data_rows`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=90;

--
-- AUTO_INCREMENT for table `data_types`
--
ALTER TABLE `data_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `main_categories`
--
ALTER TABLE `main_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `menu_items`
--
ALTER TABLE `menu_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `sub_categories`
--
ALTER TABLE `sub_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `translations`
--
ALTER TABLE `translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Constraints for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
