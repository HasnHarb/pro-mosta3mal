-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 17, 2020 at 09:43 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mosta3maldb`
--

-- --------------------------------------------------------

--
-- Table structure for table `ads`
--

CREATE TABLE `ads` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sub_category_id` int(11) NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `images` text COLLATE utf8mb4_unicode_ci,
  `location` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ad_attributes`
--

CREATE TABLE `ad_attributes` (
  `id` int(10) UNSIGNED NOT NULL,
  `attribute_group_id` int(11) NOT NULL,
  `sub_category_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `form_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'selected',
  `required` bit(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ad_attributes`
--

INSERT INTO `ad_attributes` (`id`, `attribute_group_id`, `sub_category_id`, `created_at`, `updated_at`, `form_type`, `required`) VALUES
(2, 13, 15, NULL, '2020-02-11 05:37:16', 'selected', b'0'),
(3, 14, 15, NULL, NULL, '', NULL),
(4, 1, 6, NULL, NULL, '', NULL),
(5, 5, 6, NULL, NULL, '', NULL),
(6, 6, 6, NULL, NULL, '', NULL),
(7, 7, 6, NULL, NULL, '', NULL),
(8, 1, 16, '2020-02-11 07:34:00', '2020-02-11 05:34:28', 'selected', b'1'),
(10, 9, 16, '2020-02-11 07:34:00', '2020-02-11 05:35:00', 'selected', b'1'),
(12, 4, 27, '2020-02-11 07:35:00', '2020-02-11 08:21:19', 'number', b'1'),
(13, 5, 27, '2020-02-11 07:35:00', '2020-02-11 05:35:45', 'number', b'0'),
(14, 8, 27, NULL, '2020-02-11 05:36:07', 'selected', b'0'),
(15, 17, 2, '2020-02-09 08:34:00', '2020-02-11 05:36:31', 'selected', b'0'),
(16, 1, 1, '2020-02-11 05:13:51', '2020-02-11 05:13:51', 'select', b'1'),
(17, 1, 29, NULL, NULL, 'selected', NULL),
(18, 2, 9, '2020-02-11 05:21:17', '2020-02-11 05:21:17', 'text', b'1'),
(19, 1, 30, NULL, '2020-02-11 14:19:21', 'selected', b'1'),
(20, 4, 30, NULL, '2020-02-11 14:19:34', 'selected', b'0'),
(21, 8, 30, NULL, '2020-02-11 14:25:35', 'date', b'1'),
(22, 1, 31, NULL, NULL, 'selected', NULL),
(23, 3, 31, NULL, NULL, 'selected', NULL),
(24, 6, 27, '2020-02-11 10:18:00', '2020-02-11 08:18:21', 'selected', b'0'),
(25, 6, 16, NULL, '2020-02-16 07:46:53', 'selected', b'0'),
(27, 16, 16, '2020-02-17 18:26:47', '2020-02-17 18:26:47', 'text', b'0');

-- --------------------------------------------------------

--
-- Table structure for table `attributes`
--

CREATE TABLE `attributes` (
  `id` int(10) UNSIGNED NOT NULL,
  `value` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `ad_attribute_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `attributes`
--

INSERT INTO `attributes` (`id`, `value`, `icon`, `created_at`, `updated_at`, `ad_attribute_id`) VALUES
(27, 'سامسونج', NULL, '2020-02-06 04:20:17', '2020-02-06 04:20:17', 8),
(28, 'ايفون', NULL, '2020-02-06 04:20:17', '2020-02-06 04:20:17', 8),
(29, 'بلاكبيري', NULL, '2020-02-06 04:20:17', '2020-02-06 04:20:17', 8),
(32, 'اوبو', NULL, '2020-02-06 04:20:18', '2020-02-06 04:20:18', 8),
(33, 'لينوفو', NULL, '2020-02-06 04:20:18', '2020-02-06 04:20:18', 8),
(94, 'حمامين', NULL, '2020-02-07 15:30:28', '2020-02-07 15:30:28', 2),
(95, 'حمامين', NULL, '2020-02-07 15:36:54', '2020-02-07 15:36:54', 2),
(96, 'ثلاث حمامات', NULL, '2020-02-07 15:36:54', '2020-02-07 15:36:54', 2),
(97, 'اربعة حمامات', NULL, '2020-02-07 15:36:54', '2020-02-07 15:36:54', 2),
(98, 'اكثر من اربعة', NULL, '2020-02-07 15:36:54', '2020-02-07 15:36:54', 2),
(99, 'طابق 1', NULL, '2020-02-09 08:39:21', '2020-02-09 08:39:21', 15),
(100, 'طابق 2', NULL, '2020-02-09 08:39:21', '2020-02-09 08:39:21', 15),
(101, 'طابق 3', NULL, '2020-02-09 08:39:21', '2020-02-09 08:39:21', 15),
(102, 'بنزين 98', NULL, '2020-02-11 07:49:20', '2020-02-11 07:49:20', 7),
(103, 'بنزين 96', NULL, '2020-02-11 07:49:20', '2020-02-11 07:49:20', 7),
(104, 'سولار', NULL, '2020-02-11 07:49:20', '2020-02-11 07:49:20', 7),
(105, 'اغو', NULL, '2020-02-11 07:49:21', '2020-02-11 07:49:21', 7),
(106, 'مستعمل', NULL, '2020-02-11 08:19:55', '2020-02-11 08:19:55', 24),
(107, 'جديد', NULL, '2020-02-11 08:19:56', '2020-02-11 08:19:56', 24),
(108, 'هونداي', NULL, '2020-02-11 08:20:38', '2020-02-11 08:20:38', 14),
(109, 'كيا', NULL, '2020-02-11 08:20:38', '2020-02-11 08:20:38', 14),
(110, 'سكودا', NULL, '2020-02-11 08:20:39', '2020-02-11 08:20:39', 14),
(111, 'بي ام دبليو', NULL, '2020-02-11 08:20:39', '2020-02-11 08:20:39', 14),
(112, 'ماكيتا', NULL, '2020-02-11 14:15:03', '2020-02-11 14:15:03', 19),
(113, 'بوش', NULL, '2020-02-11 14:15:03', '2020-02-11 14:15:03', 19),
(114, 'مفرقاة', NULL, '2020-02-11 14:15:03', '2020-02-11 14:15:03', 19),
(115, 'سامسنج', NULL, '2020-02-11 14:15:03', '2020-02-11 14:15:03', 19),
(116, '2019', NULL, '2020-02-11 14:16:14', '2020-02-11 14:16:14', 20),
(117, '2018', NULL, '2020-02-11 14:16:15', '2020-02-11 14:16:15', 20),
(118, '2017', NULL, '2020-02-11 14:16:15', '2020-02-11 14:16:15', 20),
(119, '2016', NULL, '2020-02-11 14:16:15', '2020-02-11 14:16:15', 20),
(120, '2015', NULL, '2020-02-11 14:16:15', '2020-02-11 14:16:15', 20),
(121, '2014', NULL, '2020-02-11 14:16:15', '2020-02-11 14:16:15', 20),
(122, '2013', NULL, '2020-02-11 14:16:15', '2020-02-11 14:16:15', 20),
(123, 'ايسوس', NULL, '2020-02-16 07:40:18', '2020-02-16 07:40:18', 8),
(124, 'ال جي', NULL, '2020-02-16 07:40:18', '2020-02-16 07:40:18', 8),
(125, 'مستعمل', NULL, '2020-02-16 07:40:55', '2020-02-16 07:40:55', 25),
(126, 'جديد', NULL, '2020-02-16 07:40:55', '2020-02-16 07:40:55', 25),
(127, '8GB', NULL, '2020-02-16 07:43:05', '2020-02-16 07:43:05', 10),
(128, '16GB', NULL, '2020-02-16 07:43:05', '2020-02-16 07:43:05', 10),
(129, '32GB', NULL, '2020-02-16 07:43:05', '2020-02-16 07:43:05', 10),
(130, '64GB', NULL, '2020-02-16 07:43:05', '2020-02-16 07:43:05', 10);

-- --------------------------------------------------------

--
-- Table structure for table `attribute_groups`
--

CREATE TABLE `attribute_groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `attribute_groups`
--

INSERT INTO `attribute_groups` (`id`, `name`, `slug`, `label`, `created_at`, `updated_at`) VALUES
(1, 'النوع', 'النوع', 'نوع المنتج او الماركة', '2020-02-04 16:02:19', '2020-02-04 16:02:19'),
(2, 'مساحة الارض', 'مساحة الارض', 'مساحة قطعة الارض', '2020-02-04 16:03:00', '2020-02-04 16:03:00'),
(3, 'الفئة', 'الفئة', 'الفئة', '2020-02-04 17:39:23', '2020-02-04 17:39:23'),
(4, 'سنة الصنع', 'سنة-الصنع', 'سنة الصنع', '2020-02-04 17:39:58', '2020-02-04 17:39:58'),
(5, 'الكيلومترات', 'الكيلومترات', 'الكيلومترات', '2020-02-04 17:40:17', '2020-02-04 17:40:17'),
(6, 'الحالة', 'الحالة', 'الحالة', '2020-02-04 17:40:56', '2020-02-04 17:40:56'),
(7, 'نوع الوقود', 'نوع الوقود', 'نوع الوقود', '2020-02-04 17:41:23', '2020-02-04 17:41:23'),
(8, 'الماركة', 'الماركة', 'الماركة', '2020-02-04 17:42:08', '2020-02-04 17:42:08'),
(9, 'سعة التخزين', 'سعة التخزين', 'سعة التخزين', '2020-02-04 17:42:29', '2020-02-04 17:42:29'),
(13, 'عدد الحمامات', 'عدد-الحمامات', 'عدد الحمامات', '2020-02-04 17:54:07', '2020-02-04 17:54:07'),
(14, 'مساحة البناء', 'مساحة-البناء', 'مساحة البناء', '2020-02-04 17:54:00', '2020-02-04 17:55:09'),
(15, 'طريقة الدفع', 'طريقة-الدفع', 'طريقة الدفع', '2020-02-04 17:55:55', '2020-02-04 17:55:55'),
(16, 'مزايا إضافية', 'مزايا-إضافية', 'مزايا إضافية', '2020-02-04 17:56:40', '2020-02-04 17:56:40'),
(17, 'رقم الطوابق', 'رقم-الطوابق', 'عدد الطوابق', '2020-02-09 08:33:52', '2020-02-09 08:33:52');

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'الخليل', '2020-01-31 16:54:48', '2020-01-31 16:54:48'),
(2, 'بيت لحم', '2020-01-31 16:54:58', '2020-01-31 16:54:58'),
(3, 'القدس', '2020-01-31 16:55:06', '2020-01-31 16:55:06'),
(4, 'رام الله', '2020-01-31 16:55:13', '2020-01-31 16:55:13'),
(5, 'قلقيلة', '2020-01-31 16:55:24', '2020-01-31 16:55:24'),
(6, 'جنين', '2020-01-31 16:55:33', '2020-01-31 16:55:33'),
(7, 'نابلس', '2020-02-06 06:25:48', '2020-02-06 06:25:48');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `budget` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `comment`, `budget`, `user_id`, `service_id`, `created_at`, `updated_at`) VALUES
(1, 'نص نص نص', 23, 1, 1, '2020-02-02 16:38:02', '2020-02-02 16:38:02');

-- --------------------------------------------------------

--
-- Table structure for table `data_rows`
--

CREATE TABLE `data_rows` (
  `id` int(10) UNSIGNED NOT NULL,
  `data_type_id` int(10) UNSIGNED NOT NULL,
  `field` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `browse` tinyint(1) NOT NULL DEFAULT '1',
  `read` tinyint(1) NOT NULL DEFAULT '1',
  `edit` tinyint(1) NOT NULL DEFAULT '1',
  `add` tinyint(1) NOT NULL DEFAULT '1',
  `delete` tinyint(1) NOT NULL DEFAULT '1',
  `details` text COLLATE utf8mb4_unicode_ci,
  `order` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_rows`
--

INSERT INTO `data_rows` (`id`, `data_type_id`, `field`, `type`, `display_name`, `required`, `browse`, `read`, `edit`, `add`, `delete`, `details`, `order`) VALUES
(1, 1, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(2, 1, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(3, 1, 'email', 'text', 'Email', 1, 1, 1, 1, 1, 1, NULL, 3),
(4, 1, 'password', 'password', 'Password', 1, 0, 0, 1, 1, 0, NULL, 4),
(5, 1, 'remember_token', 'text', 'Remember Token', 0, 0, 0, 0, 0, 0, NULL, 5),
(6, 1, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, NULL, 6),
(7, 1, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 7),
(8, 1, 'avatar', 'image', 'Avatar', 0, 1, 1, 1, 1, 1, NULL, 8),
(9, 1, 'user_belongsto_role_relationship', 'relationship', 'Role', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":0}', 10),
(10, 1, 'user_belongstomany_role_relationship', 'relationship', 'Roles', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}', 11),
(11, 1, 'settings', 'hidden', 'Settings', 0, 0, 0, 0, 0, 0, NULL, 12),
(12, 2, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(13, 2, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(14, 2, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(15, 2, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(16, 3, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(17, 3, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(18, 3, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(19, 3, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(20, 3, 'display_name', 'text', 'Display Name', 1, 1, 1, 1, 1, 1, NULL, 5),
(21, 1, 'role_id', 'text', 'Role', 1, 1, 1, 1, 1, 1, NULL, 9),
(22, 4, 'id', 'hidden', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(23, 4, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '{}', 2),
(24, 4, 'description', 'text_area', 'Description', 0, 1, 1, 1, 1, 1, '{}', 3),
(25, 4, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 5),
(26, 4, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(27, 5, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(28, 5, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '{}', 4),
(29, 5, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 5),
(30, 5, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(31, 5, 'main_category_id', 'hidden', 'Main Category Id', 1, 1, 1, 1, 1, 1, '{}', 3),
(32, 5, 'sub_category_belongsto_main_category_relationship', 'relationship', 'main_categories', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\MainCategory\",\"table\":\"main_categories\",\"type\":\"belongsTo\",\"column\":\"main_category_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"data_rows\",\"pivot\":\"0\",\"taggable\":\"0\"}', 2),
(34, 4, 'main_category_hasmany_sub_category_relationship', 'relationship', 'sub_categories', 0, 1, 1, 0, 0, 1, '{\"model\":\"App\\\\SubCategory\",\"table\":\"sub_categories\",\"type\":\"hasMany\",\"column\":\"main_category_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"data_rows\",\"pivot\":\"0\",\"taggable\":\"0\"}', 7),
(35, 6, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(36, 6, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '{}', 2),
(37, 6, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 3),
(38, 6, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 4),
(39, 7, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(40, 7, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, '{}', 2),
(41, 7, 'slug', 'text', 'Slug', 0, 0, 0, 0, 0, 0, '{}', 3),
(42, 7, 'description', 'text_area', 'Description', 1, 0, 1, 1, 1, 1, '{}', 4),
(43, 7, 'budget_min', 'number', 'Budget Min', 1, 1, 1, 1, 1, 1, '{}', 5),
(44, 7, 'budget_max', 'number', 'Budget Max', 1, 1, 1, 1, 1, 1, '{}', 6),
(45, 7, 'status', 'text', 'Status', 0, 1, 1, 1, 1, 1, '{}', 7),
(47, 7, 'city_id', 'hidden', 'City Id', 1, 1, 1, 1, 1, 1, '{}', 10),
(48, 7, 'sub_category_id', 'hidden', 'Sub Category Id', 1, 1, 1, 1, 1, 1, '{}', 12),
(49, 7, 'user_id', 'hidden', 'User Id', 0, 1, 1, 1, 1, 1, '{}', 14),
(50, 7, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 15),
(51, 7, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 16),
(52, 7, 'service_belongsto_city_relationship', 'relationship', 'cities', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\City\",\"table\":\"cities\",\"type\":\"belongsTo\",\"column\":\"city_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"cities\",\"pivot\":\"0\",\"taggable\":\"0\"}', 9),
(53, 7, 'service_belongsto_sub_category_relationship', 'relationship', 'sub_categories', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\SubCategory\",\"table\":\"sub_categories\",\"type\":\"belongsTo\",\"column\":\"sub_category_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"cities\",\"pivot\":\"0\",\"taggable\":\"0\"}', 11),
(54, 7, 'service_belongsto_user_relationship', 'relationship', 'users', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\User\",\"table\":\"users\",\"type\":\"belongsTo\",\"column\":\"user_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"cities\",\"pivot\":\"0\",\"taggable\":\"0\"}', 13),
(55, 8, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(56, 8, 'comment', 'text', 'Comment', 1, 1, 1, 1, 1, 1, '{}', 6),
(57, 8, 'budget', 'number', 'Budget', 1, 1, 1, 1, 1, 1, '{}', 7),
(58, 8, 'user_id', 'hidden', 'User Id', 1, 1, 1, 1, 1, 1, '{}', 5),
(59, 8, 'service_id', 'hidden', 'Service Id', 1, 1, 1, 1, 1, 1, '{}', 3),
(60, 8, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 8),
(61, 8, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 9),
(62, 8, 'comment_belongsto_user_relationship', 'relationship', 'users', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\User\",\"table\":\"users\",\"type\":\"belongsTo\",\"column\":\"user_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"cities\",\"pivot\":\"0\",\"taggable\":\"0\"}', 4),
(63, 8, 'comment_belongsto_service_relationship', 'relationship', 'services', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Service\",\"table\":\"services\",\"type\":\"belongsTo\",\"column\":\"service_id\",\"key\":\"id\",\"label\":\"title\",\"pivot_table\":\"cities\",\"pivot\":\"0\",\"taggable\":\"0\"}', 2),
(64, 9, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(65, 9, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '{}', 2),
(66, 9, 'slug', 'text', 'Slug', 1, 1, 1, 0, 0, 1, '{}', 3),
(67, 9, 'label', 'text', 'Label', 0, 1, 1, 1, 1, 1, '{}', 4),
(68, 9, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 5),
(69, 9, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(75, 5, 'sub_category_belongstomany_attribute_group_relationship', 'relationship', 'attribute_groups', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\AttributeGroup\",\"table\":\"attribute_groups\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"ad_attributes\",\"pivot\":\"1\",\"taggable\":\"on\"}', 7),
(76, 13, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(77, 13, 'attribute_group_id', 'hidden', 'Attribute Group Id', 1, 1, 1, 1, 1, 1, '{}', 5),
(78, 13, 'sub_category_id', 'hidden', 'Sub Category Id', 1, 1, 1, 1, 1, 1, '{}', 3),
(79, 13, 'created_at', 'timestamp', 'Created At', 0, 0, 1, 1, 0, 1, '{}', 8),
(80, 13, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 9),
(81, 13, 'ad_attribute_belongsto_attribute_group_relationship', 'relationship', 'attribute_groups', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\AttributeGroup\",\"table\":\"attribute_groups\",\"type\":\"belongsTo\",\"column\":\"attribute_group_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"ad_attributes\",\"pivot\":\"0\",\"taggable\":\"0\"}', 4),
(82, 13, 'ad_attribute_belongsto_sub_category_relationship', 'relationship', 'sub_categories', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\SubCategory\",\"table\":\"sub_categories\",\"type\":\"belongsTo\",\"column\":\"sub_category_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"ad_attributes\",\"pivot\":\"0\",\"taggable\":\"0\"}', 2),
(83, 14, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(84, 14, 'value', 'text', 'Value', 1, 1, 1, 1, 1, 1, '{}', 4),
(85, 14, 'icon', 'image', 'Icon', 0, 1, 1, 1, 1, 1, '{}', 5),
(86, 14, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 6),
(87, 14, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 7),
(88, 14, 'ad_attribute_id', 'text', 'Ad Attribute Id', 1, 1, 1, 1, 1, 1, '{}', 3),
(89, 14, 'attribute_belongsto_ad_attribute_relationship', 'relationship', 'ad_attributes', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\AdAttribute\",\"table\":\"ad_attributes\",\"type\":\"belongsTo\",\"column\":\"ad_attribute_id\",\"key\":\"id\",\"label\":\"ad_att_name\",\"pivot_table\":\"ad_attributes\",\"pivot\":\"0\",\"taggable\":\"0\"}', 2),
(90, 13, 'ad_attribute_hasmany_attribute_relationship', 'relationship', 'attributes', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Attribute\",\"table\":\"attributes\",\"type\":\"hasMany\",\"column\":\"ad_attribute_id\",\"key\":\"id\",\"label\":\"value\",\"pivot_table\":\"ad_attributes\",\"pivot\":\"0\",\"taggable\":\"0\"}', 10),
(91, 15, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(92, 15, 'key', 'text', 'Key', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required|unique:statuses,key\"}}', 3),
(93, 15, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 4),
(94, 15, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 5),
(95, 15, 'display', 'text', 'Display', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"}}', 2),
(96, 4, 'icon', 'image', 'Icon', 0, 1, 1, 1, 1, 1, '{}', 4),
(97, 13, 'form_type', 'select_dropdown', 'Form Type', 1, 1, 1, 1, 1, 1, '{\"default\":\"\",\"options\":{\"\":\"None\",\"selected\":\"selected\",\"text\":\"text\",\"textarea\":\"textarea\",\"checkbox\":\"checkbox\",\"number\":\"number\",\"date\":\"date\"}}', 6),
(98, 13, 'required', 'checkbox', 'Required', 0, 1, 1, 1, 1, 1, '{}', 7),
(99, 16, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(100, 16, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, '{}', 2),
(101, 16, 'description', 'text_area', 'Description', 1, 1, 1, 1, 1, 1, '{}', 3),
(102, 16, 'price', 'number', 'Price', 1, 1, 1, 1, 1, 1, '{}', 4),
(103, 16, 'city_id', 'text', 'City Id', 1, 1, 1, 1, 1, 1, '{}', 5),
(104, 16, 'status', 'text', 'Status', 0, 1, 1, 1, 1, 1, '{}', 6),
(105, 16, 'sub_category_id', 'text', 'Sub Category Id', 1, 1, 1, 1, 1, 1, '{}', 7),
(106, 16, 'slug', 'text', 'Slug', 0, 1, 1, 1, 1, 1, '{}', 8),
(107, 16, 'images', 'file', 'Images', 0, 1, 1, 1, 1, 1, '{}', 9),
(108, 16, 'location', 'text', 'Location', 0, 1, 1, 1, 1, 1, '{}', 10),
(109, 16, 'user_name', 'text', 'User Name', 0, 1, 1, 1, 1, 1, '{}', 11),
(110, 16, 'phone', 'text', 'Phone', 1, 1, 1, 1, 1, 1, '{}', 12),
(111, 16, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 13),
(112, 16, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 14),
(113, 17, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(114, 17, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '{}', 3),
(115, 17, 'attribute_group_id', 'hidden', 'Attribute Group Id', 1, 1, 1, 1, 1, 1, '{}', 4),
(116, 17, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 5),
(117, 17, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(118, 17, 'unit_belongsto_attribute_group_relationship', 'relationship', 'attribute_groups', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\AttributeGroup\",\"table\":\"attribute_groups\",\"type\":\"belongsTo\",\"column\":\"attribute_group_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"ad_attributes\",\"pivot\":\"0\",\"taggable\":\"0\"}', 2);

-- --------------------------------------------------------

--
-- Table structure for table `data_types`
--

CREATE TABLE `data_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT '0',
  `server_side` tinyint(4) NOT NULL DEFAULT '0',
  `details` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_types`
--

INSERT INTO `data_types` (`id`, `name`, `slug`, `display_name_singular`, `display_name_plural`, `icon`, `model_name`, `policy_name`, `controller`, `description`, `generate_permissions`, `server_side`, `details`, `created_at`, `updated_at`) VALUES
(1, 'users', 'users', 'User', 'Users', 'voyager-person', 'TCG\\Voyager\\Models\\User', 'TCG\\Voyager\\Policies\\UserPolicy', 'TCG\\Voyager\\Http\\Controllers\\VoyagerUserController', '', 1, 0, NULL, '2020-01-31 13:04:15', '2020-01-31 13:04:15'),
(2, 'menus', 'menus', 'Menu', 'Menus', 'voyager-list', 'TCG\\Voyager\\Models\\Menu', NULL, '', '', 1, 0, NULL, '2020-01-31 13:04:15', '2020-01-31 13:04:15'),
(3, 'roles', 'roles', 'Role', 'Roles', 'voyager-lock', 'TCG\\Voyager\\Models\\Role', NULL, '', '', 1, 0, NULL, '2020-01-31 13:04:15', '2020-01-31 13:04:15'),
(4, 'main_categories', 'main-categories', 'Main Category', 'Main Categories', NULL, 'App\\MainCategory', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-01-31 15:51:50', '2020-02-10 15:33:01'),
(5, 'sub_categories', 'sub-categories', 'Sub Category', 'Sub Categories', NULL, 'App\\SubCategory', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-01-31 15:55:19', '2020-02-04 16:00:18'),
(6, 'cities', 'cities', 'City', 'Cities', NULL, 'App\\City', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-01-31 16:54:20', '2020-01-31 16:54:20'),
(7, 'services', 'services', 'Service', 'Services', NULL, 'App\\Service', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-01-31 17:01:30', '2020-02-08 17:28:10'),
(8, 'comments', 'comments', 'Comment', 'Comments', NULL, 'App\\Comment', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-02-02 16:28:43', '2020-02-02 16:37:33'),
(9, 'attribute_groups', 'attribute-groups', 'Attribute Group', 'Attribute Groups', NULL, 'App\\AttributeGroup', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-02-04 15:57:44', '2020-02-04 17:53:13'),
(13, 'ad_attributes', 'ad-attributes', 'Ad Attribute', 'Ad Attributes', NULL, 'App\\AdAttribute', NULL, NULL, NULL, 1, 0, '{\"order_column\":\"created_at\",\"order_display_column\":\"created_at\",\"order_direction\":\"desc\",\"default_search_key\":null,\"scope\":null}', '2020-02-04 17:25:43', '2020-02-11 14:25:24'),
(14, 'attributes', 'attributes', 'Attribute', 'Attributes', NULL, 'App\\Attribute', NULL, 'App\\Http\\Controllers\\AttributesController', NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-02-05 11:09:24', '2020-02-05 19:15:39'),
(15, 'statuses', 'statuses', 'Status', 'Statuses', NULL, 'App\\Status', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-02-09 17:57:36', '2020-02-09 17:57:36'),
(16, 'ads', 'ads', 'Ad', 'Ads', NULL, 'App\\Ad', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-02-11 17:11:55', '2020-02-11 17:11:55'),
(17, 'units', 'units', 'Unit', 'Units', NULL, 'App\\Unit', NULL, 'App\\Http\\Controllers\\UnitsController', NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-02-15 16:30:38', '2020-02-15 16:48:40');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `main_categories`
--

CREATE TABLE `main_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `main_categories`
--

INSERT INTO `main_categories` (`id`, `name`, `description`, `created_at`, `updated_at`, `icon`) VALUES
(1, 'سيارات ومركبات', 'سيارات ومركبات', '2020-01-31 16:00:00', '2020-02-10 15:59:33', 'main-categories\\February2020\\EQrGvDv1kdxBHS724R28.png'),
(2, 'عقارات للبيع', 'عقارات للبيع', '2020-01-31 16:07:00', '2020-02-10 16:00:08', 'main-categories\\February2020\\OB0CnUOmi7LoVAnTyQpj.png'),
(6, 'اثاث وديكور', 'اثاث وديكور', '2020-01-31 16:42:00', '2020-02-10 16:01:44', 'main-categories\\February2020\\fJHjmIoVN5TH09wrbMTF.png'),
(7, 'اجهزة الكترونية', 'اجهزة الكترونية', '2020-01-31 16:43:00', '2020-02-10 15:56:19', 'main-categories\\February2020\\BmmwfaszwVhK03Uz5p6j.png'),
(8, 'عقارات للايجار', 'عقارات للايجار  عقارات للايجار', '2020-02-04 18:46:00', '2020-02-10 16:03:57', 'main-categories\\February2020\\ueun7Dlqxf39E6rM5I93.png'),
(13, 'قطع مركبات', 'قطع مركباتقطع مركبات قطع مركباتقطع مركبات', '2020-02-06 06:16:00', '2020-02-10 15:34:08', 'main-categories\\February2020\\1IwHD5DzbzUArsf3qGer.png');

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', '2020-01-31 13:04:16', '2020-01-31 13:04:16');

-- --------------------------------------------------------

--
-- Table structure for table `menu_items`
--

CREATE TABLE `menu_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menu_items`
--

INSERT INTO `menu_items` (`id`, `menu_id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `order`, `created_at`, `updated_at`, `route`, `parameters`) VALUES
(1, 1, 'Dashboard', '', '_self', 'voyager-boat', NULL, NULL, 1, '2020-01-31 13:04:16', '2020-01-31 13:04:16', 'voyager.dashboard', NULL),
(2, 1, 'Media', '', '_self', 'voyager-images', NULL, NULL, 5, '2020-01-31 13:04:16', '2020-01-31 13:04:16', 'voyager.media.index', NULL),
(3, 1, 'Users', '', '_self', 'voyager-person', NULL, NULL, 3, '2020-01-31 13:04:16', '2020-01-31 13:04:16', 'voyager.users.index', NULL),
(4, 1, 'Roles', '', '_self', 'voyager-lock', NULL, NULL, 2, '2020-01-31 13:04:17', '2020-01-31 13:04:17', 'voyager.roles.index', NULL),
(5, 1, 'Tools', '', '_self', 'voyager-tools', NULL, NULL, 9, '2020-01-31 13:04:17', '2020-01-31 13:04:17', NULL, NULL),
(6, 1, 'Menu Builder', '', '_self', 'voyager-list', NULL, 5, 10, '2020-01-31 13:04:17', '2020-01-31 13:04:17', 'voyager.menus.index', NULL),
(7, 1, 'Database', '', '_self', 'voyager-data', NULL, 5, 11, '2020-01-31 13:04:17', '2020-01-31 13:04:17', 'voyager.database.index', NULL),
(8, 1, 'Compass', '', '_self', 'voyager-compass', NULL, 5, 12, '2020-01-31 13:04:17', '2020-01-31 13:04:17', 'voyager.compass.index', NULL),
(9, 1, 'BREAD', '', '_self', 'voyager-bread', NULL, 5, 13, '2020-01-31 13:04:17', '2020-01-31 13:04:17', 'voyager.bread.index', NULL),
(10, 1, 'Settings', '', '_self', 'voyager-settings', NULL, NULL, 14, '2020-01-31 13:04:17', '2020-01-31 13:04:17', 'voyager.settings.index', NULL),
(11, 1, 'Hooks', '', '_self', 'voyager-hook', NULL, 5, 13, '2020-01-31 13:04:21', '2020-01-31 13:04:21', 'voyager.hooks', NULL),
(12, 1, 'Main Categories', '', '_self', NULL, NULL, NULL, 15, '2020-01-31 15:51:51', '2020-01-31 15:51:51', 'voyager.main-categories.index', NULL),
(13, 1, 'Sub Categories', '', '_self', NULL, NULL, NULL, 16, '2020-01-31 15:55:19', '2020-01-31 15:55:19', 'voyager.sub-categories.index', NULL),
(14, 1, 'Cities', '', '_self', NULL, NULL, NULL, 17, '2020-01-31 16:54:21', '2020-01-31 16:54:21', 'voyager.cities.index', NULL),
(15, 1, 'Services', '', '_self', NULL, NULL, NULL, 18, '2020-01-31 17:01:30', '2020-01-31 17:01:30', 'voyager.services.index', NULL),
(16, 1, 'Comments', '', '_self', NULL, NULL, NULL, 19, '2020-02-02 16:28:44', '2020-02-02 16:28:44', 'voyager.comments.index', NULL),
(17, 1, 'Attribute Groups', '', '_self', NULL, NULL, NULL, 20, '2020-02-04 15:57:46', '2020-02-04 15:57:46', 'voyager.attribute-groups.index', NULL),
(19, 1, 'Ad Attributes', '', '_self', NULL, NULL, NULL, 22, '2020-02-04 17:25:43', '2020-02-04 17:25:43', 'voyager.ad-attributes.index', NULL),
(20, 1, 'Attributes', '', '_self', NULL, NULL, NULL, 23, '2020-02-05 11:09:24', '2020-02-05 11:09:24', 'voyager.attributes.index', NULL),
(21, 1, 'Statuses', '', '_self', NULL, NULL, NULL, 24, '2020-02-09 17:57:37', '2020-02-09 17:57:37', 'voyager.statuses.index', NULL),
(22, 1, 'Ads', '', '_self', NULL, NULL, NULL, 25, '2020-02-11 17:11:56', '2020-02-11 17:11:56', 'voyager.ads.index', NULL),
(23, 1, 'Units', '', '_self', NULL, NULL, NULL, 26, '2020-02-15 16:30:38', '2020-02-15 16:30:38', 'voyager.units.index', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_01_01_000000_add_voyager_user_fields', 1),
(4, '2016_01_01_000000_create_data_types_table', 1),
(5, '2016_05_19_173453_create_menu_table', 1),
(6, '2016_10_21_190000_create_roles_table', 1),
(7, '2016_10_21_190000_create_settings_table', 1),
(8, '2016_11_30_135954_create_permission_table', 1),
(9, '2016_11_30_141208_create_permission_role_table', 1),
(10, '2016_12_26_201236_data_types__add__server_side', 1),
(11, '2017_01_13_000000_add_route_to_menu_items_table', 1),
(12, '2017_01_14_005015_create_translations_table', 1),
(13, '2017_01_15_000000_make_table_name_nullable_in_permissions_table', 1),
(14, '2017_03_06_000000_add_controller_to_data_types_table', 1),
(15, '2017_04_21_000000_add_order_to_data_rows_table', 1),
(16, '2017_07_05_210000_add_policyname_to_data_types_table', 1),
(17, '2017_08_05_000000_add_group_to_settings_table', 1),
(18, '2017_11_26_013050_add_user_role_relationship', 1),
(19, '2017_11_26_015000_create_user_roles_table', 1),
(20, '2018_03_11_000000_add_user_settings', 1),
(21, '2018_03_14_000000_add_details_to_data_types_table', 1),
(22, '2018_03_16_000000_make_settings_value_nullable', 1),
(23, '2019_08_19_000000_create_failed_jobs_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`) VALUES
(1, 'browse_admin', NULL, '2020-01-31 13:04:17', '2020-01-31 13:04:17'),
(2, 'browse_bread', NULL, '2020-01-31 13:04:17', '2020-01-31 13:04:17'),
(3, 'browse_database', NULL, '2020-01-31 13:04:17', '2020-01-31 13:04:17'),
(4, 'browse_media', NULL, '2020-01-31 13:04:17', '2020-01-31 13:04:17'),
(5, 'browse_compass', NULL, '2020-01-31 13:04:17', '2020-01-31 13:04:17'),
(6, 'browse_menus', 'menus', '2020-01-31 13:04:18', '2020-01-31 13:04:18'),
(7, 'read_menus', 'menus', '2020-01-31 13:04:18', '2020-01-31 13:04:18'),
(8, 'edit_menus', 'menus', '2020-01-31 13:04:18', '2020-01-31 13:04:18'),
(9, 'add_menus', 'menus', '2020-01-31 13:04:18', '2020-01-31 13:04:18'),
(10, 'delete_menus', 'menus', '2020-01-31 13:04:18', '2020-01-31 13:04:18'),
(11, 'browse_roles', 'roles', '2020-01-31 13:04:18', '2020-01-31 13:04:18'),
(12, 'read_roles', 'roles', '2020-01-31 13:04:18', '2020-01-31 13:04:18'),
(13, 'edit_roles', 'roles', '2020-01-31 13:04:18', '2020-01-31 13:04:18'),
(14, 'add_roles', 'roles', '2020-01-31 13:04:18', '2020-01-31 13:04:18'),
(15, 'delete_roles', 'roles', '2020-01-31 13:04:18', '2020-01-31 13:04:18'),
(16, 'browse_users', 'users', '2020-01-31 13:04:18', '2020-01-31 13:04:18'),
(17, 'read_users', 'users', '2020-01-31 13:04:18', '2020-01-31 13:04:18'),
(18, 'edit_users', 'users', '2020-01-31 13:04:18', '2020-01-31 13:04:18'),
(19, 'add_users', 'users', '2020-01-31 13:04:18', '2020-01-31 13:04:18'),
(20, 'delete_users', 'users', '2020-01-31 13:04:18', '2020-01-31 13:04:18'),
(21, 'browse_settings', 'settings', '2020-01-31 13:04:18', '2020-01-31 13:04:18'),
(22, 'read_settings', 'settings', '2020-01-31 13:04:19', '2020-01-31 13:04:19'),
(23, 'edit_settings', 'settings', '2020-01-31 13:04:19', '2020-01-31 13:04:19'),
(24, 'add_settings', 'settings', '2020-01-31 13:04:19', '2020-01-31 13:04:19'),
(25, 'delete_settings', 'settings', '2020-01-31 13:04:19', '2020-01-31 13:04:19'),
(26, 'browse_hooks', NULL, '2020-01-31 13:04:21', '2020-01-31 13:04:21'),
(27, 'browse_main_categories', 'main_categories', '2020-01-31 15:51:50', '2020-01-31 15:51:50'),
(28, 'read_main_categories', 'main_categories', '2020-01-31 15:51:50', '2020-01-31 15:51:50'),
(29, 'edit_main_categories', 'main_categories', '2020-01-31 15:51:50', '2020-01-31 15:51:50'),
(30, 'add_main_categories', 'main_categories', '2020-01-31 15:51:50', '2020-01-31 15:51:50'),
(31, 'delete_main_categories', 'main_categories', '2020-01-31 15:51:51', '2020-01-31 15:51:51'),
(32, 'browse_sub_categories', 'sub_categories', '2020-01-31 15:55:19', '2020-01-31 15:55:19'),
(33, 'read_sub_categories', 'sub_categories', '2020-01-31 15:55:19', '2020-01-31 15:55:19'),
(34, 'edit_sub_categories', 'sub_categories', '2020-01-31 15:55:19', '2020-01-31 15:55:19'),
(35, 'add_sub_categories', 'sub_categories', '2020-01-31 15:55:19', '2020-01-31 15:55:19'),
(36, 'delete_sub_categories', 'sub_categories', '2020-01-31 15:55:19', '2020-01-31 15:55:19'),
(37, 'browse_cities', 'cities', '2020-01-31 16:54:21', '2020-01-31 16:54:21'),
(38, 'read_cities', 'cities', '2020-01-31 16:54:21', '2020-01-31 16:54:21'),
(39, 'edit_cities', 'cities', '2020-01-31 16:54:21', '2020-01-31 16:54:21'),
(40, 'add_cities', 'cities', '2020-01-31 16:54:21', '2020-01-31 16:54:21'),
(41, 'delete_cities', 'cities', '2020-01-31 16:54:21', '2020-01-31 16:54:21'),
(42, 'browse_services', 'services', '2020-01-31 17:01:30', '2020-01-31 17:01:30'),
(43, 'read_services', 'services', '2020-01-31 17:01:30', '2020-01-31 17:01:30'),
(44, 'edit_services', 'services', '2020-01-31 17:01:30', '2020-01-31 17:01:30'),
(45, 'add_services', 'services', '2020-01-31 17:01:30', '2020-01-31 17:01:30'),
(46, 'delete_services', 'services', '2020-01-31 17:01:30', '2020-01-31 17:01:30'),
(47, 'browse_comments', 'comments', '2020-02-02 16:28:44', '2020-02-02 16:28:44'),
(48, 'read_comments', 'comments', '2020-02-02 16:28:44', '2020-02-02 16:28:44'),
(49, 'edit_comments', 'comments', '2020-02-02 16:28:44', '2020-02-02 16:28:44'),
(50, 'add_comments', 'comments', '2020-02-02 16:28:44', '2020-02-02 16:28:44'),
(51, 'delete_comments', 'comments', '2020-02-02 16:28:44', '2020-02-02 16:28:44'),
(52, 'browse_attribute_groups', 'attribute_groups', '2020-02-04 15:57:45', '2020-02-04 15:57:45'),
(53, 'read_attribute_groups', 'attribute_groups', '2020-02-04 15:57:45', '2020-02-04 15:57:45'),
(54, 'edit_attribute_groups', 'attribute_groups', '2020-02-04 15:57:45', '2020-02-04 15:57:45'),
(55, 'add_attribute_groups', 'attribute_groups', '2020-02-04 15:57:45', '2020-02-04 15:57:45'),
(56, 'delete_attribute_groups', 'attribute_groups', '2020-02-04 15:57:45', '2020-02-04 15:57:45'),
(57, 'browse_add_attributes', 'add_attributes', '2020-02-04 15:58:03', '2020-02-04 15:58:03'),
(58, 'read_add_attributes', 'add_attributes', '2020-02-04 15:58:03', '2020-02-04 15:58:03'),
(59, 'edit_add_attributes', 'add_attributes', '2020-02-04 15:58:03', '2020-02-04 15:58:03'),
(60, 'add_add_attributes', 'add_attributes', '2020-02-04 15:58:03', '2020-02-04 15:58:03'),
(61, 'delete_add_attributes', 'add_attributes', '2020-02-04 15:58:03', '2020-02-04 15:58:03'),
(62, 'browse_ad_attributes', 'ad_attributes', '2020-02-04 17:25:43', '2020-02-04 17:25:43'),
(63, 'read_ad_attributes', 'ad_attributes', '2020-02-04 17:25:43', '2020-02-04 17:25:43'),
(64, 'edit_ad_attributes', 'ad_attributes', '2020-02-04 17:25:43', '2020-02-04 17:25:43'),
(65, 'add_ad_attributes', 'ad_attributes', '2020-02-04 17:25:43', '2020-02-04 17:25:43'),
(66, 'delete_ad_attributes', 'ad_attributes', '2020-02-04 17:25:43', '2020-02-04 17:25:43'),
(67, 'browse_attributes', 'attributes', '2020-02-05 11:09:24', '2020-02-05 11:09:24'),
(68, 'read_attributes', 'attributes', '2020-02-05 11:09:24', '2020-02-05 11:09:24'),
(69, 'edit_attributes', 'attributes', '2020-02-05 11:09:24', '2020-02-05 11:09:24'),
(70, 'add_attributes', 'attributes', '2020-02-05 11:09:24', '2020-02-05 11:09:24'),
(71, 'delete_attributes', 'attributes', '2020-02-05 11:09:24', '2020-02-05 11:09:24'),
(72, 'browse_statuses', 'statuses', '2020-02-09 17:57:36', '2020-02-09 17:57:36'),
(73, 'read_statuses', 'statuses', '2020-02-09 17:57:36', '2020-02-09 17:57:36'),
(74, 'edit_statuses', 'statuses', '2020-02-09 17:57:36', '2020-02-09 17:57:36'),
(75, 'add_statuses', 'statuses', '2020-02-09 17:57:36', '2020-02-09 17:57:36'),
(76, 'delete_statuses', 'statuses', '2020-02-09 17:57:36', '2020-02-09 17:57:36'),
(77, 'browse_ads', 'ads', '2020-02-11 17:11:55', '2020-02-11 17:11:55'),
(78, 'read_ads', 'ads', '2020-02-11 17:11:55', '2020-02-11 17:11:55'),
(79, 'edit_ads', 'ads', '2020-02-11 17:11:55', '2020-02-11 17:11:55'),
(80, 'add_ads', 'ads', '2020-02-11 17:11:55', '2020-02-11 17:11:55'),
(81, 'delete_ads', 'ads', '2020-02-11 17:11:55', '2020-02-11 17:11:55'),
(82, 'browse_units', 'units', '2020-02-15 16:30:38', '2020-02-15 16:30:38'),
(83, 'read_units', 'units', '2020-02-15 16:30:38', '2020-02-15 16:30:38'),
(84, 'edit_units', 'units', '2020-02-15 16:30:38', '2020-02-15 16:30:38'),
(85, 'add_units', 'units', '2020-02-15 16:30:38', '2020-02-15 16:30:38'),
(86, 'delete_units', 'units', '2020-02-15 16:30:38', '2020-02-15 16:30:38');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(27, 1),
(28, 1),
(29, 1),
(30, 1),
(31, 1),
(32, 1),
(33, 1),
(34, 1),
(35, 1),
(36, 1),
(37, 1),
(38, 1),
(39, 1),
(40, 1),
(41, 1),
(42, 1),
(43, 1),
(44, 1),
(45, 1),
(46, 1),
(47, 1),
(48, 1),
(49, 1),
(50, 1),
(51, 1),
(52, 1),
(53, 1),
(54, 1),
(55, 1),
(56, 1),
(57, 1),
(58, 1),
(59, 1),
(60, 1),
(61, 1),
(62, 1),
(63, 1),
(64, 1),
(65, 1),
(66, 1),
(67, 1),
(68, 1),
(69, 1),
(70, 1),
(71, 1),
(72, 1),
(73, 1),
(74, 1),
(75, 1),
(76, 1),
(77, 1),
(78, 1),
(79, 1),
(80, 1),
(81, 1),
(82, 1),
(83, 1),
(84, 1),
(85, 1),
(86, 1);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Administrator', '2020-01-31 13:04:17', '2020-01-31 13:04:17'),
(2, 'user', 'Normal User', '2020-01-31 13:04:17', '2020-01-31 13:04:17');

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `budget_min` int(10) UNSIGNED NOT NULL,
  `budget_max` int(10) UNSIGNED NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city_id` int(11) NOT NULL,
  `sub_category_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `title`, `slug`, `description`, `budget_min`, `budget_max`, `status`, `city_id`, `sub_category_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'مطلوب سيارة للبيع', NULL, 'لوريم ايبسوم هو نموذج افتراضي يوضع في التصاميم لتعرض على العميل ليتصور طريقه وضع النصوص بالتصاميم سواء كانت تصاميم مطبوعه ... بروشور او فلاير على سبيل المثال ... او نماذج مواقع انترنت \r\nلوريم ايبسوم هو نموذج افتراضي يوضع في التصاميم لتعرض على العميل ليتصور طريقه وضع النصوص بالتصاميم سواء كانت تصاميم مطبوعه ... بروشور او فلاير على سبيل المثال ... او نماذج مواقع انترنت', 23, 55, 'نص نص', 1, 11, 1, '2020-01-31 17:07:00', '2020-02-08 07:34:06'),
(2, 'ما هو لوريم ايبسوم', 'ما هو لوريم ايبسوم', 'هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زياد التى يولدها التطبيق.\r\nهذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زياد التى يولدها التطبيق.', 12, 34, 'بحث تعداد الجنوب.', 1, 1, 1, '2020-01-31 17:09:00', '2020-02-08 07:35:50'),
(3, 'سيارة مشطووبة للبيع', 'ann-coleman', '<p><span style=\"color: #000000; font-family: \'Open Sans\', Arial, sans-serif; text-align: justify;\">هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها. ولذلك</span></p>', 1205, 11, 'الحالة', 1, 1, 1, '2020-02-07 17:06:30', '2020-02-07 17:06:30'),
(4, 'سيارة مشطووبة للبيع', '-1', 'هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها. ولذلك\r\nهناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها. ولذلك', 12, 23, 'ءؤءؤ', 1, 1, 1, '2020-02-07 17:54:46', '2020-02-07 17:54:46');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `details` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `group` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `key`, `display_name`, `value`, `details`, `type`, `order`, `group`) VALUES
(1, 'site.title', 'Site Title', 'Site Title', '', 'text', 1, 'Site'),
(2, 'site.description', 'Site Description', 'Site Description', '', 'text', 2, 'Site'),
(3, 'site.logo', 'Site Logo', '', '', 'image', 3, 'Site'),
(4, 'site.google_analytics_tracking_id', 'Google Analytics Tracking ID', '', '', 'text', 4, 'Site'),
(5, 'admin.bg_image', 'Admin Background Image', '', '', 'image', 5, 'Admin'),
(6, 'admin.title', 'Admin Title', 'Voyager', '', 'text', 1, 'Admin'),
(7, 'admin.description', 'Admin Description', 'Welcome to Voyager. The Missing Admin for Laravel', '', 'text', 2, 'Admin'),
(8, 'admin.loader', 'Admin Loader', '', '', 'image', 3, 'Admin'),
(9, 'admin.icon_image', 'Admin Icon Image', '', '', 'image', 4, 'Admin'),
(10, 'admin.google_analytics_client_id', 'Google Analytics Client ID (used for admin dashboard)', '', '', 'text', 1, 'Admin');

-- --------------------------------------------------------

--
-- Table structure for table `statuses`
--

CREATE TABLE `statuses` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varbinary(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `display` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sub_categories`
--

CREATE TABLE `sub_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `main_category_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sub_categories`
--

INSERT INTO `sub_categories` (`id`, `name`, `created_at`, `updated_at`, `main_category_id`) VALUES
(1, 'سيارات للبيع', '2020-01-31 16:02:56', '2020-01-31 16:02:56', 1),
(2, 'شقق للييع', '2020-01-31 16:08:21', '2020-01-31 16:08:21', 2),
(3, 'منازل للايجار', '2020-01-31 16:21:52', '2020-01-31 16:21:52', 2),
(4, 'سيارات للبيع', '2020-01-31 16:45:00', '2020-01-31 16:48:56', 1),
(5, 'كمبيوتر', '2020-01-31 16:47:00', '2020-01-31 16:48:16', 7),
(6, 'سيارات للإيجار', '2020-01-31 16:49:00', '2020-02-04 17:58:33', 1),
(7, 'دراجات نارية', '2020-01-31 16:49:32', '2020-01-31 16:49:32', 1),
(8, 'فلل - قصور للبيع', '2020-01-31 16:49:58', '2020-01-31 16:49:58', 2),
(9, 'اراضي - مزارع للبيع', '2020-01-31 16:50:13', '2020-01-31 16:50:13', 2),
(10, 'مسجلات', '2020-01-31 16:50:57', '2020-01-31 16:50:57', 7),
(11, 'ستيريو - مسجلات - راديو', '2020-01-31 16:51:34', '2020-01-31 16:51:34', 7),
(12, 'مودم - راوتر', '2020-01-31 16:51:59', '2020-01-31 16:51:59', 7),
(13, 'ادوات المطبخ - ضيافة', '2020-01-31 16:52:33', '2020-01-31 16:52:33', 6),
(14, 'أدوات منزلية', '2020-01-31 16:52:49', '2020-01-31 16:52:49', 6),
(15, 'شقق للايجار', '2020-02-04 16:01:00', '2020-02-04 16:05:42', 6),
(16, 'موبايل - هواتف', '2020-02-04 17:59:00', '2020-02-16 07:38:09', 7),
(17, 'شقق للايجار', '2020-02-04 18:54:53', '2020-02-04 18:54:53', 8),
(18, 'عمارات للايجار', '2020-02-04 18:55:58', '2020-02-04 18:55:58', 8),
(19, 'اراضي - مزارع للايجار', '2020-02-04 18:56:23', '2020-02-04 18:56:23', 8),
(20, 'غرف سكن - شقق فندقية', '2020-02-04 18:56:54', '2020-02-04 18:56:54', 8),
(21, 'عقارات أجنبية للإيجار', '2020-02-04 18:57:51', '2020-02-04 18:57:51', 8),
(22, 'فلل - قصور للايجار', '2020-02-04 18:58:34', '2020-02-04 18:58:34', 8),
(23, 'خدمات توصيل', '2020-02-04 18:59:01', '2020-02-04 18:59:01', 1),
(24, 'لوحة مميزة', '2020-02-04 18:59:20', '2020-02-04 18:59:20', 1),
(25, 'شاحنات - اليات ثقيلة', '2020-02-04 18:59:47', '2020-02-04 18:59:47', 1),
(27, 'مواتير سيارت', '2020-02-06 08:50:00', '2020-02-06 08:51:43', 13),
(28, 'اجهزة مطبخ', '2020-02-11 05:16:53', '2020-02-11 05:16:53', 7),
(29, 'اجهزة مطبخ', '2020-02-11 05:17:45', '2020-02-11 05:17:45', 7),
(30, 'معدات', '2020-02-11 05:22:24', '2020-02-11 05:22:24', 7),
(31, 'ابواب', '2020-02-11 05:28:50', '2020-02-11 05:28:50', 13);

-- --------------------------------------------------------

--
-- Table structure for table `translations`
--

CREATE TABLE `translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `units`
--

CREATE TABLE `units` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `attribute_group_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `units`
--

INSERT INTO `units` (`id`, `name`, `attribute_group_id`, `created_at`, `updated_at`) VALUES
(1, 'دونم', 2, '2020-02-15 16:52:00', '2020-02-15 16:52:00'),
(2, 'متر مربع', 2, '2020-02-15 16:52:00', '2020-02-15 16:52:00'),
(3, 'كيلومتر', 2, '2020-02-15 16:52:01', '2020-02-15 16:52:01'),
(4, 'فدان', 2, '2020-02-15 16:52:01', '2020-02-15 16:52:01');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `avatar`, `email_verified_at`, `password`, `remember_token`, `settings`, `created_at`, `updated_at`) VALUES
(1, 1, 'Hasan', 'admin@admin.com', 'users\\February2020\\m7TXYkQtHemADmKI0CZt.png', NULL, '$2y$10$FKhgv4kud65PaM7irijRxepA6/bVqAZ9WPuU4.02p/PExiJieCUQm', NULL, '{\"locale\":\"en\"}', '2020-01-31 13:04:37', '2020-02-08 07:40:38'),
(2, 2, 'asdasd', 'ha@ha.com', 'users/default.png', NULL, '$2y$10$GaCdLKvP06HHZThkCSXyuujIHo9wDxHRY2jx7actlzqoQ5LqOrozG', NULL, NULL, '2020-02-08 10:52:04', '2020-02-08 10:52:05'),
(3, 2, 'نتلاتنتنذ', 'sdfsdf@dsfdf.com', 'users/default.png', NULL, '$2y$10$qYB4eVg3jyuIO8zz.lrNheMnlpnnM3FYA8gzjo3dA70wwpnSNopKW', NULL, NULL, '2020-02-09 06:46:29', '2020-02-09 06:46:29'),
(4, 2, 'حسن زهور', 'h@h.com', 'users/default.png', NULL, '$2y$10$rN4a/hb8E.zXDxkjVpurAuzGO1NseK6VHZBQhHAZUqATJHmY8oOVS', NULL, NULL, '2020-02-10 14:43:17', '2020-02-10 14:43:17');

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE `user_roles` (
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ads`
--
ALTER TABLE `ads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ad_attributes`
--
ALTER TABLE `ad_attributes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `attributes`
--
ALTER TABLE `attributes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `attribute_groups`
--
ALTER TABLE `attribute_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD PRIMARY KEY (`id`),
  ADD KEY `data_rows_data_type_id_foreign` (`data_type_id`);

--
-- Indexes for table `data_types`
--
ALTER TABLE `data_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `data_types_name_unique` (`name`),
  ADD UNIQUE KEY `data_types_slug_unique` (`slug`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `main_categories`
--
ALTER TABLE `main_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menus_name_unique` (`name`);

--
-- Indexes for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_items_menu_id_foreign` (`menu_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_key_index` (`key`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_key_unique` (`key`);

--
-- Indexes for table `statuses`
--
ALTER TABLE `statuses`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `statuses_key_unique` (`key`);

--
-- Indexes for table `sub_categories`
--
ALTER TABLE `sub_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `translations`
--
ALTER TABLE `translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`);

--
-- Indexes for table `units`
--
ALTER TABLE `units`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `user_roles_user_id_index` (`user_id`),
  ADD KEY `user_roles_role_id_index` (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ads`
--
ALTER TABLE `ads`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ad_attributes`
--
ALTER TABLE `ad_attributes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `attributes`
--
ALTER TABLE `attributes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=131;

--
-- AUTO_INCREMENT for table `attribute_groups`
--
ALTER TABLE `attribute_groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `data_rows`
--
ALTER TABLE `data_rows`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=119;

--
-- AUTO_INCREMENT for table `data_types`
--
ALTER TABLE `data_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `main_categories`
--
ALTER TABLE `main_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `menu_items`
--
ALTER TABLE `menu_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=87;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `statuses`
--
ALTER TABLE `statuses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sub_categories`
--
ALTER TABLE `sub_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `translations`
--
ALTER TABLE `translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `units`
--
ALTER TABLE `units`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Constraints for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
