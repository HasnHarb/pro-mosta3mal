-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 01, 2020 at 09:05 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mosta3maldb`
--

-- --------------------------------------------------------

--
-- Table structure for table `ads`
--

CREATE TABLE `ads` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `status` int(10) UNSIGNED DEFAULT '0',
  `sub_category_id` int(11) NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `images` text COLLATE utf8mb4_unicode_ci,
  `location` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `currency_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ads`
--

INSERT INTO `ads` (`id`, `title`, `description`, `price`, `city_id`, `status`, `sub_category_id`, `slug`, `images`, `location`, `user_name`, `phone`, `created_at`, `updated_at`, `user_id`, `currency_id`) VALUES
(17, 'تلفون ساسونج للبيع', 'تلفون ساسونج للبيع تلفون ساسونج للبيع تلفون ساسونج للبيع تلفون ساسونج للبيع', 500, 1, 1, 16, 'تلفون-ساسونج-للبيع-5', '[{\"download_link\":\"ads\\\\February2020\\\\ewbv6M9Na45QEqt7KZcD.jpg\",\"original_name\":\"61YNlYdUK6L._SL1000_-20191224141810-1000x1000.jpg\"},{\"download_link\":\"ads\\\\February2020\\\\gTbdhJcR4sOqjonCpDKE.jpg\",\"original_name\":\"\\u062a\\u0646\\u0632\\u064a\\u0644 (25).jpg\"}]', 'بيت كاحل', 'Hasan', '+9720598933532', '2020-02-29 16:06:10', '2020-02-29 16:06:10', 1, 1),
(18, 'سيارة كيا للبيع', 'سيارة كيا للبيع سيارة كيا للبيع سيارة كيا للبيع', 25000, 6, 1, 27, 'سيارة-كيا-للبيع', '[{\"download_link\":\"ads\\\\March2020\\\\Z5JKp8Kv3O9IfQzFT5Bi.png\",\"original_name\":\"image_154859072921221583.png\"},{\"download_link\":\"ads\\\\March2020\\\\DRq2QO9ze6CPABRFKrsA.png\",\"original_name\":\"illustration-tech-esign-tran3-1024x725.png\"},{\"download_link\":\"ads\\\\March2020\\\\bDh7Y3kTZ1f32tRlbZga.jpg\",\"original_name\":\"shutterstock_1060094186.jpg\"}]', 'جمرورة', 'Hasan', '+9720598933532', '2020-03-01 06:20:08', '2020-03-01 06:20:08', 1, 1),
(19, 'تلفون لينوفو', 'تلفون لينوفو تلفون لينوفو تلفون لينوفو تلفون لينوفو', 250, 4, 0, 16, 'تلفون-لينوفو', '[{\"download_link\":\"ads\\\\March2020\\\\pNUPPRal0oMPDVJ1NfjL.jpg\",\"original_name\":\"\\u062a\\u0646\\u0632\\u064a\\u0644 (25).jpg\"}]', 'هناك عناك هناك', 'حمزة زهور', '05998933532', '2020-03-01 08:50:00', '2020-04-29 07:14:27', 6, 1),
(20, 'هاتف ال جي جديد', 'هاتف ال جي جديدهاتف ال جي جديد هاتف ال جي جديدهاتف ال جي جديد', 1033, 3, NULL, 16, 'هاتف-ال-جي-جديد', '[{\"download_link\":\"ads\\\\March2020\\\\QIZcNL7Xhl0lriU3Lfai.jpg\",\"original_name\":\"shutterstock_1060094186.jpg\"}]', 'هناك عناك هناك', 'حمزة زهور', '05998933532', '2020-03-01 08:54:57', '2020-03-01 08:54:57', 6, 1),
(23, 'تلفون ساسونج A10 للبيع', 'هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها. ولذلك يتم استخدام طريقة لوريم إيبسوم لأنها تعطي توزيعاَ طبيعياَ -إلى حد ما- للأحرف عوضاً عن استخدام \"هنا يوجد محتوى نصي، هنا يوجد محتوى نصي\" فتجعلها تبدو (أي الأحرف) وكأنها نص مقروء.', 250, 1, 1, 16, 'تلفون-ساسونج-A10-للبيع', '[{\"download_link\":\"ads\\\\March2020\\\\ISHSBautVcBLUBQi4AZG.jpg\",\"original_name\":\"192.jpg\"},{\"download_link\":\"ads\\\\March2020\\\\oXTKRP4Qxq2GUXHNMYqJ.jpg\",\"original_name\":\"images.jpg\"},{\"download_link\":\"ads\\\\March2020\\\\hZ4F8DRi9Bq9mUMd36Fe.png\",\"original_name\":\"\\u0645\\u0631\\u0627\\u062c\\u0639\\u0629 \\u0645\\u0648\\u0627\\u0635\\u0641\\u0627\\u062a samsung galaxy a10 \\u0645\\u0645\\u064a\\u0632\\u0627\\u062a \\u0648\\u0639\\u064a\\u0648\\u0628 \\u0633\\u0627\\u0645\\u0633\\u0648\\u0646\\u062c A10 (1).png\"},{\"download_link\":\"ads\\\\March2020\\\\05EsdP0zkROFKa8wqQXF.jpg\",\"original_name\":\"61YNlYdUK6L._SL1000_-20191224141810-1000x1000.jpg\"}]', 'بيت كاحل', 'حسن عايش زهور', '+9720598933532', '2020-03-01 18:27:00', '2020-04-29 15:15:11', 1, 2),
(25, 'هاتف مستعمل', 'معلومات الاتصال\r\n\r\nمعلومات الاتصال\r\n\r\nمعلومات الاتصال\r\n\r\nمعلومات الاتصال', 1500, 4, 1, 16, 'هاتف-مستعمل', '[{\"download_link\":\"ads\\\\March2020\\\\vVqtdcuTSc3hYXMD88yP.jpg\",\"original_name\":\"61YNlYdUK6L._SL1000_-20191224141810-1000x1000.jpg\"},{\"download_link\":\"ads\\\\March2020\\\\eCvnqFzamqmBpUAVZrS3.jpg\",\"original_name\":\"\\u062a\\u0646\\u0632\\u064a\\u0644 (25).jpg\"},{\"download_link\":\"ads\\\\March2020\\\\2CjuP4OzxagaGSZ5HlLH.jpg\",\"original_name\":\"192.jpg\"}]', NULL, 'Hasan', '+9720598933532', '2020-03-03 08:36:00', '2020-04-29 14:13:54', 1, 1),
(34, 'ايفون للبيع', 'معروض للبيع هذا الهاتف غير مستحدم كثيرا', 3000, 1, 0, 16, 'ايفون-للبيع', '[{\"download_link\":\"ads\\\\default-image.jpg\",\"original_name\":\"default-image.jpg\"}]', 'شارع السلام', 'حمزة زهور', '0595730715', '2020-05-01 15:18:51', '2020-05-01 15:18:51', 26, 1),
(35, 'ارض جمرورة للبيع', 'ارض مزروعة زيتون وتحتوي على بئر ماء', 50000, 1, 1, 9, 'ارض-جمرورة-للبيع', '[{\"download_link\":\"ads\\\\May2020\\\\hl3FXXuXivFYkO1y73M9.jpg\",\"original_name\":\"\\u0627\\u0631\\u0636.jpg\"}]', 'بيت كاحل جمرورة', 'حمزة زهور', '0595701000', '2020-05-01 15:22:00', '2020-05-01 16:05:04', 26, 3),
(36, 'سيارة مرسيدس', 'ولا ضربة جديد جديد', 200000, 3, 1, 1, 'سيارة-مرسيدس', '[{\"download_link\":\"ads\\\\May2020\\\\mYjbidjiXyN0oVQgBdM1.jpg\",\"original_name\":\"\\u062a\\u0646\\u0632\\u064a\\u0644 (8).jpg\"},{\"download_link\":\"ads\\\\May2020\\\\S61ivsNQDBlHSeEgeSJz.jpg\",\"original_name\":\"\\u062a\\u0646\\u0632\\u064a\\u0644 (9).jpg\"}]', 'باب العامود', 'حمزة زهور', '05999999', '2020-05-01 15:54:00', '2020-05-01 16:04:32', 26, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ad_attributes`
--

CREATE TABLE `ad_attributes` (
  `id` int(10) UNSIGNED NOT NULL,
  `attribute_group_id` int(11) NOT NULL,
  `sub_category_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `form_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'selected',
  `required` bit(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ad_attributes`
--

INSERT INTO `ad_attributes` (`id`, `attribute_group_id`, `sub_category_id`, `created_at`, `updated_at`, `form_type`, `required`) VALUES
(2, 13, 15, NULL, '2020-02-11 05:37:16', 'selected', b'0'),
(3, 14, 15, NULL, NULL, '', NULL),
(4, 1, 6, NULL, NULL, '', NULL),
(5, 5, 6, NULL, NULL, '', NULL),
(6, 6, 6, NULL, NULL, '', NULL),
(7, 7, 6, NULL, NULL, '', NULL),
(8, 1, 16, '2020-02-11 07:34:00', '2020-02-11 05:34:28', 'selected', b'1'),
(10, 9, 16, '2020-02-11 07:34:00', '2020-02-11 05:35:00', 'selected', b'1'),
(12, 4, 27, '2020-02-11 07:35:00', '2020-02-11 08:21:19', 'number', b'1'),
(13, 5, 27, '2020-02-11 07:35:00', '2020-02-11 05:35:45', 'number', b'0'),
(14, 8, 27, NULL, '2020-02-11 05:36:07', 'selected', b'0'),
(15, 17, 2, '2020-02-09 08:34:00', '2020-02-11 05:36:31', 'selected', b'0'),
(16, 1, 1, '2020-02-11 05:13:00', '2020-05-01 15:52:49', 'selected', b'1'),
(17, 1, 29, NULL, NULL, 'selected', NULL),
(18, 2, 9, '2020-02-11 05:21:17', '2020-02-11 05:21:17', 'text', b'1'),
(19, 1, 30, NULL, '2020-02-11 14:19:21', 'selected', b'1'),
(20, 4, 30, NULL, '2020-02-11 14:19:34', 'selected', b'0'),
(21, 8, 30, '2020-04-09 16:40:00', '2020-04-09 13:40:26', 'selected', b'1'),
(22, 1, 31, NULL, NULL, 'selected', NULL),
(23, 3, 31, NULL, NULL, 'selected', NULL),
(24, 6, 27, '2020-02-11 10:18:00', '2020-02-11 08:18:21', 'selected', b'0'),
(25, 6, 16, NULL, '2020-02-16 07:46:53', 'selected', b'0'),
(27, 16, 16, '2020-02-17 18:26:00', '2020-02-23 04:47:58', 'text', b'1'),
(28, 16, 9, '2020-02-23 06:49:00', '2020-02-23 04:49:04', 'text', b'1'),
(29, 1, 32, '2020-02-25 11:40:00', '2020-02-25 09:43:14', 'selected', b'1'),
(30, 4, 32, NULL, NULL, 'selected', NULL),
(31, 9, 32, NULL, NULL, 'selected', NULL),
(33, 18, 16, '2020-02-29 13:37:45', '2020-02-29 13:37:45', 'text', b'0'),
(35, 2, 33, NULL, '2020-04-09 13:41:00', 'number', b'1'),
(36, 14, 33, '2020-04-09 16:42:00', '2020-04-09 13:42:24', 'number', b'1'),
(37, 19, 33, '2020-04-09 16:47:00', '2020-04-09 13:47:35', 'number', b'1'),
(42, 1, 34, NULL, NULL, 'selected', NULL),
(43, 6, 34, NULL, NULL, 'selected', NULL),
(44, 8, 34, '2020-04-09 17:56:00', '2020-04-09 14:57:08', 'text', b'1'),
(47, 1, 35, NULL, '2020-04-09 15:17:47', 'selected', b'1'),
(48, 8, 35, '2020-04-09 18:15:00', '2020-04-09 15:15:13', 'text', b'1'),
(51, 25, 35, NULL, '2020-04-09 15:16:51', 'selected', b'1'),
(53, 1, 36, NULL, '2020-04-09 15:38:36', 'selected', b'1'),
(54, 6, 36, NULL, '2020-04-09 15:38:57', 'selected', b'1'),
(55, 8, 36, NULL, '2020-04-09 15:38:04', 'text', b'1'),
(58, 1, 37, NULL, '2020-04-09 15:57:01', 'selected', b'1'),
(59, 6, 37, NULL, '2020-04-09 15:57:26', 'selected', b'1'),
(63, 29, 37, NULL, '2020-04-09 16:00:03', 'selected', b'1'),
(64, 13, 38, NULL, NULL, 'selected', NULL),
(65, 30, 38, NULL, NULL, 'selected', NULL),
(66, 31, 38, NULL, NULL, 'selected', NULL),
(67, 32, 38, NULL, NULL, 'selected', NULL),
(69, 2, 39, NULL, NULL, 'selected', NULL),
(78, 6, 42, NULL, '2020-04-09 16:24:20', 'selected', b'1'),
(81, 38, 42, NULL, '2020-04-09 16:27:30', 'number', b'1'),
(82, 1, 43, NULL, '2020-04-09 16:29:16', 'selected', b'1'),
(83, 6, 43, NULL, '2020-04-09 16:29:34', 'selected', b'1'),
(86, 24, 5, '2020-05-01 15:13:00', '2020-05-01 15:13:49', 'selected', b'1'),
(87, 4, 1, '2020-05-01 18:25:00', '2020-05-01 15:25:50', 'selected', b'1'),
(88, 5, 1, '2020-05-01 18:25:00', '2020-05-01 15:25:54', 'selected', b'1'),
(89, 7, 1, '2020-05-01 18:26:00', '2020-05-01 15:26:05', 'selected', b'1'),
(90, 39, 1, '2020-05-01 18:26:00', '2020-05-01 15:26:15', 'selected', b'1'),
(91, 40, 1, '2020-05-01 18:26:00', '2020-05-01 15:26:29', 'selected', b'1');

-- --------------------------------------------------------

--
-- Table structure for table `ad_reports`
--

CREATE TABLE `ad_reports` (
  `id` int(10) UNSIGNED NOT NULL,
  `ad_id` int(10) UNSIGNED NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ad_reports`
--

INSERT INTO `ad_reports` (`id`, `ad_id`, `body`, `created_at`, `updated_at`, `email`) VALUES
(1, 25, 'dsflsdj foskdfj sdf k', '2020-04-20 14:32:38', '2020-04-20 14:32:38', 'admin@admin.com'),
(2, 17, 'kbh xdjfkjsd fksjh lkajdh fkljashdfas dfkjhasdkf sakj', '2020-04-20 14:33:45', '2020-04-20 14:33:45', 'admin@admin.com');

-- --------------------------------------------------------

--
-- Table structure for table `against_currencies`
--

CREATE TABLE `against_currencies` (
  `id` int(10) UNSIGNED NOT NULL,
  `from_currency_id` int(10) UNSIGNED NOT NULL,
  `to_currency_id` int(10) UNSIGNED NOT NULL,
  `buy` double UNSIGNED NOT NULL,
  `sell` double UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `against_currencies`
--

INSERT INTO `against_currencies` (`id`, `from_currency_id`, `to_currency_id`, `buy`, `sell`, `created_at`, `updated_at`) VALUES
(1, 2, 1, 3.589, 3.609, '2020-04-08 07:43:00', '2020-04-08 07:44:04'),
(2, 3, 1, 4.9847, 5.0939, '2020-04-08 07:45:12', '2020-04-08 07:45:12'),
(3, 4, 1, 3.8689, 3.9338, '2020-04-08 07:46:13', '2020-04-08 07:46:13'),
(4, 2, 3, 0.7085, 0.72, '2020-04-08 07:46:37', '2020-04-08 07:46:37'),
(5, 4, 2, 1.078, 1.09, '2020-04-08 07:47:00', '2020-04-08 07:47:00');

-- --------------------------------------------------------

--
-- Table structure for table `attributes`
--

CREATE TABLE `attributes` (
  `id` int(10) UNSIGNED NOT NULL,
  `value` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `ad_attribute_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `attributes`
--

INSERT INTO `attributes` (`id`, `value`, `icon`, `created_at`, `updated_at`, `ad_attribute_id`) VALUES
(27, 'سامسونج', NULL, '2020-02-06 04:20:17', '2020-02-06 04:20:17', 8),
(28, 'ايفون', NULL, '2020-02-06 04:20:17', '2020-02-06 04:20:17', 8),
(29, 'بلاكبيري', NULL, '2020-02-06 04:20:17', '2020-02-06 04:20:17', 8),
(32, 'اوبو', NULL, '2020-02-06 04:20:18', '2020-02-06 04:20:18', 8),
(33, 'لينوفو', NULL, '2020-02-06 04:20:18', '2020-02-06 04:20:18', 8),
(94, 'حمامين', NULL, '2020-02-07 15:30:28', '2020-02-07 15:30:28', 2),
(95, 'حمامين', NULL, '2020-02-07 15:36:54', '2020-02-07 15:36:54', 2),
(96, 'ثلاث حمامات', NULL, '2020-02-07 15:36:54', '2020-02-07 15:36:54', 2),
(97, 'اربعة حمامات', NULL, '2020-02-07 15:36:54', '2020-02-07 15:36:54', 2),
(98, 'اكثر من اربعة', NULL, '2020-02-07 15:36:54', '2020-02-07 15:36:54', 2),
(99, 'طابق 1', NULL, '2020-02-09 08:39:21', '2020-02-09 08:39:21', 15),
(100, 'طابق 2', NULL, '2020-02-09 08:39:21', '2020-02-09 08:39:21', 15),
(101, 'طابق 3', NULL, '2020-02-09 08:39:21', '2020-02-09 08:39:21', 15),
(102, 'بنزين 98', NULL, '2020-02-11 07:49:20', '2020-02-11 07:49:20', 7),
(103, 'بنزين 96', NULL, '2020-02-11 07:49:20', '2020-02-11 07:49:20', 7),
(104, 'سولار', NULL, '2020-02-11 07:49:20', '2020-02-11 07:49:20', 7),
(105, 'اغو', NULL, '2020-02-11 07:49:21', '2020-02-11 07:49:21', 7),
(106, 'مستعمل', NULL, '2020-02-11 08:19:55', '2020-02-11 08:19:55', 24),
(107, 'جديد', NULL, '2020-02-11 08:19:56', '2020-02-11 08:19:56', 24),
(108, 'هونداي', NULL, '2020-02-11 08:20:38', '2020-02-11 08:20:38', 14),
(109, 'كيا', NULL, '2020-02-11 08:20:38', '2020-02-11 08:20:38', 14),
(110, 'سكودا', NULL, '2020-02-11 08:20:39', '2020-02-11 08:20:39', 14),
(111, 'بي ام دبليو', NULL, '2020-02-11 08:20:39', '2020-02-11 08:20:39', 14),
(112, 'ماكيتا', NULL, '2020-02-11 14:15:03', '2020-02-11 14:15:03', 19),
(113, 'بوش', NULL, '2020-02-11 14:15:03', '2020-02-11 14:15:03', 19),
(114, 'مفرقاة', NULL, '2020-02-11 14:15:03', '2020-02-11 14:15:03', 19),
(115, 'سامسنج', NULL, '2020-02-11 14:15:03', '2020-02-11 14:15:03', 19),
(116, '2019', NULL, '2020-02-11 14:16:14', '2020-02-11 14:16:14', 20),
(117, '2018', NULL, '2020-02-11 14:16:15', '2020-02-11 14:16:15', 20),
(118, '2017', NULL, '2020-02-11 14:16:15', '2020-02-11 14:16:15', 20),
(119, '2016', NULL, '2020-02-11 14:16:15', '2020-02-11 14:16:15', 20),
(120, '2015', NULL, '2020-02-11 14:16:15', '2020-02-11 14:16:15', 20),
(121, '2014', NULL, '2020-02-11 14:16:15', '2020-02-11 14:16:15', 20),
(122, '2013', NULL, '2020-02-11 14:16:15', '2020-02-11 14:16:15', 20),
(123, 'ايسوس', NULL, '2020-02-16 07:40:18', '2020-02-16 07:40:18', 8),
(124, 'ال جي', NULL, '2020-02-16 07:40:18', '2020-02-16 07:40:18', 8),
(125, 'مستعمل', NULL, '2020-02-16 07:40:55', '2020-02-16 07:40:55', 25),
(126, 'جديد', NULL, '2020-02-16 07:40:55', '2020-02-16 07:40:55', 25),
(127, '8GB', NULL, '2020-02-16 07:43:05', '2020-02-16 07:43:05', 10),
(128, '16GB', NULL, '2020-02-16 07:43:05', '2020-02-16 07:43:05', 10),
(129, '32GB', NULL, '2020-02-16 07:43:05', '2020-02-16 07:43:05', 10),
(130, '64GB', NULL, '2020-02-16 07:43:05', '2020-02-16 07:43:05', 10),
(131, 'سامسونج', NULL, '2020-02-25 09:42:01', '2020-02-25 09:42:01', 29),
(132, 'ال جي', NULL, '2020-02-25 09:42:01', '2020-02-25 09:42:01', 29),
(133, 'سوني', NULL, '2020-02-25 09:42:01', '2020-02-25 09:42:01', 29),
(134, 'جولد', NULL, '2020-02-25 09:42:01', '2020-02-25 09:42:01', 29),
(154, 'HP', NULL, '2020-04-09 14:54:15', '2020-04-09 14:54:15', 42),
(155, 'Dell', NULL, '2020-04-09 14:54:16', '2020-04-09 14:54:16', 42),
(156, 'Lenovo', NULL, '2020-04-09 14:54:16', '2020-04-09 14:54:16', 42),
(157, 'Acer', NULL, '2020-04-09 14:54:16', '2020-04-09 14:54:16', 42),
(158, 'MSI', NULL, '2020-04-09 14:54:16', '2020-04-09 14:54:16', 42),
(159, 'Asus', NULL, '2020-04-09 14:54:17', '2020-04-09 14:54:17', 42),
(160, 'Apple', NULL, '2020-04-09 14:54:17', '2020-04-09 14:54:17', 42),
(161, 'Razer', NULL, '2020-04-09 14:54:17', '2020-04-09 14:54:17', 42),
(162, 'Samsung', NULL, '2020-04-09 14:54:17', '2020-04-09 14:54:17', 42),
(163, 'جديد', NULL, '2020-04-09 15:00:59', '2020-04-09 15:00:59', 43),
(164, 'مستعمل', NULL, '2020-04-09 15:00:59', '2020-04-09 15:00:59', 43),
(165, 'LG', NULL, '2020-04-09 15:06:50', '2020-04-09 15:06:50', 47),
(166, 'Samsung', NULL, '2020-04-09 15:06:50', '2020-04-09 15:06:50', 47),
(167, 'Sony', NULL, '2020-04-09 15:06:50', '2020-04-09 15:06:50', 47),
(168, '32بوصة', NULL, '2020-04-09 15:12:54', '2020-04-09 15:12:54', 51),
(169, '26 بوصة', NULL, '2020-04-09 15:12:54', '2020-04-09 15:12:54', 51),
(170, '37 بوصة', NULL, '2020-04-09 15:12:55', '2020-04-09 15:12:55', 51),
(171, 'بوصة 40', NULL, '2020-04-09 15:12:55', '2020-04-09 15:12:55', 51),
(172, '46  بوصة', NULL, '2020-04-09 15:12:55', '2020-04-09 15:12:55', 51),
(173, '50  بوصة', NULL, '2020-04-09 15:12:55', '2020-04-09 15:12:55', 51),
(174, '52  بوصة', NULL, '2020-04-09 15:12:55', '2020-04-09 15:12:55', 51),
(175, '55  بوصة', NULL, '2020-04-09 15:12:55', '2020-04-09 15:12:55', 51),
(176, '57  بوصة', NULL, '2020-04-09 15:12:55', '2020-04-09 15:12:55', 51),
(177, '60  بوصة', NULL, '2020-04-09 15:12:55', '2020-04-09 15:12:55', 51),
(180, 'مراقبة', NULL, '2020-04-09 15:42:44', '2020-04-09 15:42:44', 53),
(181, 'تصوير', NULL, '2020-04-09 15:42:44', '2020-04-09 15:42:44', 53),
(182, 'Analog', NULL, '2020-04-09 15:47:01', '2020-04-09 15:47:01', 55),
(183, 'TVL', NULL, '2020-04-09 15:47:02', '2020-04-09 15:47:02', 55),
(184, 'Fujifilm', NULL, '2020-04-09 15:47:02', '2020-04-09 15:47:02', 55),
(185, 'Canon', NULL, '2020-04-09 15:47:02', '2020-04-09 15:47:02', 55),
(186, 'Nikon', NULL, '2020-04-09 15:47:02', '2020-04-09 15:47:02', 55),
(187, 'Sony Alpha', NULL, '2020-04-09 15:47:02', '2020-04-09 15:47:02', 55),
(188, 'Olympus OM-D E-M', NULL, '2020-04-09 15:47:02', '2020-04-09 15:47:02', 55),
(189, 'جدبد', NULL, '2020-04-09 15:52:56', '2020-04-09 15:52:56', 54),
(190, 'مستعمل', NULL, '2020-04-09 15:52:57', '2020-04-09 15:52:57', 54),
(193, 'فل اوتماتيك', NULL, '2020-04-09 16:02:37', '2020-04-09 16:02:37', 58),
(194, 'عادية', NULL, '2020-04-09 16:02:37', '2020-04-09 16:02:37', 58),
(195, 'اوتماتيك', NULL, '2020-04-09 16:02:37', '2020-04-09 16:02:37', 58),
(196, 'مستعمل', NULL, '2020-04-09 16:03:13', '2020-04-09 16:03:13', 59),
(197, 'جديد', NULL, '2020-04-09 16:03:13', '2020-04-09 16:03:13', 59),
(198, '7 كيلو', NULL, '2020-04-09 16:09:57', '2020-04-09 16:09:57', 63),
(199, '10 كيلو', NULL, '2020-04-09 16:09:57', '2020-04-09 16:09:57', 63),
(200, '9 كيلو', NULL, '2020-04-09 16:09:57', '2020-04-09 16:09:57', 63),
(201, '8 كيلو', NULL, '2020-04-09 16:09:57', '2020-04-09 16:09:57', 63),
(202, '11 كيلو', NULL, '2020-04-09 16:09:57', '2020-04-09 16:09:57', 63),
(203, '13 كيلو', NULL, '2020-04-09 16:09:57', '2020-04-09 16:09:57', 63),
(204, '12 كيلو', NULL, '2020-04-09 16:09:57', '2020-04-09 16:09:57', 63),
(205, '14 كيلو', NULL, '2020-04-09 16:09:57', '2020-04-09 16:09:57', 63),
(206, '15 كيلو', NULL, '2020-04-09 16:09:57', '2020-04-09 16:09:57', 63),
(207, '18 كيلو', NULL, '2020-04-09 16:09:57', '2020-04-09 16:09:57', 63),
(214, 'جديد', NULL, '2020-04-09 16:28:01', '2020-04-09 16:28:01', 78),
(215, 'مستعمل', NULL, '2020-04-09 16:28:01', '2020-04-09 16:28:01', 78),
(216, 'جديد', NULL, '2020-04-09 16:30:02', '2020-04-09 16:30:02', 83),
(217, 'مستعمل', NULL, '2020-04-09 16:30:02', '2020-04-09 16:30:02', 83),
(218, 'تخت', NULL, '2020-04-09 16:32:49', '2020-04-09 16:32:49', 82),
(219, 'ارضية', NULL, '2020-04-09 16:32:49', '2020-04-09 16:32:49', 82),
(220, 'مرسيدس', NULL, '2020-05-01 15:42:15', '2020-05-01 15:42:15', 16),
(221, 'لامبورغيني', '0', '2020-05-01 15:42:15', '2020-05-01 15:42:15', 16),
(222, 'تويوتا', '0', '2020-05-01 15:42:15', '2020-05-01 15:42:15', 16),
(223, 'فورد', '0', '2020-05-01 15:42:15', '2020-05-01 15:42:15', 16),
(224, 'انفينيتي', '0', '2020-05-01 15:42:15', '2020-05-01 15:42:15', 16),
(225, 'انفينيتي', '0', '2020-05-01 15:42:15', '2020-05-01 15:42:15', 16),
(226, 'تويوتا', '0', '2020-05-01 15:42:15', '2020-05-01 15:42:15', 16),
(227, '2020', NULL, '2020-05-01 15:45:59', '2020-05-01 15:45:59', 87),
(228, '2019', '0', '2020-05-01 15:45:59', '2020-05-01 15:45:59', 87),
(229, '2018', '0', '2020-05-01 15:45:59', '2020-05-01 15:45:59', 87),
(230, '2017', '0', '2020-05-01 15:45:59', '2020-05-01 15:45:59', 87),
(231, '2016', '0', '2020-05-01 15:45:59', '2020-05-01 15:45:59', 87),
(232, '2015', '0', '2020-05-01 15:46:00', '2020-05-01 15:46:00', 87),
(233, '2014', '0', '2020-05-01 15:46:00', '2020-05-01 15:46:00', 87),
(234, '2013', '0', '2020-05-01 15:46:00', '2020-05-01 15:46:00', 87),
(235, '2012', '0', '2020-05-01 15:46:00', '2020-05-01 15:46:00', 87),
(236, '2011', '0', '2020-05-01 15:46:00', '2020-05-01 15:46:00', 87),
(237, '2010', '0', '2020-05-01 15:46:00', '2020-05-01 15:46:00', 87),
(238, '50 حصان', NULL, '2020-05-01 15:46:05', '2020-05-01 15:46:05', 91),
(239, '60 حصان', '0', '2020-05-01 15:46:06', '2020-05-01 15:46:06', 91),
(240, '100 حصان', '0', '2020-05-01 15:46:06', '2020-05-01 15:46:06', 91),
(241, '200 حصان', '0', '2020-05-01 15:46:06', '2020-05-01 15:46:06', 91),
(242, '300 حصان', '0', '2020-05-01 15:46:06', '2020-05-01 15:46:06', 91),
(243, '255 حصان', '0', '2020-05-01 15:46:06', '2020-05-01 15:46:06', 91),
(244, '340 حصان', '0', '2020-05-01 15:46:06', '2020-05-01 15:46:06', 91),
(245, 'اضفر', NULL, '2020-05-01 15:47:53', '2020-05-01 15:47:53', 90),
(246, 'احمر', '0', '2020-05-01 15:47:53', '2020-05-01 15:47:53', 90),
(247, 'ابيض', '0', '2020-05-01 15:47:53', '2020-05-01 15:47:53', 90),
(248, 'اسود', '0', '2020-05-01 15:47:54', '2020-05-01 15:47:54', 90),
(249, 'ازرق', '0', '2020-05-01 15:47:54', '2020-05-01 15:47:54', 90),
(250, 'بني', '0', '2020-05-01 15:47:54', '2020-05-01 15:47:54', 90),
(251, 'بنزين 98', NULL, '2020-05-01 15:48:01', '2020-05-01 15:48:01', 89),
(252, 'بنزين 95', '0', '2020-05-01 15:48:01', '2020-05-01 15:48:01', 89),
(253, 'سولار', '0', '2020-05-01 15:48:01', '2020-05-01 15:48:01', 89),
(254, 'كاز', '0', '2020-05-01 15:48:01', '2020-05-01 15:48:01', 89),
(255, '1000-2000 كيلو متر', NULL, '2020-05-01 15:50:21', '2020-05-01 15:50:21', 88),
(256, '2000-3000 كيلو متر', '0', '2020-05-01 15:50:22', '2020-05-01 15:50:22', 88),
(257, '3000-4000 كيلو متر', '0', '2020-05-01 15:50:22', '2020-05-01 15:50:22', 88),
(258, '4000-5000 كيلو متر', '0', '2020-05-01 15:50:22', '2020-05-01 15:50:22', 88),
(259, 'اكثر من ذلك', '0', '2020-05-01 15:50:22', '2020-05-01 15:50:22', 88),
(260, '5000-6000 كيلو متر', '0', '2020-05-01 15:50:22', '2020-05-01 15:50:22', 88);

-- --------------------------------------------------------

--
-- Table structure for table `attribute_groups`
--

CREATE TABLE `attribute_groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `attribute_groups`
--

INSERT INTO `attribute_groups` (`id`, `name`, `slug`, `label`, `created_at`, `updated_at`) VALUES
(1, 'النوع', 'النوع', 'نوع المنتج او الماركة', '2020-02-04 16:02:19', '2020-02-04 16:02:19'),
(2, 'مساحة الارض', 'مساحة الارض', 'مساحة قطعة الارض', '2020-02-04 16:03:00', '2020-02-04 16:03:00'),
(3, 'الفئة', 'الفئة', 'الفئة', '2020-02-04 17:39:23', '2020-02-04 17:39:23'),
(4, 'سنة الصنع', 'سنة-الصنع', 'سنة الصنع', '2020-02-04 17:39:58', '2020-02-04 17:39:58'),
(5, 'الكيلومترات', 'الكيلومترات', 'الكيلومترات', '2020-02-04 17:40:17', '2020-02-04 17:40:17'),
(6, 'الحالة', 'الحالة', 'الحالة', '2020-02-04 17:40:56', '2020-02-04 17:40:56'),
(7, 'نوع الوقود', 'نوع الوقود', 'نوع الوقود', '2020-02-04 17:41:23', '2020-02-04 17:41:23'),
(8, 'الماركة', 'الماركة', 'الماركة', '2020-02-04 17:42:08', '2020-02-04 17:42:08'),
(9, 'سعة التخزين', 'سعة التخزين', 'سعة التخزين', '2020-02-04 17:42:29', '2020-02-04 17:42:29'),
(13, 'عدد الحمامات', 'عدد-الحمامات', 'عدد الحمامات', '2020-02-04 17:54:07', '2020-02-04 17:54:07'),
(14, 'مساحة البناء', 'مساحة-البناء', 'مساحة البناء', '2020-02-04 17:54:00', '2020-02-04 17:55:09'),
(15, 'طريقة الدفع', 'طريقة-الدفع', 'طريقة الدفع', '2020-02-04 17:55:55', '2020-02-04 17:55:55'),
(16, 'مزايا إضافية', 'مزايا-إضافية', 'مزايا إضافية', '2020-02-04 17:56:40', '2020-02-04 17:56:40'),
(17, 'رقم الطوابق', 'رقم-الطوابق', 'عدد الطوابق', '2020-02-09 08:33:52', '2020-02-09 08:33:52'),
(18, 'سعة البطارية', 'سعة-البطارية', 'سعة البطارية', '2020-02-29 13:37:30', '2020-02-29 13:37:30'),
(19, 'عدد الطوابق', 'عدد-الطوابق', 'عدد طوابق البناء', '2020-04-09 13:24:00', '2020-04-09 13:50:35'),
(20, 'المدينة', 'المدينة', NULL, '2020-04-09 13:25:12', '2020-04-09 13:25:12'),
(23, 'السعر', 'السعر', NULL, '2020-04-09 13:25:54', '2020-04-09 13:25:54'),
(24, 'حجم الرام', 'حجم-الرام', NULL, '2020-04-09 13:31:26', '2020-04-09 13:31:26'),
(25, 'حجم الشاشة', 'حجم-الشاشة', NULL, '2020-04-09 13:35:09', '2020-04-09 13:35:09'),
(29, 'السعة', 'السعة', NULL, '2020-04-09 14:12:29', '2020-04-09 14:12:29'),
(30, 'مساحة الشقة', 'مساحة-الشقة', NULL, '2020-04-09 14:16:12', '2020-04-09 14:16:12'),
(31, 'رقم الطابق', 'رقم-الطابق', NULL, '2020-04-09 14:16:20', '2020-04-09 14:16:20'),
(32, 'عدد الغرف', 'عدد-الغرف', NULL, '2020-04-09 14:16:46', '2020-04-09 14:16:46'),
(38, 'العدد', 'العدد', NULL, '2020-04-09 14:27:09', '2020-04-09 14:27:09'),
(39, 'لون', 'لون', NULL, '2020-05-01 15:24:15', '2020-05-01 15:24:15'),
(40, 'قوة المحرك', 'قوة-المحرك', NULL, '2020-05-01 15:24:35', '2020-05-01 15:24:35');

-- --------------------------------------------------------

--
-- Table structure for table `auctions`
--

CREATE TABLE `auctions` (
  `id` int(10) UNSIGNED NOT NULL,
  `ad_id` int(11) NOT NULL,
  `base_price` int(10) UNSIGNED NOT NULL,
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `auctions`
--

INSERT INTO `auctions` (`id`, `ad_id`, `base_price`, `start_date`, `end_date`, `created_at`, `updated_at`) VALUES
(3, 19, 144, '2020-03-23 11:28:00', '2020-03-28 11:28:00', '2020-03-23 07:28:38', '2020-03-23 07:28:38'),
(7, 23, 155, '2020-05-01 11:36:00', '2020-05-02 11:30:00', '2020-05-01 05:36:00', '2020-05-01 12:04:21');

-- --------------------------------------------------------

--
-- Table structure for table `auction_offers`
--

CREATE TABLE `auction_offers` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `price` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `auction_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `auction_offers`
--

INSERT INTO `auction_offers` (`id`, `user_id`, `price`, `created_at`, `updated_at`, `auction_id`) VALUES
(1, 1, 122, '2020-04-06 15:16:43', '2020-04-06 15:16:43', 1),
(2, 1, 156, '2020-04-14 11:54:40', '2020-04-14 11:54:40', 5),
(3, 1, 1000, '2020-04-14 14:31:44', '2020-04-14 14:31:44', 5);

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'الخليل', '2020-01-31 16:54:48', '2020-01-31 16:54:48'),
(2, 'بيت لحم', '2020-01-31 16:54:58', '2020-01-31 16:54:58'),
(3, 'القدس', '2020-01-31 16:55:06', '2020-01-31 16:55:06'),
(4, 'رام الله', '2020-01-31 16:55:13', '2020-01-31 16:55:13'),
(5, 'قلقيلة', '2020-01-31 16:55:24', '2020-01-31 16:55:24'),
(6, 'جنين', '2020-01-31 16:55:33', '2020-01-31 16:55:33'),
(7, 'نابلس', '2020-02-06 06:25:48', '2020-02-06 06:25:48');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `ad_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `body`, `user_id`, `ad_id`, `created_at`, `updated_at`) VALUES
(1, 'نص نص نص', 1, 23, '2020-02-02 16:38:00', '2020-04-29 07:59:21'),
(2, 'ارض ببيت كاحل ارض ببيت كاحل ارض ببيت كاحل ارض ببيت كاحل ارض ببيت كاحل ارض ببيت كاحل ارض ببيت كاحل ارض ببيت كاحل ارض ببيت كاحل ارض ببيت كاحل ارض ببيت كاحل ارض ببيت كاحل', 1, 22, '2020-04-29 08:05:08', '2020-04-29 08:05:08'),
(3, 'ارض ببيت كاحل ارض ببيت كاحل ارض ببيت كاحل ارض ببيت كاحل  ارض ببيت كاحل ارض ببيت كاحل ارض ببيت كاحل ارض ببيت كاحل', 14, 22, '2020-04-29 08:06:06', '2020-04-29 08:06:06'),
(4, 'This is one of the most popular example that shows how to swap numbers using call by reference.', 1, 22, '2020-04-29 09:16:45', '2020-04-29 09:16:45'),
(5, 'wef dfsdf dfasdfasdf asdfs df asfa sdfasdfad', 6, 25, '2020-04-29 14:20:54', '2020-04-29 14:20:54'),
(6, 'الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص،', 19, 26, '2020-04-30 16:03:25', '2020-04-30 16:03:25'),
(7, 'الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص،', 19, 26, '2020-04-30 16:03:34', '2020-04-30 16:03:34');

-- --------------------------------------------------------

--
-- Table structure for table `currencies`
--

CREATE TABLE `currencies` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `currencies`
--

INSERT INTO `currencies` (`id`, `name`, `icon`, `created_at`, `updated_at`) VALUES
(1, 'شيكل', '₪', '2020-02-23 04:07:27', '2020-02-23 04:07:27'),
(2, 'دولار', '$', '2020-02-23 04:07:41', '2020-02-23 04:07:41'),
(3, 'دينار', 'JOD', '2020-04-08 07:44:42', '2020-04-08 07:44:42'),
(4, 'يورو', 'EUP', '2020-04-08 07:45:42', '2020-04-08 07:45:42');

-- --------------------------------------------------------

--
-- Table structure for table `data_rows`
--

CREATE TABLE `data_rows` (
  `id` int(10) UNSIGNED NOT NULL,
  `data_type_id` int(10) UNSIGNED NOT NULL,
  `field` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `browse` tinyint(1) NOT NULL DEFAULT '1',
  `read` tinyint(1) NOT NULL DEFAULT '1',
  `edit` tinyint(1) NOT NULL DEFAULT '1',
  `add` tinyint(1) NOT NULL DEFAULT '1',
  `delete` tinyint(1) NOT NULL DEFAULT '1',
  `details` text COLLATE utf8mb4_unicode_ci,
  `order` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_rows`
--

INSERT INTO `data_rows` (`id`, `data_type_id`, `field`, `type`, `display_name`, `required`, `browse`, `read`, `edit`, `add`, `delete`, `details`, `order`) VALUES
(1, 1, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '{}', 1),
(2, 1, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '{}', 2),
(3, 1, 'email', 'text', 'Email', 1, 1, 1, 1, 1, 1, '{}', 3),
(4, 1, 'password', 'password', 'Password', 1, 0, 0, 1, 1, 0, '{}', 4),
(5, 1, 'remember_token', 'text', 'Remember Token', 0, 0, 0, 0, 0, 0, '{}', 5),
(6, 1, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, '{}', 6),
(7, 1, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 8),
(8, 1, 'avatar', 'image', 'Avatar', 0, 1, 1, 1, 1, 1, '{}', 9),
(9, 1, 'user_belongsto_role_relationship', 'relationship', 'Role', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":\"0\",\"taggable\":\"0\"}', 11),
(10, 1, 'user_belongstomany_role_relationship', 'relationship', 'Roles', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}', 12),
(11, 1, 'settings', 'hidden', 'Settings', 0, 0, 0, 0, 0, 0, '{}', 14),
(12, 2, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(13, 2, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(14, 2, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(15, 2, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(16, 3, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '{}', 1),
(17, 3, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '{}', 2),
(18, 3, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '{}', 3),
(19, 3, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 4),
(20, 3, 'display_name', 'text', 'Display Name', 1, 1, 1, 1, 1, 1, '{}', 5),
(21, 1, 'role_id', 'text', 'Role', 0, 1, 1, 1, 1, 1, '{}', 10),
(22, 4, 'id', 'hidden', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(23, 4, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required|max:255|unique:cities,name\"}}', 2),
(24, 4, 'description', 'text_area', 'Description', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"max:255\"}}', 3),
(25, 4, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 5),
(26, 4, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(27, 5, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(28, 5, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required|max:255\"}}', 4),
(29, 5, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 5),
(30, 5, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(31, 5, 'main_category_id', 'hidden', 'Main Category Id', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"}}', 3),
(32, 5, 'sub_category_belongsto_main_category_relationship', 'relationship', 'Main Category', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\MainCategory\",\"table\":\"main_categories\",\"type\":\"belongsTo\",\"column\":\"main_category_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"data_rows\",\"pivot\":\"0\",\"taggable\":\"0\"}', 2),
(34, 4, 'main_category_hasmany_sub_category_relationship', 'relationship', 'SubCategories', 0, 1, 1, 0, 0, 1, '{\"model\":\"App\\\\SubCategory\",\"table\":\"sub_categories\",\"type\":\"hasMany\",\"column\":\"main_category_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"data_rows\",\"pivot\":\"0\",\"taggable\":\"0\"}', 7),
(35, 6, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(36, 6, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required|max:255|unique:cities,name\"}}', 2),
(37, 6, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 3),
(38, 6, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 4),
(39, 7, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(40, 7, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required|max:255\"}}', 2),
(41, 7, 'slug', 'text', 'Slug', 0, 0, 0, 0, 0, 0, '{}', 3),
(42, 7, 'description', 'text_area', 'Description', 1, 0, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required|max:3000\"}}', 4),
(43, 7, 'budget_min', 'number', 'Budget Min', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required|integer|min:0\"}}', 5),
(44, 7, 'budget_max', 'number', 'Budget Max', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required_with|integer|gte:budget_min|min:0\"}}', 6),
(45, 7, 'status', 'checkbox', 'Status', 0, 1, 1, 1, 1, 1, '{\"off\":\"\\u063a\\u064a\\u0631 \\u0641\\u0639\\u0627\\u0644\",\"on\":\"\\u0641\\u0639\\u0627\\u0644\"}', 7),
(47, 7, 'city_id', 'hidden', 'City Id', 1, 1, 1, 1, 1, 1, '{}', 9),
(48, 7, 'sub_category_id', 'hidden', 'Sub Category Id', 1, 1, 1, 1, 1, 1, '{}', 11),
(49, 7, 'user_id', 'hidden', 'User Id', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"}}', 15),
(50, 7, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 16),
(51, 7, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 17),
(52, 7, 'service_belongsto_city_relationship', 'relationship', 'cities', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\City\",\"table\":\"cities\",\"type\":\"belongsTo\",\"column\":\"city_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"cities\",\"pivot\":\"0\",\"taggable\":\"0\"}', 8),
(53, 7, 'service_belongsto_sub_category_relationship', 'relationship', 'sub_categories', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\SubCategory\",\"table\":\"sub_categories\",\"type\":\"belongsTo\",\"column\":\"sub_category_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"cities\",\"pivot\":\"0\",\"taggable\":\"0\"}', 10),
(54, 7, 'service_belongsto_user_relationship', 'relationship', 'users', 0, 1, 1, 0, 0, 1, '{\"model\":\"App\\\\User\",\"table\":\"users\",\"type\":\"belongsTo\",\"column\":\"user_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"cities\",\"pivot\":\"0\",\"taggable\":\"0\"}', 14),
(55, 8, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(58, 8, 'user_id', 'hidden', 'User Id', 1, 1, 1, 1, 1, 1, '{}', 3),
(60, 8, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 7),
(61, 8, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 8),
(62, 8, 'comment_belongsto_user_relationship', 'relationship', 'users', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\User\",\"table\":\"users\",\"type\":\"belongsTo\",\"column\":\"user_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"cities\",\"pivot\":\"0\",\"taggable\":\"0\"}', 2),
(64, 9, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(65, 9, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required|max:255|unique:attribute_groups,name\"}}', 2),
(66, 9, 'slug', 'hidden', 'Slug', 1, 1, 1, 0, 0, 1, '{}', 3),
(67, 9, 'label', 'text', 'Label', 0, 0, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"max:255\"}}', 4),
(68, 9, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 5),
(69, 9, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(75, 5, 'sub_category_belongstomany_attribute_group_relationship', 'relationship', 'Attribute Groups', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\AttributeGroup\",\"table\":\"attribute_groups\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"ad_attributes\",\"pivot\":\"1\",\"taggable\":\"on\"}', 7),
(76, 13, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(77, 13, 'attribute_group_id', 'hidden', 'Attribute Group Id', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"}}', 5),
(78, 13, 'sub_category_id', 'hidden', 'Sub Category Id', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"}}', 3),
(79, 13, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 8),
(80, 13, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 9),
(81, 13, 'ad_attribute_belongsto_attribute_group_relationship', 'relationship', 'Att Group', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\AttributeGroup\",\"table\":\"attribute_groups\",\"type\":\"belongsTo\",\"column\":\"attribute_group_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"ad_attributes\",\"pivot\":\"0\",\"taggable\":\"0\"}', 4),
(82, 13, 'ad_attribute_belongsto_sub_category_relationship', 'relationship', 'SubCategory', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\SubCategory\",\"table\":\"sub_categories\",\"type\":\"belongsTo\",\"column\":\"sub_category_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"ad_attributes\",\"pivot\":\"0\",\"taggable\":\"0\"}', 2),
(83, 14, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(84, 14, 'value', 'text', 'Value', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"}}', 4),
(85, 14, 'icon', 'hidden', 'Icon', 0, 1, 1, 1, 1, 1, '{}', 5),
(86, 14, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 6),
(87, 14, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 7),
(88, 14, 'ad_attribute_id', 'text', 'Ad Attribute Id', 1, 1, 1, 1, 1, 1, '{}', 3),
(89, 14, 'attribute_belongsto_ad_attribute_relationship', 'relationship', 'Ad Attributes', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\AdAttribute\",\"table\":\"ad_attributes\",\"type\":\"belongsTo\",\"column\":\"ad_attribute_id\",\"key\":\"id\",\"label\":\"ad_att_name\",\"pivot_table\":\"ad_attributes\",\"pivot\":\"0\",\"taggable\":\"0\"}', 2),
(90, 13, 'ad_attribute_hasmany_attribute_relationship', 'relationship', 'Attributes Val', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Attribute\",\"table\":\"attributes\",\"type\":\"hasMany\",\"column\":\"ad_attribute_id\",\"key\":\"id\",\"label\":\"value\",\"pivot_table\":\"ad_attributes\",\"pivot\":\"0\",\"taggable\":\"0\"}', 10),
(91, 15, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(92, 15, 'key', 'text', 'Key', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required|unique:statuses,key\"}}', 3),
(93, 15, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 4),
(94, 15, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 5),
(95, 15, 'display', 'text', 'Display', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"}}', 2),
(96, 4, 'icon', 'image', 'Icon', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required|mimes:jpeg,jpg,png,gif|max:10000\"}}', 4),
(97, 13, 'form_type', 'select_dropdown', 'Form Type', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required|max:255\"},\"default\":\"\",\"options\":{\"\":\"None\",\"selected\":\"selected\",\"text\":\"text\",\"textarea\":\"textarea\",\"checkbox\":\"checkbox\",\"number\":\"number\",\"date\":\"date\"}}', 6),
(98, 13, 'required', 'checkbox', 'Required', 0, 1, 1, 1, 1, 1, '{\"on\":\"Required\",\"off\":\"Not Required\"}', 7),
(99, 16, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(100, 16, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required|max:255\"}}', 2),
(101, 16, 'description', 'text_area', 'Description', 1, 0, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required|max:3000\"}}', 3),
(102, 16, 'price', 'number', 'Price', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required|integer\"}}', 16),
(103, 16, 'city_id', 'hidden', 'City Id', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"}}', 5),
(104, 16, 'status', 'checkbox', 'Status', 0, 1, 1, 0, 0, 1, '{\"on\":\"Active\",\"off\":\"Deactive\"}', 6),
(105, 16, 'sub_category_id', 'hidden', 'Sub Category Id', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"}}', 8),
(106, 16, 'slug', 'text', 'Slug', 0, 0, 1, 0, 0, 1, '{}', 9),
(107, 16, 'images', 'file', 'Images', 0, 0, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required|mimes:jpeg,jpg,png,gif|max:10000\"}}', 10),
(108, 16, 'location', 'text', 'Location', 0, 0, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"max:255\"}}', 11),
(109, 16, 'user_name', 'text', 'User Name', 0, 0, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"max:255\"}}', 12),
(110, 16, 'phone', 'text', 'Phone', 1, 0, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required|max:20\"}}', 13),
(111, 16, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 19),
(112, 16, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 20),
(113, 17, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(114, 17, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '{}', 3),
(115, 17, 'attribute_group_id', 'hidden', 'Attribute Group Id', 1, 1, 1, 1, 1, 1, '{}', 4),
(116, 17, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 5),
(117, 17, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(118, 17, 'unit_belongsto_attribute_group_relationship', 'relationship', 'attribute_groups', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\AttributeGroup\",\"table\":\"attribute_groups\",\"type\":\"belongsTo\",\"column\":\"attribute_group_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"ad_attributes\",\"pivot\":\"0\",\"taggable\":\"0\"}', 2),
(119, 18, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(120, 18, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '{}', 2),
(121, 18, 'icon', 'text', 'Icon', 1, 1, 1, 1, 1, 1, '{}', 3),
(122, 18, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 4),
(123, 18, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 5),
(124, 7, 'service_belongsto_currency_relationship', 'relationship', 'currencies', 0, 0, 1, 1, 1, 1, '{\"model\":\"App\\\\Currency\",\"table\":\"currencies\",\"type\":\"belongsTo\",\"column\":\"currency_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"ad_attributes\",\"pivot\":\"0\",\"taggable\":\"0\"}', 12),
(125, 7, 'currency_id', 'hidden', 'Currency Id', 1, 0, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"}}', 13),
(126, 1, 'email_verified_at', 'timestamp', 'Email Verified At', 0, 1, 1, 1, 1, 1, '{}', 7),
(127, 1, 'phone', 'text', 'Phone', 0, 0, 1, 1, 1, 1, '{}', 13),
(128, 16, 'user_id', 'hidden', 'User Id', 0, 1, 1, 1, 1, 1, '{}', 15),
(129, 16, 'currency_id', 'text', 'Currency Id', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"}}', 18),
(130, 19, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(131, 19, 'slug', 'text', 'Slug', 1, 1, 1, 1, 1, 1, '{}', 2),
(132, 19, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, '{}', 3),
(133, 19, 'body', 'rich_text_box', 'Body', 0, 1, 1, 1, 1, 1, '{}', 4),
(134, 19, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 5),
(135, 19, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(142, 24, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(143, 24, 'ad_id', 'hidden', 'Ad Id', 1, 1, 1, 1, 1, 1, '{}', 3),
(144, 24, 'base_price', 'number', 'Base Price', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required|integer\"}}', 4),
(145, 24, 'start_date', 'timestamp', 'Start Date', 0, 1, 1, 1, 1, 1, '{}', 5),
(146, 24, 'end_date', 'timestamp', 'End Date', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"date|after_or_equal:now\"}}', 6),
(147, 24, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 7),
(148, 24, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 8),
(149, 24, 'auction_belongsto_ad_relationship', 'relationship', 'Ad', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Ad\",\"table\":\"ads\",\"type\":\"belongsTo\",\"column\":\"ad_id\",\"key\":\"id\",\"label\":\"title\",\"pivot_table\":\"ad_attributes\",\"pivot\":\"0\",\"taggable\":\"0\"}', 2),
(150, 25, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(151, 25, 'user_id', 'hidden', 'User Id', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"}}', 5),
(152, 25, 'price', 'text', 'Price', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"}}', 6),
(153, 25, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 7),
(154, 25, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 8),
(155, 25, 'auction_id', 'hidden', 'Auction Id', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"}}', 3),
(156, 26, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(157, 26, 'from_currency_id', 'hidden', 'From Currency Id', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"}}', 3),
(158, 26, 'to_currency_id', 'hidden', 'To Currency Id', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"}}', 5),
(159, 26, 'buy', 'number', 'Buy', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"}}', 6),
(160, 26, 'sell', 'number', 'Sell', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"}}', 7),
(161, 26, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 8),
(162, 26, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 9),
(163, 26, 'against_currency_belongsto_currency_relationship', 'relationship', 'Currency From', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Currency\",\"table\":\"currencies\",\"type\":\"belongsTo\",\"column\":\"from_currency_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"ad_attributes\",\"pivot\":\"0\",\"taggable\":\"0\"}', 2),
(164, 26, 'against_currency_belongsto_currency_relationship_1', 'relationship', 'Currency To', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Currency\",\"table\":\"currencies\",\"type\":\"belongsTo\",\"column\":\"to_currency_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"ad_attributes\",\"pivot\":\"0\",\"taggable\":\"0\"}', 4),
(165, 27, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(166, 27, 'type', 'text', 'Type', 1, 1, 1, 1, 1, 1, '{}', 2),
(167, 27, 'body', 'text_area', 'Body', 1, 1, 1, 1, 1, 1, '{}', 3),
(168, 27, 'email', 'text', 'Email', 1, 1, 1, 1, 1, 1, '{}', 4),
(169, 27, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 5),
(170, 27, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(171, 28, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(172, 28, 'ad_id', 'hidden', 'Ad Id', 1, 1, 1, 1, 1, 1, '{}', 3),
(173, 28, 'body', 'text', 'Body', 1, 1, 1, 1, 1, 1, '{}', 5),
(174, 28, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 6),
(175, 28, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 7),
(176, 28, 'ad_report_belongsto_ad_relationship', 'relationship', 'Ad Title', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Ad\",\"table\":\"ads\",\"type\":\"belongsTo\",\"column\":\"ad_id\",\"key\":\"id\",\"label\":\"title\",\"pivot_table\":\"ad_attributes\",\"pivot\":\"0\",\"taggable\":\"0\"}', 2),
(177, 28, 'email', 'text', 'Email', 0, 1, 1, 1, 1, 1, '{}', 4),
(178, 16, 'ad_belongsto_user_relationship', 'relationship', 'User', 0, 1, 1, 0, 0, 1, '{\"model\":\"App\\\\User\",\"table\":\"users\",\"type\":\"belongsTo\",\"column\":\"user_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"ad_attributes\",\"pivot\":\"0\",\"taggable\":\"0\"}', 14),
(179, 16, 'ad_belongsto_city_relationship', 'relationship', 'City', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\City\",\"table\":\"cities\",\"type\":\"belongsTo\",\"column\":\"city_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"ad_attributes\",\"pivot\":\"0\",\"taggable\":\"0\"}', 4),
(180, 16, 'ad_belongsto_currency_relationship', 'relationship', 'Currency', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Currency\",\"table\":\"currencies\",\"type\":\"belongsTo\",\"column\":\"currency_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"ad_attributes\",\"pivot\":\"0\",\"taggable\":\"0\"}', 17),
(181, 16, 'ad_belongsto_sub_category_relationship', 'relationship', 'SubCategory', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\SubCategory\",\"table\":\"sub_categories\",\"type\":\"belongsTo\",\"column\":\"sub_category_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"ad_attributes\",\"pivot\":\"0\",\"taggable\":\"0\"}', 7),
(182, 1, 'status', 'checkbox', 'Status', 0, 1, 1, 0, 0, 0, '{\"on\":\"Active\",\"off\":\"Deactive\"}', 15),
(183, 1, 'username', 'text', 'Username', 0, 0, 1, 1, 1, 1, '{}', 16),
(184, 1, 'bio', 'text', 'Bio', 0, 0, 1, 1, 1, 1, '{}', 17),
(186, 29, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(187, 29, 'service_id', 'hidden', 'Service Id', 1, 1, 1, 1, 1, 1, '{}', 3),
(188, 29, 'user_id', 'hidden', 'User Id', 1, 1, 1, 1, 1, 1, '{}', 5),
(189, 29, 'amount', 'text', 'Amount', 1, 1, 1, 1, 1, 1, '{}', 6),
(190, 29, 'currency_id', 'hidden', 'Currency Id', 1, 1, 1, 1, 1, 1, '{}', 8),
(191, 29, 'body', 'text_area', 'Body', 1, 1, 1, 1, 1, 1, '{}', 9),
(192, 29, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 10),
(193, 29, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 11),
(194, 29, 'offer_belongsto_user_relationship', 'relationship', 'users', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\User\",\"table\":\"users\",\"type\":\"belongsTo\",\"column\":\"user_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"ad_attributes\",\"pivot\":\"0\",\"taggable\":\"0\"}', 4),
(195, 29, 'offer_belongsto_service_relationship', 'relationship', 'services', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Service\",\"table\":\"services\",\"type\":\"belongsTo\",\"column\":\"service_id\",\"key\":\"id\",\"label\":\"title\",\"pivot_table\":\"ad_attributes\",\"pivot\":\"0\",\"taggable\":\"0\"}', 2),
(196, 29, 'offer_belongsto_currency_relationship', 'relationship', 'currencies', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Currency\",\"table\":\"currencies\",\"type\":\"belongsTo\",\"column\":\"currency_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"ad_attributes\",\"pivot\":\"0\",\"taggable\":\"0\"}', 7),
(197, 25, 'auction_offer_belongsto_user_relationship', 'relationship', 'User', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\User\",\"table\":\"users\",\"type\":\"belongsTo\",\"column\":\"user_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"ad_attributes\",\"pivot\":\"0\",\"taggable\":\"0\"}', 4),
(198, 25, 'auction_offer_belongsto_auction_relationship', 'relationship', 'Auction', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Auction\",\"table\":\"auctions\",\"type\":\"belongsTo\",\"column\":\"auction_id\",\"key\":\"id\",\"label\":\"ad_title\",\"pivot_table\":\"ad_attributes\",\"pivot\":\"0\",\"taggable\":\"0\"}', 2),
(199, 30, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(200, 30, 'slug', 'text', 'Slug', 1, 1, 1, 1, 1, 1, '{}', 2),
(201, 30, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, '{}', 3),
(202, 30, 'body', 'rich_text_box', 'Body', 0, 1, 1, 1, 1, 1, '{}', 4),
(203, 30, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 6),
(204, 30, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 7),
(205, 30, 'image', 'image', 'Image', 0, 1, 1, 1, 1, 1, '{}', 5),
(206, 31, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(207, 31, 'value', 'number', 'Value', 1, 1, 1, 1, 1, 1, '{}', 4),
(208, 31, 'body', 'text', 'Body', 0, 1, 1, 1, 1, 1, '{}', 5),
(209, 31, 'user_id', 'hidden', 'User Id', 1, 1, 1, 1, 1, 1, '{}', 3),
(210, 31, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 8),
(211, 31, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 9),
(212, 31, 'user_rate_id', 'hidden', 'User Rate Id', 1, 1, 1, 1, 1, 1, '{}', 7),
(213, 31, 'rating_belongsto_user_relationship', 'relationship', 'User', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\User\",\"table\":\"users\",\"type\":\"belongsTo\",\"column\":\"user_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"ad_attributes\",\"pivot\":\"0\",\"taggable\":\"0\"}', 2),
(214, 31, 'rating_belongsto_user_relationship_1', 'relationship', 'User Rater', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\User\",\"table\":\"users\",\"type\":\"belongsTo\",\"column\":\"user_rate_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"ad_attributes\",\"pivot\":\"0\",\"taggable\":\"0\"}', 6),
(215, 8, 'body', 'text_area', 'Body', 1, 1, 1, 1, 1, 1, '{}', 6),
(216, 8, 'ad_id', 'hidden', 'Ad Id', 1, 1, 1, 1, 1, 1, '{\"relationship\":{\"key\":\"id\",\"label\":\"title\"}}', 5),
(217, 8, 'comment_belongsto_ad_relationship', 'relationship', 'ads', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Ad\",\"table\":\"ads\",\"type\":\"belongsTo\",\"column\":\"ad_id\",\"key\":\"id\",\"label\":\"title\",\"pivot_table\":\"ad_attributes\",\"pivot\":\"0\",\"taggable\":\"0\"}', 4),
(218, 1, 'city_id', 'hidden', 'City Id', 0, 1, 1, 1, 1, 1, '{}', 19),
(220, 1, 'user_belongsto_city_relationship_1', 'relationship', 'cities', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\City\",\"table\":\"cities\",\"type\":\"belongsTo\",\"column\":\"city_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"ad_attributes\",\"pivot\":\"0\",\"taggable\":\"0\"}', 18),
(221, 1, 'blocked_date', 'timestamp', 'Blocked Date', 0, 1, 1, 1, 1, 1, '{}', 17);

-- --------------------------------------------------------

--
-- Table structure for table `data_types`
--

CREATE TABLE `data_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT '0',
  `server_side` tinyint(4) NOT NULL DEFAULT '0',
  `details` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_types`
--

INSERT INTO `data_types` (`id`, `name`, `slug`, `display_name_singular`, `display_name_plural`, `icon`, `model_name`, `policy_name`, `controller`, `description`, `generate_permissions`, `server_side`, `details`, `created_at`, `updated_at`) VALUES
(1, 'users', 'users', 'User', 'Users', 'voyager-person', 'TCG\\Voyager\\Models\\User', 'TCG\\Voyager\\Policies\\UserPolicy', 'TCG\\Voyager\\Http\\Controllers\\VoyagerUserController', NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"desc\",\"default_search_key\":null,\"scope\":null}', '2020-01-31 13:04:15', '2020-05-01 11:47:09'),
(2, 'menus', 'menus', 'Menu', 'Menus', 'voyager-list', 'TCG\\Voyager\\Models\\Menu', NULL, '', '', 1, 0, NULL, '2020-01-31 13:04:15', '2020-01-31 13:04:15'),
(3, 'roles', 'roles', 'Role', 'Roles', 'voyager-lock', 'TCG\\Voyager\\Models\\Role', NULL, 'TCG\\Voyager\\Http\\Controllers\\VoyagerRoleController', NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"desc\",\"default_search_key\":null,\"scope\":null}', '2020-01-31 13:04:15', '2020-04-21 20:00:51'),
(4, 'main_categories', 'main-categories', 'Main Category', 'Main Categories', NULL, 'App\\MainCategory', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-01-31 15:51:50', '2020-05-01 12:15:56'),
(5, 'sub_categories', 'sub-categories', 'Sub Category', 'Sub Categories', NULL, 'App\\SubCategory', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-01-31 15:55:19', '2020-05-01 12:15:32'),
(6, 'cities', 'cities', 'City', 'Cities', NULL, 'App\\City', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-01-31 16:54:20', '2020-05-01 12:06:58'),
(7, 'services', 'services', 'Service', 'Services', NULL, 'App\\Service', NULL, 'App\\Http\\Controllers\\VoaygerServicesController', NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":\"serviceUser\"}', '2020-01-31 17:01:30', '2020-04-30 11:04:24'),
(8, 'comments', 'comments', 'Comment', 'Comments', NULL, 'App\\Comment', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":\"commentAds\"}', '2020-02-02 16:28:43', '2020-04-30 15:58:00'),
(9, 'attribute_groups', 'attribute-groups', 'Attribute Group', 'Attribute Groups', NULL, 'App\\AttributeGroup', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-02-04 15:57:44', '2020-05-01 14:44:11'),
(13, 'ad_attributes', 'ad-attributes', 'Ad Attribute', 'Ad Attributes', NULL, 'App\\AdAttribute', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-02-04 17:25:43', '2020-05-01 15:03:51'),
(14, 'attributes', 'attributes', 'Attribute', 'Attributes', NULL, 'App\\Attribute', NULL, 'App\\Http\\Controllers\\AttributesController', NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-02-05 11:09:24', '2020-05-01 15:40:45'),
(15, 'statuses', 'statuses', 'Status', 'Statuses', NULL, 'App\\Status', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-02-09 17:57:36', '2020-02-09 17:57:36'),
(16, 'ads', 'ads', 'Ad', 'Ads', NULL, 'App\\Ad', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":\"adUser\"}', '2020-02-11 17:11:55', '2020-05-01 16:02:09'),
(17, 'units', 'units', 'Unit', 'Units', NULL, 'App\\Unit', NULL, 'App\\Http\\Controllers\\UnitsController', NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-02-15 16:30:38', '2020-02-15 16:48:40'),
(18, 'currencies', 'currencies', 'Currency', 'Currencies', NULL, 'App\\Currency', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-02-21 15:55:18', '2020-02-21 15:55:18'),
(19, 'guidelines', 'guidelines', 'Guideline', 'Guidelines', NULL, 'App\\Guideline', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-03-10 05:22:13', '2020-03-10 05:22:13'),
(24, 'auctions', 'auctions', 'Auction', 'Auctions', NULL, 'App\\Auction', NULL, 'App\\Http\\Controllers\\VoyagerAuctionsController', NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-03-23 07:23:21', '2020-05-01 12:03:32'),
(25, 'auction_offers', 'auction-offers', 'Auction Offer', 'Auction Offers', NULL, 'App\\AuctionOffer', NULL, 'App\\Http\\Controllers\\VoyagerAuctionOffersController', NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-04-06 12:48:55', '2020-05-01 11:53:11'),
(26, 'against_currencies', 'against-currencies', 'Against Currency', 'Against Currencies', NULL, 'App\\AgainstCurrency', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-04-08 07:40:13', '2020-05-01 11:38:09'),
(27, 'messages', 'messages', 'Message', 'Messages', NULL, 'App\\Message', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-04-15 15:07:39', '2020-05-01 12:19:00'),
(28, 'ad_reports', 'ad-reports', 'Ad Report', 'Ad Reports', NULL, 'App\\AdReport', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-04-20 14:25:54', '2020-04-20 14:31:46'),
(29, 'offers', 'offers', 'Offer', 'Offers', 'voyager-activity', 'App\\Offer', NULL, 'App\\Http\\Controllers\\VoaygerOffersController', NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-04-22 05:29:37', '2020-04-22 06:00:41'),
(30, 'pages', 'pages', 'Page', 'Pages', NULL, 'App\\Page', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-04-24 07:15:56', '2020-04-24 07:15:56'),
(31, 'ratings', 'ratings', 'Rating', 'Ratings', NULL, 'App\\Rating', NULL, 'App\\Http\\Controllers\\VoyagerRatingsController', NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":\"rateUser\"}', '2020-04-25 11:40:37', '2020-05-01 12:19:43');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `followers`
--

CREATE TABLE `followers` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `following_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `followers`
--

INSERT INTO `followers` (`id`, `user_id`, `following_id`, `created_at`, `updated_at`) VALUES
(10, 2, 1, NULL, NULL),
(11, 1, 13, NULL, NULL),
(13, 1, 1, NULL, NULL),
(14, 1, 6, NULL, NULL),
(16, 6, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `input_ad_values`
--

CREATE TABLE `input_ad_values` (
  `id` int(10) UNSIGNED NOT NULL,
  `ad_id` int(11) NOT NULL,
  `ad_attribute_id` int(11) NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `unit_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `input_ad_values`
--

INSERT INTO `input_ad_values` (`id`, `ad_id`, `ad_attribute_id`, `value`, `unit_id`) VALUES
(1, 14, 27, 'مع كفر وشاحن لاسلكي', NULL),
(2, 14, 33, '1500', 5),
(3, 15, 27, 'مع كفر وشاحن لاسلكي', NULL),
(4, 15, 33, '1500', 5),
(5, 16, 27, 'مع كفر وشاحن لاسلكي', NULL),
(6, 16, 33, '1500', 5),
(7, 17, 27, 'مع كفر وشاحن لاسلكي', NULL),
(8, 17, 33, '1500', 5),
(9, 18, 12, '2019', NULL),
(10, 18, 13, '1500', NULL),
(11, 19, 27, 'مع كفر وشاحن لاسلكي', NULL),
(12, 19, 33, '1500', 6),
(13, 20, 27, 'شاشة ضد الكسر', NULL),
(14, 20, 33, '5000', 5),
(15, 21, 18, '15933', 3),
(16, 21, 28, 'شارع', NULL),
(17, 22, 18, '15933', 3),
(18, 22, 28, 'شارع', NULL),
(19, 23, 27, 'شاشة ضد الكسر و كفر منيح', NULL),
(20, 23, 33, '1500', 5),
(21, 25, 27, 'شاشة ضد الكسر', NULL),
(22, 25, 33, '2000', 5),
(23, 30, 76, '234245', NULL),
(24, 31, 80, '123', NULL),
(25, 31, 81, '9', NULL),
(26, 32, 80, '3431', NULL),
(27, 32, 81, '88', NULL),
(28, 34, 27, 'لا يوجد', NULL),
(29, 34, 33, '4000', 5),
(30, 35, 18, '1000', 2),
(31, 35, 28, 'على شارع رئيسي', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `main_categories`
--

CREATE TABLE `main_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `main_categories`
--

INSERT INTO `main_categories` (`id`, `name`, `description`, `created_at`, `updated_at`, `icon`) VALUES
(1, 'سيارات ومركبات', 'سيارات ومركبات', '2020-01-31 16:00:00', '2020-02-10 15:59:33', 'main-categories\\February2020\\EQrGvDv1kdxBHS724R28.png'),
(2, 'عقارات للبيع', 'عقارات للبيع', '2020-01-31 16:07:00', '2020-02-10 16:00:08', 'main-categories\\February2020\\OB0CnUOmi7LoVAnTyQpj.png'),
(6, 'اثاث وديكور', 'اثاث وديكور', '2020-01-31 16:42:00', '2020-02-10 16:01:44', 'main-categories\\February2020\\fJHjmIoVN5TH09wrbMTF.png'),
(7, 'اجهزة الكترونية', 'اجهزة الكترونية', '2020-01-31 16:43:00', '2020-02-10 15:56:19', 'main-categories\\February2020\\BmmwfaszwVhK03Uz5p6j.png'),
(8, 'عقارات للايجار', 'عقارات للايجار  عقارات للايجار', '2020-02-04 18:46:00', '2020-02-10 16:03:57', 'main-categories\\February2020\\ueun7Dlqxf39E6rM5I93.png'),
(13, 'قطع مركبات', 'قطع مركباتقطع مركبات قطع مركباتقطع مركبات', '2020-02-06 06:16:00', '2020-02-10 15:34:08', 'main-categories\\February2020\\1IwHD5DzbzUArsf3qGer.png');

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', '2020-01-31 13:04:16', '2020-01-31 13:04:16');

-- --------------------------------------------------------

--
-- Table structure for table `menu_items`
--

CREATE TABLE `menu_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menu_items`
--

INSERT INTO `menu_items` (`id`, `menu_id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `order`, `created_at`, `updated_at`, `route`, `parameters`) VALUES
(1, 1, 'Dashboard', '', '_self', 'voyager-boat', NULL, NULL, 1, '2020-01-31 13:04:16', '2020-01-31 13:04:16', 'voyager.dashboard', NULL),
(2, 1, 'Media', '', '_self', 'voyager-images', NULL, NULL, 4, '2020-01-31 13:04:16', '2020-04-21 20:32:38', 'voyager.media.index', NULL),
(3, 1, 'Users', '', '_self', 'voyager-person', NULL, NULL, 3, '2020-01-31 13:04:16', '2020-01-31 13:04:16', 'voyager.users.index', NULL),
(4, 1, 'Roles', '', '_self', 'voyager-lock', NULL, NULL, 2, '2020-01-31 13:04:17', '2020-01-31 13:04:17', 'voyager.roles.index', NULL),
(5, 1, 'Tools', '', '_self', 'voyager-tools', NULL, NULL, 5, '2020-01-31 13:04:17', '2020-04-21 20:32:38', NULL, NULL),
(6, 1, 'Menu Builder', '', '_self', 'voyager-list', NULL, 5, 1, '2020-01-31 13:04:17', '2020-04-21 20:32:38', 'voyager.menus.index', NULL),
(7, 1, 'Database', '', '_self', 'voyager-data', NULL, 5, 2, '2020-01-31 13:04:17', '2020-04-21 20:32:38', 'voyager.database.index', NULL),
(8, 1, 'Compass', '', '_self', 'voyager-compass', NULL, 5, 3, '2020-01-31 13:04:17', '2020-04-21 20:32:38', 'voyager.compass.index', NULL),
(9, 1, 'BREAD', '', '_self', 'voyager-bread', NULL, 5, 4, '2020-01-31 13:04:17', '2020-04-21 20:32:38', 'voyager.bread.index', NULL),
(10, 1, 'Settings', '', '_self', 'voyager-settings', NULL, NULL, 6, '2020-01-31 13:04:17', '2020-04-21 20:32:38', 'voyager.settings.index', NULL),
(11, 1, 'Hooks', '', '_self', 'voyager-hook', NULL, 5, 5, '2020-01-31 13:04:21', '2020-04-21 20:32:38', 'voyager.hooks', NULL),
(12, 1, 'Main Categories', '', '_self', 'voyager-file-text', '#000000', 35, 1, '2020-01-31 15:51:51', '2020-04-21 20:40:22', 'voyager.main-categories.index', 'null'),
(13, 1, 'Sub Categories', '', '_self', 'voyager-list', '#000000', 35, 2, '2020-01-31 15:55:19', '2020-04-21 20:40:24', 'voyager.sub-categories.index', 'null'),
(14, 1, 'Cities', '', '_self', 'voyager-location', '#000000', NULL, 8, '2020-01-31 16:54:21', '2020-04-21 20:40:24', 'voyager.cities.index', 'null'),
(15, 1, 'Services', '', '_self', 'voyager-browser', '#000000', NULL, 9, '2020-01-31 17:01:30', '2020-04-29 06:12:47', 'voyager.services.index', 'null'),
(16, 1, 'Comments', '', '_self', 'voyager-chat', '#000000', NULL, 11, '2020-02-02 16:28:44', '2020-04-29 06:12:47', 'voyager.comments.index', 'null'),
(17, 1, 'Attribute Groups', '', '_self', 'voyager-params', '#000000', 34, 1, '2020-02-04 15:57:46', '2020-05-01 14:38:08', 'voyager.attribute-groups.index', 'null'),
(19, 1, 'Ad Attributes', '', '_self', 'voyager-megaphone', '#000000', 34, 2, '2020-02-04 17:25:43', '2020-05-01 14:38:14', 'voyager.ad-attributes.index', 'null'),
(20, 1, 'Attributes', '', '_self', 'voyager-pen', '#000000', 34, 3, '2020-02-05 11:09:24', '2020-05-01 14:38:14', 'voyager.attributes.index', 'null'),
(21, 1, 'Statuses', '', '_self', 'voyager-milestone', '#000000', NULL, 19, '2020-02-09 17:57:37', '2020-04-29 06:13:06', 'voyager.statuses.index', 'null'),
(22, 1, 'Ads', '', '_self', 'voyager-megaphone', '#000000', NULL, 13, '2020-02-11 17:11:56', '2020-04-29 06:12:57', 'voyager.ads.index', 'null'),
(23, 1, 'Units', '', '_self', 'voyager-data', '#000000', 34, 4, '2020-02-15 16:30:38', '2020-04-21 20:39:28', 'voyager.units.index', 'null'),
(24, 1, 'Currencies', '', '_self', 'voyager-dollar', '#000000', NULL, 16, '2020-02-21 15:55:19', '2020-04-29 06:13:06', 'voyager.currencies.index', 'null'),
(27, 1, 'Auctions', '', '_self', 'voyager-download', '#000000', 32, 1, '2020-03-23 07:23:22', '2020-04-29 06:13:03', 'voyager.auctions.index', 'null'),
(28, 1, 'Auction Offers', '', '_self', 'voyager-resize-full', '#000000', 32, 2, '2020-04-06 12:48:56', '2020-04-29 06:13:06', 'voyager.auction-offers.index', 'null'),
(29, 1, 'Against Currencies', '', '_self', 'voyager-treasure', '#000000', NULL, 17, '2020-04-08 07:40:14', '2020-04-29 06:13:06', 'voyager.against-currencies.index', 'null'),
(30, 1, 'Messages', '', '_self', 'voyager-mail', '#000000', NULL, 18, '2020-04-15 15:07:40', '2020-04-29 06:13:06', 'voyager.messages.index', 'null'),
(31, 1, 'Ad Reports', '', '_self', 'voyager-bolt', '#000000', NULL, 14, '2020-04-20 14:25:54', '2020-04-29 06:12:57', 'voyager.ad-reports.index', 'null'),
(32, 1, 'Auctions', '', '_self', 'voyager-megaphone', '#000000', NULL, 15, '2020-04-21 20:35:33', '2020-04-29 06:13:25', NULL, ''),
(34, 1, 'Attributes', '', '_self', 'voyager-pen', '#000000', NULL, 12, '2020-04-21 20:38:36', '2020-04-29 06:12:47', NULL, ''),
(35, 1, 'Categories', '', '_self', 'voyager-file-text', '#000000', NULL, 7, '2020-04-21 20:40:08', '2020-04-21 20:40:34', NULL, ''),
(36, 1, 'Service Offers', '', '_self', 'voyager-activity', '#000000', NULL, 10, '2020-04-22 05:29:39', '2020-04-30 05:52:14', 'voyager.offers.index', 'null'),
(37, 1, 'Pages', '', '_self', 'voyager-book', '#000000', NULL, 20, '2020-04-24 07:15:56', '2020-04-29 06:26:28', 'voyager.pages.index', 'null'),
(38, 1, 'Ratings', '', '_self', 'voyager-params', '#000000', NULL, 21, '2020-04-25 11:40:38', '2020-04-29 06:13:06', 'voyager.ratings.index', 'null');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `type`, `body`, `email`, `created_at`, `updated_at`) VALUES
(1, 'مشكلة تقنية', 'في شكلة بالنزام', 'admin@admin.com', '2020-04-15 15:07:00', '2020-04-15 15:07:00');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_01_01_000000_add_voyager_user_fields', 1),
(4, '2016_01_01_000000_create_data_types_table', 1),
(5, '2016_05_19_173453_create_menu_table', 1),
(6, '2016_10_21_190000_create_roles_table', 1),
(7, '2016_10_21_190000_create_settings_table', 1),
(8, '2016_11_30_135954_create_permission_table', 1),
(9, '2016_11_30_141208_create_permission_role_table', 1),
(10, '2016_12_26_201236_data_types__add__server_side', 1),
(11, '2017_01_13_000000_add_route_to_menu_items_table', 1),
(12, '2017_01_14_005015_create_translations_table', 1),
(13, '2017_01_15_000000_make_table_name_nullable_in_permissions_table', 1),
(14, '2017_03_06_000000_add_controller_to_data_types_table', 1),
(15, '2017_04_21_000000_add_order_to_data_rows_table', 1),
(16, '2017_07_05_210000_add_policyname_to_data_types_table', 1),
(17, '2017_08_05_000000_add_group_to_settings_table', 1),
(18, '2017_11_26_013050_add_user_role_relationship', 1),
(19, '2017_11_26_015000_create_user_roles_table', 1),
(20, '2018_03_11_000000_add_user_settings', 1),
(21, '2018_03_14_000000_add_details_to_data_types_table', 1),
(22, '2018_03_16_000000_make_settings_value_nullable', 1),
(23, '2019_08_19_000000_create_failed_jobs_table', 1),
(24, '2020_04_24_121548_create_social_facebook_accounts_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `offers`
--

CREATE TABLE `offers` (
  `id` int(10) UNSIGNED NOT NULL,
  `service_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `amount` int(10) UNSIGNED NOT NULL,
  `currency_id` int(10) UNSIGNED NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `offers`
--

INSERT INTO `offers` (`id`, `service_id`, `user_id`, `amount`, `currency_id`, `body`, `created_at`, `updated_at`) VALUES
(1, 15, 1, 12, 1, 'sf sdf sdf sdf df', '2020-02-28 12:30:47', '2020-02-28 12:30:47'),
(2, 15, 1, 34, 1, 'xd d fsdf sdfsdf', '2020-02-28 12:31:12', '2020-02-28 12:31:12'),
(3, 15, 1, 12, 1, 'ابدأ ببناء مشروعك ابدأ ببناء مشروعك\r\nابدأ ببناء مشروعك\r\nابدأ ببناء مشروعك\r\n\r\nابدأ ببناء مشروعك\r\nابدأ ببناء مشروعكابدأ ببناء مشروعك ابدأ ببناء مشروعك\r\nابدأ ببناء مشروعك\r\nابدأ ببناء مشروعك\r\n\r\nابدأ ببناء مشروعك\r\nابدأ ببناء مشروعك', '2020-02-28 13:06:33', '2020-02-28 13:06:33'),
(4, 11, 1, 123, 1, 'من خلال مستقل تستطيع بناء مشروعك بالشكل الذي تريد، أدخل تفاصيل المشروع والميزانية المتوقعة، ليتم مراجعة مشروعك ونشره مجاناً على مستقل. بعد ذلك، سيتقدّم أفضل المستقلين المسجلين بعروض مختلفة لتختار العرض المناسب لك وتبدأ بتنفيذ مشروعك.', '2020-02-28 14:38:50', '2020-02-28 14:38:50'),
(5, 11, 1, 156, 2, 'من خلال مستقل تستطيع بناء مشروعك بالشكل الذي تريد، أدخل تفاصيل المشروع والميزانية المتوقعة، ليتم مرا', '2020-02-28 14:42:11', '2020-02-28 14:42:11'),
(6, 18, 1, 17000, 1, 'من خلال مستقل تستطيع بناء مشروعك بالشكل الذي تريد، أدخل تفاصيل المشروع والميزانية المتوقعة، ليتم مراجعة مشروعك ونشره مجاناً على', '2020-03-03 08:26:52', '2020-03-03 08:26:52'),
(7, 18, 1, 100, 1, 'fdxdf dd xfs  s asasasda sd asd a', '2020-04-22 07:10:57', '2020-04-22 07:10:57');

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `slug`, `title`, `body`, `created_at`, `updated_at`, `image`) VALUES
(1, 'ads.index', 'الصفحة  الخاصة بعرض وتصفح المعلومات', '<p style=\"text-align: center;\"><span style=\"color: #000000;\">تستطيع البحث عن الاعلانات التي تريدها والتواصل مع اصحابها، كما ويمكنك فرز تلك الاعلانات من خلال قائمة البحث</span></p>', '2020-03-10 08:55:00', '2020-04-24 07:31:49', NULL),
(2, 'about-as', 'من نحن ؟', '<p style=\"text-align: justify; font-size: 16px;\">هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها. ولذلك يتم استخدام طريقة لوريم إيبسوم لأنها تعطي توزيعاَ طبيعياَ -إلى حد ما- للأحرف عوضاً عن استخدام \"هنا يوجد محتوى نصي، هنا يوجد محتوى نصي\" فتجعلها تبدو (أي الأحرف) وكأنها نص مقروء. العديد من برامح النشر المكتبي وبرامح تحرير صفحات الويب تستخدم لوريم إيبسوم بشكل إفتراضي كنموذج عن النص، وإذا قمت بإدخال \"lorem ipsum\" في أي محرك بحث ستظهر العديد من المواقع الحديثة العهد في نتائج البحث. على مدى السنين ظهرت نسخ جديدة ومختلفة من نص لوريم إيبسوم، أحياناً عن طريق الصدفة، وأحياناً عن عمد كإدخال بعض العبارات الفكاهية إليها.</p>', '2020-04-24 07:48:00', '2020-04-24 08:01:18', 'pages\\April2020\\cjvsGO09Uf7tgf4Jrppd.png'),
(3, 'privacy', 'سياسة الخصوصية والاحكام', '<p>&nbsp;</p>\r\n<h2>الخصوصية وبيان سريّة المعلومات</h2>\r\n<p>&nbsp;</p>\r\n<p>نقدر مخاوفكم واهتمامكم بشأن خصوصية بياناتكم على شبكة الإنترنت.</p>\r\n<p>لقد تم إعداد هذه السياسة لمساعدتكم في تفهم طبيعة البيانات التي نقوم بتجميعها منكم عند زيارتكم لموقعنا على شبكة الانترنت وكيفية تعاملنا مع هذه البيانات الشخصية.</p>\r\n<p>&nbsp;</p>\r\n<p><strong>التصفح</strong></p>\r\n<p>لم نقم بتصميم هذا الموقع من أجل تجميع بياناتك الشخصية من جهاز الكمبيوتر الخاص بك أثناء تصفحك لهذا الموقع، وإنما سيتم فقط استخدام البيانات المقدمة من قبلك بمعرفتك ومحض إرادتك.</p>\r\n<p>&nbsp;</p>\r\n<p><strong>عنوان بروتوكول شبكة الإنترنت (IP)</strong></p>\r\n<p>&nbsp; في أي وقت تزور فيه اي موقع انترنت بما فيها هذا الموقع , سيقوم السيرفر المضيف بتسجيل عنوان بروتوكول شبكة الإنترنت&nbsp; &nbsp;(IP) الخاص بك , تاريخ ووقت الزيارة ونوع متصفح الإنترنت الذي تستخدمه والعنوان URL الخاص بأي موقع من مواقع الإنترنت التي تقوم بإحالتك إلى الى هذا الموقع على الشبكة.</p>\r\n<p>&nbsp;</p>\r\n<p><strong>عمليات المسح على الشبكة</strong></p>\r\n<p>إن عمليات المسح التي نقوم بها مباشرة على الشبكة تمكننا من تجميع بيانات محددة مثل البيانات المطلوبة منك بخصوص نظرتك وشعورك تجاه موقعنا. تعتبر ردودك ذات أهمية قصوى , ومحل تقديرنا حيث أنها تمكننا من تحسين مستوى موقعنا, ولك كامل الحرية والاختيار في تقديم البيانات المتعلقة باسمك والبيانات الأخرى.</p>\r\n<p>&nbsp;</p>\r\n<p><strong>الروابط بالمواقع الأخرى على شبكة الإنترنت</strong></p>\r\n<p>قد يشتمل موقعنا على روابط بالمواقع الأخرى على شبكة الإنترنت. او اعلانات من مواقع اخرى مثل Google AdSense ولا نعتبر مسئولين عن أساليب تجميع البيانات من قبل تلك المواقع، يمكنك الاطلاع على سياسات السرية والمحتويات الخاصة بتلك المواقع التي يتم الدخول إليها من خلال أي رابط ضمن هذا الموقع.</p>\r\n<p>&nbsp;</p>\r\n<p>نحن قد نستعين بشركات إعلان لأطراف ثالثة لعرض الإعلانات عندما تزور موقعنا على الويب. يحق لهذه الشركات أن تستخدم معلومات حول زياراتك لهذا الموقع ولمواقع الويب الأخرى (باستثناء الاسم أو العنوان أو عنوان البريد الإلكتروني أو رقم الهاتف)، وذلك من أجل تقديم إعلانات حول البضائع والخدمات التي تهمك. إذا كنت ترغب في مزيد من المعلومات حول هذا الأمر وكذلك إذا كنت تريد معرفة الاختيارات المتاحة لك لمنع استخدام هذه المعلومات من قِبل هذه الشركات، فالرجاء النقر هنا.</p>\r\n<p>&nbsp;</p>\r\n<p><strong>إفشاء المعلومات</strong></p>\r\n<p>سنحافظ في كافة الأوقات على خصوصية وسرية كافة البيانات الشخصية التي نتحصل عليها. ولن يتم إفشاء هذه المعلومات إلا إذا كان ذلك مطلوباً بموجب أي قانون أو عندما نعتقد بحسن نية أن مثل هذا الإجراء سيكون مطلوباً أو مرغوباً فيه للتمشي مع القانون , أو للدفاع عن أو حماية حقوق الملكية الخاصة بهذا الموقع أو الجهات المستفيدة منه.</p>\r\n<p>&nbsp;</p>\r\n<p><strong>البيانات اللازمة لتنفيذ المعاملات المطلوبة من قبلك</strong></p>\r\n<p>عندما نحتاج إلى أية بيانات خاصة بك , فإننا سنطلب منك تقديمها بمحض إرادتك. حيث ستساعدنا هذه المعلومات في الاتصال بك وتنفيذ طلباتك حيثما كان ذلك ممكنناً. لن يتم اطلاقاً بيع البيانات المقدمة من قبلك إلى أي طرف ثالث بغرض تسويقها لمصلحته الخاصة دون الحصول على موافقتك المسبقة والمكتوبة ما لم يتم ذلك على أساس أنها ضمن بيانات جماعية تستخدم للأغراض الإحصائية والأبحاث دون اشتمالها على أية بيانات من الممكن استخدامها للتعريف بك.</p>\r\n<p>&nbsp;</p>\r\n<p><strong>عند الاتصال بنا</strong></p>\r\n<p>سيتم التعامل مع كافة البيانات المقدمة من قبلك على أساس أنها سرية. تتطلب النماذج التي يتم تقديمها مباشرة على الشبكة تقديم البيانات التي ستساعدنا في تحسين موقعنا. سيتم استخدام البيانات التي يتم تقديمها من قبلك في الرد على كافة استفساراتك , ملاحظاتك , أو طلباتك من قبل هذا الموقع أو أيا من المواقع التابعة له .</p>\r\n<p>&nbsp;</p>\r\n<p><strong>إفشاء المعلومات لأي طرف ثالث</strong></p>\r\n<p>لن نقوم ببيع، المتاجرة، تأجير، أو إفشاء أية معلومات لمصلحة أي طرف ثالث خارج هذا الموقع، أو المواقع التابعة له. وسيتم الكشف عن المعلومات فقط في حالة صدور أمر بذلك من قبل أي سلطة قضائية أو تنظيمية.</p>\r\n<p>&nbsp;</p>\r\n<p><strong>التعديلات على سياسة سرية وخصوصية المعلومات</strong></p>\r\n<p>نحتفظ بالحق في تعديل بنود وشروط سياسة سرية وخصوصية المعلومات إن لزم الأمر ومتى كان ذلك ملائماً. سيتم تنفيذ التعديلات هنا او على صفحة سياسة الخصوصية الرئيسية وسيتم بصفة مستمرة إخطارك بالبيانات التي حصلنا عليها , وكيف سنستخدمها والجهة التي سنقوم بتزويدها بهذه البيانات.</p>\r\n<p>&nbsp;</p>\r\n<p><strong>الاتصال بنا</strong></p>\r\n<p>يمكنكم الاتصال بنا عند الحاجة من خلال الضغط على رابط اتصل بنا المتوفر في روابط موقعنا او الارسال الى بريدنا الالكتروني info على اسم النطاق اعلاه</p>\r\n<p>&nbsp;</p>\r\n<p><strong>اخيرا</strong></p>\r\n<p>إن مخاوفك واهتمامك بشأن سرية وخصوصية البيانات تعتبر مسألة في غاية الأهمية بالنسبة لنا. نحن نأمل أن يتم تحقيق ذلك من خلال هذه السياسة.</p>\r\n<p>&nbsp;</p>\r\n<h2>تحيات فريق عمل موقع كراكيب</h2>', '2020-04-24 14:19:00', '2020-04-24 14:19:00', NULL),
(4, 'user.welcome', 'اهلا وسهلا بك في موقع كراكيب', '<p>موقع كراركيب برحيب فيك وفي اهلك ، شرفتنا بتسجيلك بالموقع يا جميل ، ان شاء الله يعجبك ويسبطك ، ترا احنا مناح كثير ، رحن نخدمك باللي بنقدر عليه ، المهم لا تجيب سيرتنا بالعاطل ، في اشياء حلوة عنا بتقدر تدور عليها ، وان شاء الله يعجبك ، واذا ما عجبك بلاش ، بتقدر ترسلنا رسالة ، وما تضل تنق علينا</p>\r\n<p>تحيايتي</p>', '2020-04-25 08:32:20', '2020-04-25 08:32:20', NULL),
(5, 'ad-accept', 'تم قبول الاعلان الخاص بك', '<p>بعد الاطلاع على الاعلان وتفاصيله وموافقته للشروط والمعايير تم قبول اعلانك ، يمكن لجميع المستخدمين رؤيته</p>', '2020-04-29 15:03:00', '2020-04-30 09:07:14', NULL),
(6, 'service-accept', 'تم قبول طلب الخدمة الخاص بك', '<p>بعد الاطلاع على طلب الخدمة وتفاصيله وموافقته للشروط والمعايير تم قبول طلب الخدمة ، يمكن لجميع المستخدمين رؤيته</p>', '2020-04-30 09:09:52', '2020-04-30 09:09:52', NULL),
(7, 'block-user', 'نعتذر عن الازعاج فقد تم حظر', '<p>بعد الاطلاع على البلاغات التي صدرت حولك فقد تم حظرك من استخدام موقع كراركيب</p>', '2020-05-01 14:23:11', '2020-05-01 14:23:11', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`) VALUES
(1, 'browse_admin', NULL, '2020-01-31 13:04:17', '2020-01-31 13:04:17'),
(2, 'browse_bread', NULL, '2020-01-31 13:04:17', '2020-01-31 13:04:17'),
(3, 'browse_database', NULL, '2020-01-31 13:04:17', '2020-01-31 13:04:17'),
(4, 'browse_media', NULL, '2020-01-31 13:04:17', '2020-01-31 13:04:17'),
(5, 'browse_compass', NULL, '2020-01-31 13:04:17', '2020-01-31 13:04:17'),
(6, 'browse_menus', 'menus', '2020-01-31 13:04:18', '2020-01-31 13:04:18'),
(7, 'read_menus', 'menus', '2020-01-31 13:04:18', '2020-01-31 13:04:18'),
(8, 'edit_menus', 'menus', '2020-01-31 13:04:18', '2020-01-31 13:04:18'),
(9, 'add_menus', 'menus', '2020-01-31 13:04:18', '2020-01-31 13:04:18'),
(10, 'delete_menus', 'menus', '2020-01-31 13:04:18', '2020-01-31 13:04:18'),
(11, 'browse_roles', 'roles', '2020-01-31 13:04:18', '2020-01-31 13:04:18'),
(12, 'read_roles', 'roles', '2020-01-31 13:04:18', '2020-01-31 13:04:18'),
(13, 'edit_roles', 'roles', '2020-01-31 13:04:18', '2020-01-31 13:04:18'),
(14, 'add_roles', 'roles', '2020-01-31 13:04:18', '2020-01-31 13:04:18'),
(15, 'delete_roles', 'roles', '2020-01-31 13:04:18', '2020-01-31 13:04:18'),
(16, 'browse_users', 'users', '2020-01-31 13:04:18', '2020-01-31 13:04:18'),
(17, 'read_users', 'users', '2020-01-31 13:04:18', '2020-01-31 13:04:18'),
(18, 'edit_users', 'users', '2020-01-31 13:04:18', '2020-01-31 13:04:18'),
(19, 'add_users', 'users', '2020-01-31 13:04:18', '2020-01-31 13:04:18'),
(20, 'delete_users', 'users', '2020-01-31 13:04:18', '2020-01-31 13:04:18'),
(21, 'browse_settings', 'settings', '2020-01-31 13:04:18', '2020-01-31 13:04:18'),
(22, 'read_settings', 'settings', '2020-01-31 13:04:19', '2020-01-31 13:04:19'),
(23, 'edit_settings', 'settings', '2020-01-31 13:04:19', '2020-01-31 13:04:19'),
(24, 'add_settings', 'settings', '2020-01-31 13:04:19', '2020-01-31 13:04:19'),
(25, 'delete_settings', 'settings', '2020-01-31 13:04:19', '2020-01-31 13:04:19'),
(26, 'browse_hooks', NULL, '2020-01-31 13:04:21', '2020-01-31 13:04:21'),
(27, 'browse_main_categories', 'main_categories', '2020-01-31 15:51:50', '2020-01-31 15:51:50'),
(28, 'read_main_categories', 'main_categories', '2020-01-31 15:51:50', '2020-01-31 15:51:50'),
(29, 'edit_main_categories', 'main_categories', '2020-01-31 15:51:50', '2020-01-31 15:51:50'),
(30, 'add_main_categories', 'main_categories', '2020-01-31 15:51:50', '2020-01-31 15:51:50'),
(31, 'delete_main_categories', 'main_categories', '2020-01-31 15:51:51', '2020-01-31 15:51:51'),
(32, 'browse_sub_categories', 'sub_categories', '2020-01-31 15:55:19', '2020-01-31 15:55:19'),
(33, 'read_sub_categories', 'sub_categories', '2020-01-31 15:55:19', '2020-01-31 15:55:19'),
(34, 'edit_sub_categories', 'sub_categories', '2020-01-31 15:55:19', '2020-01-31 15:55:19'),
(35, 'add_sub_categories', 'sub_categories', '2020-01-31 15:55:19', '2020-01-31 15:55:19'),
(36, 'delete_sub_categories', 'sub_categories', '2020-01-31 15:55:19', '2020-01-31 15:55:19'),
(37, 'browse_cities', 'cities', '2020-01-31 16:54:21', '2020-01-31 16:54:21'),
(38, 'read_cities', 'cities', '2020-01-31 16:54:21', '2020-01-31 16:54:21'),
(39, 'edit_cities', 'cities', '2020-01-31 16:54:21', '2020-01-31 16:54:21'),
(40, 'add_cities', 'cities', '2020-01-31 16:54:21', '2020-01-31 16:54:21'),
(41, 'delete_cities', 'cities', '2020-01-31 16:54:21', '2020-01-31 16:54:21'),
(42, 'browse_services', 'services', '2020-01-31 17:01:30', '2020-01-31 17:01:30'),
(43, 'read_services', 'services', '2020-01-31 17:01:30', '2020-01-31 17:01:30'),
(44, 'edit_services', 'services', '2020-01-31 17:01:30', '2020-01-31 17:01:30'),
(45, 'add_services', 'services', '2020-01-31 17:01:30', '2020-01-31 17:01:30'),
(46, 'delete_services', 'services', '2020-01-31 17:01:30', '2020-01-31 17:01:30'),
(47, 'browse_comments', 'comments', '2020-02-02 16:28:44', '2020-02-02 16:28:44'),
(48, 'read_comments', 'comments', '2020-02-02 16:28:44', '2020-02-02 16:28:44'),
(49, 'edit_comments', 'comments', '2020-02-02 16:28:44', '2020-02-02 16:28:44'),
(50, 'add_comments', 'comments', '2020-02-02 16:28:44', '2020-02-02 16:28:44'),
(51, 'delete_comments', 'comments', '2020-02-02 16:28:44', '2020-02-02 16:28:44'),
(52, 'browse_attribute_groups', 'attribute_groups', '2020-02-04 15:57:45', '2020-02-04 15:57:45'),
(53, 'read_attribute_groups', 'attribute_groups', '2020-02-04 15:57:45', '2020-02-04 15:57:45'),
(54, 'edit_attribute_groups', 'attribute_groups', '2020-02-04 15:57:45', '2020-02-04 15:57:45'),
(55, 'add_attribute_groups', 'attribute_groups', '2020-02-04 15:57:45', '2020-02-04 15:57:45'),
(56, 'delete_attribute_groups', 'attribute_groups', '2020-02-04 15:57:45', '2020-02-04 15:57:45'),
(57, 'browse_add_attributes', 'add_attributes', '2020-02-04 15:58:03', '2020-02-04 15:58:03'),
(58, 'read_add_attributes', 'add_attributes', '2020-02-04 15:58:03', '2020-02-04 15:58:03'),
(59, 'edit_add_attributes', 'add_attributes', '2020-02-04 15:58:03', '2020-02-04 15:58:03'),
(60, 'add_add_attributes', 'add_attributes', '2020-02-04 15:58:03', '2020-02-04 15:58:03'),
(61, 'delete_add_attributes', 'add_attributes', '2020-02-04 15:58:03', '2020-02-04 15:58:03'),
(62, 'browse_ad_attributes', 'ad_attributes', '2020-02-04 17:25:43', '2020-02-04 17:25:43'),
(63, 'read_ad_attributes', 'ad_attributes', '2020-02-04 17:25:43', '2020-02-04 17:25:43'),
(64, 'edit_ad_attributes', 'ad_attributes', '2020-02-04 17:25:43', '2020-02-04 17:25:43'),
(65, 'add_ad_attributes', 'ad_attributes', '2020-02-04 17:25:43', '2020-02-04 17:25:43'),
(66, 'delete_ad_attributes', 'ad_attributes', '2020-02-04 17:25:43', '2020-02-04 17:25:43'),
(67, 'browse_attributes', 'attributes', '2020-02-05 11:09:24', '2020-02-05 11:09:24'),
(68, 'read_attributes', 'attributes', '2020-02-05 11:09:24', '2020-02-05 11:09:24'),
(69, 'edit_attributes', 'attributes', '2020-02-05 11:09:24', '2020-02-05 11:09:24'),
(70, 'add_attributes', 'attributes', '2020-02-05 11:09:24', '2020-02-05 11:09:24'),
(71, 'delete_attributes', 'attributes', '2020-02-05 11:09:24', '2020-02-05 11:09:24'),
(72, 'browse_statuses', 'statuses', '2020-02-09 17:57:36', '2020-02-09 17:57:36'),
(73, 'read_statuses', 'statuses', '2020-02-09 17:57:36', '2020-02-09 17:57:36'),
(74, 'edit_statuses', 'statuses', '2020-02-09 17:57:36', '2020-02-09 17:57:36'),
(75, 'add_statuses', 'statuses', '2020-02-09 17:57:36', '2020-02-09 17:57:36'),
(76, 'delete_statuses', 'statuses', '2020-02-09 17:57:36', '2020-02-09 17:57:36'),
(77, 'browse_ads', 'ads', '2020-02-11 17:11:55', '2020-02-11 17:11:55'),
(78, 'read_ads', 'ads', '2020-02-11 17:11:55', '2020-02-11 17:11:55'),
(79, 'edit_ads', 'ads', '2020-02-11 17:11:55', '2020-02-11 17:11:55'),
(80, 'add_ads', 'ads', '2020-02-11 17:11:55', '2020-02-11 17:11:55'),
(81, 'delete_ads', 'ads', '2020-02-11 17:11:55', '2020-02-11 17:11:55'),
(82, 'browse_units', 'units', '2020-02-15 16:30:38', '2020-02-15 16:30:38'),
(83, 'read_units', 'units', '2020-02-15 16:30:38', '2020-02-15 16:30:38'),
(84, 'edit_units', 'units', '2020-02-15 16:30:38', '2020-02-15 16:30:38'),
(85, 'add_units', 'units', '2020-02-15 16:30:38', '2020-02-15 16:30:38'),
(86, 'delete_units', 'units', '2020-02-15 16:30:38', '2020-02-15 16:30:38'),
(87, 'browse_currencies', 'currencies', '2020-02-21 15:55:18', '2020-02-21 15:55:18'),
(88, 'read_currencies', 'currencies', '2020-02-21 15:55:18', '2020-02-21 15:55:18'),
(89, 'edit_currencies', 'currencies', '2020-02-21 15:55:18', '2020-02-21 15:55:18'),
(90, 'add_currencies', 'currencies', '2020-02-21 15:55:18', '2020-02-21 15:55:18'),
(91, 'delete_currencies', 'currencies', '2020-02-21 15:55:18', '2020-02-21 15:55:18'),
(92, 'browse_guidelines', 'guidelines', '2020-03-10 05:22:13', '2020-03-10 05:22:13'),
(93, 'read_guidelines', 'guidelines', '2020-03-10 05:22:13', '2020-03-10 05:22:13'),
(94, 'edit_guidelines', 'guidelines', '2020-03-10 05:22:13', '2020-03-10 05:22:13'),
(95, 'add_guidelines', 'guidelines', '2020-03-10 05:22:13', '2020-03-10 05:22:13'),
(96, 'delete_guidelines', 'guidelines', '2020-03-10 05:22:13', '2020-03-10 05:22:13'),
(102, 'browse_auctions', 'auctions', '2020-03-23 07:23:21', '2020-03-23 07:23:21'),
(103, 'read_auctions', 'auctions', '2020-03-23 07:23:21', '2020-03-23 07:23:21'),
(104, 'edit_auctions', 'auctions', '2020-03-23 07:23:21', '2020-03-23 07:23:21'),
(105, 'add_auctions', 'auctions', '2020-03-23 07:23:21', '2020-03-23 07:23:21'),
(106, 'delete_auctions', 'auctions', '2020-03-23 07:23:21', '2020-03-23 07:23:21'),
(107, 'browse_auction_offers', 'auction_offers', '2020-04-06 12:48:55', '2020-04-06 12:48:55'),
(108, 'read_auction_offers', 'auction_offers', '2020-04-06 12:48:56', '2020-04-06 12:48:56'),
(109, 'edit_auction_offers', 'auction_offers', '2020-04-06 12:48:56', '2020-04-06 12:48:56'),
(110, 'add_auction_offers', 'auction_offers', '2020-04-06 12:48:56', '2020-04-06 12:48:56'),
(111, 'delete_auction_offers', 'auction_offers', '2020-04-06 12:48:56', '2020-04-06 12:48:56'),
(112, 'browse_against_currencies', 'against_currencies', '2020-04-08 07:40:14', '2020-04-08 07:40:14'),
(113, 'read_against_currencies', 'against_currencies', '2020-04-08 07:40:14', '2020-04-08 07:40:14'),
(114, 'edit_against_currencies', 'against_currencies', '2020-04-08 07:40:14', '2020-04-08 07:40:14'),
(115, 'add_against_currencies', 'against_currencies', '2020-04-08 07:40:14', '2020-04-08 07:40:14'),
(116, 'delete_against_currencies', 'against_currencies', '2020-04-08 07:40:14', '2020-04-08 07:40:14'),
(117, 'browse_messages', 'messages', '2020-04-15 15:07:40', '2020-04-15 15:07:40'),
(118, 'read_messages', 'messages', '2020-04-15 15:07:40', '2020-04-15 15:07:40'),
(119, 'edit_messages', 'messages', '2020-04-15 15:07:40', '2020-04-15 15:07:40'),
(120, 'add_messages', 'messages', '2020-04-15 15:07:40', '2020-04-15 15:07:40'),
(121, 'delete_messages', 'messages', '2020-04-15 15:07:40', '2020-04-15 15:07:40'),
(122, 'browse_ad_reports', 'ad_reports', '2020-04-20 14:25:54', '2020-04-20 14:25:54'),
(123, 'read_ad_reports', 'ad_reports', '2020-04-20 14:25:54', '2020-04-20 14:25:54'),
(124, 'edit_ad_reports', 'ad_reports', '2020-04-20 14:25:54', '2020-04-20 14:25:54'),
(125, 'add_ad_reports', 'ad_reports', '2020-04-20 14:25:54', '2020-04-20 14:25:54'),
(126, 'delete_ad_reports', 'ad_reports', '2020-04-20 14:25:54', '2020-04-20 14:25:54'),
(127, 'browse_offers', 'offers', '2020-04-22 05:29:38', '2020-04-22 05:29:38'),
(128, 'read_offers', 'offers', '2020-04-22 05:29:38', '2020-04-22 05:29:38'),
(129, 'edit_offers', 'offers', '2020-04-22 05:29:38', '2020-04-22 05:29:38'),
(130, 'add_offers', 'offers', '2020-04-22 05:29:38', '2020-04-22 05:29:38'),
(131, 'delete_offers', 'offers', '2020-04-22 05:29:38', '2020-04-22 05:29:38'),
(132, 'browse_pages', 'pages', '2020-04-24 07:15:56', '2020-04-24 07:15:56'),
(133, 'read_pages', 'pages', '2020-04-24 07:15:56', '2020-04-24 07:15:56'),
(134, 'edit_pages', 'pages', '2020-04-24 07:15:56', '2020-04-24 07:15:56'),
(135, 'add_pages', 'pages', '2020-04-24 07:15:56', '2020-04-24 07:15:56'),
(136, 'delete_pages', 'pages', '2020-04-24 07:15:56', '2020-04-24 07:15:56'),
(137, 'browse_ratings', 'ratings', '2020-04-25 11:40:37', '2020-04-25 11:40:37'),
(138, 'read_ratings', 'ratings', '2020-04-25 11:40:38', '2020-04-25 11:40:38'),
(139, 'edit_ratings', 'ratings', '2020-04-25 11:40:38', '2020-04-25 11:40:38'),
(140, 'add_ratings', 'ratings', '2020-04-25 11:40:38', '2020-04-25 11:40:38'),
(141, 'delete_ratings', 'ratings', '2020-04-25 11:40:38', '2020-04-25 11:40:38');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(1, 2),
(1, 3),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(16, 3),
(17, 1),
(17, 3),
(18, 1),
(18, 3),
(19, 1),
(19, 3),
(20, 1),
(21, 1),
(21, 3),
(22, 1),
(22, 3),
(23, 1),
(23, 3),
(24, 1),
(24, 3),
(25, 1),
(25, 3),
(26, 1),
(27, 1),
(27, 3),
(28, 1),
(28, 3),
(29, 1),
(29, 3),
(30, 1),
(30, 3),
(31, 1),
(31, 3),
(32, 1),
(32, 3),
(33, 1),
(33, 3),
(34, 1),
(34, 3),
(35, 1),
(35, 3),
(36, 1),
(36, 3),
(37, 1),
(37, 3),
(38, 1),
(38, 3),
(39, 1),
(39, 3),
(40, 1),
(40, 3),
(41, 1),
(41, 3),
(42, 1),
(42, 2),
(42, 3),
(43, 1),
(43, 2),
(43, 3),
(44, 1),
(44, 2),
(45, 1),
(45, 2),
(45, 3),
(46, 1),
(46, 2),
(47, 1),
(47, 2),
(47, 3),
(48, 1),
(48, 2),
(48, 3),
(49, 1),
(50, 1),
(50, 3),
(51, 1),
(51, 2),
(52, 1),
(52, 3),
(53, 1),
(53, 3),
(54, 1),
(54, 3),
(55, 1),
(55, 3),
(56, 1),
(56, 3),
(57, 1),
(57, 3),
(58, 1),
(58, 3),
(59, 1),
(59, 3),
(60, 1),
(60, 3),
(61, 1),
(61, 3),
(62, 1),
(62, 3),
(63, 1),
(63, 3),
(64, 1),
(64, 3),
(65, 1),
(65, 3),
(66, 1),
(66, 3),
(67, 1),
(67, 3),
(68, 1),
(68, 3),
(69, 1),
(69, 3),
(70, 1),
(70, 3),
(71, 1),
(71, 3),
(72, 1),
(73, 1),
(74, 1),
(75, 1),
(76, 1),
(77, 1),
(77, 2),
(77, 3),
(78, 1),
(78, 2),
(78, 3),
(79, 1),
(79, 2),
(80, 1),
(80, 2),
(80, 3),
(81, 1),
(81, 2),
(82, 1),
(82, 3),
(83, 1),
(83, 3),
(84, 1),
(84, 3),
(85, 1),
(85, 3),
(86, 1),
(86, 3),
(87, 1),
(87, 3),
(88, 1),
(88, 3),
(89, 1),
(89, 3),
(90, 1),
(90, 3),
(91, 1),
(91, 3),
(92, 1),
(93, 1),
(94, 1),
(95, 1),
(96, 1),
(102, 1),
(102, 2),
(102, 3),
(103, 1),
(103, 2),
(103, 3),
(104, 1),
(104, 2),
(105, 1),
(105, 2),
(105, 3),
(106, 1),
(106, 2),
(107, 1),
(107, 2),
(107, 3),
(108, 1),
(108, 2),
(108, 3),
(109, 1),
(110, 1),
(111, 1),
(112, 1),
(112, 3),
(113, 1),
(113, 3),
(114, 1),
(114, 3),
(115, 1),
(115, 3),
(116, 1),
(116, 3),
(117, 1),
(117, 3),
(118, 1),
(118, 3),
(119, 1),
(119, 3),
(120, 1),
(120, 3),
(121, 1),
(121, 3),
(122, 1),
(122, 3),
(123, 1),
(123, 3),
(124, 1),
(124, 3),
(125, 1),
(125, 3),
(126, 1),
(126, 3),
(127, 1),
(127, 2),
(127, 3),
(128, 1),
(128, 2),
(128, 3),
(129, 1),
(129, 2),
(129, 3),
(130, 1),
(130, 2),
(131, 1),
(131, 2),
(132, 1),
(132, 3),
(133, 1),
(133, 3),
(134, 1),
(134, 3),
(135, 1),
(135, 3),
(136, 1),
(136, 3),
(137, 1),
(137, 2),
(137, 3),
(138, 1),
(138, 2),
(138, 3),
(139, 1),
(140, 1),
(140, 3),
(141, 1),
(141, 3);

-- --------------------------------------------------------

--
-- Table structure for table `ratings`
--

CREATE TABLE `ratings` (
  `id` int(10) UNSIGNED NOT NULL,
  `value` int(10) UNSIGNED NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_rate_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ratings`
--

INSERT INTO `ratings` (`id`, `value`, `body`, `user_id`, `created_at`, `updated_at`, `user_rate_id`) VALUES
(1, 5, 'نص نص نص نصن', 1, '2020-04-25 11:45:07', '2020-04-25 11:45:07', 14),
(2, 5, NULL, 1, '2020-04-25 11:46:00', '2020-04-25 11:59:23', 1),
(3, 1, 'نص نص نص نصن', 1, '2020-04-25 11:47:00', '2020-04-25 14:46:12', 1),
(4, 5, 'sdasdasa dasd as', 1, '2020-04-25 17:07:23', '2020-04-25 17:07:23', 1),
(5, 3, 'sfsdsdf sdfsf sdfsdf s', 1, '2020-04-25 17:09:00', '2020-04-25 17:09:00', 1),
(6, 1, 'dfdsfs', 1, '2020-04-25 17:32:13', '2020-04-25 17:32:13', 1),
(7, 4, 'شخصية عظيمة', 6, '2020-04-29 07:45:11', '2020-04-29 07:45:11', 1),
(8, 4, 'يا وحش', 6, '2020-04-29 07:47:29', '2020-04-29 07:47:29', 1);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Administrator', '2020-01-31 13:04:17', '2020-01-31 13:04:17'),
(2, 'user', 'Normal User', '2020-01-31 13:04:17', '2020-01-31 13:04:17'),
(3, 'manager', 'Manager User', '2020-04-19 13:45:57', '2020-04-19 13:45:57');

-- --------------------------------------------------------

--
-- Table structure for table `saved_ads`
--

CREATE TABLE `saved_ads` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `ad_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `saved_ads`
--

INSERT INTO `saved_ads` (`id`, `user_id`, `ad_id`) VALUES
(5, 1, 16),
(6, 4, 23),
(7, 4, 21),
(8, 1, 22),
(10, 1, 17),
(14, 1, 25),
(15, 6, 25);

-- --------------------------------------------------------

--
-- Table structure for table `saved_services`
--

CREATE TABLE `saved_services` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `service_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `saved_services`
--

INSERT INTO `saved_services` (`id`, `user_id`, `service_id`) VALUES
(16, 1, 15),
(17, 1, 11),
(19, 1, 23),
(23, 4, 18),
(25, 4, 15),
(29, 1, 9);

-- --------------------------------------------------------

--
-- Table structure for table `selected_ad_values`
--

CREATE TABLE `selected_ad_values` (
  `id` int(10) UNSIGNED NOT NULL,
  `ad_id` int(11) NOT NULL,
  `ad_attribute_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `selected_ad_values`
--

INSERT INTO `selected_ad_values` (`id`, `ad_id`, `ad_attribute_id`, `attribute_id`) VALUES
(1, 14, 8, 27),
(2, 14, 10, 130),
(3, 14, 25, 125),
(4, 15, 8, 27),
(5, 15, 10, 130),
(6, 15, 25, 125),
(7, 16, 8, 27),
(8, 16, 10, 130),
(9, 16, 25, 125),
(10, 17, 8, 27),
(11, 17, 10, 130),
(12, 17, 25, 125),
(13, 18, 14, 109),
(14, 18, 24, 106),
(15, 19, 8, 33),
(16, 19, 10, 127),
(17, 19, 25, 125),
(18, 20, 8, 124),
(19, 20, 10, 129),
(20, 20, 25, 126),
(21, 23, 8, 27),
(22, 23, 10, 129),
(23, 23, 25, 125),
(24, 25, 8, 33),
(25, 25, 10, 129),
(26, 25, 25, 125),
(27, 30, 75, 148),
(28, 30, 77, 212),
(29, 31, 78, 214),
(30, 31, 79, 150),
(31, 32, 78, 215),
(32, 32, 79, 151),
(33, 34, 8, 28),
(34, 34, 10, 128),
(35, 34, 25, 125),
(36, 36, 16, 220),
(37, 36, 87, 228),
(38, 36, 88, 255),
(39, 36, 89, 251),
(40, 36, 90, 246),
(41, 36, 91, 242);

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `budget_min` int(10) UNSIGNED NOT NULL,
  `budget_max` int(10) UNSIGNED NOT NULL,
  `status` int(10) UNSIGNED DEFAULT '0',
  `city_id` int(11) NOT NULL,
  `sub_category_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `currency_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `title`, `slug`, `description`, `budget_min`, `budget_max`, `status`, `city_id`, `sub_category_id`, `user_id`, `created_at`, `updated_at`, `currency_id`) VALUES
(5, 'مطلوب سيارة للبيع', 'مطلوب-سيارة-للبيع', 'مطلوب سيارة للبيع مطلوب سيارة للبيع مطلوب سيارة للبيع', 6, 8, 1, 2, 16, 1, '2020-02-20 14:33:08', '2020-02-20 14:33:08', 1),
(6, 'sdfsdf', 'sdfsdf', 'dfsdfkmsd flksd fl', 345435, 454, 1, 3, 12, 1, '2020-02-20 14:33:39', '2020-02-20 14:33:39', 1),
(7, 'ابدأ ببناء مشروعك', 'ابدأ-ببناء-مشروعك', 'من خلال مستقل تستطيع بناء مشروعك بالشكل الذي تريد، أدخل تفاصيل المشروع و', 12, 15, 1, 3, 12, 1, '2020-02-20 14:54:44', '2020-02-20 14:54:44', 1),
(9, 'ابدأ ببناء مشروعك', 'ابدأ-ببناء-مشروعك-2', 'من خلال مستقل تستطيع بناء مشروعك بالشكل الذي تريد، أدخل تفاصيل المشروع والميزانية المتوقعة، ليتم مراجعة مشروعك ونشره مجاناً على مستقل. بعد ذلك، سيتقدّم أفضل المستقلين المسجلين بعروض مختلفة لتختار العرض المناسب لك وتبدأ بتنفيذ مشروعك.', 15, 232, 0, 2, 28, 1, '2020-02-20 15:43:08', '2020-02-20 15:43:08', 1),
(11, 'ابدأ ببناء مشروعك ابدأ ببناء مشروعك', 'ابدأ-ببناء-مشروعك-ابدأ-ببناء-مشروعك', 'من خلال مستقل تستطيع بناء مشروعك بالشكل الذي تريد، أدخل تفاصيل المشروع والميزانية المتوقعة، ليتم مراجعة مشروعك ونشره مجاناً على مستقل. بعد ذلك، سيتقدّم أفضل المستقلين المسجلين بعروض مختلفة لتختار العرض المناسب لك وتبدأ بتنفيذ مشروعك. \r\n\r\nمن خلال مستقل تستطيع بناء مشروعك بالشكل الذي تريد، أدخل تفاصيل المشروع والميزانية المتوقعة، ليتم مراجعة مشروعك ونشره مجاناً على مستقل. بعد ذلك، سيتقدّم أفضل المستقلين المسجلين بعروض مختلفة لتختار العرض المناسب لك وتبدأ بتنفيذ مشروعك.\r\n\r\nمن خلال مستقل تستطيع بناء مشروعك بالشكل الذي تريد، أدخل تفاصيل المشروع والميزانية المتوقعة، ليتم مراجعة مشروعك ونشره مجاناً على مستقل. بعد ذلك، سيتقدّم أفضل المستقلين المسجلين بعروض مختلفة لتختار العرض المناسب لك وتبدأ بتنفيذ مشروعك.', 12, 15, 1, 3, 8, 1, '2020-02-21 08:41:00', '2020-02-21 16:35:40', 1),
(16, 'هاتف ايفون', 'هاتف-ايفون', 'هاي كشمسين شنكسمينش كسمين شكسمينشكمس', 1200, 1500, 1, 1, 16, 1, '2020-02-23 06:00:00', '2020-02-23 06:01:05', 1),
(17, 'ما هو \"لوريم إيبسوم\" ؟', 'ما-هو-لوريم-إيبسوم-', 'لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة', 500, 1400, 0, 1, 5, 1, '2020-03-02 15:57:49', '2020-03-02 15:57:49', 1),
(18, 'مطلوب سيارة', 'مطلوب-سيارة', 'من خلال مستقل تستطيع بناء مشروعك بالشكل الذي تريد، أدخل تفاصيل المشروع والميزانية المتوقعة، ليتم مراجعة مشروعك ونشره مجاناً على مستقل. بعد ذلك، سيتقدّم أفضل المستقلين المسجلين بعروض مختلفة لتختار العرض المناسب لك وتبدأ بتنفيذ مشروعك.', 15000, 20000, 0, 4, 16, 6, '2020-03-03 08:23:00', '2020-04-30 09:10:00', 2),
(19, 'ما هو \"لوريم إيبسوم\" ؟', 'ما-هو-لوريم-إيبسوم--1', 'ناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيله', 12, 255, 1, 1, 16, 19, '2020-04-30 06:08:00', '2020-04-30 09:11:29', 2);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `details` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `group` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `key`, `display_name`, `value`, `details`, `type`, `order`, `group`) VALUES
(1, 'site.title', 'Site Title', 'Site Title', '', 'text', 1, 'Site'),
(2, 'site.description', 'Site Description', 'Site Description', '', 'text', 2, 'Site'),
(3, 'site.logo', 'Site Logo', 'settings\\April2020\\keXNdbhPgw3ZNN0Fua2I.png', '', 'image', 3, 'Site'),
(4, 'site.google_analytics_tracking_id', 'Google Analytics Tracking ID', NULL, '', 'text', 4, 'Site'),
(5, 'admin.bg_image', 'Admin Background Image', 'settings\\April2020\\hL3NAZEgnHISPcyaIktU.jpg', '', 'image', 5, 'Admin'),
(6, 'admin.title', 'Admin Title', 'Krakeeb', '', 'text', 1, 'Admin'),
(7, 'admin.description', 'Admin Description', 'Welcome to Voyager. The Missing Admin for Laravel', '', 'text', 2, 'Admin'),
(8, 'admin.loader', 'Admin Loader', 'settings\\March2020\\1ddxoDfJqjIPPRpvL2MA.png', '', 'image', 3, 'Admin'),
(9, 'admin.icon_image', 'Admin Icon Image', 'settings\\March2020\\mC9tz44erLYyZ8OQfhWS.png', '', 'image', 4, 'Admin'),
(10, 'admin.google_analytics_client_id', 'Google Analytics Client ID (used for admin dashboard)', NULL, '', 'text', 1, 'Admin');

-- --------------------------------------------------------

--
-- Table structure for table `social_facebook_accounts`
--

CREATE TABLE `social_facebook_accounts` (
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `provider_user_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `provider` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `statuses`
--

CREATE TABLE `statuses` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varbinary(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `display` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sub_categories`
--

CREATE TABLE `sub_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `main_category_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sub_categories`
--

INSERT INTO `sub_categories` (`id`, `name`, `created_at`, `updated_at`, `main_category_id`) VALUES
(1, 'سيارات للبيع', '2020-01-31 16:02:00', '2020-05-01 15:24:46', 1),
(2, 'شقق للييع', '2020-01-31 16:08:21', '2020-01-31 16:08:21', 2),
(3, 'منازل للايجار', '2020-01-31 16:21:52', '2020-01-31 16:21:52', 2),
(5, 'كمبيوتر', '2020-01-31 16:47:00', '2020-01-31 16:48:16', 7),
(6, 'سيارات للإيجار', '2020-01-31 16:49:00', '2020-02-04 17:58:33', 1),
(7, 'دراجات نارية', '2020-01-31 16:49:32', '2020-01-31 16:49:32', 1),
(8, 'فلل - قصور للبيع', '2020-01-31 16:49:58', '2020-01-31 16:49:58', 2),
(9, 'اراضي - مزارع للبيع', '2020-01-31 16:50:00', '2020-02-23 04:47:32', 2),
(10, 'مسجلات', '2020-01-31 16:50:57', '2020-01-31 16:50:57', 7),
(11, 'ستيريو - مسجلات - راديو', '2020-01-31 16:51:34', '2020-01-31 16:51:34', 7),
(12, 'مودم - راوتر', '2020-01-31 16:51:59', '2020-01-31 16:51:59', 7),
(13, 'ادوات المطبخ - ضيافة', '2020-01-31 16:52:33', '2020-01-31 16:52:33', 6),
(14, 'أدوات منزلية', '2020-01-31 16:52:49', '2020-01-31 16:52:49', 6),
(15, 'شقق للايجار', '2020-02-04 16:01:00', '2020-02-04 16:05:42', 6),
(16, 'موبايل - هواتف', '2020-02-04 17:59:00', '2020-02-16 07:38:09', 7),
(17, 'شقق للايجار', '2020-02-04 18:54:53', '2020-02-04 18:54:53', 8),
(18, 'عمارات للايجار', '2020-02-04 18:55:58', '2020-02-04 18:55:58', 8),
(19, 'اراضي - مزارع للايجار', '2020-02-04 18:56:23', '2020-02-04 18:56:23', 8),
(20, 'غرف سكن - شقق فندقية', '2020-02-04 18:56:54', '2020-02-04 18:56:54', 8),
(21, 'عقارات أجنبية للإيجار', '2020-02-04 18:57:51', '2020-02-04 18:57:51', 8),
(22, 'فلل - قصور للايجار', '2020-02-04 18:58:34', '2020-02-04 18:58:34', 8),
(23, 'خدمات توصيل', '2020-02-04 18:59:01', '2020-02-04 18:59:01', 1),
(24, 'لوحة مميزة', '2020-02-04 18:59:20', '2020-02-04 18:59:20', 1),
(25, 'شاحنات - اليات ثقيلة', '2020-02-04 18:59:47', '2020-02-04 18:59:47', 1),
(27, 'مواتير سيارت', '2020-02-06 08:50:00', '2020-02-06 08:51:43', 13),
(28, 'اجهزة مطبخ', '2020-02-11 05:16:53', '2020-02-11 05:16:53', 7),
(29, 'اجهزة مطبخ', '2020-02-11 05:17:45', '2020-02-11 05:17:45', 7),
(30, 'معدات', '2020-02-11 05:22:24', '2020-02-11 05:22:24', 7),
(31, 'ابواب', '2020-02-11 05:28:50', '2020-02-11 05:28:50', 13),
(32, 'ثلاجة', '2020-02-25 09:39:38', '2020-02-25 09:39:38', 7),
(33, 'عمارات للبيع', '2020-04-09 13:26:00', '2020-04-09 13:29:23', 2),
(34, 'لابتوبات / كمبيوترات', '2020-04-09 13:33:09', '2020-04-09 13:33:09', 7),
(35, 'شاشات تلفاز', '2020-04-09 13:36:17', '2020-04-09 13:36:17', 7),
(36, 'كاميرات', '2020-04-09 14:09:46', '2020-04-09 14:09:46', 7),
(37, 'غسالات', '2020-04-09 14:14:29', '2020-04-09 14:14:29', 7),
(38, 'شقق للبيع', '2020-04-09 14:15:00', '2020-04-09 14:17:48', 2),
(39, 'اراضي للبيع', '2020-04-09 14:18:00', '2020-04-09 14:19:58', 2),
(40, 'غرف نوم', '2020-04-09 14:21:20', '2020-04-09 14:21:20', 6),
(41, 'طاولات', '2020-04-09 14:25:58', '2020-04-09 14:25:58', 6),
(42, 'كنب /كراسي', '2020-04-09 14:27:39', '2020-04-09 14:27:39', 6),
(43, 'فرشات', '2020-04-09 14:30:31', '2020-04-09 14:30:31', 6);

-- --------------------------------------------------------

--
-- Table structure for table `translations`
--

CREATE TABLE `translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `units`
--

CREATE TABLE `units` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `attribute_group_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `units`
--

INSERT INTO `units` (`id`, `name`, `attribute_group_id`, `created_at`, `updated_at`) VALUES
(1, 'دونم', 2, '2020-02-15 16:52:00', '2020-02-15 16:52:00'),
(2, 'متر مربع', 2, '2020-02-15 16:52:00', '2020-02-15 16:52:00'),
(3, 'كيلومتر', 2, '2020-02-15 16:52:01', '2020-02-15 16:52:01'),
(4, 'فدان', 2, '2020-02-15 16:52:01', '2020-02-15 16:52:01'),
(5, 'Ah', 18, '2020-02-29 13:38:33', '2020-02-29 13:38:33'),
(6, 'Volte', 18, '2020-02-29 13:38:33', '2020-02-29 13:38:33'),
(7, 'متر', 14, '2020-04-09 13:43:30', '2020-04-09 13:43:30'),
(8, 'قدم', 14, '2020-04-09 13:43:30', '2020-04-09 13:43:30'),
(9, 'فحجة', 14, '2020-04-09 13:43:30', '2020-04-09 13:43:30');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `phone` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT '1',
  `username` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bio` text COLLATE utf8mb4_unicode_ci,
  `city_id` int(10) UNSIGNED DEFAULT NULL,
  `blocked_date` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `avatar`, `email_verified_at`, `password`, `remember_token`, `settings`, `created_at`, `updated_at`, `phone`, `status`, `username`, `bio`, `city_id`, `blocked_date`) VALUES
(1, 1, 'حسن حرب', 'admin@admin.com', 'users\\May2020\\W8DO7jNsPfuAhiqoBmsV.jpg', NULL, '$2y$10$FaqRsGrrJDZIDgHYjFdGaOaZSvBcIdxPozYVIyciji0YreVd4fmBK', NULL, '{\"locale\":\"en\"}', '2020-01-31 13:04:37', '2020-05-01 12:10:01', NULL, 1, NULL, NULL, NULL, NULL),
(6, 2, 'حمزة عبد يوسف زهور', 'harb.hasn98@gmail.com', 'users\\April2020\\3vHYWG7KnQRYWTOaWuSy.jpg', NULL, '$2y$10$WbqNrevTlVdIUwrefHk2ae8Mgrh7l.RyGQbRPtLz1sqc9o2fdYllS', 'MWMmuPpr0Z5t1Zo9Wt9VzbcBWp84B2HJlv4j5nsOBeikzHfWEGyyZ7AxAsEf', '{\"locale\":\"en\"}', '2020-03-01 08:44:47', '2020-04-29 15:18:32', NULL, 1, NULL, NULL, NULL, NULL),
(14, 2, 'حمزة نعيم زهور', 'ha@ha.com3333', 'users/default.png', NULL, '$2y$10$7wKZV8rgnLsLszpWwraoNeNxhVCs9igOBMy76uluoBt9JxNVgeILa', NULL, NULL, '2020-03-15 16:26:43', '2020-03-15 16:26:44', NULL, 1, 'hmz_naaym_zhor_14', NULL, NULL, NULL),
(15, 2, 'asdasd', 'admin@admin.comsdsdsd', 'users/default.png', NULL, '$2y$10$wt5hR.MvVZ8lQbHbkLZ7aeW2Lg2jh6LWXVSVVsdlTSOA4c/qaOp7O', NULL, NULL, '2020-04-24 10:03:15', '2020-04-24 10:03:15', NULL, 1, 'asdasd', NULL, NULL, NULL),
(16, 2, 'fgfdf5e6', 'admin@admin.comfvvvv', 'users/default.png', NULL, '$2y$10$OnjXTy6nIjJvferg9dX76OoYBakI4bkp/apj1NSMDgjA16vyzWTYG', NULL, NULL, '2020-04-24 10:07:08', '2020-04-24 10:07:08', NULL, 1, 'fgfdf5e6_rd53ng3', NULL, NULL, NULL),
(19, 3, 'حسن عاييش', 'harb.hasn98@hotmail.com', 'users/default.png', NULL, '$2y$10$ZTgS6xLNskWg8IxSa5bA1eYHRFpztCIG9ipQm/zZBZU0YDnl.u8Yu', NULL, '{\"locale\":\"en\"}', '2020-04-29 16:45:28', '2020-05-01 10:14:23', NULL, 1, 'admin@admin.com', NULL, NULL, NULL),
(24, 2, 'حسن زهور', 'h@h.com', 'users/default.png', NULL, '$2y$10$uTCMD2wKegTFnidJJdIX.O49IAq2IiswmjIEXCslQqnpL1HvRvitm', NULL, NULL, '2020-05-01 05:14:39', '2020-05-01 08:51:20', NULL, 1, 'hsn_zhor_2c1t', NULL, 1, NULL),
(25, 2, 'حسن زهور', 'admin@admin.comqqqqq', 'users/default.png', NULL, '$2y$10$V5XX7QTDfOY7J9SfJ3iKi.jgTr5GiJZ9elFmZTh0svcLcDesLfq/K', NULL, NULL, '2020-05-01 08:55:50', '2020-05-01 09:51:22', NULL, 0, 'hsn_zhor_zivr', NULL, 2, NULL),
(26, 1, 'حمزة زهور', 'hamzazhoor4@gmail.com', 'users\\May2020\\2zlI687qU2R4z2ZBSm2P.jpg', NULL, '$2y$10$GbASQKAiKMI/7mbLKsxi.O0uD7oez1Lqzn73kaS9pNuwL/oiJA7Y.', NULL, '{\"locale\":\"en\"}', '2020-05-01 11:41:04', '2020-05-01 16:04:12', NULL, 1, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE `user_roles` (
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ads`
--
ALTER TABLE `ads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ad_attributes`
--
ALTER TABLE `ad_attributes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ad_reports`
--
ALTER TABLE `ad_reports`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `against_currencies`
--
ALTER TABLE `against_currencies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `attributes`
--
ALTER TABLE `attributes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `attribute_groups`
--
ALTER TABLE `attribute_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `auctions`
--
ALTER TABLE `auctions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `auction_offers`
--
ALTER TABLE `auction_offers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `currencies`
--
ALTER TABLE `currencies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD PRIMARY KEY (`id`),
  ADD KEY `data_rows_data_type_id_foreign` (`data_type_id`);

--
-- Indexes for table `data_types`
--
ALTER TABLE `data_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `data_types_name_unique` (`name`),
  ADD UNIQUE KEY `data_types_slug_unique` (`slug`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `followers`
--
ALTER TABLE `followers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `input_ad_values`
--
ALTER TABLE `input_ad_values`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `main_categories`
--
ALTER TABLE `main_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menus_name_unique` (`name`);

--
-- Indexes for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_items_menu_id_foreign` (`menu_id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `offers`
--
ALTER TABLE `offers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_key_index` (`key`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Indexes for table `ratings`
--
ALTER TABLE `ratings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `saved_ads`
--
ALTER TABLE `saved_ads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `saved_services`
--
ALTER TABLE `saved_services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `selected_ad_values`
--
ALTER TABLE `selected_ad_values`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_key_unique` (`key`);

--
-- Indexes for table `social_facebook_accounts`
--
ALTER TABLE `social_facebook_accounts`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `statuses`
--
ALTER TABLE `statuses`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `statuses_key_unique` (`key`);

--
-- Indexes for table `sub_categories`
--
ALTER TABLE `sub_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `translations`
--
ALTER TABLE `translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`);

--
-- Indexes for table `units`
--
ALTER TABLE `units`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `user_roles_user_id_index` (`user_id`),
  ADD KEY `user_roles_role_id_index` (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ads`
--
ALTER TABLE `ads`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `ad_attributes`
--
ALTER TABLE `ad_attributes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=92;

--
-- AUTO_INCREMENT for table `ad_reports`
--
ALTER TABLE `ad_reports`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `against_currencies`
--
ALTER TABLE `against_currencies`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `attributes`
--
ALTER TABLE `attributes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=261;

--
-- AUTO_INCREMENT for table `attribute_groups`
--
ALTER TABLE `attribute_groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `auctions`
--
ALTER TABLE `auctions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `auction_offers`
--
ALTER TABLE `auction_offers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `currencies`
--
ALTER TABLE `currencies`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `data_rows`
--
ALTER TABLE `data_rows`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=222;

--
-- AUTO_INCREMENT for table `data_types`
--
ALTER TABLE `data_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `followers`
--
ALTER TABLE `followers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `input_ad_values`
--
ALTER TABLE `input_ad_values`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `main_categories`
--
ALTER TABLE `main_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `menu_items`
--
ALTER TABLE `menu_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `offers`
--
ALTER TABLE `offers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=142;

--
-- AUTO_INCREMENT for table `ratings`
--
ALTER TABLE `ratings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `saved_ads`
--
ALTER TABLE `saved_ads`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `saved_services`
--
ALTER TABLE `saved_services`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `selected_ad_values`
--
ALTER TABLE `selected_ad_values`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `social_facebook_accounts`
--
ALTER TABLE `social_facebook_accounts`
  MODIFY `user_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `statuses`
--
ALTER TABLE `statuses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sub_categories`
--
ALTER TABLE `sub_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `translations`
--
ALTER TABLE `translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `units`
--
ALTER TABLE `units`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Constraints for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
