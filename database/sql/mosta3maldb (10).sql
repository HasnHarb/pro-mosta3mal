-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 21, 2020 at 09:17 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mosta3maldb`
--

-- --------------------------------------------------------

--
-- Table structure for table `ads`
--

CREATE TABLE `ads` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `status` int(10) UNSIGNED DEFAULT '0',
  `sub_category_id` int(11) NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `images` text COLLATE utf8mb4_unicode_ci,
  `location` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `currency_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ads`
--

INSERT INTO `ads` (`id`, `title`, `description`, `price`, `city_id`, `status`, `sub_category_id`, `slug`, `images`, `location`, `user_name`, `phone`, `created_at`, `updated_at`, `user_id`, `currency_id`) VALUES
(10, 'sdfsdf', 'sdfsdf', 34, 1, 1, 1, 'sdfsdf', '[{\"download_link\":\"ads\\\\February2020\\\\R7xtANUtE4vb4YzyvP1Q.png\",\"original_name\":\"5831-programmer-desktop-wallpaper.png\"},{\"download_link\":\"ads\\\\February2020\\\\kzmtgc7Ohs9Zk8kQpA6E.png\",\"original_name\":\"unnamed.png\"}]', 'asdasd', 'asdas', 'sadas', '2020-02-24 17:18:49', '2020-02-24 17:18:49', 1, 1),
(11, 'asdasd', 'ASDASD', 23, 3, 1, 1, 'asdasd', '[{\"download_link\":\"ads\\\\February2020\\\\uNtn03HYV9HaaBDWV6cJ.png\",\"original_name\":\"5831-programmer-desktop-wallpaper.png\"},{\"download_link\":\"ads\\\\February2020\\\\FyKrwmP02sKkgcqg9z8p.png\",\"original_name\":\"unnamed.png\"},{\"download_link\":\"ads\\\\February2020\\\\5EjXaSsozTaaawu8rGKd.jpg\",\"original_name\":\"580.jpg\"}]', 'WEW', 'Hasan', '+9720598933532', '2020-02-24 17:18:59', '2020-02-24 17:18:59', 1, 1),
(12, 'تلفون ساسونج للبيع', 'تلفون ساسونج للبيع تلفون ساسونج للبيع تلفون ساسونج للبيع تلفون ساسونج للبيع', 500, 1, 1, 16, 'تلفون-ساسونج-للبيع', '[{\"download_link\":\"ads\\\\February2020\\\\wSfov8VkXQScsVaYtrcV.jpg\",\"original_name\":\"61YNlYdUK6L._SL1000_-20191224141810-1000x1000.jpg\"},{\"download_link\":\"ads\\\\February2020\\\\fVDrf8zoX0bsQB9F5KcJ.jpg\",\"original_name\":\"\\u062a\\u0646\\u0632\\u064a\\u0644 (25).jpg\"}]', 'بيت كاحل', 'Hasan', '+9720598933532', '2020-02-29 16:03:00', '2020-02-29 16:03:00', 1, 1),
(13, 'تلفون ساسونج للبيع', 'تلفون ساسونج للبيع تلفون ساسونج للبيع تلفون ساسونج للبيع تلفون ساسونج للبيع', 500, 1, 1, 16, 'تلفون-ساسونج-للبيع-1', '[{\"download_link\":\"ads\\\\February2020\\\\62O79nmyvBQsZebF8c3H.jpg\",\"original_name\":\"61YNlYdUK6L._SL1000_-20191224141810-1000x1000.jpg\"},{\"download_link\":\"ads\\\\February2020\\\\7zp9CAGoC6lfz8vifX2p.jpg\",\"original_name\":\"\\u062a\\u0646\\u0632\\u064a\\u0644 (25).jpg\"}]', 'بيت كاحل', 'Hasan', '+9720598933532', '2020-02-29 16:04:56', '2020-02-29 16:04:56', 1, 1),
(14, 'تلفون ساسونج للبيع', 'تلفون ساسونج للبيع تلفون ساسونج للبيع تلفون ساسونج للبيع تلفون ساسونج للبيع', 500, 1, 1, 16, 'تلفون-ساسونج-للبيع-2', '[{\"download_link\":\"ads\\\\February2020\\\\7t4oUOq68pSA6kuk4YqZ.jpg\",\"original_name\":\"61YNlYdUK6L._SL1000_-20191224141810-1000x1000.jpg\"},{\"download_link\":\"ads\\\\February2020\\\\hjDP0KDYcaXa8tOVe3UK.jpg\",\"original_name\":\"\\u062a\\u0646\\u0632\\u064a\\u0644 (25).jpg\"}]', 'بيت كاحل', 'Hasan', '+9720598933532', '2020-02-29 16:05:09', '2020-02-29 16:05:09', 1, 1),
(15, 'تلفون ساسونج للبيع', 'تلفون ساسونج للبيع تلفون ساسونج للبيع تلفون ساسونج للبيع تلفون ساسونج للبيع', 500, 1, 1, 16, 'تلفون-ساسونج-للبيع-3', '[{\"download_link\":\"ads\\\\February2020\\\\RjitTJ3smjvqtQCPv0Rf.jpg\",\"original_name\":\"61YNlYdUK6L._SL1000_-20191224141810-1000x1000.jpg\"},{\"download_link\":\"ads\\\\February2020\\\\4iWSTiwhnLMPxQOBvNcc.jpg\",\"original_name\":\"\\u062a\\u0646\\u0632\\u064a\\u0644 (25).jpg\"}]', 'بيت كاحل', 'Hasan', '+9720598933532', '2020-02-29 16:05:16', '2020-02-29 16:05:16', 1, 1),
(16, 'تلفون ساسونج للبيع', 'تلفون ساسونج للبيع تلفون ساسونج للبيع تلفون ساسونج للبيع تلفون ساسونج للبيع', 500, 1, 1, 16, 'تلفون-ساسونج-للبيع-4', '[{\"download_link\":\"ads\\\\February2020\\\\zpG5hxmZOsql5jYdEkiB.jpg\",\"original_name\":\"61YNlYdUK6L._SL1000_-20191224141810-1000x1000.jpg\"},{\"download_link\":\"ads\\\\February2020\\\\eAGgaT9JwIFW3Jp6s1ws.jpg\",\"original_name\":\"\\u062a\\u0646\\u0632\\u064a\\u0644 (25).jpg\"}]', 'بيت كاحل', 'Hasan', '+9720598933532', '2020-02-29 16:05:34', '2020-02-29 16:05:34', 1, 1),
(17, 'تلفون ساسونج للبيع', 'تلفون ساسونج للبيع تلفون ساسونج للبيع تلفون ساسونج للبيع تلفون ساسونج للبيع', 500, 1, 1, 16, 'تلفون-ساسونج-للبيع-5', '[{\"download_link\":\"ads\\\\February2020\\\\ewbv6M9Na45QEqt7KZcD.jpg\",\"original_name\":\"61YNlYdUK6L._SL1000_-20191224141810-1000x1000.jpg\"},{\"download_link\":\"ads\\\\February2020\\\\gTbdhJcR4sOqjonCpDKE.jpg\",\"original_name\":\"\\u062a\\u0646\\u0632\\u064a\\u0644 (25).jpg\"}]', 'بيت كاحل', 'Hasan', '+9720598933532', '2020-02-29 16:06:10', '2020-02-29 16:06:10', 1, 1),
(18, 'سيارة كيا للبيع', 'سيارة كيا للبيع سيارة كيا للبيع سيارة كيا للبيع', 25000, 6, 1, 27, 'سيارة-كيا-للبيع', '[{\"download_link\":\"ads\\\\March2020\\\\Z5JKp8Kv3O9IfQzFT5Bi.png\",\"original_name\":\"image_154859072921221583.png\"},{\"download_link\":\"ads\\\\March2020\\\\DRq2QO9ze6CPABRFKrsA.png\",\"original_name\":\"illustration-tech-esign-tran3-1024x725.png\"},{\"download_link\":\"ads\\\\March2020\\\\bDh7Y3kTZ1f32tRlbZga.jpg\",\"original_name\":\"shutterstock_1060094186.jpg\"}]', 'جمرورة', 'Hasan', '+9720598933532', '2020-03-01 06:20:08', '2020-03-01 06:20:08', 1, 1),
(19, 'تلفون لينوفو', 'تلفون لينوفو تلفون لينوفو تلفون لينوفو تلفون لينوفو', 250, 4, NULL, 16, 'تلفون-لينوفو', '[{\"download_link\":\"ads\\\\March2020\\\\pNUPPRal0oMPDVJ1NfjL.jpg\",\"original_name\":\"\\u062a\\u0646\\u0632\\u064a\\u0644 (25).jpg\"}]', 'هناك عناك هناك', 'حمزة زهور', '05998933532', '2020-03-01 08:50:32', '2020-03-01 08:50:32', 6, NULL),
(20, 'هاتف ال جي جديد', 'هاتف ال جي جديدهاتف ال جي جديد هاتف ال جي جديدهاتف ال جي جديد', 1033, 3, NULL, 16, 'هاتف-ال-جي-جديد', '[{\"download_link\":\"ads\\\\March2020\\\\QIZcNL7Xhl0lriU3Lfai.jpg\",\"original_name\":\"shutterstock_1060094186.jpg\"}]', 'هناك عناك هناك', 'حمزة زهور', '05998933532', '2020-03-01 08:54:57', '2020-03-01 08:54:57', 6, 1),
(21, 'ارض ببيت كاحل', 'ارض ببيت كاحل  ارض ببيت كاحل', 1500, 2, 1, 9, 'ارض-ببيت-كاحل', '[{\"download_link\":\"ads\\\\March2020\\\\7Qe5WhnmhiOazM7upGnA.jpg\",\"original_name\":\"\\u0637\\u0631\\u064a\\u0642\\u0629_\\u0637\\u0628\\u062e_\\u0627\\u0644\\u0633\\u0645\\u0627\\u0646.jpg\"},{\"download_link\":\"ads\\\\March2020\\\\e6w43WNCzivCvCCzFF9V.jpg\",\"original_name\":\"4a0b8321fe15fd2274ed88c7d185dc81_w750_h750.jpg\"}]', 'هناك عناك هناك', 'حمزة زهور', '+9720598933532', '2020-03-01 09:00:08', '2020-03-01 09:00:08', 6, 2),
(22, 'ارض ببيت كاحل', 'ارض ببيت كاحل  ارض ببيت كاحل', 1500, 2, 1, 9, 'ارض-ببيت-كاحل-1', '[{\"download_link\":\"ads\\\\March2020\\\\rSlbaZjrJ7mxtf4k10eW.jpg\",\"original_name\":\"\\u0637\\u0631\\u064a\\u0642\\u0629_\\u0637\\u0628\\u062e_\\u0627\\u0644\\u0633\\u0645\\u0627\\u0646.jpg\"},{\"download_link\":\"ads\\\\March2020\\\\GCdAGBgq5aiBE1RWywsC.jpg\",\"original_name\":\"4a0b8321fe15fd2274ed88c7d185dc81_w750_h750.jpg\"}]', 'هناك عناك هناك', 'حمزة زهور', '+9720598933532', '2020-03-01 09:03:03', '2020-03-01 09:03:03', 6, 2),
(23, 'تلفون ساسونج A10 للبيع', 'هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها. ولذلك يتم استخدام طريقة لوريم إيبسوم لأنها تعطي توزيعاَ طبيعياَ -إلى حد ما- للأحرف عوضاً عن استخدام \"هنا يوجد محتوى نصي، هنا يوجد محتوى نصي\" فتجعلها تبدو (أي الأحرف) وكأنها نص مقروء.', 250, 1, 1, 16, 'تلفون-ساسونج-A10-للبيع', '[{\"download_link\":\"ads\\\\March2020\\\\ISHSBautVcBLUBQi4AZG.jpg\",\"original_name\":\"192.jpg\"},{\"download_link\":\"ads\\\\March2020\\\\oXTKRP4Qxq2GUXHNMYqJ.jpg\",\"original_name\":\"images.jpg\"},{\"download_link\":\"ads\\\\March2020\\\\hZ4F8DRi9Bq9mUMd36Fe.png\",\"original_name\":\"\\u0645\\u0631\\u0627\\u062c\\u0639\\u0629 \\u0645\\u0648\\u0627\\u0635\\u0641\\u0627\\u062a samsung galaxy a10 \\u0645\\u0645\\u064a\\u0632\\u0627\\u062a \\u0648\\u0639\\u064a\\u0648\\u0628 \\u0633\\u0627\\u0645\\u0633\\u0648\\u0646\\u062c A10 (1).png\"},{\"download_link\":\"ads\\\\March2020\\\\05EsdP0zkROFKa8wqQXF.jpg\",\"original_name\":\"61YNlYdUK6L._SL1000_-20191224141810-1000x1000.jpg\"}]', 'بيت كاحل', 'حسن عايش زهور', '+9720598933532', '2020-03-01 18:27:00', '2020-03-01 18:27:00', 1, 2),
(25, 'هاتف مستعمل', 'معلومات الاتصال\r\n\r\nمعلومات الاتصال\r\n\r\nمعلومات الاتصال\r\n\r\nمعلومات الاتصال', 1500, 4, 1, 16, 'هاتف-مستعمل', '[{\"download_link\":\"ads\\\\March2020\\\\vVqtdcuTSc3hYXMD88yP.jpg\",\"original_name\":\"61YNlYdUK6L._SL1000_-20191224141810-1000x1000.jpg\"},{\"download_link\":\"ads\\\\March2020\\\\eCvnqFzamqmBpUAVZrS3.jpg\",\"original_name\":\"\\u062a\\u0646\\u0632\\u064a\\u0644 (25).jpg\"},{\"download_link\":\"ads\\\\March2020\\\\2CjuP4OzxagaGSZ5HlLH.jpg\",\"original_name\":\"192.jpg\"}]', NULL, 'Hasan', '+9720598933532', '2020-03-03 08:36:00', '2020-03-03 08:36:41', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ad_attributes`
--

CREATE TABLE `ad_attributes` (
  `id` int(10) UNSIGNED NOT NULL,
  `attribute_group_id` int(11) NOT NULL,
  `sub_category_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `form_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'selected',
  `required` bit(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ad_attributes`
--

INSERT INTO `ad_attributes` (`id`, `attribute_group_id`, `sub_category_id`, `created_at`, `updated_at`, `form_type`, `required`) VALUES
(2, 13, 15, NULL, '2020-02-11 05:37:16', 'selected', b'0'),
(3, 14, 15, NULL, NULL, '', NULL),
(4, 1, 6, NULL, NULL, '', NULL),
(5, 5, 6, NULL, NULL, '', NULL),
(6, 6, 6, NULL, NULL, '', NULL),
(7, 7, 6, NULL, NULL, '', NULL),
(8, 1, 16, '2020-02-11 07:34:00', '2020-02-11 05:34:28', 'selected', b'1'),
(10, 9, 16, '2020-02-11 07:34:00', '2020-02-11 05:35:00', 'selected', b'1'),
(12, 4, 27, '2020-02-11 07:35:00', '2020-02-11 08:21:19', 'number', b'1'),
(13, 5, 27, '2020-02-11 07:35:00', '2020-02-11 05:35:45', 'number', b'0'),
(14, 8, 27, NULL, '2020-02-11 05:36:07', 'selected', b'0'),
(15, 17, 2, '2020-02-09 08:34:00', '2020-02-11 05:36:31', 'selected', b'0'),
(16, 1, 1, '2020-02-11 05:13:51', '2020-02-11 05:13:51', 'select', b'1'),
(17, 1, 29, NULL, NULL, 'selected', NULL),
(18, 2, 9, '2020-02-11 05:21:17', '2020-02-11 05:21:17', 'text', b'1'),
(19, 1, 30, NULL, '2020-02-11 14:19:21', 'selected', b'1'),
(20, 4, 30, NULL, '2020-02-11 14:19:34', 'selected', b'0'),
(21, 8, 30, NULL, '2020-02-11 14:25:35', 'date', b'1'),
(22, 1, 31, NULL, NULL, 'selected', NULL),
(23, 3, 31, NULL, NULL, 'selected', NULL),
(24, 6, 27, '2020-02-11 10:18:00', '2020-02-11 08:18:21', 'selected', b'0'),
(25, 6, 16, NULL, '2020-02-16 07:46:53', 'selected', b'0'),
(27, 16, 16, '2020-02-17 18:26:00', '2020-02-23 04:47:58', 'text', b'1'),
(28, 16, 9, '2020-02-23 06:49:00', '2020-02-23 04:49:04', 'text', b'1'),
(29, 1, 32, '2020-02-25 11:40:00', '2020-02-25 09:43:14', 'selected', b'1'),
(30, 4, 32, NULL, NULL, 'selected', NULL),
(31, 9, 32, NULL, NULL, 'selected', NULL),
(33, 18, 16, '2020-02-29 13:37:45', '2020-02-29 13:37:45', 'text', b'0'),
(34, 17, 16, '2020-03-04 05:17:10', '2020-03-04 05:17:10', 'selected', b'0');

-- --------------------------------------------------------

--
-- Table structure for table `attributes`
--

CREATE TABLE `attributes` (
  `id` int(10) UNSIGNED NOT NULL,
  `value` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `ad_attribute_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `attributes`
--

INSERT INTO `attributes` (`id`, `value`, `icon`, `created_at`, `updated_at`, `ad_attribute_id`) VALUES
(27, 'سامسونج', NULL, '2020-02-06 04:20:17', '2020-02-06 04:20:17', 8),
(28, 'ايفون', NULL, '2020-02-06 04:20:17', '2020-02-06 04:20:17', 8),
(29, 'بلاكبيري', NULL, '2020-02-06 04:20:17', '2020-02-06 04:20:17', 8),
(32, 'اوبو', NULL, '2020-02-06 04:20:18', '2020-02-06 04:20:18', 8),
(33, 'لينوفو', NULL, '2020-02-06 04:20:18', '2020-02-06 04:20:18', 8),
(94, 'حمامين', NULL, '2020-02-07 15:30:28', '2020-02-07 15:30:28', 2),
(95, 'حمامين', NULL, '2020-02-07 15:36:54', '2020-02-07 15:36:54', 2),
(96, 'ثلاث حمامات', NULL, '2020-02-07 15:36:54', '2020-02-07 15:36:54', 2),
(97, 'اربعة حمامات', NULL, '2020-02-07 15:36:54', '2020-02-07 15:36:54', 2),
(98, 'اكثر من اربعة', NULL, '2020-02-07 15:36:54', '2020-02-07 15:36:54', 2),
(99, 'طابق 1', NULL, '2020-02-09 08:39:21', '2020-02-09 08:39:21', 15),
(100, 'طابق 2', NULL, '2020-02-09 08:39:21', '2020-02-09 08:39:21', 15),
(101, 'طابق 3', NULL, '2020-02-09 08:39:21', '2020-02-09 08:39:21', 15),
(102, 'بنزين 98', NULL, '2020-02-11 07:49:20', '2020-02-11 07:49:20', 7),
(103, 'بنزين 96', NULL, '2020-02-11 07:49:20', '2020-02-11 07:49:20', 7),
(104, 'سولار', NULL, '2020-02-11 07:49:20', '2020-02-11 07:49:20', 7),
(105, 'اغو', NULL, '2020-02-11 07:49:21', '2020-02-11 07:49:21', 7),
(106, 'مستعمل', NULL, '2020-02-11 08:19:55', '2020-02-11 08:19:55', 24),
(107, 'جديد', NULL, '2020-02-11 08:19:56', '2020-02-11 08:19:56', 24),
(108, 'هونداي', NULL, '2020-02-11 08:20:38', '2020-02-11 08:20:38', 14),
(109, 'كيا', NULL, '2020-02-11 08:20:38', '2020-02-11 08:20:38', 14),
(110, 'سكودا', NULL, '2020-02-11 08:20:39', '2020-02-11 08:20:39', 14),
(111, 'بي ام دبليو', NULL, '2020-02-11 08:20:39', '2020-02-11 08:20:39', 14),
(112, 'ماكيتا', NULL, '2020-02-11 14:15:03', '2020-02-11 14:15:03', 19),
(113, 'بوش', NULL, '2020-02-11 14:15:03', '2020-02-11 14:15:03', 19),
(114, 'مفرقاة', NULL, '2020-02-11 14:15:03', '2020-02-11 14:15:03', 19),
(115, 'سامسنج', NULL, '2020-02-11 14:15:03', '2020-02-11 14:15:03', 19),
(116, '2019', NULL, '2020-02-11 14:16:14', '2020-02-11 14:16:14', 20),
(117, '2018', NULL, '2020-02-11 14:16:15', '2020-02-11 14:16:15', 20),
(118, '2017', NULL, '2020-02-11 14:16:15', '2020-02-11 14:16:15', 20),
(119, '2016', NULL, '2020-02-11 14:16:15', '2020-02-11 14:16:15', 20),
(120, '2015', NULL, '2020-02-11 14:16:15', '2020-02-11 14:16:15', 20),
(121, '2014', NULL, '2020-02-11 14:16:15', '2020-02-11 14:16:15', 20),
(122, '2013', NULL, '2020-02-11 14:16:15', '2020-02-11 14:16:15', 20),
(123, 'ايسوس', NULL, '2020-02-16 07:40:18', '2020-02-16 07:40:18', 8),
(124, 'ال جي', NULL, '2020-02-16 07:40:18', '2020-02-16 07:40:18', 8),
(125, 'مستعمل', NULL, '2020-02-16 07:40:55', '2020-02-16 07:40:55', 25),
(126, 'جديد', NULL, '2020-02-16 07:40:55', '2020-02-16 07:40:55', 25),
(127, '8GB', NULL, '2020-02-16 07:43:05', '2020-02-16 07:43:05', 10),
(128, '16GB', NULL, '2020-02-16 07:43:05', '2020-02-16 07:43:05', 10),
(129, '32GB', NULL, '2020-02-16 07:43:05', '2020-02-16 07:43:05', 10),
(130, '64GB', NULL, '2020-02-16 07:43:05', '2020-02-16 07:43:05', 10),
(131, 'سامسونج', NULL, '2020-02-25 09:42:01', '2020-02-25 09:42:01', 29),
(132, 'ال جي', NULL, '2020-02-25 09:42:01', '2020-02-25 09:42:01', 29),
(133, 'سوني', NULL, '2020-02-25 09:42:01', '2020-02-25 09:42:01', 29),
(134, 'جولد', NULL, '2020-02-25 09:42:01', '2020-02-25 09:42:01', 29),
(135, 'طابق 1', NULL, '2020-03-04 05:18:06', '2020-03-04 05:18:06', 34),
(136, 'طابق 2', NULL, '2020-03-04 05:18:06', '2020-03-04 05:18:06', 34),
(137, 'طابق 3', NULL, '2020-03-04 05:18:06', '2020-03-04 05:18:06', 34);

-- --------------------------------------------------------

--
-- Table structure for table `attribute_groups`
--

CREATE TABLE `attribute_groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `attribute_groups`
--

INSERT INTO `attribute_groups` (`id`, `name`, `slug`, `label`, `created_at`, `updated_at`) VALUES
(1, 'النوع', 'النوع', 'نوع المنتج او الماركة', '2020-02-04 16:02:19', '2020-02-04 16:02:19'),
(2, 'مساحة الارض', 'مساحة الارض', 'مساحة قطعة الارض', '2020-02-04 16:03:00', '2020-02-04 16:03:00'),
(3, 'الفئة', 'الفئة', 'الفئة', '2020-02-04 17:39:23', '2020-02-04 17:39:23'),
(4, 'سنة الصنع', 'سنة-الصنع', 'سنة الصنع', '2020-02-04 17:39:58', '2020-02-04 17:39:58'),
(5, 'الكيلومترات', 'الكيلومترات', 'الكيلومترات', '2020-02-04 17:40:17', '2020-02-04 17:40:17'),
(6, 'الحالة', 'الحالة', 'الحالة', '2020-02-04 17:40:56', '2020-02-04 17:40:56'),
(7, 'نوع الوقود', 'نوع الوقود', 'نوع الوقود', '2020-02-04 17:41:23', '2020-02-04 17:41:23'),
(8, 'الماركة', 'الماركة', 'الماركة', '2020-02-04 17:42:08', '2020-02-04 17:42:08'),
(9, 'سعة التخزين', 'سعة التخزين', 'سعة التخزين', '2020-02-04 17:42:29', '2020-02-04 17:42:29'),
(13, 'عدد الحمامات', 'عدد-الحمامات', 'عدد الحمامات', '2020-02-04 17:54:07', '2020-02-04 17:54:07'),
(14, 'مساحة البناء', 'مساحة-البناء', 'مساحة البناء', '2020-02-04 17:54:00', '2020-02-04 17:55:09'),
(15, 'طريقة الدفع', 'طريقة-الدفع', 'طريقة الدفع', '2020-02-04 17:55:55', '2020-02-04 17:55:55'),
(16, 'مزايا إضافية', 'مزايا-إضافية', 'مزايا إضافية', '2020-02-04 17:56:40', '2020-02-04 17:56:40'),
(17, 'رقم الطوابق', 'رقم-الطوابق', 'عدد الطوابق', '2020-02-09 08:33:52', '2020-02-09 08:33:52'),
(18, 'سعة البطارية', 'سعة-البطارية', 'سعة البطارية', '2020-02-29 13:37:30', '2020-02-29 13:37:30');

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'الخليل', '2020-01-31 16:54:48', '2020-01-31 16:54:48'),
(2, 'بيت لحم', '2020-01-31 16:54:58', '2020-01-31 16:54:58'),
(3, 'القدس', '2020-01-31 16:55:06', '2020-01-31 16:55:06'),
(4, 'رام الله', '2020-01-31 16:55:13', '2020-01-31 16:55:13'),
(5, 'قلقيلة', '2020-01-31 16:55:24', '2020-01-31 16:55:24'),
(6, 'جنين', '2020-01-31 16:55:33', '2020-01-31 16:55:33'),
(7, 'نابلس', '2020-02-06 06:25:48', '2020-02-06 06:25:48');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `budget` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `comment`, `budget`, `user_id`, `service_id`, `created_at`, `updated_at`) VALUES
(1, 'نص نص نص', 23, 1, 1, '2020-02-02 16:38:02', '2020-02-02 16:38:02');

-- --------------------------------------------------------

--
-- Table structure for table `currencies`
--

CREATE TABLE `currencies` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `currencies`
--

INSERT INTO `currencies` (`id`, `name`, `icon`, `created_at`, `updated_at`) VALUES
(1, 'شيكل', '₪', '2020-02-23 04:07:27', '2020-02-23 04:07:27'),
(2, 'دولار', '$', '2020-02-23 04:07:41', '2020-02-23 04:07:41');

-- --------------------------------------------------------

--
-- Table structure for table `data_rows`
--

CREATE TABLE `data_rows` (
  `id` int(10) UNSIGNED NOT NULL,
  `data_type_id` int(10) UNSIGNED NOT NULL,
  `field` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `browse` tinyint(1) NOT NULL DEFAULT '1',
  `read` tinyint(1) NOT NULL DEFAULT '1',
  `edit` tinyint(1) NOT NULL DEFAULT '1',
  `add` tinyint(1) NOT NULL DEFAULT '1',
  `delete` tinyint(1) NOT NULL DEFAULT '1',
  `details` text COLLATE utf8mb4_unicode_ci,
  `order` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_rows`
--

INSERT INTO `data_rows` (`id`, `data_type_id`, `field`, `type`, `display_name`, `required`, `browse`, `read`, `edit`, `add`, `delete`, `details`, `order`) VALUES
(1, 1, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '{}', 1),
(2, 1, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '{}', 2),
(3, 1, 'email', 'text', 'Email', 1, 1, 1, 1, 1, 1, '{}', 3),
(4, 1, 'password', 'password', 'Password', 1, 0, 0, 1, 1, 0, '{}', 4),
(5, 1, 'remember_token', 'text', 'Remember Token', 0, 0, 0, 0, 0, 0, '{}', 5),
(6, 1, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, '{}', 6),
(7, 1, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 7),
(8, 1, 'avatar', 'image', 'Avatar', 0, 1, 1, 1, 1, 1, '{}', 8),
(9, 1, 'user_belongsto_role_relationship', 'relationship', 'Role', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":\"0\",\"taggable\":\"0\"}', 10),
(10, 1, 'user_belongstomany_role_relationship', 'relationship', 'Roles', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}', 11),
(11, 1, 'settings', 'hidden', 'Settings', 0, 0, 0, 0, 0, 0, '{}', 12),
(12, 2, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(13, 2, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(14, 2, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(15, 2, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(16, 3, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(17, 3, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(18, 3, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(19, 3, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(20, 3, 'display_name', 'text', 'Display Name', 1, 1, 1, 1, 1, 1, NULL, 5),
(21, 1, 'role_id', 'text', 'Role', 0, 1, 1, 1, 1, 1, '{}', 9),
(22, 4, 'id', 'hidden', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(23, 4, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '{}', 2),
(24, 4, 'description', 'text_area', 'Description', 0, 1, 1, 1, 1, 1, '{}', 3),
(25, 4, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 5),
(26, 4, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(27, 5, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(28, 5, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '{}', 4),
(29, 5, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 5),
(30, 5, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(31, 5, 'main_category_id', 'hidden', 'Main Category Id', 1, 1, 1, 1, 1, 1, '{}', 3),
(32, 5, 'sub_category_belongsto_main_category_relationship', 'relationship', 'main_categories', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\MainCategory\",\"table\":\"main_categories\",\"type\":\"belongsTo\",\"column\":\"main_category_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"data_rows\",\"pivot\":\"0\",\"taggable\":\"0\"}', 2),
(34, 4, 'main_category_hasmany_sub_category_relationship', 'relationship', 'sub_categories', 0, 1, 1, 0, 0, 1, '{\"model\":\"App\\\\SubCategory\",\"table\":\"sub_categories\",\"type\":\"hasMany\",\"column\":\"main_category_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"data_rows\",\"pivot\":\"0\",\"taggable\":\"0\"}', 7),
(35, 6, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(36, 6, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '{}', 2),
(37, 6, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 3),
(38, 6, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 4),
(39, 7, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(40, 7, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, '{}', 2),
(41, 7, 'slug', 'text', 'Slug', 0, 0, 0, 0, 0, 0, '{}', 3),
(42, 7, 'description', 'text_area', 'Description', 1, 0, 1, 1, 1, 1, '{}', 4),
(43, 7, 'budget_min', 'number', 'Budget Min', 1, 1, 1, 1, 1, 1, '{}', 5),
(44, 7, 'budget_max', 'number', 'Budget Max', 1, 1, 1, 1, 1, 1, '{}', 6),
(45, 7, 'status', 'select_dropdown', 'Status', 0, 1, 1, 1, 1, 1, '{\"options\":{\"0\":\"\\u063a\\u064a\\u0631 \\u0641\\u0639\\u0627\\u0644\",\"1\":\"\\u0641\\u0639\\u0627\\u0644\"}}', 7),
(47, 7, 'city_id', 'hidden', 'City Id', 1, 1, 1, 1, 1, 1, '{}', 9),
(48, 7, 'sub_category_id', 'hidden', 'Sub Category Id', 1, 1, 1, 1, 1, 1, '{}', 11),
(49, 7, 'user_id', 'hidden', 'User Id', 0, 1, 1, 1, 1, 1, '{}', 15),
(50, 7, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 16),
(51, 7, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 17),
(52, 7, 'service_belongsto_city_relationship', 'relationship', 'cities', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\City\",\"table\":\"cities\",\"type\":\"belongsTo\",\"column\":\"city_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"cities\",\"pivot\":\"0\",\"taggable\":\"0\"}', 8),
(53, 7, 'service_belongsto_sub_category_relationship', 'relationship', 'sub_categories', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\SubCategory\",\"table\":\"sub_categories\",\"type\":\"belongsTo\",\"column\":\"sub_category_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"cities\",\"pivot\":\"0\",\"taggable\":\"0\"}', 10),
(54, 7, 'service_belongsto_user_relationship', 'relationship', 'users', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\User\",\"table\":\"users\",\"type\":\"belongsTo\",\"column\":\"user_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"cities\",\"pivot\":\"0\",\"taggable\":\"0\"}', 12),
(55, 8, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(56, 8, 'comment', 'text', 'Comment', 1, 1, 1, 1, 1, 1, '{}', 6),
(57, 8, 'budget', 'number', 'Budget', 1, 1, 1, 1, 1, 1, '{}', 7),
(58, 8, 'user_id', 'hidden', 'User Id', 1, 1, 1, 1, 1, 1, '{}', 5),
(59, 8, 'service_id', 'hidden', 'Service Id', 1, 1, 1, 1, 1, 1, '{}', 3),
(60, 8, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 8),
(61, 8, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 9),
(62, 8, 'comment_belongsto_user_relationship', 'relationship', 'users', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\User\",\"table\":\"users\",\"type\":\"belongsTo\",\"column\":\"user_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"cities\",\"pivot\":\"0\",\"taggable\":\"0\"}', 4),
(63, 8, 'comment_belongsto_service_relationship', 'relationship', 'services', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Service\",\"table\":\"services\",\"type\":\"belongsTo\",\"column\":\"service_id\",\"key\":\"id\",\"label\":\"title\",\"pivot_table\":\"cities\",\"pivot\":\"0\",\"taggable\":\"0\"}', 2),
(64, 9, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(65, 9, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '{}', 2),
(66, 9, 'slug', 'text', 'Slug', 1, 1, 1, 0, 0, 1, '{}', 3),
(67, 9, 'label', 'text', 'Label', 0, 1, 1, 1, 1, 1, '{}', 4),
(68, 9, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 5),
(69, 9, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(75, 5, 'sub_category_belongstomany_attribute_group_relationship', 'relationship', 'attribute_groups', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\AttributeGroup\",\"table\":\"attribute_groups\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"ad_attributes\",\"pivot\":\"1\",\"taggable\":\"on\"}', 7),
(76, 13, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(77, 13, 'attribute_group_id', 'hidden', 'Attribute Group Id', 1, 1, 1, 1, 1, 1, '{}', 5),
(78, 13, 'sub_category_id', 'hidden', 'Sub Category Id', 1, 1, 1, 1, 1, 1, '{}', 3),
(79, 13, 'created_at', 'timestamp', 'Created At', 0, 0, 1, 1, 0, 1, '{}', 8),
(80, 13, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 9),
(81, 13, 'ad_attribute_belongsto_attribute_group_relationship', 'relationship', 'attribute_groups', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\AttributeGroup\",\"table\":\"attribute_groups\",\"type\":\"belongsTo\",\"column\":\"attribute_group_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"ad_attributes\",\"pivot\":\"0\",\"taggable\":\"0\"}', 4),
(82, 13, 'ad_attribute_belongsto_sub_category_relationship', 'relationship', 'sub_categories', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\SubCategory\",\"table\":\"sub_categories\",\"type\":\"belongsTo\",\"column\":\"sub_category_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"ad_attributes\",\"pivot\":\"0\",\"taggable\":\"0\"}', 2),
(83, 14, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(84, 14, 'value', 'text', 'Value', 1, 1, 1, 1, 1, 1, '{}', 4),
(85, 14, 'icon', 'image', 'Icon', 0, 1, 1, 1, 1, 1, '{}', 5),
(86, 14, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 6),
(87, 14, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 7),
(88, 14, 'ad_attribute_id', 'text', 'Ad Attribute Id', 1, 1, 1, 1, 1, 1, '{}', 3),
(89, 14, 'attribute_belongsto_ad_attribute_relationship', 'relationship', 'ad_attributes', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\AdAttribute\",\"table\":\"ad_attributes\",\"type\":\"belongsTo\",\"column\":\"ad_attribute_id\",\"key\":\"id\",\"label\":\"ad_att_name\",\"pivot_table\":\"ad_attributes\",\"pivot\":\"0\",\"taggable\":\"0\"}', 2),
(90, 13, 'ad_attribute_hasmany_attribute_relationship', 'relationship', 'attributes', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Attribute\",\"table\":\"attributes\",\"type\":\"hasMany\",\"column\":\"ad_attribute_id\",\"key\":\"id\",\"label\":\"value\",\"pivot_table\":\"ad_attributes\",\"pivot\":\"0\",\"taggable\":\"0\"}', 10),
(91, 15, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(92, 15, 'key', 'text', 'Key', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required|unique:statuses,key\"}}', 3),
(93, 15, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 4),
(94, 15, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 5),
(95, 15, 'display', 'text', 'Display', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"}}', 2),
(96, 4, 'icon', 'image', 'Icon', 0, 1, 1, 1, 1, 1, '{}', 4),
(97, 13, 'form_type', 'select_dropdown', 'Form Type', 1, 1, 1, 1, 1, 1, '{\"default\":\"\",\"options\":{\"\":\"None\",\"selected\":\"selected\",\"text\":\"text\",\"textarea\":\"textarea\",\"checkbox\":\"checkbox\",\"number\":\"number\",\"date\":\"date\"}}', 6),
(98, 13, 'required', 'checkbox', 'Required', 0, 1, 1, 1, 1, 1, '{}', 7),
(99, 16, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(100, 16, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, '{}', 2),
(101, 16, 'description', 'text_area', 'Description', 1, 1, 1, 1, 1, 1, '{}', 3),
(102, 16, 'price', 'number', 'Price', 1, 1, 1, 1, 1, 1, '{}', 4),
(103, 16, 'city_id', 'text', 'City Id', 1, 1, 1, 1, 1, 1, '{}', 5),
(104, 16, 'status', 'text', 'Status', 0, 1, 1, 1, 1, 1, '{}', 6),
(105, 16, 'sub_category_id', 'text', 'Sub Category Id', 1, 1, 1, 1, 1, 1, '{}', 7),
(106, 16, 'slug', 'text', 'Slug', 0, 1, 1, 1, 1, 1, '{}', 8),
(107, 16, 'images', 'file', 'Images', 0, 1, 1, 1, 1, 1, '{}', 9),
(108, 16, 'location', 'text', 'Location', 0, 1, 1, 1, 1, 1, '{}', 10),
(109, 16, 'user_name', 'text', 'User Name', 0, 1, 1, 1, 1, 1, '{}', 11),
(110, 16, 'phone', 'text', 'Phone', 1, 1, 1, 1, 1, 1, '{}', 12),
(111, 16, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 13),
(112, 16, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 14),
(113, 17, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(114, 17, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '{}', 3),
(115, 17, 'attribute_group_id', 'hidden', 'Attribute Group Id', 1, 1, 1, 1, 1, 1, '{}', 4),
(116, 17, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 5),
(117, 17, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(118, 17, 'unit_belongsto_attribute_group_relationship', 'relationship', 'attribute_groups', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\AttributeGroup\",\"table\":\"attribute_groups\",\"type\":\"belongsTo\",\"column\":\"attribute_group_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"ad_attributes\",\"pivot\":\"0\",\"taggable\":\"0\"}', 2),
(119, 18, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(120, 18, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '{}', 2),
(121, 18, 'icon', 'text', 'Icon', 1, 1, 1, 1, 1, 1, '{}', 3),
(122, 18, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 4),
(123, 18, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 5),
(124, 7, 'service_belongsto_currency_relationship', 'relationship', 'currencies', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Currency\",\"table\":\"currencies\",\"type\":\"belongsTo\",\"column\":\"currency_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"ad_attributes\",\"pivot\":\"0\",\"taggable\":\"0\"}', 13),
(125, 7, 'currency_id', 'hidden', 'Currency Id', 1, 1, 1, 1, 1, 1, '{}', 14),
(126, 1, 'email_verified_at', 'timestamp', 'Email Verified At', 0, 1, 1, 1, 1, 1, '{}', 6),
(127, 1, 'phone', 'text', 'Phone', 0, 1, 1, 1, 1, 1, '{}', 12),
(128, 16, 'user_id', 'text', 'User Id', 0, 1, 1, 1, 1, 1, '{}', 15),
(129, 16, 'currency_id', 'text', 'Currency Id', 0, 1, 1, 1, 1, 1, '{}', 16),
(130, 19, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(131, 19, 'slug', 'text', 'Slug', 1, 1, 1, 1, 1, 1, '{}', 2),
(132, 19, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, '{}', 3),
(133, 19, 'body', 'rich_text_box', 'Body', 0, 1, 1, 1, 1, 1, '{}', 4),
(134, 19, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 5),
(135, 19, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(136, 23, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(137, 23, 'slug', 'text', 'Slug', 1, 1, 1, 1, 1, 1, '{}', 2),
(138, 23, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, '{}', 3),
(139, 23, 'body', 'rich_text_box', 'Body', 0, 1, 1, 1, 1, 1, '{}', 4),
(140, 23, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 5),
(141, 23, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6);

-- --------------------------------------------------------

--
-- Table structure for table `data_types`
--

CREATE TABLE `data_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT '0',
  `server_side` tinyint(4) NOT NULL DEFAULT '0',
  `details` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_types`
--

INSERT INTO `data_types` (`id`, `name`, `slug`, `display_name_singular`, `display_name_plural`, `icon`, `model_name`, `policy_name`, `controller`, `description`, `generate_permissions`, `server_side`, `details`, `created_at`, `updated_at`) VALUES
(1, 'users', 'users', 'User', 'Users', 'voyager-person', 'TCG\\Voyager\\Models\\User', 'TCG\\Voyager\\Policies\\UserPolicy', 'TCG\\Voyager\\Http\\Controllers\\VoyagerUserController', NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"desc\",\"default_search_key\":null,\"scope\":null}', '2020-01-31 13:04:15', '2020-02-24 15:10:12'),
(2, 'menus', 'menus', 'Menu', 'Menus', 'voyager-list', 'TCG\\Voyager\\Models\\Menu', NULL, '', '', 1, 0, NULL, '2020-01-31 13:04:15', '2020-01-31 13:04:15'),
(3, 'roles', 'roles', 'Role', 'Roles', 'voyager-lock', 'TCG\\Voyager\\Models\\Role', NULL, '', '', 1, 0, NULL, '2020-01-31 13:04:15', '2020-01-31 13:04:15'),
(4, 'main_categories', 'main-categories', 'Main Category', 'Main Categories', NULL, 'App\\MainCategory', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-01-31 15:51:50', '2020-02-10 15:33:01'),
(5, 'sub_categories', 'sub-categories', 'Sub Category', 'Sub Categories', NULL, 'App\\SubCategory', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-01-31 15:55:19', '2020-02-04 16:00:18'),
(6, 'cities', 'cities', 'City', 'Cities', NULL, 'App\\City', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-01-31 16:54:20', '2020-01-31 16:54:20'),
(7, 'services', 'services', 'Service', 'Services', NULL, 'App\\Service', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-01-31 17:01:30', '2020-02-23 04:33:06'),
(8, 'comments', 'comments', 'Comment', 'Comments', NULL, 'App\\Comment', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-02-02 16:28:43', '2020-02-02 16:37:33'),
(9, 'attribute_groups', 'attribute-groups', 'Attribute Group', 'Attribute Groups', NULL, 'App\\AttributeGroup', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-02-04 15:57:44', '2020-02-04 17:53:13'),
(13, 'ad_attributes', 'ad-attributes', 'Ad Attribute', 'Ad Attributes', NULL, 'App\\AdAttribute', NULL, NULL, NULL, 1, 0, '{\"order_column\":\"created_at\",\"order_display_column\":\"created_at\",\"order_direction\":\"desc\",\"default_search_key\":null,\"scope\":null}', '2020-02-04 17:25:43', '2020-02-11 14:25:24'),
(14, 'attributes', 'attributes', 'Attribute', 'Attributes', NULL, 'App\\Attribute', NULL, 'App\\Http\\Controllers\\AttributesController', NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-02-05 11:09:24', '2020-02-05 19:15:39'),
(15, 'statuses', 'statuses', 'Status', 'Statuses', NULL, 'App\\Status', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-02-09 17:57:36', '2020-02-09 17:57:36'),
(16, 'ads', 'ads', 'Ad', 'Ads', NULL, 'App\\Ad', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-02-11 17:11:55', '2020-03-01 18:45:28'),
(17, 'units', 'units', 'Unit', 'Units', NULL, 'App\\Unit', NULL, 'App\\Http\\Controllers\\UnitsController', NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-02-15 16:30:38', '2020-02-15 16:48:40'),
(18, 'currencies', 'currencies', 'Currency', 'Currencies', NULL, 'App\\Currency', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-02-21 15:55:18', '2020-02-21 15:55:18'),
(19, 'guidelines', 'guidelines', 'Guideline', 'Guidelines', NULL, 'App\\Guideline', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-03-10 05:22:13', '2020-03-10 05:22:13'),
(23, 'directions', 'directions', 'Direction', 'Directions', NULL, 'App\\Direction', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-03-10 05:26:58', '2020-03-10 05:26:58');

-- --------------------------------------------------------

--
-- Table structure for table `directions`
--

CREATE TABLE `directions` (
  `id` int(10) UNSIGNED NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `directions`
--

INSERT INTO `directions` (`id`, `slug`, `title`, `body`, `created_at`, `updated_at`) VALUES
(1, 'ads.index', 'الصفحة  الخاصة بعرض وتصفح المعلومات', '<p><span style=\"text-decoration: underline;\"><span style=\"color: #000000; text-decoration: underline;\">تستطيع البحث عن الاعلانات التي تريدها والتواصل مع اصحابها، كما ويمكنك فرز تلك الاعلانات من خلال قائمة البحث</span></span></p>', '2020-03-10 08:55:00', '2020-03-10 09:02:08');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `input_ad_values`
--

CREATE TABLE `input_ad_values` (
  `id` int(10) UNSIGNED NOT NULL,
  `ad_id` int(11) NOT NULL,
  `ad_attribute_id` int(11) NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `unit_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `input_ad_values`
--

INSERT INTO `input_ad_values` (`id`, `ad_id`, `ad_attribute_id`, `value`, `unit_id`) VALUES
(1, 14, 27, 'مع كفر وشاحن لاسلكي', NULL),
(2, 14, 33, '1500', 5),
(3, 15, 27, 'مع كفر وشاحن لاسلكي', NULL),
(4, 15, 33, '1500', 5),
(5, 16, 27, 'مع كفر وشاحن لاسلكي', NULL),
(6, 16, 33, '1500', 5),
(7, 17, 27, 'مع كفر وشاحن لاسلكي', NULL),
(8, 17, 33, '1500', 5),
(9, 18, 12, '2019', NULL),
(10, 18, 13, '1500', NULL),
(11, 19, 27, 'مع كفر وشاحن لاسلكي', NULL),
(12, 19, 33, '1500', 6),
(13, 20, 27, 'شاشة ضد الكسر', NULL),
(14, 20, 33, '5000', 5),
(15, 21, 18, '15933', 3),
(16, 21, 28, 'شارع', NULL),
(17, 22, 18, '15933', 3),
(18, 22, 28, 'شارع', NULL),
(19, 23, 27, 'شاشة ضد الكسر و كفر منيح', NULL),
(20, 23, 33, '1500', 5),
(21, 25, 27, 'شاشة ضد الكسر', NULL),
(22, 25, 33, '2000', 5);

-- --------------------------------------------------------

--
-- Table structure for table `main_categories`
--

CREATE TABLE `main_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `main_categories`
--

INSERT INTO `main_categories` (`id`, `name`, `description`, `created_at`, `updated_at`, `icon`) VALUES
(1, 'سيارات ومركبات', 'سيارات ومركبات', '2020-01-31 16:00:00', '2020-02-10 15:59:33', 'main-categories\\February2020\\EQrGvDv1kdxBHS724R28.png'),
(2, 'عقارات للبيع', 'عقارات للبيع', '2020-01-31 16:07:00', '2020-02-10 16:00:08', 'main-categories\\February2020\\OB0CnUOmi7LoVAnTyQpj.png'),
(6, 'اثاث وديكور', 'اثاث وديكور', '2020-01-31 16:42:00', '2020-02-10 16:01:44', 'main-categories\\February2020\\fJHjmIoVN5TH09wrbMTF.png'),
(7, 'اجهزة الكترونية', 'اجهزة الكترونية', '2020-01-31 16:43:00', '2020-02-10 15:56:19', 'main-categories\\February2020\\BmmwfaszwVhK03Uz5p6j.png'),
(8, 'عقارات للايجار', 'عقارات للايجار  عقارات للايجار', '2020-02-04 18:46:00', '2020-02-10 16:03:57', 'main-categories\\February2020\\ueun7Dlqxf39E6rM5I93.png'),
(13, 'قطع مركبات', 'قطع مركباتقطع مركبات قطع مركباتقطع مركبات', '2020-02-06 06:16:00', '2020-02-10 15:34:08', 'main-categories\\February2020\\1IwHD5DzbzUArsf3qGer.png');

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', '2020-01-31 13:04:16', '2020-01-31 13:04:16');

-- --------------------------------------------------------

--
-- Table structure for table `menu_items`
--

CREATE TABLE `menu_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menu_items`
--

INSERT INTO `menu_items` (`id`, `menu_id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `order`, `created_at`, `updated_at`, `route`, `parameters`) VALUES
(1, 1, 'Dashboard', '', '_self', 'voyager-boat', NULL, NULL, 1, '2020-01-31 13:04:16', '2020-01-31 13:04:16', 'voyager.dashboard', NULL),
(2, 1, 'Media', '', '_self', 'voyager-images', NULL, NULL, 5, '2020-01-31 13:04:16', '2020-01-31 13:04:16', 'voyager.media.index', NULL),
(3, 1, 'Users', '', '_self', 'voyager-person', NULL, NULL, 3, '2020-01-31 13:04:16', '2020-01-31 13:04:16', 'voyager.users.index', NULL),
(4, 1, 'Roles', '', '_self', 'voyager-lock', NULL, NULL, 2, '2020-01-31 13:04:17', '2020-01-31 13:04:17', 'voyager.roles.index', NULL),
(5, 1, 'Tools', '', '_self', 'voyager-tools', NULL, NULL, 9, '2020-01-31 13:04:17', '2020-01-31 13:04:17', NULL, NULL),
(6, 1, 'Menu Builder', '', '_self', 'voyager-list', NULL, 5, 10, '2020-01-31 13:04:17', '2020-01-31 13:04:17', 'voyager.menus.index', NULL),
(7, 1, 'Database', '', '_self', 'voyager-data', NULL, 5, 11, '2020-01-31 13:04:17', '2020-01-31 13:04:17', 'voyager.database.index', NULL),
(8, 1, 'Compass', '', '_self', 'voyager-compass', NULL, 5, 12, '2020-01-31 13:04:17', '2020-01-31 13:04:17', 'voyager.compass.index', NULL),
(9, 1, 'BREAD', '', '_self', 'voyager-bread', NULL, 5, 13, '2020-01-31 13:04:17', '2020-01-31 13:04:17', 'voyager.bread.index', NULL),
(10, 1, 'Settings', '', '_self', 'voyager-settings', NULL, NULL, 14, '2020-01-31 13:04:17', '2020-01-31 13:04:17', 'voyager.settings.index', NULL),
(11, 1, 'Hooks', '', '_self', 'voyager-hook', NULL, 5, 13, '2020-01-31 13:04:21', '2020-01-31 13:04:21', 'voyager.hooks', NULL),
(12, 1, 'Main Categories', '', '_self', NULL, NULL, NULL, 15, '2020-01-31 15:51:51', '2020-01-31 15:51:51', 'voyager.main-categories.index', NULL),
(13, 1, 'Sub Categories', '', '_self', NULL, NULL, NULL, 16, '2020-01-31 15:55:19', '2020-01-31 15:55:19', 'voyager.sub-categories.index', NULL),
(14, 1, 'Cities', '', '_self', NULL, NULL, NULL, 17, '2020-01-31 16:54:21', '2020-01-31 16:54:21', 'voyager.cities.index', NULL),
(15, 1, 'Services', '', '_self', NULL, NULL, NULL, 18, '2020-01-31 17:01:30', '2020-01-31 17:01:30', 'voyager.services.index', NULL),
(16, 1, 'Comments', '', '_self', NULL, NULL, NULL, 19, '2020-02-02 16:28:44', '2020-02-02 16:28:44', 'voyager.comments.index', NULL),
(17, 1, 'Attribute Groups', '', '_self', NULL, NULL, NULL, 20, '2020-02-04 15:57:46', '2020-02-04 15:57:46', 'voyager.attribute-groups.index', NULL),
(19, 1, 'Ad Attributes', '', '_self', NULL, NULL, NULL, 22, '2020-02-04 17:25:43', '2020-02-04 17:25:43', 'voyager.ad-attributes.index', NULL),
(20, 1, 'Attributes', '', '_self', NULL, NULL, NULL, 23, '2020-02-05 11:09:24', '2020-02-05 11:09:24', 'voyager.attributes.index', NULL),
(21, 1, 'Statuses', '', '_self', NULL, NULL, NULL, 24, '2020-02-09 17:57:37', '2020-02-09 17:57:37', 'voyager.statuses.index', NULL),
(22, 1, 'Ads', '', '_self', NULL, NULL, NULL, 25, '2020-02-11 17:11:56', '2020-02-11 17:11:56', 'voyager.ads.index', NULL),
(23, 1, 'Units', '', '_self', NULL, NULL, NULL, 26, '2020-02-15 16:30:38', '2020-02-15 16:30:38', 'voyager.units.index', NULL),
(24, 1, 'Currencies', '', '_self', NULL, NULL, NULL, 27, '2020-02-21 15:55:19', '2020-02-21 15:55:19', 'voyager.currencies.index', NULL),
(26, 1, 'Directions', '', '_self', NULL, NULL, NULL, 29, '2020-03-10 05:26:58', '2020-03-10 05:26:58', 'voyager.directions.index', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_01_01_000000_add_voyager_user_fields', 1),
(4, '2016_01_01_000000_create_data_types_table', 1),
(5, '2016_05_19_173453_create_menu_table', 1),
(6, '2016_10_21_190000_create_roles_table', 1),
(7, '2016_10_21_190000_create_settings_table', 1),
(8, '2016_11_30_135954_create_permission_table', 1),
(9, '2016_11_30_141208_create_permission_role_table', 1),
(10, '2016_12_26_201236_data_types__add__server_side', 1),
(11, '2017_01_13_000000_add_route_to_menu_items_table', 1),
(12, '2017_01_14_005015_create_translations_table', 1),
(13, '2017_01_15_000000_make_table_name_nullable_in_permissions_table', 1),
(14, '2017_03_06_000000_add_controller_to_data_types_table', 1),
(15, '2017_04_21_000000_add_order_to_data_rows_table', 1),
(16, '2017_07_05_210000_add_policyname_to_data_types_table', 1),
(17, '2017_08_05_000000_add_group_to_settings_table', 1),
(18, '2017_11_26_013050_add_user_role_relationship', 1),
(19, '2017_11_26_015000_create_user_roles_table', 1),
(20, '2018_03_11_000000_add_user_settings', 1),
(21, '2018_03_14_000000_add_details_to_data_types_table', 1),
(22, '2018_03_16_000000_make_settings_value_nullable', 1),
(23, '2019_08_19_000000_create_failed_jobs_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `offers`
--

CREATE TABLE `offers` (
  `id` int(10) UNSIGNED NOT NULL,
  `service_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `amount` int(10) UNSIGNED NOT NULL,
  `currency_id` int(10) UNSIGNED NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `offers`
--

INSERT INTO `offers` (`id`, `service_id`, `user_id`, `amount`, `currency_id`, `body`, `created_at`, `updated_at`) VALUES
(1, 15, 1, 12, 1, 'sf sdf sdf sdf df', '2020-02-28 12:30:47', '2020-02-28 12:30:47'),
(2, 15, 1, 34, 1, 'xd d fsdf sdfsdf', '2020-02-28 12:31:12', '2020-02-28 12:31:12'),
(3, 15, 1, 12, 1, 'ابدأ ببناء مشروعك ابدأ ببناء مشروعك\r\nابدأ ببناء مشروعك\r\nابدأ ببناء مشروعك\r\n\r\nابدأ ببناء مشروعك\r\nابدأ ببناء مشروعكابدأ ببناء مشروعك ابدأ ببناء مشروعك\r\nابدأ ببناء مشروعك\r\nابدأ ببناء مشروعك\r\n\r\nابدأ ببناء مشروعك\r\nابدأ ببناء مشروعك', '2020-02-28 13:06:33', '2020-02-28 13:06:33'),
(4, 11, 1, 123, 1, 'من خلال مستقل تستطيع بناء مشروعك بالشكل الذي تريد، أدخل تفاصيل المشروع والميزانية المتوقعة، ليتم مراجعة مشروعك ونشره مجاناً على مستقل. بعد ذلك، سيتقدّم أفضل المستقلين المسجلين بعروض مختلفة لتختار العرض المناسب لك وتبدأ بتنفيذ مشروعك.', '2020-02-28 14:38:50', '2020-02-28 14:38:50'),
(5, 11, 1, 156, 2, 'من خلال مستقل تستطيع بناء مشروعك بالشكل الذي تريد، أدخل تفاصيل المشروع والميزانية المتوقعة، ليتم مرا', '2020-02-28 14:42:11', '2020-02-28 14:42:11'),
(6, 18, 1, 17000, 1, 'من خلال مستقل تستطيع بناء مشروعك بالشكل الذي تريد، أدخل تفاصيل المشروع والميزانية المتوقعة، ليتم مراجعة مشروعك ونشره مجاناً على', '2020-03-03 08:26:52', '2020-03-03 08:26:52');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('harb.hasn98@gmail.com', '$2y$10$y/1ku7X.ahq4Jp2MrYCf9eo9V5.8CxtVWzmdpdQ6LID/f.8KrmlHK', '2020-03-17 18:37:02');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`) VALUES
(1, 'browse_admin', NULL, '2020-01-31 13:04:17', '2020-01-31 13:04:17'),
(2, 'browse_bread', NULL, '2020-01-31 13:04:17', '2020-01-31 13:04:17'),
(3, 'browse_database', NULL, '2020-01-31 13:04:17', '2020-01-31 13:04:17'),
(4, 'browse_media', NULL, '2020-01-31 13:04:17', '2020-01-31 13:04:17'),
(5, 'browse_compass', NULL, '2020-01-31 13:04:17', '2020-01-31 13:04:17'),
(6, 'browse_menus', 'menus', '2020-01-31 13:04:18', '2020-01-31 13:04:18'),
(7, 'read_menus', 'menus', '2020-01-31 13:04:18', '2020-01-31 13:04:18'),
(8, 'edit_menus', 'menus', '2020-01-31 13:04:18', '2020-01-31 13:04:18'),
(9, 'add_menus', 'menus', '2020-01-31 13:04:18', '2020-01-31 13:04:18'),
(10, 'delete_menus', 'menus', '2020-01-31 13:04:18', '2020-01-31 13:04:18'),
(11, 'browse_roles', 'roles', '2020-01-31 13:04:18', '2020-01-31 13:04:18'),
(12, 'read_roles', 'roles', '2020-01-31 13:04:18', '2020-01-31 13:04:18'),
(13, 'edit_roles', 'roles', '2020-01-31 13:04:18', '2020-01-31 13:04:18'),
(14, 'add_roles', 'roles', '2020-01-31 13:04:18', '2020-01-31 13:04:18'),
(15, 'delete_roles', 'roles', '2020-01-31 13:04:18', '2020-01-31 13:04:18'),
(16, 'browse_users', 'users', '2020-01-31 13:04:18', '2020-01-31 13:04:18'),
(17, 'read_users', 'users', '2020-01-31 13:04:18', '2020-01-31 13:04:18'),
(18, 'edit_users', 'users', '2020-01-31 13:04:18', '2020-01-31 13:04:18'),
(19, 'add_users', 'users', '2020-01-31 13:04:18', '2020-01-31 13:04:18'),
(20, 'delete_users', 'users', '2020-01-31 13:04:18', '2020-01-31 13:04:18'),
(21, 'browse_settings', 'settings', '2020-01-31 13:04:18', '2020-01-31 13:04:18'),
(22, 'read_settings', 'settings', '2020-01-31 13:04:19', '2020-01-31 13:04:19'),
(23, 'edit_settings', 'settings', '2020-01-31 13:04:19', '2020-01-31 13:04:19'),
(24, 'add_settings', 'settings', '2020-01-31 13:04:19', '2020-01-31 13:04:19'),
(25, 'delete_settings', 'settings', '2020-01-31 13:04:19', '2020-01-31 13:04:19'),
(26, 'browse_hooks', NULL, '2020-01-31 13:04:21', '2020-01-31 13:04:21'),
(27, 'browse_main_categories', 'main_categories', '2020-01-31 15:51:50', '2020-01-31 15:51:50'),
(28, 'read_main_categories', 'main_categories', '2020-01-31 15:51:50', '2020-01-31 15:51:50'),
(29, 'edit_main_categories', 'main_categories', '2020-01-31 15:51:50', '2020-01-31 15:51:50'),
(30, 'add_main_categories', 'main_categories', '2020-01-31 15:51:50', '2020-01-31 15:51:50'),
(31, 'delete_main_categories', 'main_categories', '2020-01-31 15:51:51', '2020-01-31 15:51:51'),
(32, 'browse_sub_categories', 'sub_categories', '2020-01-31 15:55:19', '2020-01-31 15:55:19'),
(33, 'read_sub_categories', 'sub_categories', '2020-01-31 15:55:19', '2020-01-31 15:55:19'),
(34, 'edit_sub_categories', 'sub_categories', '2020-01-31 15:55:19', '2020-01-31 15:55:19'),
(35, 'add_sub_categories', 'sub_categories', '2020-01-31 15:55:19', '2020-01-31 15:55:19'),
(36, 'delete_sub_categories', 'sub_categories', '2020-01-31 15:55:19', '2020-01-31 15:55:19'),
(37, 'browse_cities', 'cities', '2020-01-31 16:54:21', '2020-01-31 16:54:21'),
(38, 'read_cities', 'cities', '2020-01-31 16:54:21', '2020-01-31 16:54:21'),
(39, 'edit_cities', 'cities', '2020-01-31 16:54:21', '2020-01-31 16:54:21'),
(40, 'add_cities', 'cities', '2020-01-31 16:54:21', '2020-01-31 16:54:21'),
(41, 'delete_cities', 'cities', '2020-01-31 16:54:21', '2020-01-31 16:54:21'),
(42, 'browse_services', 'services', '2020-01-31 17:01:30', '2020-01-31 17:01:30'),
(43, 'read_services', 'services', '2020-01-31 17:01:30', '2020-01-31 17:01:30'),
(44, 'edit_services', 'services', '2020-01-31 17:01:30', '2020-01-31 17:01:30'),
(45, 'add_services', 'services', '2020-01-31 17:01:30', '2020-01-31 17:01:30'),
(46, 'delete_services', 'services', '2020-01-31 17:01:30', '2020-01-31 17:01:30'),
(47, 'browse_comments', 'comments', '2020-02-02 16:28:44', '2020-02-02 16:28:44'),
(48, 'read_comments', 'comments', '2020-02-02 16:28:44', '2020-02-02 16:28:44'),
(49, 'edit_comments', 'comments', '2020-02-02 16:28:44', '2020-02-02 16:28:44'),
(50, 'add_comments', 'comments', '2020-02-02 16:28:44', '2020-02-02 16:28:44'),
(51, 'delete_comments', 'comments', '2020-02-02 16:28:44', '2020-02-02 16:28:44'),
(52, 'browse_attribute_groups', 'attribute_groups', '2020-02-04 15:57:45', '2020-02-04 15:57:45'),
(53, 'read_attribute_groups', 'attribute_groups', '2020-02-04 15:57:45', '2020-02-04 15:57:45'),
(54, 'edit_attribute_groups', 'attribute_groups', '2020-02-04 15:57:45', '2020-02-04 15:57:45'),
(55, 'add_attribute_groups', 'attribute_groups', '2020-02-04 15:57:45', '2020-02-04 15:57:45'),
(56, 'delete_attribute_groups', 'attribute_groups', '2020-02-04 15:57:45', '2020-02-04 15:57:45'),
(57, 'browse_add_attributes', 'add_attributes', '2020-02-04 15:58:03', '2020-02-04 15:58:03'),
(58, 'read_add_attributes', 'add_attributes', '2020-02-04 15:58:03', '2020-02-04 15:58:03'),
(59, 'edit_add_attributes', 'add_attributes', '2020-02-04 15:58:03', '2020-02-04 15:58:03'),
(60, 'add_add_attributes', 'add_attributes', '2020-02-04 15:58:03', '2020-02-04 15:58:03'),
(61, 'delete_add_attributes', 'add_attributes', '2020-02-04 15:58:03', '2020-02-04 15:58:03'),
(62, 'browse_ad_attributes', 'ad_attributes', '2020-02-04 17:25:43', '2020-02-04 17:25:43'),
(63, 'read_ad_attributes', 'ad_attributes', '2020-02-04 17:25:43', '2020-02-04 17:25:43'),
(64, 'edit_ad_attributes', 'ad_attributes', '2020-02-04 17:25:43', '2020-02-04 17:25:43'),
(65, 'add_ad_attributes', 'ad_attributes', '2020-02-04 17:25:43', '2020-02-04 17:25:43'),
(66, 'delete_ad_attributes', 'ad_attributes', '2020-02-04 17:25:43', '2020-02-04 17:25:43'),
(67, 'browse_attributes', 'attributes', '2020-02-05 11:09:24', '2020-02-05 11:09:24'),
(68, 'read_attributes', 'attributes', '2020-02-05 11:09:24', '2020-02-05 11:09:24'),
(69, 'edit_attributes', 'attributes', '2020-02-05 11:09:24', '2020-02-05 11:09:24'),
(70, 'add_attributes', 'attributes', '2020-02-05 11:09:24', '2020-02-05 11:09:24'),
(71, 'delete_attributes', 'attributes', '2020-02-05 11:09:24', '2020-02-05 11:09:24'),
(72, 'browse_statuses', 'statuses', '2020-02-09 17:57:36', '2020-02-09 17:57:36'),
(73, 'read_statuses', 'statuses', '2020-02-09 17:57:36', '2020-02-09 17:57:36'),
(74, 'edit_statuses', 'statuses', '2020-02-09 17:57:36', '2020-02-09 17:57:36'),
(75, 'add_statuses', 'statuses', '2020-02-09 17:57:36', '2020-02-09 17:57:36'),
(76, 'delete_statuses', 'statuses', '2020-02-09 17:57:36', '2020-02-09 17:57:36'),
(77, 'browse_ads', 'ads', '2020-02-11 17:11:55', '2020-02-11 17:11:55'),
(78, 'read_ads', 'ads', '2020-02-11 17:11:55', '2020-02-11 17:11:55'),
(79, 'edit_ads', 'ads', '2020-02-11 17:11:55', '2020-02-11 17:11:55'),
(80, 'add_ads', 'ads', '2020-02-11 17:11:55', '2020-02-11 17:11:55'),
(81, 'delete_ads', 'ads', '2020-02-11 17:11:55', '2020-02-11 17:11:55'),
(82, 'browse_units', 'units', '2020-02-15 16:30:38', '2020-02-15 16:30:38'),
(83, 'read_units', 'units', '2020-02-15 16:30:38', '2020-02-15 16:30:38'),
(84, 'edit_units', 'units', '2020-02-15 16:30:38', '2020-02-15 16:30:38'),
(85, 'add_units', 'units', '2020-02-15 16:30:38', '2020-02-15 16:30:38'),
(86, 'delete_units', 'units', '2020-02-15 16:30:38', '2020-02-15 16:30:38'),
(87, 'browse_currencies', 'currencies', '2020-02-21 15:55:18', '2020-02-21 15:55:18'),
(88, 'read_currencies', 'currencies', '2020-02-21 15:55:18', '2020-02-21 15:55:18'),
(89, 'edit_currencies', 'currencies', '2020-02-21 15:55:18', '2020-02-21 15:55:18'),
(90, 'add_currencies', 'currencies', '2020-02-21 15:55:18', '2020-02-21 15:55:18'),
(91, 'delete_currencies', 'currencies', '2020-02-21 15:55:18', '2020-02-21 15:55:18'),
(92, 'browse_guidelines', 'guidelines', '2020-03-10 05:22:13', '2020-03-10 05:22:13'),
(93, 'read_guidelines', 'guidelines', '2020-03-10 05:22:13', '2020-03-10 05:22:13'),
(94, 'edit_guidelines', 'guidelines', '2020-03-10 05:22:13', '2020-03-10 05:22:13'),
(95, 'add_guidelines', 'guidelines', '2020-03-10 05:22:13', '2020-03-10 05:22:13'),
(96, 'delete_guidelines', 'guidelines', '2020-03-10 05:22:13', '2020-03-10 05:22:13'),
(97, 'browse_directions', 'directions', '2020-03-10 05:26:58', '2020-03-10 05:26:58'),
(98, 'read_directions', 'directions', '2020-03-10 05:26:58', '2020-03-10 05:26:58'),
(99, 'edit_directions', 'directions', '2020-03-10 05:26:58', '2020-03-10 05:26:58'),
(100, 'add_directions', 'directions', '2020-03-10 05:26:58', '2020-03-10 05:26:58'),
(101, 'delete_directions', 'directions', '2020-03-10 05:26:58', '2020-03-10 05:26:58');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(27, 1),
(28, 1),
(29, 1),
(30, 1),
(31, 1),
(32, 1),
(33, 1),
(34, 1),
(35, 1),
(36, 1),
(37, 1),
(38, 1),
(39, 1),
(40, 1),
(41, 1),
(42, 1),
(43, 1),
(44, 1),
(45, 1),
(46, 1),
(47, 1),
(48, 1),
(49, 1),
(50, 1),
(51, 1),
(52, 1),
(53, 1),
(54, 1),
(55, 1),
(56, 1),
(57, 1),
(58, 1),
(59, 1),
(60, 1),
(61, 1),
(62, 1),
(63, 1),
(64, 1),
(65, 1),
(66, 1),
(67, 1),
(68, 1),
(69, 1),
(70, 1),
(71, 1),
(72, 1),
(73, 1),
(74, 1),
(75, 1),
(76, 1),
(77, 1),
(78, 1),
(79, 1),
(80, 1),
(81, 1),
(82, 1),
(83, 1),
(84, 1),
(85, 1),
(86, 1),
(87, 1),
(88, 1),
(89, 1),
(90, 1),
(91, 1),
(92, 1),
(93, 1),
(94, 1),
(95, 1),
(96, 1),
(97, 1),
(98, 1),
(99, 1),
(100, 1),
(101, 1);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Administrator', '2020-01-31 13:04:17', '2020-01-31 13:04:17'),
(2, 'user', 'Normal User', '2020-01-31 13:04:17', '2020-01-31 13:04:17');

-- --------------------------------------------------------

--
-- Table structure for table `saved_ads`
--

CREATE TABLE `saved_ads` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `ad_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `saved_ads`
--

INSERT INTO `saved_ads` (`id`, `user_id`, `ad_id`) VALUES
(5, 1, 16),
(6, 4, 23),
(7, 4, 21);

-- --------------------------------------------------------

--
-- Table structure for table `saved_services`
--

CREATE TABLE `saved_services` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `service_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `saved_services`
--

INSERT INTO `saved_services` (`id`, `user_id`, `service_id`) VALUES
(16, 1, 15),
(17, 1, 11),
(19, 1, 23),
(23, 4, 18),
(25, 4, 15);

-- --------------------------------------------------------

--
-- Table structure for table `selected_ad_values`
--

CREATE TABLE `selected_ad_values` (
  `id` int(10) UNSIGNED NOT NULL,
  `ad_id` int(11) NOT NULL,
  `ad_attribute_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `selected_ad_values`
--

INSERT INTO `selected_ad_values` (`id`, `ad_id`, `ad_attribute_id`, `attribute_id`) VALUES
(1, 14, 8, 27),
(2, 14, 10, 130),
(3, 14, 25, 125),
(4, 15, 8, 27),
(5, 15, 10, 130),
(6, 15, 25, 125),
(7, 16, 8, 27),
(8, 16, 10, 130),
(9, 16, 25, 125),
(10, 17, 8, 27),
(11, 17, 10, 130),
(12, 17, 25, 125),
(13, 18, 14, 109),
(14, 18, 24, 106),
(15, 19, 8, 33),
(16, 19, 10, 127),
(17, 19, 25, 125),
(18, 20, 8, 124),
(19, 20, 10, 129),
(20, 20, 25, 126),
(21, 23, 8, 27),
(22, 23, 10, 129),
(23, 23, 25, 125),
(24, 25, 8, 33),
(25, 25, 10, 129),
(26, 25, 25, 125);

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `budget_min` int(10) UNSIGNED NOT NULL,
  `budget_max` int(10) UNSIGNED NOT NULL,
  `status` int(10) UNSIGNED DEFAULT '0',
  `city_id` int(11) NOT NULL,
  `sub_category_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `currency_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `title`, `slug`, `description`, `budget_min`, `budget_max`, `status`, `city_id`, `sub_category_id`, `user_id`, `created_at`, `updated_at`, `currency_id`) VALUES
(5, 'مطلوب سيارة للبيع', 'مطلوب-سيارة-للبيع', 'مطلوب سيارة للبيع مطلوب سيارة للبيع مطلوب سيارة للبيع', 6, 8, 1, 2, 16, 1, '2020-02-20 14:33:08', '2020-02-20 14:33:08', 1),
(6, 'sdfsdf', 'sdfsdf', 'dfsdfkmsd flksd fl', 345435, 454, 1, 3, 12, 1, '2020-02-20 14:33:39', '2020-02-20 14:33:39', 1),
(7, 'ابدأ ببناء مشروعك', 'ابدأ-ببناء-مشروعك', 'من خلال مستقل تستطيع بناء مشروعك بالشكل الذي تريد، أدخل تفاصيل المشروع و', 12, 15, 1, 3, 12, 1, '2020-02-20 14:54:44', '2020-02-20 14:54:44', 1),
(8, 'ابدأ ببناء مشروعك', 'ابدأ-ببناء-مشروعك-1', 'من خلال مستقل تستطيع بناء مشروعك بالشكل الذي تريد، أدخل تفاصيل المشروع والميزانية المتوقعة، ليتم مراجعة مشروعك ونشره مجاناً على مستقل. بعد ذلك، سيتقدّم أفضل المستقلين المسجلين بعروض مختلفة لتختار العرض المناسب لك وتبدأ بتنفيذ مشروعك.', 12, 123, 0, 1, 6, 1, '2020-02-20 15:41:49', '2020-02-20 15:41:49', 1),
(9, 'ابدأ ببناء مشروعك', 'ابدأ-ببناء-مشروعك-2', 'من خلال مستقل تستطيع بناء مشروعك بالشكل الذي تريد، أدخل تفاصيل المشروع والميزانية المتوقعة، ليتم مراجعة مشروعك ونشره مجاناً على مستقل. بعد ذلك، سيتقدّم أفضل المستقلين المسجلين بعروض مختلفة لتختار العرض المناسب لك وتبدأ بتنفيذ مشروعك.', 15, 232, 0, 2, 28, 1, '2020-02-20 15:43:08', '2020-02-20 15:43:08', 1),
(11, 'ابدأ ببناء مشروعك ابدأ ببناء مشروعك', 'ابدأ-ببناء-مشروعك-ابدأ-ببناء-مشروعك', 'من خلال مستقل تستطيع بناء مشروعك بالشكل الذي تريد، أدخل تفاصيل المشروع والميزانية المتوقعة، ليتم مراجعة مشروعك ونشره مجاناً على مستقل. بعد ذلك، سيتقدّم أفضل المستقلين المسجلين بعروض مختلفة لتختار العرض المناسب لك وتبدأ بتنفيذ مشروعك. \r\n\r\nمن خلال مستقل تستطيع بناء مشروعك بالشكل الذي تريد، أدخل تفاصيل المشروع والميزانية المتوقعة، ليتم مراجعة مشروعك ونشره مجاناً على مستقل. بعد ذلك، سيتقدّم أفضل المستقلين المسجلين بعروض مختلفة لتختار العرض المناسب لك وتبدأ بتنفيذ مشروعك.\r\n\r\nمن خلال مستقل تستطيع بناء مشروعك بالشكل الذي تريد، أدخل تفاصيل المشروع والميزانية المتوقعة، ليتم مراجعة مشروعك ونشره مجاناً على مستقل. بعد ذلك، سيتقدّم أفضل المستقلين المسجلين بعروض مختلفة لتختار العرض المناسب لك وتبدأ بتنفيذ مشروعك.', 12, 15, 1, 3, 8, 1, '2020-02-21 08:41:00', '2020-02-21 16:35:40', 1),
(12, 'ابدأ ببناء مشروعك ابدأ ببناء مشروعك', 'ابدأ-ببناء-مشروعك-ابدأ-ببناء-مشروعك-1', 'ابدأ ببناء مشروعك ابدأ ببناء مشروعك\r\nابدأ ببناء مشروعك\r\nابدأ ببناء مشروعك\r\n\r\nابدأ ببناء مشروعك\r\nابدأ ببناء مشروعك\r\n\r\n\r\nابدأ ببناء مشروعك\r\nابدأ ببناء مشروعك\r\nابدأ ببناء مشروعك', 12, 34, 1, 3, 4, 5, '2020-02-23 03:53:00', '2020-02-23 04:33:57', 1),
(13, 'ابدأ ببناء مشروعك ابدأ ببناء مشروعك', 'ابدأ-ببناء-مشروعك-ابدأ-ببناء-مشروعك-2', 'ابدأ ببناء مشروعك ابدأ ببناء مشروعك\r\nابدأ ببناء مشروعك\r\nابدأ ببناء مشروعك\r\n\r\nابدأ ببناء مشروعك\r\nابدأ ببناء مشروعك\r\n\r\n\r\nابدأ ببناء مشروعك\r\nابدأ ببناء مشروعك\r\nابدأ ببناء مشروعك', 12, 34, 0, 3, 4, 5, '2020-02-23 03:54:00', '2020-02-23 04:33:39', 1),
(14, 'ابدأ ببناء مشروعك ابدأ ببناء مشروعك', 'ابدأ-ببناء-مشروعك-ابدأ-ببناء-مشروعك-3', 'ابدأ ببناء مشروعك ابدأ ببناء مشروعك\r\nابدأ ببناء مشروعك\r\nابدأ ببناء مشروعك\r\n\r\nابدأ ببناء مشروعك\r\nابدأ ببناء مشروعك\r\n\r\n\r\nابدأ ببناء مشروعك\r\nابدأ ببناء مشروعك\r\nابدأ ببناء مشروعك', 12, 34, 0, 3, 4, 5, '2020-02-23 03:57:35', '2020-02-23 03:57:35', 0),
(15, 'ابدأ ببناء مشروعك ابدأ ببناء مشروعك', 'ابدأ-ببناء-مشروعك-ابدأ-ببناء-مشروعك-4', 'ابدأ ببناء مشروعك ابدأ ببناء مشروعك\r\nابدأ ببناء مشروعك\r\nابدأ ببناء مشروعك\r\n\r\nابدأ ببناء مشروعك\r\nابدأ ببناء مشروعك\r\n\r\n\r\nابدأ ببناء مشروعك\r\nابدأ ببناء مشروعك\r\nابدأ ببناء مشروعك', 12, 34, 1, 3, 4, 5, '2020-02-23 03:58:00', '2020-02-23 04:34:44', 1),
(16, 'هاتف ايفون', 'هاتف-ايفون', 'هاي كشمسين شنكسمينش كسمين شكسمينشكمس', 1200, 1500, 1, 1, 16, 1, '2020-02-23 06:00:00', '2020-02-23 06:01:05', 1),
(17, 'ما هو \"لوريم إيبسوم\" ؟', 'ما-هو-لوريم-إيبسوم-', 'لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة', 500, 1400, 0, 1, 5, 1, '2020-03-02 15:57:49', '2020-03-02 15:57:49', 1),
(18, 'مطلوب سيارة', 'مطلوب-سيارة', 'من خلال مستقل تستطيع بناء مشروعك بالشكل الذي تريد، أدخل تفاصيل المشروع والميزانية المتوقعة، ليتم مراجعة مشروعك ونشره مجاناً على مستقل. بعد ذلك، سيتقدّم أفضل المستقلين المسجلين بعروض مختلفة لتختار العرض المناسب لك وتبدأ بتنفيذ مشروعك.', 15000, 20000, 1, 4, 16, 1, '2020-03-03 08:23:00', '2020-03-03 08:24:42', 2);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `details` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `group` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `key`, `display_name`, `value`, `details`, `type`, `order`, `group`) VALUES
(1, 'site.title', 'Site Title', 'Site Title', '', 'text', 1, 'Site'),
(2, 'site.description', 'Site Description', 'Site Description', '', 'text', 2, 'Site'),
(3, 'site.logo', 'Site Logo', '', '', 'image', 3, 'Site'),
(4, 'site.google_analytics_tracking_id', 'Google Analytics Tracking ID', NULL, '', 'text', 4, 'Site'),
(5, 'admin.bg_image', 'Admin Background Image', 'settings\\March2020\\YsjpnRyzVZIezkM70lKo.jpg', '', 'image', 5, 'Admin'),
(6, 'admin.title', 'Admin Title', 'Krakeeb', '', 'text', 1, 'Admin'),
(7, 'admin.description', 'Admin Description', 'Welcome to Voyager. The Missing Admin for Laravel', '', 'text', 2, 'Admin'),
(8, 'admin.loader', 'Admin Loader', 'settings\\March2020\\1ddxoDfJqjIPPRpvL2MA.png', '', 'image', 3, 'Admin'),
(9, 'admin.icon_image', 'Admin Icon Image', 'settings\\March2020\\mC9tz44erLYyZ8OQfhWS.png', '', 'image', 4, 'Admin'),
(10, 'admin.google_analytics_client_id', 'Google Analytics Client ID (used for admin dashboard)', NULL, '', 'text', 1, 'Admin');

-- --------------------------------------------------------

--
-- Table structure for table `statuses`
--

CREATE TABLE `statuses` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varbinary(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `display` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sub_categories`
--

CREATE TABLE `sub_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `main_category_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sub_categories`
--

INSERT INTO `sub_categories` (`id`, `name`, `created_at`, `updated_at`, `main_category_id`) VALUES
(1, 'سيارات للبيع', '2020-01-31 16:02:56', '2020-01-31 16:02:56', 1),
(2, 'شقق للييع', '2020-01-31 16:08:21', '2020-01-31 16:08:21', 2),
(3, 'منازل للايجار', '2020-01-31 16:21:52', '2020-01-31 16:21:52', 2),
(4, 'سيارات للبيع', '2020-01-31 16:45:00', '2020-01-31 16:48:56', 1),
(5, 'كمبيوتر', '2020-01-31 16:47:00', '2020-01-31 16:48:16', 7),
(6, 'سيارات للإيجار', '2020-01-31 16:49:00', '2020-02-04 17:58:33', 1),
(7, 'دراجات نارية', '2020-01-31 16:49:32', '2020-01-31 16:49:32', 1),
(8, 'فلل - قصور للبيع', '2020-01-31 16:49:58', '2020-01-31 16:49:58', 2),
(9, 'اراضي - مزارع للبيع', '2020-01-31 16:50:00', '2020-02-23 04:47:32', 2),
(10, 'مسجلات', '2020-01-31 16:50:57', '2020-01-31 16:50:57', 7),
(11, 'ستيريو - مسجلات - راديو', '2020-01-31 16:51:34', '2020-01-31 16:51:34', 7),
(12, 'مودم - راوتر', '2020-01-31 16:51:59', '2020-01-31 16:51:59', 7),
(13, 'ادوات المطبخ - ضيافة', '2020-01-31 16:52:33', '2020-01-31 16:52:33', 6),
(14, 'أدوات منزلية', '2020-01-31 16:52:49', '2020-01-31 16:52:49', 6),
(15, 'شقق للايجار', '2020-02-04 16:01:00', '2020-02-04 16:05:42', 6),
(16, 'موبايل - هواتف', '2020-02-04 17:59:00', '2020-02-16 07:38:09', 7),
(17, 'شقق للايجار', '2020-02-04 18:54:53', '2020-02-04 18:54:53', 8),
(18, 'عمارات للايجار', '2020-02-04 18:55:58', '2020-02-04 18:55:58', 8),
(19, 'اراضي - مزارع للايجار', '2020-02-04 18:56:23', '2020-02-04 18:56:23', 8),
(20, 'غرف سكن - شقق فندقية', '2020-02-04 18:56:54', '2020-02-04 18:56:54', 8),
(21, 'عقارات أجنبية للإيجار', '2020-02-04 18:57:51', '2020-02-04 18:57:51', 8),
(22, 'فلل - قصور للايجار', '2020-02-04 18:58:34', '2020-02-04 18:58:34', 8),
(23, 'خدمات توصيل', '2020-02-04 18:59:01', '2020-02-04 18:59:01', 1),
(24, 'لوحة مميزة', '2020-02-04 18:59:20', '2020-02-04 18:59:20', 1),
(25, 'شاحنات - اليات ثقيلة', '2020-02-04 18:59:47', '2020-02-04 18:59:47', 1),
(27, 'مواتير سيارت', '2020-02-06 08:50:00', '2020-02-06 08:51:43', 13),
(28, 'اجهزة مطبخ', '2020-02-11 05:16:53', '2020-02-11 05:16:53', 7),
(29, 'اجهزة مطبخ', '2020-02-11 05:17:45', '2020-02-11 05:17:45', 7),
(30, 'معدات', '2020-02-11 05:22:24', '2020-02-11 05:22:24', 7),
(31, 'ابواب', '2020-02-11 05:28:50', '2020-02-11 05:28:50', 13),
(32, 'ثلاجة', '2020-02-25 09:39:38', '2020-02-25 09:39:38', 7);

-- --------------------------------------------------------

--
-- Table structure for table `translations`
--

CREATE TABLE `translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `units`
--

CREATE TABLE `units` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `attribute_group_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `units`
--

INSERT INTO `units` (`id`, `name`, `attribute_group_id`, `created_at`, `updated_at`) VALUES
(1, 'دونم', 2, '2020-02-15 16:52:00', '2020-02-15 16:52:00'),
(2, 'متر مربع', 2, '2020-02-15 16:52:00', '2020-02-15 16:52:00'),
(3, 'كيلومتر', 2, '2020-02-15 16:52:01', '2020-02-15 16:52:01'),
(4, 'فدان', 2, '2020-02-15 16:52:01', '2020-02-15 16:52:01'),
(5, 'Ah', 18, '2020-02-29 13:38:33', '2020-02-29 13:38:33'),
(6, 'Volte', 18, '2020-02-29 13:38:33', '2020-02-29 13:38:33');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `phone` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` bit(1) DEFAULT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `avatar`, `email_verified_at`, `password`, `remember_token`, `settings`, `created_at`, `updated_at`, `phone`, `status`, `username`) VALUES
(1, 1, 'Hasan', 'admin@admin.com', 'users\\February2020\\m7TXYkQtHemADmKI0CZt.png', NULL, '$2y$10$FKhgv4kud65PaM7irijRxepA6/bVqAZ9WPuU4.02p/PExiJieCUQm', NULL, '{\"locale\":\"en\"}', '2020-01-31 13:04:37', '2020-02-08 07:40:38', '+9720598933532', b'0', 'admin_123'),
(2, 2, 'asdasd', 'ha@ha.com', 'users/default.png', NULL, '$2y$10$GaCdLKvP06HHZThkCSXyuujIHo9wDxHRY2jx7actlzqoQ5LqOrozG', NULL, NULL, '2020-02-08 10:52:04', '2020-02-08 10:52:05', '+972597501037', NULL, NULL),
(3, 2, 'نتلاتنتنذ', 'sdfsdf@dsfdf.com', 'users/default.png', NULL, '$2y$10$qYB4eVg3jyuIO8zz.lrNheMnlpnnM3FYA8gzjo3dA70wwpnSNopKW', NULL, NULL, '2020-02-09 06:46:29', '2020-02-09 06:46:29', NULL, NULL, NULL),
(4, 2, 'حسن زهور', 'h@h.com', 'users/default.png', NULL, '$2y$10$rN4a/hb8E.zXDxkjVpurAuzGO1NseK6VHZBQhHAZUqATJHmY8oOVS', NULL, NULL, '2020-02-10 14:43:17', '2020-02-10 14:43:17', NULL, NULL, 'hasn123'),
(5, 2, 'حسن زهور', 'harb.hasn98@gmail.com', 'users\\February2020\\EH7HVTy84ILg7x165Dm7.jpg', NULL, '$2y$10$z1CG.Hk0h0YkPfig8XTGTeJrzopehjGyzKQlP/G6I1b974qeBib3a', NULL, '{\"locale\":\"en\"}', '2020-02-22 06:08:27', '2020-02-23 04:21:49', '05998933532', NULL, NULL),
(6, 2, 'حمزة زهور', 'hamza@gmail.com', 'users/default.png', NULL, '$2y$10$TaouDkaCMFrK03cheHk4P.jL152HWYJjGDN0.N4hSf.f6bRchnFaO', NULL, NULL, '2020-03-01 08:44:47', '2020-03-01 08:44:47', NULL, NULL, NULL),
(7, 2, 'حسن زهور', 'ha@ha.com11', 'users/default.png', NULL, '$2y$10$SGlbQkXcqRxqC5.Yf75BKuZf6rotQxDb76MridzR80N6fWmZPHhr.', NULL, NULL, '2020-03-14 19:46:17', '2020-03-14 19:46:17', NULL, NULL, NULL),
(8, 2, 'حسن زهور', 'ha@ha.com333', 'users/default.png', NULL, '$2y$10$iMd0E5yL9UG3wZSbvltV.ug24z.jl5Lc54Ij3G.Z4jrSJOEdwmlwi', NULL, NULL, '2020-03-14 20:00:06', '2020-03-14 20:00:06', NULL, NULL, NULL),
(9, 2, 'حسن زهور', 'h@h.comww', 'users/default.png', NULL, '$2y$10$fe9y/dR31YFyKw7X3I3dXOAxJdiFLAtAwLfjjk6X.uXLmqLAJiOTO', NULL, NULL, '2020-03-14 20:00:51', '2020-03-14 20:00:51', NULL, NULL, NULL),
(10, 2, 'حسن زهور', 'ss@ss.comwww', 'users/default.png', NULL, '$2y$10$xUVbi9Oy0hNTaPnOGz9X1eW6/qUPyjx8r3vnVsjRlmqFwQoSuZ/WS', NULL, NULL, '2020-03-14 20:08:13', '2020-03-14 20:08:13', NULL, NULL, NULL),
(11, 2, 'حسن زهور', 'h@h.com222', 'users/default.png', NULL, '$2y$10$pEJMr/UbKX5ilcFImnqgcuXbBvmahcmKZdvzls70qD55DcYw6Qh8C', NULL, NULL, '2020-03-14 20:09:26', '2020-03-14 20:09:26', NULL, NULL, NULL),
(12, 2, 'حسن زهور', 'admin@admin.com111', 'users/default.png', NULL, '$2y$10$R0Ze0rNJr9aJUPvFaAcGIu09XH3Ody4OzdzO2QsHJjZV.XGTTD.RO', NULL, NULL, '2020-03-14 20:15:32', '2020-03-14 20:15:32', NULL, NULL, NULL),
(13, 2, 'حسن زهور', 'wwwh@h.com', 'users/default.png', NULL, '$2y$10$65uktZDBLN/tGDDpbe0RXuJAIw.EOonIGcwacyz2ykrfqO6OXCPNu', NULL, NULL, '2020-03-14 20:25:01', '2020-03-14 20:25:01', NULL, NULL, 'hsn_zhor_13'),
(14, 2, 'حمزة نعيم زهور', 'ha@ha.com3333', 'users/default.png', NULL, '$2y$10$7wKZV8rgnLsLszpWwraoNeNxhVCs9igOBMy76uluoBt9JxNVgeILa', NULL, NULL, '2020-03-15 16:26:43', '2020-03-15 16:26:44', NULL, NULL, 'hmz_naaym_zhor_14');

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE `user_roles` (
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ads`
--
ALTER TABLE `ads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ad_attributes`
--
ALTER TABLE `ad_attributes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `attributes`
--
ALTER TABLE `attributes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `attribute_groups`
--
ALTER TABLE `attribute_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `currencies`
--
ALTER TABLE `currencies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD PRIMARY KEY (`id`),
  ADD KEY `data_rows_data_type_id_foreign` (`data_type_id`);

--
-- Indexes for table `data_types`
--
ALTER TABLE `data_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `data_types_name_unique` (`name`),
  ADD UNIQUE KEY `data_types_slug_unique` (`slug`);

--
-- Indexes for table `directions`
--
ALTER TABLE `directions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `input_ad_values`
--
ALTER TABLE `input_ad_values`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `main_categories`
--
ALTER TABLE `main_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menus_name_unique` (`name`);

--
-- Indexes for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_items_menu_id_foreign` (`menu_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `offers`
--
ALTER TABLE `offers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_key_index` (`key`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `saved_ads`
--
ALTER TABLE `saved_ads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `saved_services`
--
ALTER TABLE `saved_services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `selected_ad_values`
--
ALTER TABLE `selected_ad_values`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_key_unique` (`key`);

--
-- Indexes for table `statuses`
--
ALTER TABLE `statuses`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `statuses_key_unique` (`key`);

--
-- Indexes for table `sub_categories`
--
ALTER TABLE `sub_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `translations`
--
ALTER TABLE `translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`);

--
-- Indexes for table `units`
--
ALTER TABLE `units`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `user_roles_user_id_index` (`user_id`),
  ADD KEY `user_roles_role_id_index` (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ads`
--
ALTER TABLE `ads`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `ad_attributes`
--
ALTER TABLE `ad_attributes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `attributes`
--
ALTER TABLE `attributes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=138;

--
-- AUTO_INCREMENT for table `attribute_groups`
--
ALTER TABLE `attribute_groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `currencies`
--
ALTER TABLE `currencies`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `data_rows`
--
ALTER TABLE `data_rows`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=142;

--
-- AUTO_INCREMENT for table `data_types`
--
ALTER TABLE `data_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `directions`
--
ALTER TABLE `directions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `input_ad_values`
--
ALTER TABLE `input_ad_values`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `main_categories`
--
ALTER TABLE `main_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `menu_items`
--
ALTER TABLE `menu_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `offers`
--
ALTER TABLE `offers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=102;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `saved_ads`
--
ALTER TABLE `saved_ads`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `saved_services`
--
ALTER TABLE `saved_services`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `selected_ad_values`
--
ALTER TABLE `selected_ad_values`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `statuses`
--
ALTER TABLE `statuses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sub_categories`
--
ALTER TABLE `sub_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `translations`
--
ALTER TABLE `translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `units`
--
ALTER TABLE `units`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Constraints for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
