<?php

use App\Mail\NewUserNotification;
use App\MainCategory;
use App\SubCategory;
use App\User;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Route;
use TCG\Voyager\Facades\Voyager;

$prefix = 'FrontendController\\';

Route::group(['prefix' => '/', 'as' => 'frontend.'], function () use ($prefix) {
    Route::get('/', $prefix . 'FrontendController@index')->name('index');
    Route::get('/signup', $prefix . 'FrontendController@signup')->name('signup');
    Route::get('/about-as', $prefix . 'FrontendController@AboutAs')->name('about-as');
    Route::get('/privacy', $prefix . 'FrontendController@Privacy')->name('privacy');
});

Route::group(['prefix' => '/services', 'as' => 'services.'], function () use ($prefix) {
    Route::get('/', $prefix . 'ServicesController@index')->name('index');
    Route::get('/create', $prefix . 'ServicesController@create')->name('create');
    Route::post('/store', $prefix . 'ServicesController@store')->name('store');
    Route::get('/show/{slug}', $prefix . 'ServicesController@show')->name('show');
    Route::get('/review/{slug}', $prefix . 'ServicesController@review')->name('review');
    Route::post('/set-offer', $prefix . 'ServicesController@setOffer')->name('set_offer');
    Route::post('/destroy/{id}', $prefix . 'ServicesController@destroy')->name('destroy');
    Route::post('/add-to-saved', $prefix . 'ServicesController@addToSaved')->name('addToSaved');
    Route::post('/block/{id}', $prefix . 'ServicesController@blockService')->name('block');
});

Route::group(['prefix' => '/ads', 'as' => 'ad.'], function () use ($prefix) {
    Route::get('/create', $prefix . 'AdsController@createAd')->name('create');
    Route::post('/generate-template', $prefix . 'AdsController@generateTemplate')->name('template');
    Route::post('/store', $prefix . 'AdsController@store')->name('store');
    Route::get('/', $prefix . 'AdsController@showAds')->name('show-ads');
    Route::get('/category/{id}', $prefix . 'AdsController@getAdsWhereCategory')->name('category');
    Route::get('/show/{id}', $prefix . 'AdsController@showAd')->name('show-ad');
    Route::post('/add-to-saved', $prefix . 'AdsController@addToSaved')->name('addToSaved');
    Route::post('/set-auction-offer', $prefix . 'AdsController@setAuctionOffer')->name('set-auction-offer');
    Route::post('/set-comment', $prefix . 'AdsController@setComment')->name('set-comment');
    Route::post('/block/{id}', $prefix . 'AdsController@blockAd')->name('block');
    Route::get('/delete-comment/{id}', $prefix . 'AdsController@deleteComment')->name('delete-comment');
});

Route::group(['prefix' => '/users', 'as' => 'users.'], function () use ($prefix) {
    Route::get('/profile/{username}', $prefix . 'UsersController@profile')->name('profile');
    Route::get('/saved', $prefix . 'UsersController@saved')->name('saved');
    Route::post('/update', $prefix . 'UsersController@update')->name('update');
    Route::post('/change-password', $prefix . 'UsersController@changePassword')->name('change-password');
    Route::post('/follow-user', $prefix . 'UsersController@followUser')->name('follow-user');
    Route::post('/user-block/{id}', $prefix . 'UsersController@userBlock')->name('block');
    Route::get('/all', $prefix . 'UsersController@allUsers')->name('all');
});

Route::group(['prefix' => '/auctions', 'as' => 'auctions.'], function () use ($prefix) {
    Route::get('/', $prefix . 'AuctionsController@index')->name('index');
});

Route::group(['prefix' => '/general', 'as' => 'general.'], function () use ($prefix) {
    Route::post('/store-message', $prefix . 'GeneralController@storeMessage')->name('store-message');
    Route::post('/store-report-ad', $prefix . 'GeneralController@storeReportAd')->name('store-report-ad');
    Route::post('/store-rating', $prefix . 'GeneralController@storeRating')->name('store-rating');
});

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});


Auth::routes(['verify' => true]);

Route::get('/home', 'HomeController@index')->name('home');
Route::resource('test', 'TestController');
Route::post('/api/sub_categories', 'Api\\CategoriesController@selectSubCategorie')->name('api.sub_categories');

Route::get('/redirect', 'SocialAuthFacebookController@redirect');
Route::get('/callback', 'SocialAuthFacebookController@callback');

Route::get('/foo', function () {
    Artisan::call('config:cache');
    Artisan::call('cache:clear');
    Artisan::call('storage:link');
    // Artisan::call('voyager:install');
    return "<h1> OK :) </h1>";
});

Route::get('/tmail', function () {
    $user = User::find(18);
    Mail::to($user)->send(new NewUserNotification($user));
});

