function timer(id, start_date, end_date) {
    const second = 1000,
        minute = second * 60,
        hour = minute * 60,
        day = hour * 24;

    let countDown = new Date(end_date).getTime(),
        x = setInterval(function() {

            let now = new Date().getTime(),
                distance = countDown - now;

            document.getElementById('days' + id).innerText = Math.floor(distance / (day)),
                document.getElementById('hours' + id).innerText = Math.floor((distance % (day)) / (hour)),
                document.getElementById('minutes' + id).innerText = Math.floor((distance % (hour)) / (minute)),
                document.getElementById('seconds' + id).innerText = Math.floor((distance % (minute)) / second);

            // do something later when date is reached
            if (distance < 0) {
                clearInterval(x);
                document.getElementById('days' + id).innerText = '--'
                document.getElementById('hours' + id).innerText = '--';
                document.getElementById('minutes' + id).innerText = '--';
                document.getElementById('seconds' + id).innerText = '--';
            }

        }, second);
}