function confirmForm(from_id) {

    Swal.fire({
        title: 'هل انت متأكد',
        text: "هل انت متاكد من الحذف",
        type: 'warning',
        showCancelButton: true,
        cancelButtonText: 'لا، اغلاق',
        confirmButtonText: 'نعم',
        reverseButtons: true,
    }).then((result) => {
        if (result.value) {
            $('#' + from_id).submit()
        } else if (
            result.dismiss === Swal.DismissReason.cancel
        ) {
            Swal.fire(
                'لم يتم الحذف',
                'لم تتم عملية الحذف :)',
                'error'
            )
        }
    });
}